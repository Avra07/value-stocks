//
//  MFOurFundwisePerformanceViewController.swift
//  DLevels
//
//  Created by SAIKAT GHOSH on 21/08/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit



class MFFundwisePerformance:NSObject {
    
    var oneMRet             = 0.00
    var oneMRet_D           = 0.00
    var oneWRet             = 0.00
    var oneWRet_D           = 0.00
    var oneYRet             = 0.00
    var oneYRet_D           = 0.00
    var threeMRet           = 0.00
    var threeMRet_D         = 0.00
    var threeYRet           = 0.00
    var threeYRet_D         = 0.00
    var fiveYRet            = 0.00
    var fiveYRet_D          = 0.00
    var sixMRet             = 0.00
    var sixMRet_D           = 0.00
    var Category            = ""
    var Fund                = ""
    var FundCategory        = ""
    var FundDisplay         = ""
    var FundHouse           = ""
    var NetAssets           = 0
    var OldName             = ""
    var Rating              = ""
    var SubCategory         = ""
    var scm_code            = ""
    var url_str             = ""
    var recomended          = "0"
    
    override init(){
        
    }
    
    
    init(fund:String,fundCategory:String,fundDisplay:String,fundHouse:String,TotalNetAssets:Int,oMRet: Double,oMRetD: Double,oWRet: Double,oWRetD: Double,oYRet: Double,oYRetD: Double, tMRet: Double,tMRetD: Double, tYRet: Double,tYRetD: Double,fYRet: Double,fYRetD: Double, sMRet: Double,sMRetD: Double, oldName:String, rating:String, subCategory:String,scmCode:String,urlStr:String, recomended : String) {
        
        self.oneMRet                    = oMRet
        self.oneMRet_D                  = oMRetD
        self.oneWRet                    = oWRet
        self.oneWRet_D                  = oWRetD
        self.oneYRet                    = oYRet
        self.oneYRet_D                  = oYRetD
        self.threeMRet                  = tMRet
        self.threeMRet_D                = tMRetD
        self.threeYRet                  = tYRet
        self.threeYRet_D                = tYRetD
        self.fiveYRet                   = fYRet
        self.fiveYRet_D                 = fYRetD
        self.sixMRet                    = sMRet
        self.sixMRet_D                  = sMRetD
        self.Fund                       = fund
        self.FundCategory               = fundCategory
        self.FundDisplay                = fundDisplay
        self.FundHouse                  = fundHouse
        self.NetAssets                  = TotalNetAssets
        self.OldName                    = oldName
        self.Rating                     = rating
        self.SubCategory                = subCategory
        self.scm_code                   = scmCode
        self.url_str                    = urlStr
        self.recomended                 = recomended
    }
    
    func getData(paramType: String, paramCategory:String,paramFundType:String, completion : @escaping ([MFFundwisePerformance])->()){
        
        
        
        var urlStr = ""
        var param = ""
        
        let parametType = paramFundType.replacingOccurrences(of: "&", with: "%26")
        
        if paramType == "category" {
            urlStr = Url.mffunddetails
            param = "category=\(paramCategory)&fundtype=\(parametType.replacingOccurrences(of: " ", with: "%20"))"
        }
        
        if paramType == "manager" {
            urlStr = Url.mffundmanagersSchemes
            param = "category=\(paramCategory)&fund_manager=\(parametType.replacingOccurrences(of: " ", with: "%20"))"
        }
        
        if paramType == "house" {
            urlStr = Url.mffundhouseSchemes
            param = "category=\(paramCategory)&amc=\(parametType.replacingOccurrences(of: " ", with: "%20"))"
        }
        
        // Add for Our Funds
        if paramType == "ourfunds" {
            urlStr = Url.ourfunds
            param = "category=\(paramCategory)"
        }
        
        
        
        
        var result = [MFFundwisePerformance]()
        
        NetworkHandler().handleRequest(url: urlStr, methodName: "POST", parameters: param, isToken: false, showLoader: true ) { (success, jsonData) in
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                var tempArray = [Any]()
                
                if paramType == "category" {
                    let tArray = jsonData.value(forKey: "response") as! [NSArray]
                    tempArray = tArray[0] as! [NSDictionary]
                }
                if paramType == "manager" {
                    tempArray = jsonData.value(forKey: "response") as! [NSDictionary]
                }
                
                if paramType == "house" {
                    tempArray = jsonData.value(forKey: "response") as! [NSDictionary]
                }
                // Add for Our Funds
                if paramType == "ourfunds" {
                    tempArray = jsonData.value(forKey: "response") as! [NSDictionary]
                }
                
                
                
                for arr in tempArray{
                    
                    if let Fundr = (arr as AnyObject).value(forKey: "Fund") as? String{
                        self.Fund = Fundr
                    }
                    if let Fundc = (arr as AnyObject).value(forKey: "FundCategory") as? String{
                        self.FundCategory = Fundc
                    }
                    if let Fundd = (arr as AnyObject).value(forKey: "FundDispaly") as? String{
                        self.FundDisplay = Fundd
                    }
                    if let Fundh = (arr as AnyObject).value(forKey: "FundHouse") as? String{
                        self.FundHouse = Fundh
                    }
                    
                    if let recomend = (arr as AnyObject).value(forKey: "Recommended") as? String{
                        self.recomended = recomend
                    }
                    
                    if let NetAsset = (arr as AnyObject).value(forKey: "NetAssets") as? String{
                        self.NetAssets = Int(NetAsset) ?? 0
                    }
                    
                    if let oneWRet = (arr as AnyObject).value(forKey: "1WRet") as? String{
                        self.oneWRet = (oneWRet as NSString).doubleValue
                    }
                    if let oneWRet_D = (arr as AnyObject).value(forKey: "1WRet_D") as? String{
                        self.oneWRet_D = (oneWRet_D as NSString).doubleValue
                    }
                    
                    if let oneMRet = (arr as AnyObject).value(forKey: "1MRet") as? String{
                        self.oneMRet = (oneMRet as NSString).doubleValue
                    }
                    if let oneMRet_D = (arr as AnyObject).value(forKey: "1MRet_D") as? String{
                        self.oneMRet_D = (oneMRet_D as NSString).doubleValue
                    }
                    
                    if let oneYRet = (arr as AnyObject).value(forKey: "1YRet") as? String{
                        self.oneYRet = (oneYRet as NSString).doubleValue
                    }
                    if let oneYRet_D = (arr as AnyObject).value(forKey: "1YRet_D") as? String{
                        self.oneYRet_D = (oneYRet_D as NSString).doubleValue
                    }
                    
                    
                    if let threeMRet = (arr as AnyObject).value(forKey: "3MRet") as? String{
                        self.threeMRet = (threeMRet as NSString).doubleValue
                    }
                    if let threeMRet_D = (arr as AnyObject).value(forKey: "3MRet_D") as? String{
                        self.threeMRet_D = (threeMRet_D as NSString).doubleValue
                    }
                    
                    if let threeYRet = (arr as AnyObject).value(forKey: "3YRet") as? String{
                        self.threeYRet = (threeYRet as NSString).doubleValue
                    }
                    if let threeYRet_D = (arr as AnyObject).value(forKey: "3YRet_D") as? String{
                        self.threeYRet_D = (threeYRet_D as NSString).doubleValue
                    }
                    
                    if let fiveYRet = (arr as AnyObject).value(forKey: "5YRet") as? String{
                        self.fiveYRet = (fiveYRet as NSString).doubleValue
                    }
                    if let fiveYRet_D = (arr as AnyObject).value(forKey: "5YRet_D") as? String{
                        self.fiveYRet_D = (fiveYRet_D as NSString).doubleValue
                    }
                    
                    if let sixMRet = (arr as AnyObject).value(forKey: "6MRet") as? String{
                        self.sixMRet = (sixMRet as NSString).doubleValue
                    }
                    if let sixMRet_D = (arr as AnyObject).value(forKey: "6MRet_D") as? String{
                        self.sixMRet_D = (sixMRet_D as NSString).doubleValue
                    }
                    
                    if let OName = (arr as AnyObject).value(forKey: "OldName") as? String{
                        self.OldName = OName
                    }
                    if let rate = (arr as AnyObject).value(forKey: "Rating") as? String{
                        self.Rating = rate
                    }
                    if let SCategory = (arr as AnyObject).value(forKey: "SubCategory") as? String{
                        self.SubCategory = SCategory
                    }
                    if let scmCode = (arr as AnyObject).value(forKey: "reg_scm_code") as? String{
                        self.scm_code = scmCode
                    }
                    if let urlStr = (arr as AnyObject).value(forKey: "url_str") as? String{
                        self.url_str = urlStr
                    }
                    
                    
                    let obj_mfFundWisePerformance = MFFundwisePerformance(fund: self.Fund, fundCategory: self.FundCategory, fundDisplay: self.FundDisplay, fundHouse: self.FundHouse, TotalNetAssets: self.NetAssets, oMRet: self.oneMRet, oMRetD: self.oneMRet_D, oWRet: self.oneWRet, oWRetD: self.oneWRet_D, oYRet: self.oneYRet, oYRetD: self.oneYRet_D, tMRet: self.threeMRet, tMRetD: self.threeMRet_D, tYRet: self.threeYRet, tYRetD: self.threeYRet_D, fYRet: self.fiveYRet, fYRetD: self.fiveYRet_D, sMRet: self.sixMRet, sMRetD: self.sixMRet_D, oldName: self.OldName, rating: self.Rating, subCategory: self.SubCategory, scmCode: self.scm_code, urlStr: self.url_str, recomended: self.recomended)
                    
                    result.append(obj_mfFundWisePerformance)
                    
                }
                
                
                completion(result)
                
            }
        }
        
        
        
    }
    
}









class MFOurFundwisePerformanceTableViewCell: UITableViewCell{
    @IBOutlet weak var lblFundHouse: UILabel!
    @IBOutlet weak var lblNetAssetss: UILabel!
    @IBOutlet weak var lblAvgReturnD: UILabel!
    @IBOutlet weak var lblAvgReturnR: UILabel!
    @IBOutlet weak var imgRecomendation: UIImageView!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var vwContainerView: UIView!
}

class MFOurFundwisePerformanceViewController: UIViewController {
    
    @IBAction func Disclaimer(_ sender: Any) {
        
        let strybrd =  UIStoryboard.init(name: "Main", bundle: nil)
        let obj = strybrd.instantiateViewController(withIdentifier: "DisclaimerViewController") as! DisclaimerViewController
        obj.disclaimerUrl = DisclaimerUrl.mf_research
        User.navigation.pushViewController(obj, animated: true)
        obj.disclaimerHeader = "MF Research"
        
    }
    @IBOutlet weak var tableVw: UITableView!
    @IBOutlet weak var stckVw: UIStackView!
    
    
    @IBOutlet weak var lblDetails: UILabel!
    
    @IBOutlet weak var btn1W: UIButton!
    @IBOutlet weak var btn1M: UIButton!
    @IBOutlet weak var btn1Y: UIButton!
    
    @IBOutlet weak var btn3M: UIButton!
    @IBOutlet weak var btn3Y: UIButton!
    
    @IBOutlet weak var btn5Y: UIButton!
    @IBOutlet weak var btn6M: UIButton!
    
    fileprivate var dataArray = [MFFundwisePerformance]()
    var isSortByFund = false, isSortByAsset = false, isSortByD = true, isSortByR = false
    
    var fname = ""
    var desc = ""
    
    var duration = "1Y"
    
    var noOfTotalFunds = 0
    var totalNetAssest = 0
    var col = ""
    var returnCol = "A"
    
    var fund = ""
    var smc_code = ""
    
    var _category: String!
    var _fundname: String!
    var _type: String!
    
    var Category:String {
        get {
            return self._category
        }
        set {
            self._category = newValue
        }
    }
    
    var FundType:String {
        get {
            return self._type
        }
        set {
            self._type = newValue
        }
    }
    
    //MARK: VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblDetails.text = desc
        for case let button as UIButton in self.stckVw.subviews {
            button.layer.cornerRadius = 0
            button.layer.borderWidth = 0
            button.layer.borderColor = nil
        }
        
        //tableVw?.delegate = self
        // tableVw?.dataSource = self
        
        btn1Y.layer.cornerRadius = 4.0
        btn1Y.layer.borderWidth = 1
        btn1Y.layer.borderColor =  UIColor(hex: "F7BB1A").cgColor
        
        tableVw.tableHeaderView = nil;
        tableVw.tableFooterView = nil;
        tableVw.backgroundView?.isHidden = true
        
        
        let obj_FundwisePerformance = MFFundwisePerformance()
        print(self.fname)
        obj_FundwisePerformance.getData(paramType: self._type, paramCategory: self._category, paramFundType: self.fname, completion: { (dataset) in
            self.dataArray = dataset
            
            //            self.sortByD()
            //self.tableVw.reloadData()
            self.tableVw.reloadSections(IndexSet(integer: 0), with: .automatic)
        })
        
        //self.lblTitle.text = self.fname + " - " + self.Category
        
    }
    
    //MARK: VIEW WILL APPEAR
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // User.clearInvestmentValues()
        
    }
    
    
    //MARK: PREPARE FOR SEGUE
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let identifier = segue.identifier {
            if identifier == "ourfundstospecifiecShow" {
                if let destinationCont = segue.destination as? MFSpecificPageViewController {
                    
                    if self._fundname != "" && self._fundname != nil {
                        
                        Funddetails.Category    = self._category
                        Funddetails.Fund        = self.fund
                        Funddetails.FundName    = self._fundname
                        Funddetails.scmCode     = self.smc_code
                        
                        destinationCont.Category = self._category
                        destinationCont.Fund     = self.fund
                        destinationCont.FundName = self._fundname
                        destinationCont.scmCode  = self.smc_code
                        
                    } else {
                        print("failed to get name from dictionary")
                    }
                }
            }
        }
    }
    
    //MARK: SHOULD PERFORM SEGUE
    override func shouldPerformSegue(withIdentifier identifier: String?, sender: Any?) -> Bool {
        
        if identifier == "ourfundstospecifiecShow" {
            if self._fundname != "" && self._fundname != nil {
                return true
            } else {
                return false
            }
        }
        
        return false
    }
    
    //MARK: HOME ACTION
    @IBAction func btnHome_Action(_ sender: UIButton) {
        let stryBoard = UIStoryboard(name: "MFStoryboard", bundle: nil)
        let kycSummaryCont = stryBoard.instantiateViewController(withIdentifier: "MFViewController") as! MFViewController
        User.navigation.pushViewController(kycSummaryCont, animated: true)
    }
    
    //MARK SEARCH ACTION
    @IBAction func btnSearch_Action(_ sender: UIButton) {
        let stryBoard = UIStoryboard(name: "MFStoryboard", bundle: nil)
        let kycSummaryCont = stryBoard.instantiateViewController(withIdentifier: "MFSearchViewController") as! MFSearchViewController
        User.navigation.pushViewController(kycSummaryCont, animated: true)
    }
    
    
    //MARK: FUND TYPE SORT ACTION
    @IBAction func btnFundTypeSort_Action(_ sender: Any) {
        isSortByFund = !isSortByFund
        
        isSortByAsset = false; isSortByD = false; isSortByR = false
        
        dataArray = isSortByFund ? dataArray.sorted (by: {$0.FundDisplay > $1.FundDisplay}) : dataArray.sorted (by: {$0.FundDisplay < $1.FundDisplay})
        self.tableVw.reloadData()
        
        // sortByD()
        
        self.moveToTop()
    }
    
    
    //MARK: SORT BY ASSET
    @IBAction func btnNetAsset_Action(_ sender: Any) {
        isSortByAsset = !isSortByAsset
        
        isSortByFund = false; isSortByD = false; isSortByR = false
        
        dataArray = isSortByAsset ? dataArray.sorted (by: {$0.NetAssets > $1.NetAssets}) : dataArray.sorted (by: {$0.NetAssets < $1.NetAssets})
        self.tableVw.reloadData()
        self.moveToTop()
    }
    
    //MARK: SORT BY D
    @IBAction func btnReturnD_Action(_ sender: Any) {
        isSortByD = !isSortByD
        sortByD()
    }
    
    //MARK: SORT FUNCTION
    private func sortByD(){
        
        isSortByFund = false; isSortByAsset = false; isSortByR = false
        
        if duration == "1W" {
            dataArray = isSortByD ? dataArray.sorted (by: {$0.oneWRet_D > $1.oneWRet_D}) : dataArray.sorted (by: {$0.oneWRet_D < $1.oneWRet_D})
        } else if duration == "1Y" {
            dataArray = isSortByD ? dataArray.sorted (by: {$0.oneYRet_D > $1.oneYRet_D}) : dataArray.sorted (
                by: {$0.oneYRet_D < $1.oneYRet_D})
            self.tableVw.reloadData()
        } else if duration == "1M" {
            dataArray = isSortByD ? dataArray.sorted (by: {$0.oneMRet_D > $1.oneMRet_D}) : dataArray.sorted (by: {$0.oneMRet_D < $1.oneMRet_D})
            self.tableVw.reloadData()
        } else if duration == "3M" {
            dataArray = isSortByD ? dataArray.sorted (by: {$0.threeMRet_D > $1.threeMRet_D}) : dataArray.sorted (by: {$0.threeMRet_D < $1.threeMRet_D})
            self.tableVw.reloadData()
        } else if duration == "6M" {
            dataArray = isSortByD ? dataArray.sorted (by: {$0.sixMRet_D > $1.sixMRet_D}) : dataArray.sorted (by: {$0.sixMRet_D < $1.sixMRet_D})
            self.tableVw.reloadData()
        } else if duration == "3Y" {
            dataArray = isSortByD ? dataArray.sorted (by: {$0.threeYRet_D > $1.threeYRet_D}) : dataArray.sorted (by: {$0.threeYRet_D < $1.threeYRet_D})
            self.tableVw.reloadData()
        } else if duration == "5Y" {
            dataArray = isSortByD ? dataArray.sorted (by: {$0.fiveYRet_D > $1.fiveYRet_D}) : dataArray.sorted (by: {$0.fiveYRet_D < $1.fiveYRet_D})
            self.tableVw.reloadData()
        }
        
        self.tableVw.reloadData()
        self.moveToTop()
    }
    
    
    //MARK: SORT BY RETURN
    @IBAction func btnReturnR_Action(_ sender: Any) {
        isSortByR = !isSortByR
        
        isSortByFund = false; isSortByAsset = false; isSortByD = false
        
        if duration == "1W" {
            dataArray = isSortByR ? dataArray.sorted (by: {$0.oneWRet > $1.oneWRet}) : dataArray.sorted (by: {$0.oneWRet < $1.oneWRet})
            self.tableVw.reloadData()
        } else if duration == "1Y" {
            dataArray = isSortByR ? dataArray.sorted (by: {$0.oneYRet > $1.oneYRet}) : dataArray.sorted (by: {$0.oneYRet < $1.oneYRet})
            self.tableVw.reloadData()
        } else if duration == "1M" {
            dataArray = isSortByR ? dataArray.sorted (by: {$0.oneMRet > $1.oneMRet}) : dataArray.sorted (by: {$0.oneMRet < $1.oneMRet})
            self.tableVw.reloadData()
        } else if duration == "3M" {
            dataArray = isSortByR ? dataArray.sorted (by: {$0.threeMRet > $1.threeMRet}) : dataArray.sorted (by: {$0.threeMRet < $1.threeMRet})
            self.tableVw.reloadData()
        }else if duration == "6M" {
            dataArray = isSortByR ? dataArray.sorted (by: {$0.sixMRet > $1.sixMRet}) : dataArray.sorted (by: {$0.sixMRet < $1.sixMRet})
            self.tableVw.reloadData()
        } else if duration == "3Y" {
            dataArray = isSortByR ? dataArray.sorted (by: {$0.threeYRet > $1.threeYRet}) : dataArray.sorted (by: {$0.threeYRet < $1.threeYRet})
            self.tableVw.reloadData()
        } else if duration == "5Y" {
            dataArray = isSortByR ? dataArray.sorted (by: {$0.fiveYRet > $1.fiveYRet}) : dataArray.sorted (by: {$0.fiveYRet < $1.fiveYRet})
            self.tableVw.reloadData()
        }
        self.moveToTop()
        
    }
    
    //MARK: BACK CLICK
    @IBAction func backClick(_ sender: Any) {
        self.navigationController!.popViewController(animated: true)
    }
    
    //MARK: SORT BY DURATION
    func sortByMonth(){
        
        if duration == "1W" {
            dataArray = dataArray.sorted (by: {$0.oneWRet_D > $1.oneWRet_D})
        } else if duration == "1Y" {
            dataArray = dataArray.sorted (
                by: {$0.oneYRet_D > $1.oneYRet_D})
            self.tableVw.reloadData()
        } else if duration == "1M" {
            dataArray = dataArray.sorted (by: {$0.oneMRet_D > $1.oneMRet_D})
            self.tableVw.reloadData()
        } else if duration == "3M" {
            dataArray = dataArray.sorted (by: {$0.threeMRet_D > $1.threeMRet_D})
            self.tableVw.reloadData()
        } else if duration == "6M" {
            dataArray = dataArray.sorted (by: {$0.sixMRet_D > $1.sixMRet_D})
            self.tableVw.reloadData()
        } else if duration == "3Y" {
            dataArray = dataArray.sorted (by: {$0.threeYRet_D > $1.threeYRet_D})
            self.tableVw.reloadData()
        } else if duration == "5Y" {
            dataArray = dataArray.sorted (by: {$0.fiveYRet_D > $1.fiveYRet_D})
            self.tableVw.reloadData()
        }
        
        self.tableVw.reloadData()
        moveToTop()
    }
    
    //MARK: MOVE TO TOP
    func moveToTop(){
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: 0 , section: 0)
            self.tableVw.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }
    
}

//MARK: EXTENSION PERFORMANCE VIEW CONTROLLER
extension MFOurFundwisePerformanceViewController  {
    
    //MARK: DURATION BUTTON CLICK
    func durationBtnClick(btn: UIButton) {
        
        if let title = btn.titleLabel?.text {
            duration =  title
        }
        
        for case let button as UIButton in self.stckVw.subviews {
            button.layer.cornerRadius =  4.0
            button.layer.borderWidth  =  2.0
            button.layer.borderColor  =  button.titleLabel?.text == duration ? UIColor(hex: "F7BB1A").cgColor : UIColor.clear.cgColor
        }
        
        sortByMonth()
        //        self.tableVw.reloadData()
        //        self.moveToTop()
    }
    
    
    
    
    @IBAction func btn1W_Action(_ sender: UIButton) {
        self.durationBtnClick(btn: sender)
    }
    
    @IBAction func btn1M_Action(_ sender: UIButton) {
        self.durationBtnClick(btn: sender)
    }
    
    @IBAction func btn1Y_Action(_ sender: UIButton) {
        self.durationBtnClick(btn: sender)
    }
    
    @IBAction func btn3M_Action(_ sender: UIButton) {
        self.durationBtnClick(btn: sender)
    }
    
    @IBAction func btn3Y_Action(_ sender: UIButton) {
        self.durationBtnClick(btn: sender)
    }
    
    @IBAction func btn5Y_Action(_ sender: UIButton) {
        self.durationBtnClick(btn: sender)
    }
    
    @IBAction func btn6M_Action(_ sender: UIButton) {
        self.durationBtnClick(btn: sender)
    }
    
}


//MARK: TABLE VIEW DELEGATE AND DATA SOURCE
extension MFOurFundwisePerformanceViewController: UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? MFOurFundwisePerformanceTableViewCell
        {
            cell.selectionStyle = .none
            
            //cell.vwContainerView.backgroundColor = UIColor(hex: AppManager.getAlternetColor(alternetColor: indexPath.row % 2 == 0))
            
            cell.vwContainerView.backgroundColor = indexPath.row % 2 == 0 ? UIColor(red: 236/255, green: 239/255, blue: 240/255, alpha: 1.0) : UIColor(red: 215/255, green: 222/255, blue: 225/255, alpha: 1.0)
            
            
            let fundText = dataArray[indexPath.row].FundDisplay.replacingOccurrences(of: "- Direct", with: "")
            
            let attributedString = NSMutableAttributedString.init(string: fundText)
            
            attributedString.addAttribute(NSUnderlineStyleAttributeName, value: 1, range: NSRange.init(location: 0, length: attributedString.length))
            cell.lblFundHouse.attributedText = attributedString
            
            cell.lblFundHouse.sizeToFit()
            
            
            cell.lblNetAssetss.text = String(dataArray[indexPath.row].NetAssets)
            cell.lblCategory.text = dataArray[indexPath.row].SubCategory// + "\n"
            
            cell.imgRecomendation.isHidden =  dataArray[indexPath.row].recomended == "1" ? false : true
            
            if duration == "1W" {
                cell.lblAvgReturnR.text = AppManager.setPositiveValue(value: dataArray[indexPath.row].oneWRet)
                cell.lblAvgReturnD.text = AppManager.setPositiveValue(value: dataArray[indexPath.row].oneWRet_D)
            }
                
            else if duration == "1M" {
                cell.lblAvgReturnR.text = AppManager.setPositiveValue(value: dataArray[indexPath.row].oneMRet)//String(dataArray[indexPath.row].oneMRet)
                cell.lblAvgReturnD.text = AppManager.setPositiveValue(value: dataArray[indexPath.row].oneMRet_D)
            }
                
            else if duration == "1Y" {
                cell.lblAvgReturnR.text = AppManager.setPositiveValue(value: dataArray[indexPath.row].oneYRet)
                cell.lblAvgReturnD.text = AppManager.setPositiveValue(value: dataArray[indexPath.row].oneYRet_D)
            }
                
            else if duration == "3M" {
                cell.lblAvgReturnR.text = AppManager.setPositiveValue(value: dataArray[indexPath.row].threeMRet)
                cell.lblAvgReturnD.text = AppManager.setPositiveValue(value: dataArray[indexPath.row].threeMRet_D)
            }
                
            else if duration == "3Y" {
                cell.lblAvgReturnR.text = AppManager.setPositiveValue(value: dataArray[indexPath.row].threeYRet)
                cell.lblAvgReturnD.text = AppManager.setPositiveValue(value: dataArray[indexPath.row].threeYRet_D)
            }
                
            else if duration == "5Y" {
                cell.lblAvgReturnR.text = AppManager.setPositiveValue(value: dataArray[indexPath.row].fiveYRet)
                cell.lblAvgReturnD.text = AppManager.setPositiveValue(value: dataArray[indexPath.row].fiveYRet_D)
            }
                
            else if duration == "6M" {
                cell.lblAvgReturnR.text = AppManager.setPositiveValue(value: dataArray[indexPath.row].sixMRet)
                cell.lblAvgReturnD.text = AppManager.setPositiveValue(value: dataArray[indexPath.row].sixMRet_D)
            }
            return cell
        }
        
        return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self._fundname  = dataArray[indexPath.row].Fund
        self.fund       = dataArray[indexPath.row].Fund
        self.smc_code   = dataArray[indexPath.row].scm_code
        
        Funddetails.Category    = self._category
        Funddetails.Fund        = self.fund
        Funddetails.FundName    = self._fundname
        Funddetails.scmCode     = self.smc_code
        let destinationCont = UIStoryboard.init(name: "MFStoryboard" , bundle : nil).instantiateViewController(withIdentifier: "MFSpecificViewController") as! MFSpecificViewController
        
        destinationCont.Category = Funddetails.Category
        destinationCont.Fund     = Funddetails.Fund
        destinationCont.FundName = Funddetails.FundName
        //destinationCont.scmCode  = self.smc_code
        
        
        User.navigation.pushViewController(destinationCont, animated: true)
        
        //    self.performSegue(withIdentifier: "ourfundstospecifiecShow", sender: indexPath.row)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    public func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
       // return 80
        return UITableViewAutomaticDimension
    }
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //return 80
          return UITableViewAutomaticDimension
    }
    
    
}



