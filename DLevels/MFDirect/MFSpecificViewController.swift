//
//  MFSpecificViewController.swift
//  DLevels
//
//  Created by SAIKAT GHOSH on 21/08/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit
import Charts
import Foundation

class cMutualFundCell : UITableViewCell{
    
    @IBOutlet weak var vwContainerView: UIView!
    @IBOutlet weak var lblVal1: UILabel!
    @IBOutlet weak var lblVal2: UILabel!
}


class MFSpecificViewController: UIViewController, ChartViewDelegate {

    @IBOutlet weak var lineChartView: LineChartView!
    @IBOutlet weak var vwTopView: UIView!
    @IBOutlet weak var vwLineChartHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var vwTopHeightConstant: NSLayoutConstraint!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lbl_ltp: UILabel!
    @IBOutlet weak var lbl_date: UILabel!
    @IBOutlet weak var lbl_diff: UILabel!
    
    @IBOutlet weak var img_up_down: UIImageView!
    @IBOutlet weak var tblFundamentals: UITableView!
    
    
    @IBOutlet weak var svButtonStackView: UIStackView!
    
    @IBOutlet weak var rootStackView: UIStackView!
    
    @IBOutlet weak var btnHoldings: UIButton!
    @IBOutlet weak var btnInvest: UIButton!
    
    var dataPoints: [String] = []
    var indPerf: [Float]     = []
    var Perf: [Float]        = []
    
    
    
    var fundDetailsDict      = [NSDictionary]()
    
    var Category    = ""
    var Fund        = ""
    var FundName    = ""
    var scmCode     = ""
    var holdingDate = ""
    
    
    //MARK: VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnHoldings.layer.cornerRadius  =  4
        btnInvest.layer.cornerRadius    =  4
        
        btnHoldings.layer.borderColor   = UIColor.black.cgColor
        btnHoldings.layer.borderWidth   = 0.5
        
        lblTitle.text                   = Funddetails.Fund
        SIPToInvestDescription.scmCode  = Funddetails.scmCode
        SIPToInvestDescription.fundName = Funddetails.FundName
        
        // KVNProgress.show(withStatus: Titles.loading)
        
        if Funddetails.scmCode == "" {
            btnInvest.isHidden                  = true
            lineChartView.isHidden              = true
            
            let chartview = self.rootStackView.arrangedSubviews[1]
            chartview.isHidden = true
            
            
            lineChartView.isHidden              = true
            getFundDetails(fund: Funddetails.Fund)
        }else {
            
            setChart { (success) in
                self.getFundDetails(fund: Funddetails.Fund)
            }
        }
        
    }
    
    //MARK: VIEW WILL APPEAR
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Funddetails.isInvestMore = false
        SIPToInvestDescription.SIP_DAY_OF_INVESTMENT = ""
    }
    
    
    
    // @IBAction func btnHome_Action(_ sender: UIButton) {
    // AppManager.moveToHomeScreen()
    // }
    
    //MARK SEARCH ACTION
    @IBAction func btnSearch_Action(_ sender: UIButton) {
        AppManager.pushToSearchScreen()
    }
    
    
    //MARK: Getting data for Ticker List
    func loadDataForTickerList(schme_code : String){
        
        var diff    = ""
        var diffper = ""
        var date    = ""
        
        let paremeters = "code=\(schme_code)"
        
        NetworkHandler().handleRequest(url: Url.mfgetnavticker, methodName: "POST", parameters: paremeters, isToken: true, showLoader: true) { (returnValue, jsonData) in
            
            print("Json Data is  Live Price:  \(jsonData)")
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                let tempArray = jsonData.value(forKey: "response") as! NSArray
                
                if(tempArray.count > 0){
                    let indexval = tempArray[0] as! NSDictionary
                    diff = indexval.value(forKey: "CHNG") as! String
                    diffper = indexval.value(forKey: "CHNG_PER") as! String
                    date = indexval.value(forKey: "CURR_NAV_DATE") as! String
                    
                    DispatchQueue.main.async(execute: {
                        
                        self.lbl_ltp.text = (indexval.value(forKey: "CURR_NAV") as! String)
                        self.lbl_ltp.sizeToFit()
                        if ((diff as NSString).floatValue >= 0)
                        {
                            self.lbl_diff.text = "\(diff)(\(diffper)%)"
                            self.lbl_diff.textColor = UIColor(red: 29/255, green: 204/255, blue: 146/255, alpha: 1)
                            self.img_up_down.image = #imageLiteral(resourceName: "up")
                        }
                        else
                        {
                            self.lbl_diff.text = "\(diff)(\(diffper)%)"
                            self.lbl_diff.textColor = UIColor.red
                            self.img_up_down.image = #imageLiteral(resourceName: "Down")
                        }
                        self.lbl_diff.sizeToFit()
                        self.lbl_date.text = "NAV of \(date)"
                        self.lbl_date.sizeToFit()
                        
                        if let holdingCount = indexval.value(forKey: "HOLDINGS_COUNT") as? String {
                            self.btnHoldings.isHidden = holdingCount != "0" ? false : true
                            
                        }
                        
                        if let holdingDate = indexval.value(forKey: "HOLDINGS_DT_TEXT") as? String {
                            self.holdingDate = holdingDate
                        }
                        
                        KVNProgress.dismiss()
                        
                    })
                }
                
                else
                {
                    
                    let firstView = self.rootStackView.arrangedSubviews[0]
                    firstView.isHidden = true
                    
                    AppManager.showAlertMessage(title: "error", message: "No Data Found!")
                    
                }
            }
            else
            {
                AppManager.showAlertMessage(title: "error", message: jsonData.value(forKey: "errmsg") as! String)
                
            }
        }
    }
    
    
    //MARK: GET FUND DETAILS
    func getFundDetails(fund : String){
        
        let category_var  =  Funddetails.Category.replacingOccurrences(of: " ", with: "%20").replacingOccurrences(of: "&", with: "%26")
        let fund_var      =  Funddetails.Fund.replacingOccurrences(of: " ", with: "%20").replacingOccurrences(of: "&", with: "%26")
        
        let parameters_val  = "category=\(category_var)&fund=\(fund_var)"
        
        print(parameters_val)
        
        NetworkHandler().handleRequest(url: Urls.mfgetFunds, methodName: "POST", parameters: parameters_val, isToken: true, showLoader: true){(returnValue, jsonData) in
            
            print("Json Data New is :  \(jsonData)")
            var ftype = "" // CURRENTLY NOT CHECKING THIS
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                DispatchQueue.main.async(execute: {
                    self.fundDetailsDict.removeAll()
                    let tempArray = jsonData.value(forKey: "response") as! [NSDictionary]
                    if(tempArray.count > 0){
                        
                        //CHECKING INVEST MORE IN THIS METHODE
                        self.getAllPortfolio(completion: { (success) in
                        })
                        
                        self.fundDetailsDict = tempArray
                        
                        for arr in tempArray{
                            if let display = arr.value(forKey: "mfp_Display") as? String{
                                if display == "Category" {
                                    self.FundName = (arr.value(forKey: "ColValue") as? String)!
                                }
                            }
                            
                            if let display = arr.value(forKey: "mfp_Display") as? String{
                                if display == "Fund Type" {
                                    ftype = (arr.value(forKey: "ColValue") as? String)!
                                }
                            }
                        }
                        
                        let section = IndexSet(integer: 0)
                        self.tblFundamentals.reloadSections(section, with: .automatic)
                        
                        //if ftype == "Equity" || ftype == "Hybrid" {
                        if Funddetails.scmCode != "" {
                            self.scmCode = Funddetails.scmCode
                            self.loadDataForTickerList(schme_code: Funddetails.scmCode)
                        }else {
                            
                            self.btnHoldings.isHidden    = true
                            self.btnHoldings.heightAnchor.constraint(equalToConstant: 0.0).isActive = true
                            self.tblFundamentals.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0.0).isActive = true
                            self.vwTopView.isHidden = true
                            KVNProgress.dismiss()
                            
                        }
                        //}
                    }else{AppManager.showAlertMessage(title: "error", message: "somethingWentWrong!!")}
                })
            }
            else{AppManager.showAlertMessage(title:"error", message:"somethingWentWrong!!!")}
        }
    }
    
    //MARK: GET ALL PORTFOLIO
    func getAllPortfolio(completion: @escaping(Bool)->Void) {
        DataProvider.shared.getDashboardDetails { (success, dashboardData, portfolioData, pendingData) in
            print(dashboardData)
            print(portfolioData)
            print(portfolioData.count)
            
            if portfolioData.isEmpty {
                self.btnInvest.setTitle("INVEST", for: .normal)
            } else {
                
                let filterArray = portfolioData.filter { $0["SchemeCode"] as! String == self.scmCode }
                Funddetails.isInvestMore = filterArray.isEmpty ? false : true
                if !filterArray.isEmpty{
                    if let folioNo = filterArray.first?.value(forKey: "portfolioFilloNo") as? String {
                        SIPToInvestDescription.folioNo = folioNo
                    }
                }
                self.btnInvest.setTitle(filterArray.isEmpty ? "INVEST" : "INVEST MORE", for: .normal)
                self.svButtonStackView.isHidden = false
            }
        }
        
    }
    
    //MARK: SET CHART
    func setChart(completion: @escaping (Bool)-> Void ) {
        
        lineChartView.delegate=self
        let paremeters = "scheme=\(SIPToInvestDescription.scmCode)&from=2018-02-26&to=3000-12-31"
        var linechartDataSet = LineChartDataSet()
        
        //Mark: Call Service for Chart Data
        NetworkHandler().handleRequest(url: Url.mfgetschemenavhistory, methodName: "GET", parameters: paremeters, isToken: true, showLoader: true) { (returnValue, jsonData) in
            
            print("Json Data is :  \(jsonData)")
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                DispatchQueue.main.async {
                    
                    let tempArray = jsonData.value(forKey: "response") as! NSArray
                    
                    if tempArray.count == 0 {
                        let chartView = self.rootStackView.arrangedSubviews[1]
                        chartView.isHidden = true
                        return
                    }
                    
                    var dataEntries: [ChartDataEntry] = []
                    var index : Double = 0.0
                    
                    for dict in tempArray {
                        let dictValue = dict as! NSDictionary
                        
                        if let navDt:String = dictValue.value(forKey: "NAV_DATE") as? String{
                            if let navVal:String = dictValue.value(forKey: "NAV_VALUE") as? String{
                                index = index + 1
                                self.dataPoints.append(navDt)
                                self.indPerf.append((navVal as NSString).floatValue)
                                if let yValue = Double(navVal) {
                                    let chartData = ChartDataEntry(x: index, y: yValue)
                                    dataEntries.append(chartData)
                                }
                            }
                        }
                    }
                    
                    self.lineChartView.isHidden = false
                    linechartDataSet        = LineChartDataSet(values: dataEntries, label: "")
                    linechartDataSet.axisDependency = .left
                    linechartDataSet.drawCirclesEnabled = false
                    linechartDataSet.drawValuesEnabled = false
                    linechartDataSet.circleColors = [NSUIColor.white]
                    linechartDataSet.setCircleColor(UIColor.white)
                    linechartDataSet.drawCircleHoleEnabled = false
                    linechartDataSet.colors = [UIColor.init(hex: "004FAA")] // [self.dIndexColor]
                    var dataSets : [LineChartDataSet] = [LineChartDataSet]()
                    dataSets.append(linechartDataSet)
                    
                    self.lineChartView.xAxis.valueFormatter = IndexAxisValueFormatter(values: self.dataPoints)
                    
                    self.lineChartView.xAxis.granularity = 14//90
                    self.lineChartView.xAxis.labelPosition = .bottom
                    self.lineChartView.rightAxis.enabled = false
                    self.lineChartView.data?.setDrawValues(true)
                    self.lineChartView.leftAxis.drawGridLinesEnabled = true
                    self.lineChartView.xAxis.drawGridLinesEnabled = true
                    self.lineChartView.legend.enabled = false
                    self.lineChartView.chartDescription?.text = ""
                    
                    // let marker:BalloonMarker = BalloonMarker(color: COLORS.yello, font: UIFont(name: "Helvetica", size: 12)!, textColor: UIColor.black, insets: UIEdgeInsets(top: 5.0, left: 5.0, bottom: 5.0, right: 5.0))
                    
                    //marker.minimumSize = CGSize(width: 75.0, height: 35.0)
                    //self.lineChartView.marker = marker
                    
                    let data: LineChartData = LineChartData(dataSets: dataSets)
                    self.lineChartView.data = data
                    
                    completion(true)
                }
                
            }
        }
        /* END Chart Section */
    }
    
    
    //MARK: PREPARE FOR SEGUE
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //segue for the popover configuration window
        
    }
    
    //MARK: BACK BUTTON ACTION
    @IBAction func backClick(_ sender: Any) {
        User.navigation.popViewController(animated: true)
    }
    
    
    //MARK: CHECK LOGIN
    /* func checkLogin() -> Bool{
     
     // User.lastViewIdentifier = "MFSpecificPageViewController"
     
     if User.MF_UserId == "" {
     AppManager.sendToLoginController()
     }
     
     else if User.MF_UserId == "0" {
     AppManager.moveToGetPanDetail()
     }
     
     // else if User.MF_UserId != "0" && UserSession.shared.getUcc() == "" {
     //   if UserSession.shared.getMFKYC() == MFKycStatus.alreadyDone {
     //        AppManager.sendToKYCSummaryController()
     //  }
     
     //      else if User.MF_kyc == MFKycStatus.inProgress {
     //      AppManager.sendToKYCSummaryController()
     //}
     else {
     //    AppManager.sendToKYCSummaryController()
     }
     }
     
     else if User.ucc != "" {
     return true
     }
     
     return false
     }
     */
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
        
    }
    
    
    
    @IBAction func searchClick(_ sender: Any) {
        let viewCont = self.storyboard?.instantiateViewController(withIdentifier: "MFSearchViewController") as! MFSearchViewController
        User.navigation.pushViewController(viewCont, animated: false)
    }
    
    
    //MARK: HOLDING BUTTON ACTION
    @IBAction func btnHolding_Action(_ sender: UIButton) {
        let holdingCont = StoryBoard.mf.instantiateViewController(withIdentifier: "MFHolidingPageViewController") as! MFHolidingPageViewController
        holdingCont.fund = Funddetails.Fund
        holdingCont.holdingDate = holdingDate
        User.navigation.pushViewController(holdingCont, animated: true)
    }
    
    
    //MARK: PERFORM SEGUE
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any!) -> Bool {
        if identifier == "InvestPopup" {
            // return checkLogin()
        }
        return true
    }
    
    //MARK: PERFORM SEGUE
    override func performSegue(withIdentifier identifier: String, sender: Any?) {
        if identifier == "InvestPopup" {
        }
    }
    
}


extension MFSpecificViewController : UITableViewDataSource, UITableViewDelegate {
    
    //MARK: ESTIMATED HEIGHT FOR ROW AT INDEX PATH
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    //MARK: HEIGHT FOR ROW AT INDEX PATH
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    //MARK: NUMBER OF ROW IN SECTIONS
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier:"cell", for: indexPath) as! cMutualFundCell
        
        cell.selectionStyle = .none
        //cell.vwContainerView.backgroundColor = UIColor(hex: AppManager.getAlternetColor(alternetColor: indexPath.row % 2 == 0))
        cell.vwContainerView.backgroundColor = indexPath.row % 2 == 0 ? UIColor(red: 236/255, green: 239/255, blue: 240/255, alpha: 1.0) : UIColor(red: 215/255, green: 222/255, blue: 225/255, alpha: 1.0)
        
        let index = fundDetailsDict[indexPath.row]
        
        if let label1 = index.value(forKey: "mfp_Display") as? String {
            cell.lblVal1.text = label1
        }
        if let label2 = index.value(forKey: "ColValue") as? String {
            cell.lblVal2.text = label2 == "0.00" ? "-" : label2
        }
        
        return cell
        
        
    }
    //MARK: NUMBER OF ROW IN SECTION
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return fundDetailsDict.count
    }
}


