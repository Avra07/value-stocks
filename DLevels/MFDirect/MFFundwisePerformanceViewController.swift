//
//  MFFundwisePerformanceViewController.swift
//  DLevels
//
//  Created by SAIKAT GHOSH on 21/08/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit





class MFFundwisePerformanceTableViewCell: UITableViewCell{
    @IBOutlet weak var lblFundHouse: UILabel!
    @IBOutlet weak var lblNetAssetss: UILabel!
    @IBOutlet weak var lblAvgReturnD: UILabel!
    @IBOutlet weak var lblAvgReturnR: UILabel!
    @IBOutlet weak var imgRecomendation: UIImageView!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var vwContainerView: UIView!
}

class MFFundwisePerformanceViewController: UIViewController {
    
    @IBAction func Disclaimer(_ sender: Any) {
        
        
        let strybrd =  UIStoryboard.init(name: "Main", bundle: nil)
        let obj = strybrd.instantiateViewController(withIdentifier: "DisclaimerViewController") as! DisclaimerViewController
        User.navigation.pushViewController(obj, animated: true)
        obj.disclaimerUrl = DisclaimerUrl.mf_research
        obj.disclaimerHeader = "MF Research"
        
        
    }
    @IBOutlet weak var tableVw: UITableView!
    @IBOutlet weak var stckVw: UIStackView!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDetails: UILabel!
    
    @IBOutlet weak var btn1W: UIButton!
    @IBOutlet weak var btn1M: UIButton!
    @IBOutlet weak var btn1Y: UIButton!
    
    @IBOutlet weak var btn3M: UIButton!
    @IBOutlet weak var btn3Y: UIButton!
    
    @IBOutlet weak var btn5Y: UIButton!
    @IBOutlet weak var btn6M: UIButton!
    
    fileprivate var dataArray = [MFFundwisePerformance]()
    var isSortByFund = false, isSortByAsset = false, isSortByD = true, isSortByR = false
    
    var fname = ""
    var desc = ""
    
    var duration = "1Y"
    
    var noOfTotalFunds = 0
    var totalNetAssest = 0
    var col = ""
    var returnCol = "A"
    
    var fund = ""
    var smc_code = ""
    
    var _category: String!
    var _fundname: String!
    var _type: String!
    
    var Category:String {
        get {
            return self._category
        }
        set {
            self._category = newValue
        }
    }
    
    var FundType:String {
        get {
            return self._type
        }
        set {
            self._type = newValue
        }
    }
    
    //MARK: VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblDetails.text = desc
        for case let button as UIButton in self.stckVw.subviews {
            button.layer.cornerRadius = 0
            button.layer.borderWidth = 0
            button.layer.borderColor = nil
        }
        
        tableVw?.delegate = self
        tableVw?.dataSource = self
        
        btn1Y.layer.cornerRadius = 4.0
        btn1Y.layer.borderWidth = 1
        btn1Y.layer.borderColor =  UIColor(hex: "F7BB1A").cgColor
        
        tableVw.tableHeaderView = nil;
        tableVw.tableFooterView = nil;
        tableVw.backgroundView?.isHidden = true
        
        
        let obj_FundwisePerformance = MFFundwisePerformance()
        print(self.fname)
        obj_FundwisePerformance.getData(paramType: self._type, paramCategory: self._category, paramFundType: self.fname, completion: { (dataset) in
            self.dataArray = dataset
            
            self.sortByD()
            
            self.tableVw.reloadSections(IndexSet(integer: 0), with: .automatic)
        })
        
        self.lblTitle.text = self.fname + " - " + self.Category
        
    }
    
    //MARK: VIEW WILL APPEAR
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UserSession.shared.clearInvestmentValues()
        
    }
    
    
    //MARK: PREPARE FOR SEGUE
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let identifier = segue.identifier {
            if identifier == "specifiecShow" {
                if let destinationCont = segue.destination as? MFSpecificPageViewController {
                    
                    if self._fundname != "" {
                        
                        Funddetails.Category    = self._category
                        Funddetails.Fund        = self.fund
                        Funddetails.FundName    = self._fundname
                        Funddetails.scmCode     = self.smc_code
                        
                        
                        destinationCont.Category = self._category
                        destinationCont.Fund     = self.fund
                        destinationCont.FundName = self._fundname
                        destinationCont.scmCode = self.smc_code
                        
                    } else {
                        print("failed to get name from dictionary")
                    }
                }
            }
        }
    }
    
    //MARK: HOME ACTION
    //  @IBAction func btnHome_Action(_ sender: UIButton) {
    //  AppManager.moveToHomeScreen()// }
    
    //MARK SEARCH ACTION
    @IBAction func btnSearch_Action(_ sender: UIButton) {
        AppManager.pushToSearchScreen()
    }
    
    
    //MARK: FUND TYPE SORT ACTION
    @IBAction func btnFundTypeSort_Action(_ sender: Any) {
        isSortByFund = !isSortByFund
        
        isSortByAsset = false; isSortByD = false; isSortByR = false
        
        dataArray = isSortByFund ? dataArray.sorted (by: {$0.FundDisplay > $1.FundDisplay}) : dataArray.sorted (by: {$0.FundDisplay < $1.FundDisplay})
        self.tableVw.reloadData()
        
        // sortByD()
        
        self.moveToTop()
    }
    
    
    //MARK: SORT BY ASSET
    @IBAction func btnNetAsset_Action(_ sender: Any) {
        isSortByAsset = !isSortByAsset
        
        isSortByFund = false; isSortByD = false; isSortByR = false
        
        dataArray = isSortByAsset ? dataArray.sorted (by: {$0.NetAssets > $1.NetAssets}) : dataArray.sorted (by: {$0.NetAssets < $1.NetAssets})
        self.tableVw.reloadData()
        self.moveToTop()
    }
    
    //MARK: SORT BY D
    @IBAction func btnReturnD_Action(_ sender: Any) {
        isSortByD = !isSortByD
        sortByD()
    }
    
    //MARK: SORT FUNCTION
    private func sortByD(){
        
        isSortByFund = false; isSortByAsset = false; isSortByR = false
        
        if duration == "1W" {
            dataArray = isSortByD ? dataArray.sorted (by: {$0.oneWRet_D > $1.oneWRet_D}) : dataArray.sorted (by: {$0.oneWRet_D < $1.oneWRet_D})
        } else if duration == "1Y" {
            dataArray = isSortByD ? dataArray.sorted (by: {$0.oneYRet_D > $1.oneYRet_D}) : dataArray.sorted (
                by: {$0.oneYRet_D < $1.oneYRet_D})
            self.tableVw.reloadData()
        } else if duration == "1M" {
            dataArray = isSortByD ? dataArray.sorted (by: {$0.oneMRet_D > $1.oneMRet_D}) : dataArray.sorted (by: {$0.oneMRet_D < $1.oneMRet_D})
            self.tableVw.reloadData()
        } else if duration == "3M" {
            dataArray = isSortByD ? dataArray.sorted (by: {$0.threeMRet_D > $1.threeMRet_D}) : dataArray.sorted (by: {$0.threeMRet_D < $1.threeMRet_D})
            self.tableVw.reloadData()
        } else if duration == "6M" {
            dataArray = isSortByD ? dataArray.sorted (by: {$0.sixMRet_D > $1.sixMRet_D}) : dataArray.sorted (by: {$0.sixMRet_D < $1.sixMRet_D})
            self.tableVw.reloadData()
        } else if duration == "3Y" {
            dataArray = isSortByD ? dataArray.sorted (by: {$0.threeYRet_D > $1.threeYRet_D}) : dataArray.sorted (by: {$0.threeYRet_D < $1.threeYRet_D})
            self.tableVw.reloadData()
        } else if duration == "5Y" {
            dataArray = isSortByD ? dataArray.sorted (by: {$0.fiveYRet_D > $1.fiveYRet_D}) : dataArray.sorted (by: {$0.fiveYRet_D < $1.fiveYRet_D})
            self.tableVw.reloadData()
        }
        
        self.tableVw.reloadData()
        self.moveToTop()
    }
    
    
    //MARK: SORT BY RETURN
    @IBAction func btnReturnR_Action(_ sender: Any) {
        isSortByR = !isSortByR
        
        isSortByFund = false; isSortByAsset = false; isSortByD = false
        
        if duration == "1W" {
            dataArray = isSortByR ? dataArray.sorted (by: {$0.oneWRet > $1.oneWRet}) : dataArray.sorted (by: {$0.oneWRet < $1.oneWRet})
            self.tableVw.reloadData()
        } else if duration == "1Y" {
            dataArray = isSortByR ? dataArray.sorted (by: {$0.oneYRet > $1.oneYRet}) : dataArray.sorted (by: {$0.oneYRet < $1.oneYRet})
            self.tableVw.reloadData()
        } else if duration == "1M" {
            dataArray = isSortByR ? dataArray.sorted (by: {$0.oneMRet > $1.oneMRet}) : dataArray.sorted (by: {$0.oneMRet < $1.oneMRet})
            self.tableVw.reloadData()
        } else if duration == "3M" {
            dataArray = isSortByR ? dataArray.sorted (by: {$0.threeMRet > $1.threeMRet}) : dataArray.sorted (by: {$0.threeMRet < $1.threeMRet})
            self.tableVw.reloadData()
        }else if duration == "6M" {
            dataArray = isSortByR ? dataArray.sorted (by: {$0.sixMRet > $1.sixMRet}) : dataArray.sorted (by: {$0.sixMRet < $1.sixMRet})
            self.tableVw.reloadData()
        } else if duration == "3Y" {
            dataArray = isSortByR ? dataArray.sorted (by: {$0.threeYRet > $1.threeYRet}) : dataArray.sorted (by: {$0.threeYRet < $1.threeYRet})
            self.tableVw.reloadData()
        } else if duration == "5Y" {
            dataArray = isSortByR ? dataArray.sorted (by: {$0.fiveYRet > $1.fiveYRet}) : dataArray.sorted (by: {$0.fiveYRet < $1.fiveYRet})
            self.tableVw.reloadData()
        }
        self.moveToTop()
        
    }
    
    //MARK: BACK CLICK
    @IBAction func backClick(_ sender: Any) {
        self.navigationController!.popViewController(animated: true)
    }
    
    //MARK: SORT BY DURATION
    func sortByMonth(){
        
        if duration == "1W" {
            dataArray = dataArray.sorted (by: {$0.oneWRet_D > $1.oneWRet_D})
        } else if duration == "1Y" {
            dataArray = dataArray.sorted (
                by: {$0.oneYRet_D > $1.oneYRet_D})
            self.tableVw.reloadData()
        } else if duration == "1M" {
            dataArray = dataArray.sorted (by: {$0.oneMRet_D > $1.oneMRet_D})
            self.tableVw.reloadData()
        } else if duration == "3M" {
            dataArray = dataArray.sorted (by: {$0.threeMRet_D > $1.threeMRet_D})
            self.tableVw.reloadData()
        } else if duration == "6M" {
            dataArray = dataArray.sorted (by: {$0.sixMRet_D > $1.sixMRet_D})
            self.tableVw.reloadData()
        } else if duration == "3Y" {
            dataArray = dataArray.sorted (by: {$0.threeYRet_D > $1.threeYRet_D})
            self.tableVw.reloadData()
        } else if duration == "5Y" {
            dataArray = dataArray.sorted (by: {$0.fiveYRet_D > $1.fiveYRet_D})
            self.tableVw.reloadData()
        }
        
        self.tableVw.reloadData()
        moveToTop()
    }
    
    //MARK: MOVE TO TOP
    private func moveToTop(){
        DispatchQueue.main.async {
            
            let indexPath = IndexPath(row: 0 , section: 0)
            
            //   self.tableVw.scrollToRow(at: indexPath, at: .bottom, animated: true)
            
        }
    }
    
}

//MARK: EXTENSION PERFORMANCE VIEW CONTROLLER
extension MFFundwisePerformanceViewController {
    
    //MARK: DURATION BUTTON CLICK
    func durationBtnClick(btn: UIButton) {
        
        if let title = btn.titleLabel?.text {
            duration =  title
        }
        
        for case let button as UIButton in self.stckVw.subviews {
            button.layer.cornerRadius = 4.0
            button.layer.borderWidth  = 2.0
            button.layer.borderColor  = button.titleLabel?.text == duration ?  UIColor(hex: "F7BB1A").cgColor : UIColor.clear.cgColor
        }
        
        sortByMonth()
        self.tableVw.reloadData()
        // self.moveToTop()
    }
    
    
    
    
    @IBAction func btn1W_Action(_ sender: UIButton) {
        self.durationBtnClick(btn: sender)
    }
    
    @IBAction func btn1M_Action(_ sender: UIButton) {
        self.durationBtnClick(btn: sender)
    }
    
    @IBAction func btn1Y_Action(_ sender: UIButton) {
        self.durationBtnClick(btn: sender)
    }
    
    @IBAction func btn3M_Action(_ sender: UIButton) {
        self.durationBtnClick(btn: sender)
    }
    
    @IBAction func btn3Y_Action(_ sender: UIButton) {
        self.durationBtnClick(btn: sender)
    }
    
    @IBAction func btn5Y_Action(_ sender: UIButton) {
        self.durationBtnClick(btn: sender)
    }
    
    @IBAction func btn6M_Action(_ sender: UIButton) {
        self.durationBtnClick(btn: sender)
    }
    
}


//MARK: TABLE VIEW DELEGATE AND DATA SOURCE
extension MFFundwisePerformanceViewController: UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? MFFundwisePerformanceTableViewCell
        {
            cell.selectionStyle = .none
            
            //cell.vwContainerView.backgroundColor = UIColor(hex: AppManager.getAlternetColor(alternetColor: indexPath.row % 2 == 0))
            cell.vwContainerView.backgroundColor = indexPath.row % 2 == 0 ? UIColor(red: 236/255, green: 239/255, blue: 240/255, alpha: 1.0) : UIColor(red: 215/255, green: 222/255, blue: 225/255, alpha: 1.0)
            
            
            let fundHouseText = dataArray[indexPath.row].FundDisplay.replacingOccurrences(of: "- Direct", with: "")
            let attributedString = NSMutableAttributedString.init(string: fundHouseText)
            attributedString.addAttribute(NSUnderlineStyleAttributeName, value: 1, range: NSRange.init(location: 0, length: attributedString.length))
            cell.lblFundHouse.attributedText = attributedString
            cell.lblFundHouse.sizeToFit()
            
            
            cell.lblNetAssetss.text = String(dataArray[indexPath.row].NetAssets)
            cell.lblCategory.text = dataArray[indexPath.row].SubCategory// + "\n"
            
            cell.imgRecomendation.isHidden =  dataArray[indexPath.row].recomended == "1" ? false : true
            
            if duration == "1W" {
                cell.lblAvgReturnR.text = AppManager.setPositiveValue(value: dataArray[indexPath.row].oneWRet)
                cell.lblAvgReturnD.text = AppManager.setPositiveValue(value: dataArray[indexPath.row].oneWRet_D)
            }
                
            else if duration == "1M" {
                cell.lblAvgReturnR.text = AppManager.setPositiveValue(value: dataArray[indexPath.row].oneMRet)//String(dataArray[indexPath.row].oneMRet)
                cell.lblAvgReturnD.text = AppManager.setPositiveValue(value: dataArray[indexPath.row].oneMRet_D)
            }
                
            else if duration == "1Y" {
                cell.lblAvgReturnR.text = AppManager.setPositiveValue(value: dataArray[indexPath.row].oneYRet)
                cell.lblAvgReturnD.text = AppManager.setPositiveValue(value: dataArray[indexPath.row].oneYRet_D)
            }
                
            else if duration == "3M" {
                cell.lblAvgReturnR.text = AppManager.setPositiveValue(value: dataArray[indexPath.row].threeMRet)
                cell.lblAvgReturnD.text = AppManager.setPositiveValue(value: dataArray[indexPath.row].threeMRet_D)
            }
                
            else if duration == "3Y" {
                cell.lblAvgReturnR.text = AppManager.setPositiveValue(value: dataArray[indexPath.row].threeYRet)
                cell.lblAvgReturnD.text = AppManager.setPositiveValue(value: dataArray[indexPath.row].threeYRet_D)
            }
                
            else if duration == "5Y" {
                cell.lblAvgReturnR.text = AppManager.setPositiveValue(value: dataArray[indexPath.row].fiveYRet)
                cell.lblAvgReturnD.text = AppManager.setPositiveValue(value: dataArray[indexPath.row].fiveYRet_D)
            }
                
            else if duration == "6M" {
                cell.lblAvgReturnR.text = AppManager.setPositiveValue(value: dataArray[indexPath.row].sixMRet)
                cell.lblAvgReturnD.text = AppManager.setPositiveValue(value: dataArray[indexPath.row].sixMRet_D)
            }
            return cell
        }
        
        return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self._fundname  = dataArray[indexPath.row].Fund
        self.fund       = dataArray[indexPath.row].Fund
        self.smc_code   = dataArray[indexPath.row].scm_code
        Funddetails.scmCode = self.smc_code
        Funddetails.Fund = self.fund
        
        // self.performSegue(withIdentifier: "specifiecShow", sender: indexPath.row)
        let destinationCont = StoryBoard.mf.instantiateViewController(withIdentifier: "MFSpecificViewController") as! MFSpecificViewController
        User.navigation.pushViewController(destinationCont, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    public func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    
}



