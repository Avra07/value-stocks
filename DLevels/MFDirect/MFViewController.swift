//
//  MFViewController.swift
//  DLevels
//
//  Created by SAIKAT GHOSH on 21/08/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class MFViewController: UIViewController {
    
    @IBOutlet weak var tblVw: UITableView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var labelcount: UILabel!
    
    
    var mcount : [NSDictionary] = []
    
    //  let localSource = [ImageSource(imageString: "mf_slider_1")!, ImageSource(imageString: "mf_slider_2")!, ImageSource(imageString: "mf_slider_3")!, ImageSource(imageString: "mf_slider_4")!, ImageSource(imageString: "mf_slider_5")!]
    
    //MARK: VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        tblVw.reloadData()
        //  AppManager.removeLoginViewControllersFromStack()
        let imageView = UIImageView(image:UIImage(named: "AppLogo"))
        self.navigationItem.titleView = imageView
        self.navigationItem.title = "Mutual Fund"
        
        
        
        
        
    }
    
    //MARK: VIEW WILL APPEAR
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //  SIPToInvestDescription.fundName = ""
        //  SIPToInvestDescription.scmCode  = ""
        
        //   AppManager.removeKVNProgress()
    }
    
    
    //MARK: HAMBURGER MENU CLICK
    @IBAction func humbergerClick(_ sender: Any) {
        //AppManager.showHamburgerMenu()
        self.navigationController?.popViewController(animated: true)
    }
    
    //LOG OUT BUTTON
    
    
}





//   func investClick(_ sender: Any) {

// }

// @IBAction func dashboardClick(_ sender: Any) {
//AppManager.sendToDashBoard()
// }

//MARK: ADVISORY CLICK
// @IBAction func advisoryClick(_ sender: Any) {
// AppManager.sendToAdvisory()
//   }
//MARK: PROFILE CLICK
// @IBAction func profileClick(_ sender: Any) {
// AppManager.sendToProfile()
// }






extension MFViewController{
    
    @objc func searchAction(sender: UIButton) {
        AppManager.pushToSearchScreen()
    }
    //MARK: CATEGORY ACTION
    @objc func categoryAction(sender: UIButton) {
        moveToAllMFCategoriesController(type: "category")
    }
    
    //MARK: MANAGERS ACTION
    @objc func managersAction(sender: UIButton) {
        moveToAllMFCategoriesController(type: "manager")
        
    }
    //MARK: HOUSE ACTION
    @objc func houseAction(sender: UIButton) {
        moveToAllMFCategoriesController(type: "house")
    }
    
    //MARK: OUR FUND PICKS ACTION
    @objc func ourfundpicksAction(sender: UIButton) {
        moveToAllMFCategoriesController(type: "ourfunds")
    }
    
    //MARK: ALL MF CATEGORIES CONTROLLER
    func moveToAllMFCategoriesController(type: String){
        let strBrd = UIStoryboard.init(name: "MFStoryboard", bundle: nil)
        let vwCtrl = strBrd.instantiateViewController(withIdentifier: "MFAllCategoriesViewController") as! MFAllCategoriesViewController
        vwCtrl.type = type
        User.navigation.pushViewController(vwCtrl, animated: true)
    }
    
    
    
}

// MARK: TABLE VIEW DELEGATE AND DATASOURCE
extension MFViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        
        let searchCell = tableView.dequeueReusableCell(withIdentifier: "searchCell", for: indexPath)
        let txtSearch = searchCell.viewWithTag(2002) as! UITextField
        txtSearch.layer.borderColor = UIColor.gray.cgColor
        txtSearch.layer.borderWidth = 0.8
        txtSearch.layer.cornerRadius = 4
        
        let btn = searchCell.viewWithTag(2001) as! UIButton
        btn.addTarget(self, action: #selector(searchAction(sender:)), for: .touchUpInside)
        
        let categoryBtn = searchCell.viewWithTag(3001) as! UIButton
        let managerBtn = searchCell.viewWithTag(3002) as! UIButton
        let houseBtn = searchCell.viewWithTag(3003) as! UIButton
        let ourfundpicksBtn = searchCell.viewWithTag(3004) as! UIButton
        
        
        categoryBtn.addTarget(self, action: #selector(categoryAction(sender:)), for: .touchUpInside)
        managerBtn.addTarget(self, action: #selector(managersAction(sender:)), for: .touchUpInside)
        houseBtn.addTarget(self, action: #selector(houseAction(sender:)), for: .touchUpInside)
        ourfundpicksBtn.addTarget(self, action: #selector(ourfundpicksAction(sender:)), for: .touchUpInside)
        
        
        return searchCell
        
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 600
        
        
    }
    
}

//... END TABLE VIEW DELEGATE AND DATASOURCE
