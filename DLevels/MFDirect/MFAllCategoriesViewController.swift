//
//  MFAllCategoriesViewController.swift
//  DLevels
//
//  Created by SAIKAT GHOSH on 21/08/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class MFAllCategoriesViewController: UIViewController {

    @IBOutlet weak var containerView: UIView!
    var pageMenu : CAPSPageMenu!
    var controllerArray : [UIViewController] = []
    var type = ""
    @IBOutlet weak var lblTitle: UILabel!
    
    //MARK: VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblTitle.text = type == "category" ? "Best Fund Categories" : type == "manager" ? "Best Fund Managers" : type == "ourfunds" ? "Our Fund Picks" : "Best Fund House"
        
        /*let objctrl = MFHomeDataModel()
         objctrl.getData(completion: { (arrResult) in
         
         for arr in arrResult {
         
         let VwCtrlr = self.storyboard!.instantiateViewController(withIdentifier: "MFCategoriesViewController") as! MFCategoriesViewController
         
         VwCtrlr.Category = arr.category
         VwCtrlr.TitleVal = arr.title
         VwCtrlr.CatType = self.type
         let menuTitleStr = arr.category
         //print(menuTitleStr.capitalized)
         VwCtrlr.title = menuTitleStr.capitalized
         self.controllerArray.append(VwCtrlr)
         
         }
         
         // Initialize scroll menu
         self.pageMenu = CAPSPageMenu(viewControllers: self.controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: self.view.frame.height), pageMenuOptions: nil)
         //  self.addChild(self.pageMenu!)
         self.containerView.addSubview(self.pageMenu!.view)
         // self.pageMenu!.didMove(toParent: self)
         })*/
        
        getFundData { (success) in
            
        }
        
    }
    
    //MARK: SEARCH BUTTON ACTION
    @IBAction func btnSearch_Action(_ sender: UIButton) {
        AppManager.pushToSearchScreen()
    }
    
    
    //MARK: GET FUND DATA
    func getFundData(completion : @escaping(Bool)-> Void) {
        
        DataProvider.shared.getFundCategories { (arrResult) in
            self.controllerArray.removeAll()
            if self.type == "ourfunds" {
                
                if !arrResult.isEmpty {
                    let catNewCont = self.storyboard?.instantiateViewController(withIdentifier: "MFOurFundwisePerformanceViewController") as! MFOurFundwisePerformanceViewController
                    catNewCont.Category = arrResult.first!.category!
                    catNewCont.FundType = self.type
                    catNewCont.fname = ""
                    catNewCont.title    = arrResult.first!.category!.capitalized
                    self.controllerArray.append(catNewCont)
                }
                
                if arrResult.count >  1 {
                    let catNewCont = self.storyboard?.instantiateViewController(withIdentifier: "MFOurFundwisePerformanceViewController") as! MFOurFundwisePerformanceViewController
                    catNewCont.Category = arrResult[1].category!
                    
                    catNewCont.FundType = self.type
                    catNewCont.fname = ""
                    catNewCont.title    = arrResult[1].category!.capitalized
                    self.controllerArray.append(catNewCont)
                }
                
                
                if arrResult.count > 2 {
                    
                    let catNewCont = self.storyboard?.instantiateViewController(withIdentifier: "MFOurFundwisePerformanceViewController") as! MFOurFundwisePerformanceViewController
                    catNewCont.Category = arrResult[2].category!
                    catNewCont.FundType = self.type
                    catNewCont.fname = ""
                    catNewCont.title    = arrResult[2].category!.capitalized
                    self.controllerArray.append(catNewCont)
                }
                
                if arrResult.count > 3 {
                    let catNewCont = self.storyboard?.instantiateViewController(withIdentifier: "MFOurFundwisePerformanceViewController") as! MFOurFundwisePerformanceViewController
                    catNewCont.Category = arrResult[3].category!
                    catNewCont.FundType = self.type
                    catNewCont.fname = ""
                    catNewCont.title    = "Solution Oriented"//arrResult[3].category.capitalized
                    self.controllerArray.append(catNewCont)
                }
            }
            else {
                if !arrResult.isEmpty {
                    let catNewCont = self.storyboard?.instantiateViewController(withIdentifier: "MFCategoriesController") as! MFCategoriesController
                    catNewCont.category    = arrResult.first!.category!
                    catNewCont.type     = self.type
                    catNewCont.title    = arrResult.first!.category!.capitalized
                    self.controllerArray.append(catNewCont)
                }
                
                if arrResult.count > 1 {
                    let catNewCont = self.storyboard?.instantiateViewController(withIdentifier: "MFCategoriesController") as! MFCategoriesController
                    catNewCont.category = arrResult[1].category!
                    catNewCont.type     = self.type
                    catNewCont.title    = arrResult[1].category!.capitalized
                    self.controllerArray.append(catNewCont)
                }
                
                
                if arrResult.count > 2 {
                    
                    let catNewCont = self.storyboard?.instantiateViewController(withIdentifier: "MFCategoriesController") as! MFCategoriesController
                    catNewCont.category = arrResult[2].category!
                    catNewCont.type     = self.type
                    catNewCont.title    = arrResult[2].category!.capitalized
                    self.controllerArray.append(catNewCont)
                }
                
                if arrResult.count > 3 {
                    let catNewCont = self.storyboard?.instantiateViewController(withIdentifier: "MFCategoriesController") as! MFCategoriesController
                    catNewCont.category = arrResult[3].category!
                    catNewCont.type     = self.type
                    catNewCont.title    = "Solution Oriented"//arrResult[3].category.capitalized
                    self.controllerArray.append(catNewCont)
                }
            }
            
            // Customize menu (Optional)
            let parameters: [CAPSPageMenuOption] = [
                .bottomMenuHairlineColor(UIColor(red: 70.0/255.0, green: 70.0/255.0, blue: 80.0/255.0, alpha: 0.5)),
                .menuItemFont(UIFont(name: "HelveticaNeue-Bold", size: 14.0)!),
                //.menuItemWidthBasedOnTitleTextWidth(true),
                .unselectedMenuItemLabelColor(UIColor(red: 40.0/255.0, green: 76.0/255.0, blue: 90.0/255.0, alpha: 0.5)),
                .selectedMenuItemLabelColor(UIColor(red: 40.0/255.0, green: 76.0/255.0, blue: 90.0/255.0, alpha: 1.0)),
                .menuHeight(40.0),
                .menuItemWidth(120.0),
                .centerMenuItems(false)
                
            ]
            
            
            
            
            /*
             for result in arrResult {
             let vwCont = self.storyboard!.instantiateViewController(withIdentifier: "MFCategoriesViewController") as! MFCategoriesViewController
             
             vwCont._category = result.category
             vwCont._title = result.title
             vwCont._type = self.type
             vwCont.title = result.category.capitalized
             self.controllerArray.append(vwCont)
             }
             */
            
            
            self.pageMenu = CAPSPageMenu(viewControllers: self.controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: self.containerView.frame.width, height: self.containerView.frame.height), pageMenuOptions: parameters)
            self.containerView.addSubview(self.pageMenu!.view)
        }
    }
    
    
    /*
     //MARK: DID TAP GO TO LEFT
     @objc func didTapGoToLeft() {
     let currentIndex = pageMenu!.currentPageIndex
     if currentIndex > 0 {
     pageMenu!.moveToPage(currentIndex - 1)
     }
     }
     
     @objc func didTapGoToRight() {
     let currentIndex = pageMenu!.currentPageIndex
     
     if currentIndex < pageMenu!.controllerArray.count {
     pageMenu!.moveToPage(currentIndex + 1)
     }
     }
     */
    
    //MARK: BACK BUTTON ACTION
    @IBAction func backClick(_ sender: Any) {
        User.navigation.popViewController(animated: true)
    }
    
    override func shouldAutomaticallyForwardRotationMethods() -> Bool {
        return true
    }
    
    
    
}
