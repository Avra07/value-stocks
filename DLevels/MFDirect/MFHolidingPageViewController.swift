//
//  MFHolidingPageViewController.swift
//  DLevels
//
//  Created by SAIKAT GHOSH on 21/08/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//


import UIKit


class holdingCell : UITableViewCell{
    
    @IBOutlet weak var vwContainer: UIView!
    @IBOutlet weak var lblPercentage: UILabel!
    @IBOutlet weak var lblVal1: UILabel!
    @IBOutlet weak var lblVal2: UILabel!
    
    @IBOutlet weak var btnSelection: UIButton!
    //Don't know why table did select not working so have to add manually
    
}

class MFHolidingPageViewController: UIViewController {
    
    @IBOutlet weak var tableVw: UITableView!
    fileprivate var dataArray = [MFHoldings]()
    
    @IBOutlet weak var lblDate: UILabel!
    
    var fund        =    ""
    var holdingDate =    ""
    
    //MARK: VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblDate.text = "As on \(holdingDate)"
        
        // Do any additional setup after loading the view.
        let obj_MFHoldings = MFHoldings(CDNSESymbol: "", INSTRUMENT4: "", DISPLAY: "", SECTOR: "", SymbolName: "", MFNoOfShares: "", per: "", val : "")
        obj_MFHoldings.getData(paramFund: fund, completion: { (dataset) in
            self.dataArray = dataset
            //            self.tableVw.reloadData()
            self.tableVw.reloadSections(IndexSet(integer: 0), with: .automatic)
        })
    }
    
    @IBAction func backClick(_ sender: Any) {
        self.navigationController!.popViewController(animated: true)
    }
    
    
    // @IBAction func btnHome_Action(_ sender: UIButton) {
    //       AppManager.moveToHomeScreen()
    // }
    
    
}

//MARK: TABLE VIEW DELEGATE AND DATA SOURCE
extension MFHolidingPageViewController: UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? holdingCell
        {
            cell.selectionStyle = .none
            cell.vwContainer.layer.masksToBounds = false;
            cell.vwContainer.layer.shadowOffset = CGSize(width: 0, height: 2)
            cell.vwContainer.layer.shadowOpacity = 0.5;
            
            
            
            cell.vwContainer.layer.cornerRadius = 4
            //            cell.lblVal1.text  = dataArray[indexPath.row].CD_NSE_Symbol
            cell.lblVal1.text  = dataArray[indexPath.row].INSTRUMENT_4
            cell.lblVal2.text  = dataArray[indexPath.row].SECTOR
            cell.lblPercentage.text = "\(dataArray[indexPath.row].Perc) %"
            
            cell.btnSelection.tag = indexPath.row
            
            cell.btnSelection.addTarget(self, action: #selector(btnSelection_Action(sender:)), for: .touchUpInside)
            
            return cell
            
        }
        
        return UITableViewCell()
    }
    
    //MARK: SELCTION ACTION
    @objc func btnSelection_Action(sender: UIButton) {
        
        //   self._fundname  = dataArray[sender.tag].FundDisplay
        //self.fund       = dataArray[sender.tag].Fund
        //   self.smc_code   = dataArray[sender.tag].scm_code
        
        
        SearchForCurrentMultibagger.serchValue = dataArray[sender.tag].Symbol_Name
        _  =  SearchForCurrentMultibagger.serchValue
        SearchForCurrentMultibagger.instrument_4 = dataArray[sender.tag].INSTRUMENT_4
        SearchForCurrentMultibagger.viewcontrollername = "MultibaggerList"
        if SearchForCurrentMultibagger.serchValue != ""{
        let viewController = StoryBoard.main.instantiateViewController(withIdentifier: "StockSpecificNewViewController") as! StockSpecificNewViewController
        User.navigation.pushViewController( viewController, animated: false)
        }
    }
    
    //MARK: DID SELECT ROW AT INDEX PATH
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //        self._fundname  = dataArray[indexPath.row].FundDisplay
        //        self.fund       = dataArray[indexPath.row].Fund
        //        self.smc_code   = dataArray[indexPath.row].scm_code
        
        
        //        SearchForCurrentMultibagger.serchValue = response[indexPath.row].value(forKey: "mrd_Security") as! String
        //        _  =  SearchForCurrentMultibagger.serchValue
        //        SearchForCurrentMultibagger.instrument_4 = response[indexPath.row].value(forKey: "mrd_Instrument_4") as! String
        //        SearchForCurrentMultibagger.viewcontrollername = "MultibaggerList"
        //
        //        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "StockSpecificNewViewController") as! StockSpecificNewViewController
        //        self.navigationController?.pushViewController(viewController, animated: false)
        //
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var numOfSections: Int = 0
        if dataArray.count == 0 {
            
            
        } else {
            numOfSections  = dataArray.count
            //tableView.backgroundView?.isHidden = true
        }
        return numOfSections
    }
    public func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    
}
