//
//  MF_SearchViewController.swift
//  DLevels
//
//  Created by SAIKAT GHOSH on 21/08/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class MF_SearchViewController: UIViewController {
    @IBOutlet weak var searchcontroller: UISearchBar!
    @IBOutlet weak var tblviewresult: UITableView!
    var tremvalstring = ""
    var response = [NSDictionary]()
    
    //MARK: VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchcontroller.delegate = self
        searchcontroller.becomeFirstResponder()
        searchcontroller.placeholder = "Search"
        
        tblviewresult.dataSource = self
        tblviewresult.delegate = self
        
        tblviewresult.estimatedRowHeight = 1000
        //  tblviewresult.rowHeight = UITableView.automaticDimension
        addDoneButtonOnKeyboard()
    }
    
    //MARK: VIEW DID APPEAR
    override func viewDidAppear(_ animated: Bool) {
        searchcontroller.becomeFirstResponder()
        
        tblviewresult.dataSource = self
        tblviewresult.delegate = self
        
        tblviewresult.estimatedRowHeight = 1000
        // tblviewresult.rowHeight = UITableView.automaticDimension
    }
    
    //MARK: ADD DONE BUTTON ON KEY BOARD
    func addDoneButtonOnKeyboard() {
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction)
        )
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        searchcontroller.inputAccessoryView = doneToolbar
    }
    
    //MARK: DONE BUTTON ACTION
    @objc func doneButtonAction() {
        searchcontroller.resignFirstResponder()
    }
    
    //MARK: SEARCH CLOSE ACTION
    @IBAction func searchClose_Action(_ sender: Any) {
        User.navigation.popViewController(animated: true)
        /*
         if let nav = self.navigationController {
         nav.popViewController(animated: true)
         } else {
         self.dismiss(animated: true, completion: nil)
         }*/
    }
    
    //MARK: Getting data
    func loadDataForResultView(searchval: String){
        var keyword_Param = ""
        keyword_Param = searchval.replacingOccurrences(of: " ", with: "%20").replacingOccurrences(of: "&", with: "%26")
        
        let paremeters = "keyword=\(keyword_Param)"
        
        NetworkHandler().handleRequest(url: Url.mfsearch , methodName: "GET", parameters: paremeters, isToken: true, showLoader: false) { (success, jsonData) in
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                if let tempArray = jsonData.value(forKey: "response") as? [NSDictionary] {
                    self.response = tempArray.isEmpty ? [NSDictionary]() : tempArray
                    self.tblviewresult.reloadData()
                }
            } else {
                AppManager.showAlertMessage(title: "errror", message: jsonData.value(forKey: "errmsg") as! String)}
        }
        
    }
    
}

extension MF_SearchViewController: UITableViewDataSource,UITableViewDelegate, UISearchBarDelegate , UINavigationControllerDelegate{
    
    //MARK: SEARCH TEXT DID CHANGE
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText == "" {
            self.response.removeAll()
            self.tblviewresult.reloadData()
        } else if searchText.count > 2 {
            loadDataForResultView(searchval: searchText)
        }
        
    }
    
    //MARK: TABLE VIEW CELL FOR ROW AT INDEX PATH
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier:"searchcell", for: indexPath)
        cell.selectionStyle = .none
        
        cell.backgroundColor = indexPath.row % 2 == 0 ?  UIColor(red: 215/255, green: 222/255, blue: 225/255, alpha: 1.0) : UIColor(red: 236/255, green: 239/255, blue: 240/255, alpha: 1.0)
        
        let lblfund = cell.viewWithTag(1001) as! UILabel
        let lblcategory = cell.viewWithTag(1002) as! UILabel
        
        if let fund = self.response[indexPath.row].value(forKey: "Fund") as? String {
            lblfund.text = fund
            
        }
        if let category = self.response[indexPath.row].value(forKey: "Category") as? String {
            lblcategory.text = category
        }
        
        
        return cell
    }
    
    //MARK: Table View Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return response.isEmpty ? 0 : response.count
    }
    
    //MARK: Orientati
    func navigationControllerSupportedInterfaceOrientations(_ navigationController: UINavigationController) -> UIInterfaceOrientationMask {
        return .portrait
    }
    
    //MARK: TABLE VIEW DID SELECT ROW AT INDEX PATH
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let viewController = StoryBoard.mf.instantiateViewController(withIdentifier: "MFSpecificViewController") as! MFSpecificViewController
        
        if let category = response[indexPath.row].value(forKey: "Category") as? String {
            viewController.Category = category
            Funddetails.Category = category
        }
        
        if let fund = response[indexPath.row].value(forKey: "Fund") as? String {
            viewController.Fund = fund
            Funddetails.Fund = fund
            Funddetails.FundName = fund
        }
        
        if let scmCode = response[indexPath.row].value(forKey: "reg_scm_code") as? String {
            viewController.scmCode = scmCode
            Funddetails.scmCode = scmCode
        }
        
        User.navigation.pushViewController(viewController, animated: false)
        
    }
    
    //MARK: TABLE VIEW ESTIMATED HEIGHT FOR ROW AT INDEX PATH
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    //MARK: TABLE VIEW HEIGHT FOR ROW AT INDEX PATH
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
        
    }
    
}
