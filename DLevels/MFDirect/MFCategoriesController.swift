//
//  MFCategoriesController.swift
//  DLevels
//
//  Created by SAIKAT GHOSH on 21/08/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit

//MARK: CATEGORIES DETAILS TABLE VIEW CELL
class CategoriesDetialsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var vwContainerView: UIView!
    @IBOutlet weak var lblFundCategories: UILabel!
    @IBOutlet weak var lblNoOfFunds: UILabel!
    @IBOutlet weak var lblNetAssets: UILabel!
    @IBOutlet weak var lblD: UILabel!
    @IBOutlet weak var lblR: UILabel!
    
}


class MFCategoriesController: UIViewController {
    
    
    @IBAction func Disclaimer(_ sender: Any) {
        
        let strybrd =  UIStoryboard.init(name: "Main", bundle: nil)
        let obj = strybrd.instantiateViewController(withIdentifier: "DisclaimerViewController") as! DisclaimerViewController
        User.navigation.pushViewController(obj, animated: true)
        obj.disclaimerUrl = DisclaimerUrl.mf_research
        obj.disclaimerHeader = "MF Research"
        
    }
    @IBOutlet weak var tblCategory: UITableView!
    
    @IBOutlet weak var disclaimerbtn: UIButton!
    @IBOutlet weak var svButtonsStackView: UIStackView!
    @IBOutlet weak var btn1W: UIButton!
    @IBOutlet weak var btn1M: UIButton!
    @IBOutlet weak var btn3M: UIButton!
    @IBOutlet weak var btn6M: UIButton!
    @IBOutlet weak var btn1Y: UIButton!
    @IBOutlet weak var btn3Y: UIButton!
    @IBOutlet weak var btn5Y: UIButton!
    
    @IBOutlet weak var lblReturn: UILabel!
    @IBOutlet weak var lblNetAssets: UILabel!
    
    @IBOutlet weak var btnFundCategories: UIButton!
    @IBOutlet weak var btnNoOfSchemes: UIButton!
    @IBOutlet weak var btnNetAssets: UIButton!
    @IBOutlet weak var btnReturn: UIButton!
    @IBOutlet weak var btnSortByD: UIButton!
    @IBOutlet weak var btnSortByR: UIButton!
    
    var isFilter        =   false
    
    var type            =   ""
    var category        =   ""
    var catTitle        =   ""
    
    var isFilteredArray =   false
    
    
    var duration    =   "1Y"
    var returnValue =   "A"
    var assetsValue =   "Net\nAssets(Cr)"
    
    var isSortByFund = false, isSortByScheme = false, isSortByD = false, isSortByR = false
    
    fileprivate var dataArray   = [MF_FundsTypeList]()
    fileprivate var filterArray = [MF_FundsTypeList]()
    
    //MARK: VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        // Do any additional setup after loading the view.
        prepareScreen()
        setButtonActions()
    }
    
    //MARK: PREPARE SCREEN
    func prepareScreen(){
        getCategoryDetails {
            
            self.tblCategory.dataSource = self
            self.tblCategory.delegate = self
            self.sortData()
            self.tblCategory.reloadData()
        }
    }
    
    //MARK: SET BUTTON ACTIONS
    private func setButtonActions(){
        
        btnFundCategories.addTarget(self, action: #selector(sortByFundCategory(sender:)), for: .touchUpInside)
        btnNoOfSchemes.addTarget(self, action: #selector(sortByScheme(sender:)), for: .touchUpInside)
        btnNetAssets.addTarget(self, action: #selector(btnAssets_Action(sender:)), for: .touchUpInside)
        btnReturn.addTarget(self, action: #selector(btnReturn_Action(sender:)), for: .touchUpInside)
        btnSortByD.addTarget(self, action: #selector(btnSortByD(sender:)), for: .touchUpInside)
        btnSortByR.addTarget(self, action: #selector(btnSortByR(sender:)), for: .touchUpInside)
        
        //SETTING STACK VIEW BUTTONS
        for case let button as UIButton in svButtonsStackView.subviews {
            button.layer.cornerRadius =  4.0
            button.layer.borderWidth  =  2.0
            button.layer.borderColor  =  button.titleLabel?.text == duration ?  UIColor(hex: "F7BB1A").cgColor : UIColor.clear.cgColor
            button.addTarget(self, action: #selector(setDuration(sender:)), for: .touchUpInside)
        }
    }
    
    //MARK: GET FUNC CATEGORIES DATA
    func getCategoryDetails(completion: @escaping()->Void){
        
        let obj_MFFundTypeListDM = MF_FundTypeListDataModel()
        obj_MFFundTypeListDM.getData( Type: type, paramCategory: category, completion:{ (dataset) in
            
            self.dataArray = dataset
            //self.filterArray = self.dataArray.sorted (by: {$0.oneY_AVG_D > $1.oneY_AVG_D})
            self.filterArray = self.dataArray
            completion()
        })
    }
    
    //MARK: MOVE TO TOP
    func moveToTop(){
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: 0 , section: 0)
            self.tblCategory.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }
    
}

//MARK: TABLE VIEW DELEGATE AND DATA SOURCE
extension MFCategoriesController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
   /*func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }*/
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterArray.count
    }
    
    //MARK: CELL FOR ROW AT INDEX PATH
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let detailsCell = tableView.dequeueReusableCell(withIdentifier: "CategoriesDetialsTableViewCell", for: indexPath) as! CategoriesDetialsTableViewCell
        
        detailsCell.selectionStyle = .none
        //detailsCell.vwContainerView.backgroundColor = UIColor(hex: AppManager.getAlternetColor(alternetColor: indexPath.row % 2 == 0))
        
        detailsCell.vwContainerView.backgroundColor = indexPath.row % 2 == 0 ? UIColor(red: 236/255, green: 239/255, blue: 240/255, alpha: 1.0) : UIColor(red: 215/255, green: 222/255, blue: 225/255, alpha: 1.0)
        
        let categoryText = filterArray[indexPath.row].FundType
        let attributedString = NSMutableAttributedString.init(string: categoryText)
        attributedString.addAttribute(NSUnderlineStyleAttributeName, value: 1, range: NSRange.init(location: 0, length: attributedString.length))
        detailsCell.lblFundCategories.attributedText = attributedString
        detailsCell.lblFundCategories.sizeToFit()
        
        detailsCell.lblNoOfFunds.text      = "\(filterArray[indexPath.row].Schemes)"
        detailsCell.lblNetAssets.text      = "\(filterArray[indexPath.row].NetAssetsInCr)"
        
        //IF AVERATE RETURN
        if returnValue == "A" {
            detailsCell.lblD.text = duration == "1W" ?  AppManager.setPositiveValue(value: filterArray[indexPath.row].oneW_AVG_D)
                : duration == "1M" ? AppManager.setPositiveValue(value:filterArray[indexPath.row].oneM_AVG_D)
                : duration == "1Y" ? AppManager.setPositiveValue(value:filterArray[indexPath.row].oneY_AVG_D)
                : duration == "3M" ? AppManager.setPositiveValue(value:filterArray[indexPath.row].threeM_AVG_D)
                : duration == "6M" ? AppManager.setPositiveValue(value:filterArray[indexPath.row].sixM_AVG_D)
                : duration == "3Y" ? AppManager.setPositiveValue(value:filterArray[indexPath.row].threeY_AVG_D)
                : duration == "5Y" ? AppManager.setPositiveValue(value:filterArray[indexPath.row].fiveY_AVG_D)
                : AppManager.setPositiveValue(value:filterArray[indexPath.row].oneY_AVG_D)
            
            detailsCell.lblR.text = duration == "1W" ?  AppManager.setPositiveValue(value: filterArray[indexPath.row].oneW_AVG)
                : duration == "1M" ? AppManager.setPositiveValue(value:filterArray[indexPath.row].oneM_AVG)
                : duration == "1Y" ? AppManager.setPositiveValue(value:filterArray[indexPath.row].oneY_AVG)
                : duration == "3M" ? AppManager.setPositiveValue(value:filterArray[indexPath.row].threeM_AVG)
                : duration == "6M" ? AppManager.setPositiveValue(value:filterArray[indexPath.row].sixM_AVG)
                : duration == "3Y" ? AppManager.setPositiveValue(value:filterArray[indexPath.row].threeY_AVG)
                : duration == "5Y" ? AppManager.setPositiveValue(value:filterArray[indexPath.row].fiveY_AVG)
                : AppManager.setPositiveValue(value:filterArray[indexPath.row].oneY_AVG)
            
        }
            
            //IF MAX RETURN
        else if returnValue == "M" {
            detailsCell.lblD.text = duration == "1W" ?  AppManager.setPositiveValue(value: filterArray[indexPath.row].oneW_MAX)
                : duration == "1M" ? AppManager.setPositiveValue(value:filterArray[indexPath.row].oneM_MAX_D)
                : duration == "1Y" ? AppManager.setPositiveValue(value:filterArray[indexPath.row].oneY_MAX_D)
                : duration == "3M" ? AppManager.setPositiveValue(value:filterArray[indexPath.row].threeM_MAX_D)
                : duration == "3Y" ? AppManager.setPositiveValue(value:filterArray[indexPath.row].threeY_MAX_D)
                : duration == "5Y" ? AppManager.setPositiveValue(value:filterArray[indexPath.row].fiveY_MAX_D)
                : duration == "6M" ? AppManager.setPositiveValue(value:filterArray[indexPath.row].sixM_MAX_D)
                : AppManager.setPositiveValue(value:filterArray[indexPath.row].sixM_MAX_D)
            
            detailsCell.lblR.text = duration == "1W" ?  AppManager.setPositiveValue(value: filterArray[indexPath.row].oneW_MAX)
                : duration == "1M" ? AppManager.setPositiveValue(value:filterArray[indexPath.row].oneM_MAX)
                : duration == "3M" ? AppManager.setPositiveValue(value:filterArray[indexPath.row].threeM_MAX)
                : duration == "6M" ? AppManager.setPositiveValue(value:filterArray[indexPath.row].sixM_MAX)
                : duration == "1Y" ? AppManager.setPositiveValue(value:filterArray[indexPath.row].oneY_MAX)
                : duration == "3Y" ? AppManager.setPositiveValue(value:filterArray[indexPath.row].threeY_MAX)
                : duration == "5Y" ? AppManager.setPositiveValue(value:filterArray[indexPath.row].fiveY_MAX)
                : AppManager.setPositiveValue(value:filterArray[indexPath.row].sixM_MAX)
        }
        
        return detailsCell
    }
    
    
    //MARK: DID SELECT ROA AT INDEX PATH
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //MARK: CHECKING FOR LAST SECTION ONLY
        let fundWisePerformanceCont = StoryBoard.mf.instantiateViewController(withIdentifier: "MFFundwisePerformanceViewController") as! MFFundwisePerformanceViewController
        // Funddetails.FundName = self.n
        Funddetails.Category = self.category
        fundWisePerformanceCont.Category = self.category
        fundWisePerformanceCont.fname    = filterArray[indexPath.row].FundType
        fundWisePerformanceCont.FundType = self.type
        fundWisePerformanceCont.desc     = filterArray[indexPath.row].Description
        User.navigation.pushViewController(fundWisePerformanceCont, animated: true)
        
        
    }
    
    
    //MARK: SORT DATA
    func sortData(){
        isSortByD = !isSortByD
        
        //SORT BY AVERAGE RETURN
        if returnValue == "A" {
            
            if duration == "1W"{
                if isFilteredArray {
                    filterArray = isSortByD ? filterArray.sorted (by: {$0.oneW_AVG_D > $1.oneW_AVG_D}) : filterArray.reversed()
                } else {
                    dataArray = isSortByD ? dataArray.sorted (by: {$0.oneW_AVG_D > $1.oneW_AVG_D}) : dataArray.reversed()
                    self.filterArray = self.dataArray
                }
            }
                
            else if duration == "1M" {
                if isFilteredArray {
                    filterArray = isSortByD ? filterArray.sorted (by: {$0.oneM_AVG_D > $1.oneM_AVG_D}) : filterArray.reversed()
                } else {
                    dataArray = isSortByD ? dataArray.sorted (by: {$0.oneM_AVG_D > $1.oneM_AVG_D}) : dataArray.reversed()
                    self.filterArray = self.dataArray
                }
            }
                
            else if duration == "3M" {
                if isFilteredArray {
                    dataArray = isSortByD ? filterArray.sorted (by: {$0.threeM_AVG_D > $1.threeM_AVG_D}) : filterArray.reversed()
                    self.filterArray = self.dataArray
                } else {
                    dataArray = isSortByD ? dataArray.sorted (by: {$0.threeM_AVG_D > $1.threeM_AVG_D}) : dataArray.reversed()
                    self.filterArray = self.dataArray
                }
            }
                
            else if duration == "6M" {
                if isFilteredArray {
                    filterArray = isSortByD ? filterArray.sorted (by: {$0.sixM_AVG_D > $1.sixM_AVG_D}) : filterArray.reversed()
                } else {
                    dataArray = isSortByD ? dataArray.sorted (by: {$0.sixM_AVG_D > $1.sixM_AVG_D}) : dataArray.reversed()
                    self.filterArray = self.dataArray
                }
            }
                
            else if duration == "1Y" {
                if isFilteredArray {
                    filterArray = isSortByD ? filterArray.sorted (by: {$0.oneY_AVG_D > $1.oneY_AVG_D}) : filterArray.reversed()
                } else {
                    dataArray = isSortByD ? dataArray.sorted (by: {$0.oneY_AVG_D > $1.oneY_AVG_D}) : dataArray.reversed()
                    self.filterArray = self.dataArray
                }
            }
                
            else if duration == "3Y" {
                if isFilteredArray {
                    filterArray = isSortByD ? filterArray.sorted (by: {$0.threeY_AVG_D > $1.threeY_AVG_D}) : filterArray.reversed()
                    
                } else {
                    dataArray = isSortByD ? dataArray.sorted (by: {$0.threeY_AVG_D > $1.threeY_AVG_D}) : dataArray.reversed()
                    self.filterArray = self.dataArray
                }
            }
                
            else if duration == "5Y" {
                if isFilteredArray {
                    filterArray = isSortByD ? filterArray.sorted (by: {$0.fiveY_AVG_D > $1.fiveY_AVG_D}) : filterArray.reversed()
                } else {
                    dataArray = isSortByD ? dataArray.sorted (by: {$0.fiveY_AVG_D > $1.fiveY_AVG_D}) : dataArray.reversed()
                    self.filterArray = self.dataArray
                }
            }
        }
            
            
            
            //SORT BY MAX RETURN
        else if returnValue == "M" {
            if duration == "1W"{
                if isFilteredArray {
                    filterArray = isSortByD ? filterArray.sorted (by: {$0.oneW_MAX_D > $1.oneW_MAX_D}) : filterArray.reversed()
                } else {
                    dataArray = isSortByD ? dataArray.sorted (by: {$0.oneW_MAX_D > $1.oneW_MAX_D}) : dataArray.reversed()
                    self.filterArray = self.dataArray
                }
            }
                
            else if duration == "1M" {
                if isFilteredArray {
                    filterArray = isSortByD ? filterArray.sorted (by: {$0.oneM_MAX_D > $1.oneM_MAX_D}) : filterArray.reversed()
                } else {
                    dataArray = isSortByD ? dataArray.sorted (by: {$0.oneM_MAX_D > $1.oneM_MAX_D}) : dataArray.reversed()
                    self.filterArray = self.dataArray
                }
            }
                
            else if duration == "3M" {
                if isFilteredArray {
                    filterArray = isSortByD ? filterArray.sorted (by: {$0.threeM_MAX_D > $1.threeM_MAX_D}) : filterArray.reversed()
                } else {
                    dataArray = isSortByD ? dataArray.sorted (by: {$0.threeM_MAX_D > $1.threeM_MAX_D}) : dataArray.reversed()
                    self.filterArray = self.dataArray
                }
            }
                
            else if duration == "6M" {
                if isFilteredArray {
                    filterArray = isSortByD ? filterArray.sorted (by: {$0.sixM_MAX_D > $1.sixM_MAX_D}) : filterArray.reversed()
                } else {
                    dataArray = isSortByD ? dataArray.sorted (by: {$0.sixM_MAX_D > $1.sixM_MAX_D}) : dataArray.reversed()
                    self.filterArray = self.dataArray
                }
            }
                
            else if duration == "1Y" {
                if isFilteredArray {
                    filterArray = isSortByD ? filterArray.sorted (by: {$0.oneY_MAX_D > $1.oneY_MAX_D}) : filterArray.reversed()
                } else {
                    dataArray = isSortByD ? dataArray.sorted (by: {$0.oneY_MAX_D > $1.oneY_MAX_D}) : dataArray.reversed()
                    self.filterArray = self.dataArray
                }
            }
                
            else if duration == "3Y" {
                if isFilteredArray {
                    filterArray = isSortByD ? filterArray.sorted (by: {$0.threeY_MAX_D > $1.threeY_MAX_D}) : filterArray.reversed()
                    
                } else {
                    dataArray = isSortByD ? dataArray.sorted (by: {$0.threeY_MAX_D > $1.threeY_MAX_D}) : dataArray.reversed()
                    self.filterArray = self.dataArray
                }
            }
                
            else if duration == "5Y" {
                if isFilteredArray {
                    filterArray = isSortByD ? filterArray.sorted (by: {$0.fiveY_MAX_D > $1.fiveY_MAX_D}) : filterArray.reversed()
                } else {
                    dataArray = isSortByD ? dataArray.sorted (by: {$0.fiveY_MAX_D > $1.fiveY_MAX_D}) : dataArray.reversed()
                    self.filterArray = self.dataArray
                }
            }
            
        }
        
        tblCategory.reloadData()
        moveToTop()
    }
    
    
    //MARK: SET DURATION ACTION
    @objc func setDuration(sender: UIButton){
        if let title = sender.titleLabel?.text {
            duration  =  title
            isSortByD = false
            isSortByR = false
            for case let button as UIButton in svButtonsStackView.subviews {
                button.layer.cornerRadius =  4.0
                button.layer.borderWidth  =  2.0
                button.layer.borderColor  = button.titleLabel?.text == duration ?  UIColor(hex: "F7BB1A").cgColor : UIColor.clear.cgColor
            }
            
            sortData()
        }
    }
    
    
    //MARK: SORT BY FUNC CATEGORY
    @objc func sortByFundCategory(sender: UIButton) {
        isSortByFund = !isSortByFund
        
        isSortByScheme = false; isSortByD = false; isSortByR = false
        
        if isFilteredArray {
            self.filterArray =  isSortByFund ?  filterArray.sorted (by: {$0.FundType < $1.FundType}) : filterArray.reversed()
        } else {
            dataArray = isSortByFund ?  dataArray.sorted (by: {$0.FundType < $1.FundType}) : dataArray.reversed()
            self.filterArray = self.dataArray
        }
        
        
        tblCategory.reloadData()
        moveToTop()
    }
    
    //MARK: SORT BY FUNC CATEGORY
    @objc func sortByScheme(sender: UIButton) {
        isSortByScheme = !isSortByScheme
        
        isSortByFund = false; isSortByD = false; isSortByR = false
        
        if isFilteredArray {
            filterArray = isSortByScheme ? filterArray.sorted (by: {$0.Schemes > $1.Schemes}) : filterArray.reversed()
        } else {
            dataArray = isSortByScheme ? dataArray.sorted (by: {$0.Schemes > $1.Schemes}) : filterArray.reversed()
            self.filterArray = self.dataArray
        }
        
        tblCategory.reloadData()
        moveToTop()
    }
    
    //MARK: SORT BY D
    @objc func btnSortByD(sender: UIButton) {
        isSortByScheme = false; isSortByR = false; isSortByFund = false; isSortByD = true
        sortData()
    }
    
    //MARK: SORT BY R
    @objc func btnSortByR(sender: UIButton) {
        isSortByR = !isSortByR
        
        isSortByScheme = false; isSortByD = false; isSortByFund = false
        
        if returnValue == "A" {
            
            if duration == "1W" {
                if isFilteredArray {
                    filterArray = isSortByR ? filterArray.sorted (by: {$0.oneW_AVG > $1.oneW_AVG}) : filterArray.reversed()
                } else {
                    dataArray = isSortByR ? dataArray.sorted (by: {$0.oneW_AVG > $1.oneW_AVG}) : dataArray.reversed()
                    self.filterArray = dataArray
                }
            }
            else if duration == "1M" {
                
                if isFilteredArray {
                    filterArray = isSortByR ? filterArray.sorted (by: {$0.oneM_AVG > $1.oneM_AVG}) : filterArray.reversed()
                } else {
                    dataArray = isSortByR ? dataArray.sorted (by: {$0.oneM_AVG > $1.oneM_AVG}) : dataArray.reversed()
                    self.filterArray = dataArray
                }
                
                
            }
            else if duration == "3M" {
                if isFilteredArray {
                    filterArray = isSortByR ? filterArray.sorted (by: {$0.threeM_AVG > $1.threeM_AVG}) : filterArray.reversed()
                } else {
                    dataArray = isSortByR ? dataArray.sorted (by: {$0.threeM_AVG > $1.threeM_AVG}) : dataArray.reversed()
                    self.filterArray = dataArray
                }
            }
            else if duration == "6M" {
                if isFilteredArray {
                    filterArray = isSortByR ? filterArray.sorted (by: {$0.sixM_AVG > $1.sixM_AVG}) : filterArray.reversed()
                } else {
                    dataArray = isSortByR ? dataArray.sorted (by: {$0.sixM_AVG > $1.sixM_AVG}) : dataArray.reversed()
                    self.filterArray = dataArray
                }
            }
            else if duration == "1Y" {
                if isFilteredArray {
                    filterArray = isSortByR ? filterArray.sorted (by: {$0.oneY_AVG > $1.oneY_AVG}) : filterArray.reversed()
                } else {
                    dataArray = isSortByR ? dataArray.sorted (by: {$0.oneY_AVG > $1.oneY_AVG}) : dataArray.reversed()
                    self.filterArray = dataArray
                }
            }
            else if duration == "3Y" {
                if isFilteredArray {
                    filterArray = isSortByR ? filterArray.sorted (by: {$0.threeY_AVG > $1.threeY_AVG}) : filterArray.reversed()
                } else {
                    dataArray = isSortByR ? dataArray.sorted (by: {$0.threeY_AVG > $1.threeY_AVG}) : dataArray.reversed()
                    self.filterArray = dataArray
                }
            }
            else if duration == "5Y" {
                if isFilteredArray {
                    filterArray = isSortByR ? filterArray.sorted (by: {$0.fiveY_AVG > $1.fiveY_AVG}) : filterArray.reversed()
                } else {
                    dataArray = isSortByR ? dataArray.sorted (by: {$0.fiveY_AVG > $1.fiveY_AVG}) : dataArray.reversed()
                    self.filterArray = dataArray
                }
            }
        }
            
            //MARK: IF MAX RETURN TYPE(M)
        else {
            if duration == "1W" {
                if isFilteredArray {
                    filterArray = isSortByR ? filterArray.sorted (by: {$0.oneW_MAX > $1.oneW_MAX}) : filterArray.reversed()
                } else {
                    dataArray = isSortByR ? dataArray.sorted (by: {$0.oneW_MAX > $1.oneW_MAX}) : dataArray.reversed()
                    self.filterArray = dataArray
                }
            }
            else if duration == "1M" {
                if isFilteredArray {
                    filterArray = isSortByR ? filterArray.sorted (by: {$0.oneM_MAX > $1.oneM_MAX}) : filterArray.reversed()
                }else {
                    dataArray = isSortByR ? dataArray.sorted (by: {$0.oneM_MAX > $1.oneM_MAX}) : dataArray.reversed()
                    self.filterArray = dataArray
                }
            }
            else if duration == "3M" {
                if isFilteredArray {
                    filterArray = isSortByR ? filterArray.sorted (by: {$0.threeM_MAX > $1.threeM_MAX}) : filterArray.reversed()
                } else {
                    dataArray = isSortByR ? dataArray.sorted (by: {$0.threeM_MAX > $1.threeM_MAX}) : dataArray.reversed()
                    self.filterArray = dataArray
                }
            }
            else if duration == "6M" {
                if isFilteredArray {
                    filterArray = isSortByR ? filterArray.sorted (by: {$0.sixM_MAX > $1.sixM_MAX}) : filterArray.reversed()
                } else {
                    dataArray = isSortByR ? dataArray.sorted (by: {$0.sixM_MAX > $1.sixM_MAX}) : dataArray.reversed()
                    self.filterArray = dataArray
                }
            }
            else if duration == "1Y" {
                if isFilteredArray {
                    filterArray = isSortByR ? filterArray.sorted (by: {$0.oneY_MAX > $1.oneY_MAX}) : filterArray.reversed()
                } else {
                    dataArray = isSortByR ? dataArray.sorted (by: {$0.oneY_MAX > $1.oneY_MAX}) : dataArray.reversed()
                    self.filterArray = dataArray
                }
                
            }
            else if duration == "3Y" {
                if isFilteredArray {
                    filterArray = isSortByR ? filterArray.sorted (by: {$0.threeY_MAX > $1.threeY_MAX}) : filterArray.reversed()
                } else {
                    dataArray = isSortByR ? dataArray.sorted (by: {$0.threeY_MAX > $1.threeY_MAX}) : dataArray.reversed()
                    self.filterArray = dataArray
                }
            }
            else if duration == "5Y" {
                if isFilteredArray {
                    filterArray = isSortByR ? filterArray.sorted (by: {$0.fiveY_MAX > $1.fiveY_MAX}) : filterArray.reversed()
                } else {
                    dataArray = isSortByR ? dataArray.sorted (by: {$0.fiveY_MAX > $1.fiveY_MAX}) : dataArray.reversed()
                    self.filterArray = dataArray
                }
            }
        }
        
        tblCategory.reloadData()
        moveToTop()
    }
    
    //MARK: RETURN BUTTON ACTION
    @objc func btnReturn_Action(sender: UIButton) {
        let actionSheet = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        
        let avgReturn = UIAlertAction(title: "Average", style: .default) { (success) in
            self.returnValue        = "A"
            self.lblReturn.text     = "Average"
            self.isSortByD          = false
            self.sortData()
        }
        
        let maxReturn = UIAlertAction(title: "Maximum", style: .default) { (success) in
            self.returnValue        = "M"
            self.lblReturn.text     = "Maximum"
            self.isSortByD          = false
            self.sortData()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        actionSheet.addAction(avgReturn)
        actionSheet.addAction(maxReturn)
        actionSheet.addAction(cancelAction)
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    //MARK: RETURN BUTTON ACTION
    @objc func btnAssets_Action(sender: UIButton) {
        let actionSheet = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        
        isFilteredArray = true
        
        let netReturn = UIAlertAction(title: "Net Assets(Cr.)", style: .default) { (success) in
            self.assetsValue = "Net\nAssets(Cr)"
            self.lblNetAssets.text = "Net\nAssets(Cr)"
            self.filterArray = self.dataArray.filter { $0.NetAssetsInCr > 0}
            self.isSortByD   = false
            self.isSortByR   = false
            self.sortData()
        }
        
        let avgReturn = UIAlertAction(title: "Assets>1000 Cr.", style: .default) { (success) in
            self.assetsValue = "Assets>\n1000 Cr."
            self.lblNetAssets.text = "Assets>\n1000 Cr."
            self.filterArray = self.dataArray.filter { $0.NetAssetsInCr > 1000}
            self.isSortByD = false
            self.isSortByR   = false
            self.sortData()
            
        }
        
        let maxReturn = UIAlertAction(title: "Assets>5000 Cr.", style: .default) { (success) in
            self.assetsValue = "Assets>\n5000 Cr."
            self.lblNetAssets.text = "Assets>\n5000 Cr."
            self.filterArray = self.dataArray.filter { $0.NetAssetsInCr > 5000}
            self.isSortByD = false
            self.isSortByR   = false
            self.sortData()
        }
        
        let maxiReturn = UIAlertAction(title: "Assets>10000 Cr.", style: .default) { (success) in
            self.assetsValue = "Assets>\n10000 Cr"
            self.lblNetAssets.text = "Assets>\n10000 Cr"
            self.filterArray = self.dataArray.filter { $0.NetAssetsInCr > 10000}
            self.isSortByD = false
            self.isSortByR   = false
            self.sortData()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (success) in
            self.isFilteredArray = false
        }
        
        actionSheet.addAction(netReturn)
        actionSheet.addAction(avgReturn)
        actionSheet.addAction(maxReturn)
        actionSheet.addAction(maxiReturn)
        actionSheet.addAction(cancelAction)
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    
}


