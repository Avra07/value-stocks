//
//  NavDetailsViewController.swift
//  DLevels
//
//  Created by SAIKAT GHOSH on 21/09/18.
//  Copyright © 2018 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class NavDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {
    
    @IBOutlet weak var tblSlideMenuLlist: UITableView!
    @IBOutlet weak var headersector: UILabel!
    
    var dataPoints: [String] = []
    var indPerf: [Float] = []
    var Perf: [Float] = []
    
    var CLOSE_PRICE: [String] = []
    var smallCap: [String] = []
    
    var type = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if type == "SMALL" {
            headersector.text = "Dynamic SmallCap Index History"
        }
        if type == "MID" {
            headersector.text = "Dynamic MidCap Index History"
        }
        
        if type == "LARGE" {
            headersector.text = "Dynamic LargeCap Index History"
        }
        
        
        loadData()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnhamburgerclick(_ sender: Any) {
        
        //        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "MultibaggerSectorViewController") as! MultibaggerSectorViewController
        //        self.navigationController?.pushViewController(viewController, animated: false)
        self.navigationController?.popViewController(animated: false)
        
        
    }
    
    func loadData() {
        
        let obj = WebService()
        let paremeters = "index_type=\(type)&for="
        var result = ""
        
        //Mark: Call Service for Chart Data
        obj.callWebServices(url: Urls.getindexhistory, methodName: "GET", parameters: paremeters, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
            
            //print("Json Data is :  \(jsonData)")
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                DispatchQueue.main.async {
                    
                    let tempArray = jsonData.value(forKey: "response") as! NSArray
                    for dict in tempArray {
                        let dictValue = dict as! NSDictionary
                        
                        if let iperf:String = dictValue.value(forKey: "indPerf") as? String{
                            if let perf:String = dictValue.value(forKey: "perf") as? String{
                                result = dictValue.value(forKey: "DIS_DATE") as! String
                                
                                self.dataPoints.append(result)
                                self.indPerf.append((iperf as NSString).floatValue)
                                self.Perf.append((perf as NSString).floatValue)
                                
                                var clsprice = ""
                                clsprice = dictValue.value(forKey: "CLOSE_PRICE") as! String
                                self.CLOSE_PRICE.append(clsprice)
                                
                                let smallCapPrice = dictValue.value(forKey: "smallCap") as! String
                                self.smallCap.append(smallCapPrice)
                                
                            }
                            
                        }
                    }
                    self.tblSlideMenuLlist.reloadData()
                }
                
                
                
            }
        }
        
    }
    
    //MARK: Table View Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return dataPoints.count
        
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
       
        
        
        
    }
    
    
    //MARK: Table View Delegate
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier:"tickercell", for: indexPath)
        cell.selectionStyle = .none
        
        DispatchQueue.main.async {
            
            cell.backgroundColor = indexPath.row % 2 == 0 ?  UIColor(red: 215/255, green: 222/255, blue: 225/255, alpha: 1.0) : UIColor(red: 236/255, green: 239/255, blue: 240/255, alpha: 1.0)
            
            cell.selectionStyle = .none
            
            cell.backgroundColor = indexPath.row % 2 == 0 ?  UIColor(red: 215/255, green: 222/255, blue: 225/255, alpha: 1.0) : UIColor(red: 236/255, green: 239/255, blue: 240/255, alpha: 1.0)
            
            let lblDate = cell.viewWithTag(2002) as! UILabel
            lblDate.text = self.dataPoints[indexPath.row]
            
            
            let lblNav = cell.viewWithTag(2004) as! UILabel
            lblNav.text = self.CLOSE_PRICE[indexPath.row]
            
        }
        
        return cell
    }

}
