//
//  mfdropdownTableViewCell.swift
//  DLevels
//
//  Created by Shailesh Saraf on 05/09/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class mfdropdownTableViewCell: UITableViewCell,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var tblvw: UITableView!
    @IBOutlet weak var lblTitle:UILabel!
    
    @IBOutlet weak var lblColName: UILabel!
    @IBOutlet weak var lblSortName: UILabel!
    
    var mfdataSource = [String]()
    var mfarrSelected = [String]()
    var mf_title = ""
    var mf_group_id = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        mfarrSelected.removeAll()
        if mfselectedFilter.mffilterParameter.count > 0 {
            for fParam in mfselectedFilter.mffilterParameter {
                if fParam.filter_column_name == lblColName.text {
                    if fParam.filter_value != "" {
                        mfarrSelected = fParam.filter_value.split(separator: ";").map { String($0) }
                    }
                }
            }
        }
        
        self.tblvw.dataSource = self
        self.tblvw.delegate = self
        
        self.tblvw.reloadData()
        
        lblTitle.text = mf_title
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
        
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        //set the values for top,left,bottom,right margins
        let margins = UIEdgeInsets(top: 0, left: 0, bottom: 5, right: 0)
        contentView.frame = UIEdgeInsetsInsetRect(contentView.frame, margins)
    }
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mfdataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let data = mfdataSource[indexPath.row]
        
        
        let cell = Bundle.main.loadNibNamed("mfdropdownCell", owner: nil, options: nil)?[0] as! UITableViewCell
        cell.selectionStyle = .none
        
        let title = data
        let lblName = cell.viewWithTag(2002) as! UILabel
        let imgChkBox = cell.viewWithTag(1002) as! UIImageView
        
        
        lblName.text = title
        
       
        
        if mfarrSelected.count > 0 {
            if mfarrSelected.contains(title) {
                imgChkBox.image = UIImage(named: "CheckboxOn")
            } else {
                imgChkBox.image = UIImage(named: "CheckboxOff")
            }
        }else {
            imgChkBox.image = UIImage(named: "CheckboxOff")
        }
        
        
        
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // getting the current cell from the index path
        let currentCell = tableView.cellForRow(at: indexPath) as! UITableViewCell
        var isRemove:Bool = false
        let lblTitle = currentCell.viewWithTag(2002) as! UILabel
        let imgChkBox = currentCell.viewWithTag(1002) as! UIImageView
        // getting the text of that cell
        let currentItem = lblTitle.text
        
        let filterParam = mfFilterResult()
        filterParam.filter_column_name = lblColName.text!
        filterParam.filter_short_name  = lblSortName.text!
        filterParam.filter_type = "dropdown"
        filterParam.filter_group_id = mf_group_id
        
        if let index = mfarrSelected.index(of: currentItem!) {
            mfarrSelected.remove(at: index)
            imgChkBox.image = UIImage(named: "CheckboxOff")
            currentCell.isSelected = true
        }else {
            mfarrSelected.append(currentItem!)
            imgChkBox.image = UIImage(named: "CheckboxOn")
            currentCell.isSelected = false
        }
        
        
        var fValue = ""
        for strSelected in mfarrSelected {
            fValue.append(strSelected)
            fValue.append(",")
        }
        
        if fValue != "" {
            fValue.removeLast()
        }
        
        
        
        if mfselectedFilter.mffilterParameter.count > 0 {
            for fParam in mfselectedFilter.mffilterParameter {
                if fParam.filter_column_name == filterParam.filter_column_name {
                    if fValue == "" {
                        if let indx = mfselectedFilter.mffilterParameter.index(of: fParam) {
                            mfselectedFilter.mffilterParameter.remove(at: indx)
                            
                            for objGroup in mfselectedFilter.mffilterGroupDataSource {
                                if objGroup.group_id == mf_group_id {
                                    objGroup.filter_count = objGroup.filter_count - 1
                                }
                            }
                            
                            
                        }
                    }else{
                        fParam.filter_value = fValue
                        //selectedFilter.filterParameter.append(filterParam)
                    }
                    
                    isRemove = true
                }
            }
        }
        
        if isRemove == false {
            filterParam.filter_value = fValue
            mfselectedFilter.mffilterParameter.append(filterParam)
            
            for objGroup in mfselectedFilter.mffilterGroupDataSource {
                if objGroup.group_id == mf_group_id {
                    objGroup.filter_count = objGroup.filter_count + 1
                }
            }
        }
        
        //print(mfselectedFilter.mffilterParameter)
    }
    
}
