//
//  mftextFilterTableViewCell.swift
//  DLevels
//
//  Created by Shailesh Saraf on 05/09/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class mftextFilterTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtValue: UITextField!
    
    @IBOutlet weak var lblColName: UILabel!
    @IBOutlet weak var lblSortName: UILabel!
    
    var toolbar:UIToolbar = UIToolbar()
    var group_id = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupToHideKeyboardOnTapOnView()
        
        txtValue.delegate = self
        
        toolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.contentView.frame.size.width, height: 35))
        toolbar.barTintColor = #colorLiteral(red: 0.7812563181, green: 0.8036255836, blue: 0.8297665119, alpha: 1)
        
        
        //create left side empty space so that done button set on right side
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        
        
        txtValue.inputAccessoryView = toolbar
        
        
        if mfselectedFilter.mffilterParameter.count > 0 {
            for fParam in mfselectedFilter.mffilterParameter {
                if fParam.filter_column_name == lblColName.text {
                    if fParam.filter_value != "" {
                        txtValue.text = fParam.filter_value
                    }
                }
            }
        }
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    func AddUpdateTextTypeFilterParam(){
        
        let filterParam = mfFilterResult()
        filterParam.filter_column_name = lblColName.text!
        filterParam.filter_short_name  = lblSortName.text!
        filterParam.filter_group_id = group_id
        filterParam.filter_type = "text"
        var isRemoval:Bool = false
        
        if mfselectedFilter.mffilterParameter.count > 0 {
            for fParam in mfselectedFilter.mffilterParameter {
                if fParam.filter_column_name == filterParam.filter_column_name {
                    fParam.filter_value = txtValue.text!
                    isRemoval = true
                    
                    for objGroup in mfselectedFilter.mffilterGroupDataSource {
                        if objGroup.group_id == group_id {
                            objGroup.filter_count = objGroup.filter_count - 1
                        }
                    }
                }
            }
        }
        
        if isRemoval == false {
            filterParam.filter_value = txtValue.text!
            mfselectedFilter.mffilterParameter.append(filterParam)
            for objGroup in mfselectedFilter.mffilterGroupDataSource {
                if objGroup.group_id == group_id {
                    objGroup.filter_count = objGroup.filter_count + 1
                }
            }
            
        }
        
    }
    
}

extension mftextFilterTableViewCell : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        print("TextField did end editing method called")
        // save the text in the map using the stored row in the tag field
        
        AddUpdateTextTypeFilterParam()
        
    }
}

extension mftextFilterTableViewCell
{
    func doneButtonAction()
    {
        self.endEditing(true)
    }
    
    func setupToHideKeyboardOnTapOnView()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(dismissKeyboard))
        
        tap.cancelsTouchesInView = false
        self.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard()
    {
        self.endEditing(true)
    }
}
