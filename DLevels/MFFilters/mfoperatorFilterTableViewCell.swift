//
//  mfoperatorFilterTableViewCell.swift
//  DLevels
//
//  Created by Shailesh Saraf on 05/09/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit
import MaterialComponents

class mfoperatorFilterTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnoperater: UIButton!
    @IBOutlet weak var txtvalue: UITextField!
    @IBOutlet weak var txtmin: UITextField!
    @IBOutlet weak var txtmax: UITextField!
    @IBOutlet weak var lblColName: UILabel!
    @IBOutlet weak var lblSortName: UILabel!
    @IBOutlet weak var vwCard: MDCCard!
    
    @IBOutlet weak var singleviewvw: UIView!
    @IBOutlet weak var minmaxvw: UIView!
    
    
    
    var group_id = ""
    
    var toolbar:UIToolbar = UIToolbar()
    var txtview = UITextField()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        txtvalue.delegate = self
        txtmin.delegate = self
        txtmax.delegate = self
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardDidHide, object: nil)
        
        
        toolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.contentView.frame.size.width, height: 35))
        toolbar.barTintColor = #colorLiteral(red: 0.7812563181, green: 0.8036255836, blue: 0.8297665119, alpha: 1)
        
        
        //create left side empty space so that done button set on right side
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        
        
        let minusButton = UIButton()
        minusButton.frame = CGRect(x:-5, y:0, width:120, height:40)
        minusButton.setTitle("-", for: .normal)
        minusButton.setTitle("-", for: .highlighted)
        
        minusButton.titleLabel?.textColor = UIColor(hex: "000000")
        
        minusButton.backgroundColor = UIColor.gray
        minusButton.layer.cornerRadius = 4.0
        minusButton.addTarget(self, action: #selector(toggleMinus), for: .touchUpInside)
        
        let leftBarButton = UIBarButtonItem(customView: minusButton)
        
        
        
        toolbar.setItems([leftBarButton,flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        
        txtvalue.inputAccessoryView = toolbar
        txtmax.inputAccessoryView = toolbar
        txtmin.inputAccessoryView = toolbar
        
        txtvalue.text = ""
        
        
        
        if mfselectedFilter.mffilterParameter.count > 0 {
            for fParam in mfselectedFilter.mffilterParameter {
                if fParam.filter_column_name == lblColName.text {
                    if fParam.filter_operator == "greater than"{
                        txtvalue.text = fParam.filter_value
                    }
                    if fParam.filter_operator == "less than"{
                        txtvalue.text = fParam.filter_value
                    }
                    if fParam.filter_operator == "equals"{
                        txtvalue.text = fParam.filter_value
                    }
                    if fParam.filter_operator == "between"{
                        txtvalue.text = ""
                        txtmin.text = fParam.filter_min
                        txtmax.text = fParam.filter_max
                    }
                }
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        //set the values for top,left,bottom,right margins
        let margins = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
        contentView.frame = UIEdgeInsetsInsetRect(contentView.frame, margins)
    }
    
    
    // MARK: Keyboard Notifications
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardHeight = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
            
            if let viewcontroller = self.superview as? UITableView {
                viewcontroller.contentInset = UIEdgeInsetsMake(0, 0, keyboardHeight, 0)
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: 0.2, animations: {
            // For some reason adding inset in keyboardWillShow is animated by itself but removing is not, that's why we have to use animateWithDuration here
            if let viewcontroller = self.superview as? UITableView {
                viewcontroller.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
            }
        })
    }
    
    func doneButtonAction() {
        AddUpdateFilterParam()
        endEditing(true)
    }
    
    func toggleMinus(){
        
        // Get text from text field
        if var text = txtview.text {
            if text.isEmpty == false{
                
                // Toggle
                if text.hasPrefix("-") {
                    text = text.replacingOccurrences(of: "-", with: "")
                }
                else
                {
                    text = "-\(text)"
                }
                
                // Set text in text field
                txtview.text = text
            }
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    func AddUpdateFilterParam(){
        
        let filterParam = mfFilterResult()
        filterParam.filter_column_name = lblColName.text!
        filterParam.filter_short_name  = lblSortName.text!
        filterParam.filter_type = ""
        filterParam.filter_group_id = group_id
        var isRemoval:Bool = false
        
        // print(txtvalue.text)
        
        var isCounterAdd = true
        
        if mfselectedFilter.mffilterParameter.count > 0 {
            for fParam in mfselectedFilter.mffilterParameter {
                if fParam.filter_column_name == filterParam.filter_column_name {
                    
                    if btnoperater.titleLabel?.text == "greater than"{
                        fParam.filter_value = txtvalue.text!
                        fParam.filter_operator = (btnoperater.titleLabel?.text)!
                        
                        if fParam.filter_value == "" {
                            isCounterAdd = false
                        }
                        
                    }
                    if btnoperater.titleLabel?.text == "less than"{
                        fParam.filter_value = txtvalue.text!
                        fParam.filter_operator = (btnoperater.titleLabel?.text)!
                        if fParam.filter_value == "" {
                            isCounterAdd = false
                        }
                    }
                    if btnoperater.titleLabel?.text == "equals"{
                        fParam.filter_value = txtvalue.text!
                        fParam.filter_operator = (btnoperater.titleLabel?.text)!
                        if fParam.filter_value == "" {
                            isCounterAdd = false
                        }
                    }
                    if btnoperater.titleLabel?.text == "between"{
                        fParam.filter_max = txtmax.text!
                        fParam.filter_min = txtmin.text!
                        fParam.filter_operator = (btnoperater.titleLabel?.text)!
                        
                        if fParam.filter_max == "" || fParam.filter_min == "" {
                            isCounterAdd = false
                        }
                        
                    }
                    
                    if isCounterAdd == false {
                        for objGroup in mfselectedFilter.mffilterGroupDataSource {
                            if objGroup.group_id == group_id {
                                objGroup.filter_count = objGroup.filter_count - 1
                            }
                        }
                    }
                    
                    
                    isRemoval = true
                    
                }
            }
        }
        
        if isRemoval == false {
            
            if btnoperater.titleLabel?.text == "greater than"{
                filterParam.filter_value = txtvalue.text!
                filterParam.filter_operator = (btnoperater.titleLabel?.text)!
            }
            if btnoperater.titleLabel?.text == "less than"{
                filterParam.filter_value = txtvalue.text!
                filterParam.filter_operator = (btnoperater.titleLabel?.text)!
            }
            if btnoperater.titleLabel?.text == "equals"{
                filterParam.filter_value = txtvalue.text!
                filterParam.filter_operator = (btnoperater.titleLabel?.text)!
            }
            if btnoperater.titleLabel?.text == "between"{
                filterParam.filter_max = txtmax.text!
                filterParam.filter_min = txtmin.text!
                filterParam.filter_operator = (btnoperater.titleLabel?.text)!
            }
            
            mfselectedFilter.mffilterParameter.append(filterParam)
            
            
            for objGroup in mfselectedFilter.mffilterGroupDataSource {
                if objGroup.group_id == group_id {
                    objGroup.filter_count = objGroup.filter_count + 1
                }
            }
            
            
            
            
        }
        
    }
    
    
    @IBAction func operatorChangeClick(_ sender: Any){
        
        let actionSheet = MDCActionSheetController(title: "", message: "")
        
        let dataSource: [String] = ["greater than", "less than", "equals", "between"]
        
        for data in dataSource {
            
            let newaction = MDCActionSheetAction(title: data, image: nil, handler: { (success) in
                self.btnoperater.setTitle(data, for: .normal)
                
                if data == "between" {
                    self.minmaxvw.isHidden = false
                    self.singleviewvw.isHidden = true
                }else {
                    self.minmaxvw.isHidden = true
                    self.singleviewvw.isHidden = false
                }
            })
            
            actionSheet.addAction(newaction)
        }
        
        
        User.navigation.present(actionSheet, animated: true) {
            
        }
    }
    
}

extension mfoperatorFilterTableViewCell : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        txtview = textField
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        print("TextField did end editing method called")
        // save the text in the map using the stored row in the tag field
        
        AddUpdateFilterParam()
        
    }
}


