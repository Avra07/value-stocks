//
//  SectorWiseView.swift
//  DLevels
//
//  Created by Dynamic-Mac-02 on 21/02/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import Foundation
import UIKit

class SectorWiseView: UIView, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var vwContenerView: UIView!
    @IBOutlet weak var tblSectors: UITableView!
    
    var nibView: UIView!
    var sectorListArray = [NSDictionary]()
    
    let arr = ["hello","Thre","How","Are", "You"]
    
    func  prepareScreen(viewSize: UIView) {
     
        nibView = Bundle.main.loadNibNamed("SectorWiseView", owner: self, options: nil)?[0] as! UIView
        nibView.tag = 100
        var tempRect = nibView.frame
        print(tempRect)
        
        tickerlist()
        
        tempRect =  CGRect(x: -300, y: 20, width: tempRect.width, height: 400)
        viewSize.addSubview(nibView)
        
        UIView.transition(with: self.nibView, duration: 0.4, options: [.curveEaseIn], animations: {
            
            tempRect.origin.x +=  305
            self.nibView.frame = tempRect
            
        }) { (success) in
            
        }
    }
    
    func tickerlist()
    {
        let obj = WebService()
        let type_param = "all"
        
        let token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL3dzLmRsZXZlbHMuY29tL2xvZ2luIiwiaWF0IjoxNDg3OTQzMjExLCJuYmYiOjE0ODc5NDMyMTEsImp0aSI6Im9HNmYxbWVTVE9wUTEzdlQiLCJzdWIiOjE5Njc5MywieHBhc3MiOiIkUCRCWDZvckg3TG1LbzhPaTNLV1d2Yjd0aGRZZVhXWW4vIn0.dUrdJxhf0uKCAEvRDeVh7aoUMlbC9DQcpwRKVf275pM"
        
        obj.callWebServices(url: Urls.sectorwise_Performence, methodName: "GET", parameters: "type=\(type_param)", istoken: true, tokenval: token) { (returnValue, jsonData) in
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                print("Selected Value From Array is : \(SearchForMultibaggerSector.serchValue)")
                self.sectorListArray = jsonData.value(forKey: "response") as! [NSDictionary]
                
                DispatchQueue.main.async {
                    self.tblSectors.reloadData()
                }
                
                /* let sectionIndex = IndexSet(integer: 0)
                 tblSectors.reloadSections(sectionIndex, with: .automatic)
 */
            }
            else
            {
                let alertobj = AppManager()
                
                print(jsonData.value(forKey: "errmsg") as! String)
            //    alertobj.showAlert(title: "Error!", message: jsonData.value(forKey: "errmsg") as! String , navigationController: self.navigationController!)
                
            }
        }
    }

    //MARK: Table View Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        print(self.sectorListArray.count)
        return arr.count //sectorListArray.count
    }
    
    


    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = Bundle.main.loadNibNamed("SectorWiseView", owner: nil, options: nil)?[1] as! UITableViewCell
        
        cell.selectionStyle = .none
        
        let lblName = cell.viewWithTag(2000) as! UILabel
        lblName.text = "Test"//nameArray[indexPath.row]
        
        let lblLtp = cell.viewWithTag(2001) as! UILabel
        lblLtp.text = "Test"//nameArray[indexPath.row]
        
        let lblPRatio = cell.viewWithTag(2002) as! UILabel
        lblPRatio.text = "Test"//nameArray[indexPath.row]
        
        let lbl3Mchng = cell.viewWithTag(2003) as! UILabel
        lbl3Mchng.text = "Test"//nameArray[indexPath.row]
        
        return cell
    }
    
    

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let cell = Bundle.main.loadNibNamed("SectorWiseView", owner: nil, options: nil)?[1] as! UITableViewCell
        
        
        
    }
    
    @IBAction func onclickBackButton(_ sender: UIButton) {
        
        nibView.removeFromSuperview()
    }
 
    /*
     
     //MARK: Table View Delegate
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
     let cell = Bundle.main.loadNibNamed("SectorWiseView", owner: nil, options: nil)?[1] as! UITableViewCell
     
     cell.selectionStyle = .none
     
     let lblName = cell.viewWithTag(2000) as! UILabel
     lblName.text = "Test"//nameArray[indexPath.row]
     
     let lblLtp = cell.viewWithTag(2001) as! UILabel
     lblLtp.text = "Test"//nameArray[indexPath.row]
     
     let lblPRatio = cell.viewWithTag(2002) as! UILabel
     lblPRatio.text = "Test"//nameArray[indexPath.row]
     
     let lbl3Mchng = cell.viewWithTag(2003) as! UILabel
     lbl3Mchng.text = "Test"//nameArray[indexPath.row]
     
     return cell
     }
     

    */
    
    /*
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 10 // nameArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // let cell
        
        let cell = Bundle.main.loadNibNamed("SectorWiseView", owner: nil, options: nil)?[1] as! UITableViewCell
        cell.selectionStyle = .none
        
        let lblName = cell.viewWithTag(2000) as! UILabel
        lblName.text = "Test"//nameArray[indexPath.row]
        
        let lblLtp = cell.viewWithTag(2001) as! UILabel
        lblLtp.text = "Test"//nameArray[indexPath.row]
        
        let lblPRatio = cell.viewWithTag(2002) as! UILabel
        lblPRatio.text = "Test"//nameArray[indexPath.row]
        
        let lbl3Mchng = cell.viewWithTag(2003) as! UILabel
        lbl3Mchng.text = "Test"//nameArray[indexPath.row]
        
      
        return cell
    } */
    
    
    func hideSlideMenu(navigationController: UINavigationController,viewSize: UIView) {
        
        UIView.animate(withDuration: 0.4) {
            for views in viewSize.subviews {
                if views.tag == 100 {
                    
                    navigationController.navigationBar.frame.origin.x = 0
                    
                    self.nibView.frame.origin.x = -self.nibView!.frame.width
                    AppManager().setStatusBarBackgroundColor()
                    
                }
            }
        }
    }
}
