//
//  LeftSlideMenu.swift
//  DLevels
//
//  Created by Dynamic-Mac-02 on 02/02/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import Foundation
import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import FirebaseInstanceID
import GoogleMobileAds




class LeftSlideMenu: UIView, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var right_arrow: UIImageView!
    @IBOutlet weak var dottedlower: UIImageView!
    @IBOutlet weak var dottedUpper: UIImageView!
    @IBOutlet weak var lbl_version: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserEmail: UILabel!
    
    @IBAction func log_inbtn(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        navController?.pushViewController(viewController, animated: true)
        
        
    }
    @IBOutlet weak var login: UIButton!
    @IBAction func myaccount(_ sender: Any) {
      UIApplication.shared.openURL(NSURL(string: "https://www.dynamiclevels.com/en/vs-my-account/\(User.token)")! as URL)
        
    }
    @IBOutlet weak var tblSlideMenuLlist: UITableView!
    
    @IBOutlet weak var accountmy: UIButton!
    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var tblheight: NSLayoutConstraint!
    
    
    var navController: UINavigationController?
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    
    var interstitial: GADInterstitial!
    
    
    var nameArray = ["Dynamic Indices", "Stock Market Today","Invest in Our PMS","Webinars"/*,"My Rewards","Refer & Earn"*/,"Disclaimer","Sponsored Code","Contact Us","Log Out"]
    
    var imageName = ["SmallcapIndex","Dashboard","PMSHamburger", "Webinars"/*,"refer","reward"*/,"Disclaimer","sponser_humberger_icon","ContactUs","LogOut"]
    
    var nibView: UIView!
    

    
    
    func prepareScreenWithView(navigationController: UINavigationController, viewSize: UIView) {
        
        
        nibView = Bundle.main.loadNibNamed("LeftSlideMenu", owner: self, options: nil)?[0] as! UIView
    
        navController = navigationController
        
        guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
        statusBar.backgroundColor = UIColor.clear
        
        nibView.tag = 100
        var tempRect = nibView.frame
        //print(tempRect)
        
        
        
        if Subscription_Data.status {
          accountmy.isHidden = false
          dottedUpper.isHidden = false
          dottedlower.isHidden = false
          right_arrow.isHidden = false
        }else {
          accountmy.isHidden = true
          dottedUpper.isHidden = true
          dottedlower.isHidden = false
          right_arrow.isHidden = true
        
        }
        
  
        
        
        if User.email == "guest@yopmail.com" {
            lblUserName.isHidden = true
            lblUserEmail.isHidden = true
            login.isHidden = false
            nameArray[7] = ""
            imageName[7] = ""
            nameArray[5] = nameArray[6]
            imageName[5] = imageName[6]
            nameArray[6] = ""
            imageName[6] = ""
        }else{
            lblUserName.isHidden = false
            lblUserEmail.isHidden = false
            login.isHidden = true
            lblUserName.text = "\(User.firstName)  \(User.lastName)"
            lblUserEmail.text = User.email
        }
        
        if User.firstName == ""{
            
        }else{
            
        }
        
        
        
        
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        print(version)
        lbl_version.text = "ver \(version)"
        
        
        tempRect =  CGRect(x: -250, y: 0, width: tempRect.width, height: UIScreen.main.bounds.height)
        viewSize.addSubview(self.nibView)
        
        self.tblSlideMenuLlist.tableHeaderView = nil;
        self.tblSlideMenuLlist.tableFooterView = nil;
        
        //self.tblheight.constant = CGFloat(self.nameArray.count * 45)
        
        
        let sectionIndex = IndexSet(integer: 0)
        tblSlideMenuLlist.reloadSections(sectionIndex, with: .top)
        
        UIView.transition(with: self.nibView, duration: 0.4, options: [.curveEaseIn], animations: {
            
            tempRect.origin.x +=  250
            self.nibView.frame = tempRect
            
        }) { (success) in
            
        }
       
    }
    
    
    
    @IBAction func btn_dlevelslink(_ sender: Any) {
        UIApplication.shared.openURL(NSURL(string: "https://www.dynamiclevels.com")! as URL)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return nameArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       // return UITableViewAutomaticDimension
        
        var isVisible = false
        
        if nameArray[indexPath.row] == "Sponsored Code" {
            if Subscription_Data.status {
                isVisible = false
            }else {
                isVisible = true
            }
            
            if isVisible {
                return UITableViewAutomaticDimension
            }else{
                return 0
            }
        }
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // let cell
        
        
        
        let cell = Bundle.main.loadNibNamed("TableViewCell", owner: nil, options: nil)?[0] as! UITableViewCell
        cell.selectionStyle = .none
        
        
        
        let lblName = cell.viewWithTag(2002) as! UILabel
        print(nameArray[indexPath.row])
        lblName.text = nameArray[indexPath.row]
        let image = cell.viewWithTag(2001) as? UIImageView
        image?.image = UIImage(named: imageName[indexPath.row])
        
        if nameArray[indexPath.row] == "My Account" {
            
                
                image?.isHidden = true
                cell.backgroundColor = UIColor.init(hex: "")
        }
        
        if nameArray[indexPath.row] == "Log Out" {
            if User.email == ""{
                lblName.isHidden = true
                image?.isHidden = true
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        navController = User.navigation
        
        let indexPath = tableView.indexPathForSelectedRow //optional, to get from any UIButton for example
        let currCell = tableView.cellForRow(at: indexPath!)
        let lblName = currCell!.viewWithTag(2002) as! UILabel
        //print(lblName.text)
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        
        
        switch lblName.text {
       // case "My Account":
         //   UIApplication.shared.openURL(NSURL(string: "https://www.dynamiclevels.com/en/vs-my-account/\(User.token)")! as URL)
        case "Dynamic Indices":
            let viewController = storyBoard.instantiateViewController(withIdentifier: "DynamicIndexController") as! DynamicIndexController
            navController?.pushViewController(viewController, animated: true)
        case "Stock Market Today":
            let viewController = storyBoard.instantiateViewController(withIdentifier: "StockMarketTodayViewController") as! StockMarketTodayViewController
            navController?.pushViewController(viewController, animated: true)
        case "Invest in Our PMS":
            if #available(iOS 11.0, *) {
                let viewcontroller = storyBoard.instantiateViewController(withIdentifier: "PMSPDFViewerViewController") as! PMSPDFViewerViewController
                navController?.pushViewController(viewcontroller, animated: true)
            } else {
                // Fallback on earlier versions
            }
        case "Webinars":
            let viewcontroller = storyBoard.instantiateViewController(withIdentifier: "WebinarsViewController") as! WebinarsViewController
            navController?.pushViewController(viewcontroller, animated: true)
        case "My Rewards":
            let rewardController = storyBoard.instantiateViewController(withIdentifier: "RewardsViewController") as! RewardsViewController
            navController?.pushViewController(rewardController, animated: true)
        case "Refer & Earn":
            let referralController = storyBoard.instantiateViewController(withIdentifier: "ReferralViewController") as! ReferralViewController
            navController?.pushViewController(referralController, animated: true)
        case "Disclaimer":
            let disclaimerController = storyBoard.instantiateViewController(withIdentifier: "DisclaimerViewController") as! DisclaimerViewController
            disclaimerController.disclaimerUrl = DisclaimerUrl.general
            disclaimerController.disclaimerHeader = "Disclaimer"
            navController?.pushViewController(disclaimerController, animated: true)
        /*case "Refer & Earn":
            InviteReferrals.launch("23576", email: User.email, mobile: User.phone, name: User.firstName, subscriptionID: "")*/
        case "Contact Us":
            let viewController = storyBoard.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
            navController?.pushViewController(viewController, animated: true)
        case "Sponsored Code":
            let viewController = storyBoard.instantiateViewController(withIdentifier: "SponsoredCodeViewController") as! SponsoredCodeViewController
            navController?.pushViewController(viewController, animated: true)
        case "Log Out":
            GIDSignIn.sharedInstance().signOut()
            GIDSignIn.sharedInstance().disconnect()
            
            if(User.regThrough == "gmail")
            {
                if GIDSignIn.sharedInstance()?.currentUser != nil {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) { // Change `2.0` to the desired number of seconds.
                        // Code you want to be delayed
                        self.normalRegistrationLogOut()
                    }
                } else {
                    self.normalRegistrationLogOut()
                }
            } else if(User.regThrough == "facebook")
            {
                let manager = LoginManager()
                manager.logOut()
                normalRegistrationLogOut()
                
            }else {
                normalRegistrationLogOut()
            }
        default:
            print("Nothing Clicked!")
        }
    }
    
    func normalRegistrationLogOut() {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let webService = WebService()
        webService.callWebServices(url: Urls.logOut, methodName: "POST", parameters: "", istoken: true, tokenval: User.token, completion: { (success, jsonResult) in
            
            DispatchQueue.main.async {
                
                
                UserDefaults.standard.set(nil, forKey: "phone")
                UserDefaults.standard.set(nil, forKey: "email")
                UserDefaults.standard.set(nil, forKey: "password")
                UserDefaults.standard.set(nil, forKey: "regThrough")
                
                
                
                User.userId         =   ""
                User.socialId         =   ""
                User.firstName        =   ""
                User.lastName         =   ""
                User.email            =   ""
                User.phone            =   ""
                User.socialToken      =   ""
                User.password         =   ""
                User.regThrough       =   ""
                User.token            =   ""
                User.OnboardinString  =   ""
                User.countryCode      =   ""
                User.isLandingDataLaded = false
                User.firebaseToken = ""
                
                User.isCommingFromSpecificPage = false
                User.instanceOfSectorPerformance = SectorPerformanceDetailsViewController()
                //User.tempIndePath:  IndexPath!
                User.navigation = UINavigationController()
                
                User.isAlternetColor = false
                
                
                
                
               /****** Proform Login For Guest User support@dynamiclevels.com ************/
                // self.performDefaultLogin()
                
                /********************/
                let viewcontroller = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                self.navController?.pushViewController(viewcontroller, animated: false)
                
                
            }
        })
    }
    func performDefaultLogin() {
        
        let appManager = AppManager()
        let webService = WebService()
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let parameters = "user_email=support@dynamiclevels.com&password=dynamic123&logthrough=normal"
            
            webService.callWebServices(url: Urls.login_without_emailverification, methodName: "POST", parameters: parameters, istoken: false, tokenval: "", completion: { (success, dictData) in
                
                print("Login Return Result: \(dictData)  \n   Wait here...")
                if dictData.value(forKey: "error") as! NSInteger == 0 {
                    self.actInd.stopAnimating()
                    // print(dictData)
                    let responce = dictData.value(forKey: "response") as! NSDictionary
                    User.token = responce.value(forKey: "token") as! String
                    self.getUser()
                    
                } else {
                    let appManager = AppManager()
                    
                    DispatchQueue.main.async(execute: {
                        self.actInd.stopAnimating()
                        
                    })
                }
            })
    }
    
    func getUser() {
        
        let webServices = WebService()
        webServices.callWebServices(url: Urls.getUser, methodName: "GET", parameters: "", istoken: true, tokenval: User.token) { (success, jsonDict) in
            
            print(jsonDict)
            if jsonDict.value(forKey: "errmsg") as! String == "" {
                
                let response = jsonDict.value(forKey: "response") as! [NSDictionary]
                
                User.firstName  = response[0].value(forKey: "firstName") as! String
                User.lastName   = response[0].value(forKey: "lastName") as! String
                User.email      = response[0].value(forKey: "emailID") as! String
                User.phone      = response[0].value(forKey: "phone") as! String
                
                if let referer_code: String = response[0].value(forKey: "referer_code") as? String {
                    User.referer_code = referer_code
                }
                if let fundaccount: String = response[0].value(forKey: "fund_account_id") as? String {
                    User.fundaccount = fundaccount
                }
                if let refererredby: String = response[0].value(forKey: "referred_by") as? String {
                    User.refererredby = refererredby
                }
                
                if let mfKyc: String = response[0].value(forKey: "MF_kyc") as? String {
                    User.MF_kyc = mfKyc
                }
                
                if let mfUccStatus: String = response[0].value(forKey: "MF_ucc_status") as? String {
                    User.MF_ucc_status = mfUccStatus
                }
                
                if let MF_UserId: String = response[0].value(forKey: "MF_UserId") as? String {
                    User.MF_UserId = MF_UserId
                }
                
                if let ucc: String = response[0].value(forKey: "ucc") as? String {
                    User.ucc = ucc
                }
                if let country_ph_code: String = response[0].value(forKey: "country_ph_code") as? String {
                    User.countryCode        = country_ph_code
                    CountryCode.countryCode = country_ph_code
                }
                
                
                User.navigation = self.navController!
                
                let userId      = response[0].value(forKey: "userId") as! String
                User.userId     = userId
                User.socialId   = userId
                
//                self.multibaggerstatload(completion: {
//                    //self.firebaseTokenAddUpdate()
//                   //self.nrifpipmsload()
//                })
                //self.nrifpipmsload()
                
            } else{
                let appManager = AppManager()
                appManager.showAlert(title: "Error!", message: jsonDict.value(forKey: "errmsg") as! String, navigationController: self.navController!)
            }
        }
    }
    func multibaggerstatload(completion : @escaping ()->())
    {
        let obj = WebService()
        let paremeters = ""
        
        self.actInd.startAnimating()
        
        obj.callWebServices(url: Urls.multibaggerStatus, methodName: "GET", parameters: paremeters, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                print(jsonData)
                
                let tempArray = jsonData.value(forKey: "response") as! [NSDictionary]
                MultibaggerStats.segResultQtrArr.removeAll()
                
                MultibaggerStats.segQtrDateArr.removeAll()
                MultibaggerStats.segYearDateArr.removeAll()
                
                MultibaggerStats.segQtrDisplayArr.removeAll()
                MultibaggerStats.segYearDisplayArr.removeAll()
                
                MultibaggerStats.qutrniftyindex.removeAll()
                MultibaggerStats.qutrsmallcapindex.removeAll()
                MultibaggerStats.yearniftyindex.removeAll()
                MultibaggerStats.yearsmallcapindex.removeAll()
                MultibaggerStats.qutrmidcapindex.removeAll()
                
                
                MultibaggerStats.newqutrsmallcapindex.removeAll()
                MultibaggerStats.newqutrmidcapindex.removeAll()
                MultibaggerStats.newqutrniftyindex.removeAll()
                
                
                for arr in tempArray
                {
                    //segQtrDateArr
                    if(arr.value(forKey: "mdt_Rep_Type") as! String == "QUARTER")
                    {
                        MultibaggerStats.segResultQtrArr.append(arr.value(forKey: "mdt_Result_Qtr") as! String)
                        MultibaggerStats.segQtrDateArr.append(arr.value(forKey: "mdt_Date") as! String)
                        MultibaggerStats.segQtrDisplayArr.append(arr.value(forKey: "mdt_Display") as! String)
                        
                        if let mrdSmallCapPer = arr.value(forKey: "mrd_SmallCap_Per") as? String {
                            MultibaggerStats.qutrsmallcapindex.append(mrdSmallCapPer)
                        }
                        if let mrdMidCapPer = arr.value(forKey: "mrd_MidCap_Per") as? String {
                            MultibaggerStats.qutrmidcapindex.append(mrdMidCapPer)
                        }
                        if let mrdNiftyPer = arr.value(forKey: "mrd_Nifty_Per") as? String  {
                            MultibaggerStats.qutrniftyindex.append(mrdNiftyPer)
                        }
                        if let mdtPerfDt = arr.value(forKey: "mdt_Perf_Dt") as? String  {
                            MultibaggerStats.qutrperformancetxt.append(mdtPerfDt)
                        }
                        
                        
                        
                        if let mrdSmpPer = arr.value(forKey: "SmpPer") as? String {
                            MultibaggerStats.newqutrsmallcapindex.append(mrdSmpPer)
                        }
                        if let mrdMidPer = arr.value(forKey: "MidPer") as? String {
                            MultibaggerStats.newqutrmidcapindex.append(mrdMidPer)
                        }
                        if let mrdLarPer = arr.value(forKey: "LarPer") as? String  {
                            MultibaggerStats.newqutrniftyindex.append(mrdLarPer)
                        }
                        
                        
                    }
                    
                    if(arr.value(forKey: "mdt_Rep_Type") as! String == "YEAR")
                    {
                        MultibaggerStats.segYearDateArr.append(arr.value(forKey: "mdt_Date") as! String)
                        MultibaggerStats.segYearDisplayArr.append(arr.value(forKey: "mdt_Display") as! String)
                        
                        if let mrdSmallCapPer = arr.value(forKey: "mrd_SmallCap_Per") as? String {
                            MultibaggerStats.yearsmallcapindex.append(mrdSmallCapPer)
                        }
                        if let mrdNiftyPer = arr.value(forKey: "mrd_Nifty_Per") as? String {
                            MultibaggerStats.yearniftyindex.append(mrdNiftyPer)
                        }
                        MultibaggerStats.yearperformancetxt.append(arr.value(forKey: "mdt_Perf_Dt") as! String )
                    }
                }
                
                
                let segQtrDateArr = UserDefaults.standard
                segQtrDateArr.set(MultibaggerStats.segQtrDateArr, forKey: "segQtrDateArr")
                
                self.actInd.stopAnimating()
                return completion()
                /*
                 DispatchQueue.main.async {
                 
                 self.nrifpipmsload()
                 
                 }*/
            }
        }
    }
    func nrifpipmsload()
    {
        let obj = WebService()
        let data = "ALL"
        let param = "ptype=\(data)"
        
        self.actInd.startAnimating()
        
        obj.callWebServices(url: Urls.NRIFPIPMS, methodName: "GET", parameters: param, istoken: false, tokenval: "") { (returnValue, jsonData) in
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                
                // print(jsonData)
                
                let tempArray = jsonData.value(forKey: "response") as! [NSDictionary]
                
                
                for arr in tempArray
                {
                    
                    if(arr.value(forKey: "p_type") as! String == "PMS")
                    {
                        PMS_DATA.PMS1 = arr.value(forKey: "p_why") as! String
                        PMS_DATA.PMS2 = arr.value(forKey: "p_what") as! String
                        PMS_DATA.PMS3 = arr.value(forKey: "p_how") as! String
                    }
                    
                    if(arr.value(forKey: "p_type") as! String == "NRI")
                    {
                        NRI_DATA.NRI1 = arr.value(forKey: "p_what") as! String
                        NRI_DATA.NRI2 = arr.value(forKey: "p_why") as! String
                        NRI_DATA.NRI3 = arr.value(forKey: "p_how") as! String
                    }
                    
                    if(arr.value(forKey: "p_type") as! String == "FPI")
                    {
                        FPI_DATA.FPI1 = arr.value(forKey: "p_what") as! String
                        FPI_DATA.FPI2 = arr.value(forKey: "p_why") as! String
                        FPI_DATA.FPI3 = arr.value(forKey: "p_how") as! String
                    }
                }
                
                DispatchQueue.main.async {
                    self.actInd.stopAnimating()
                    
                    var pageIdentifier    =   ""
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    
                    if(NotificationData.clickAction == ""){
                       // self.getSeminarAd()
                    }
                    else {
                        if(NotificationData.clickAction == "webinars")
                        {
                            pageIdentifier  =   "WebinarsViewController"
                            
                            let viewcontroller = storyBoard.instantiateViewController(withIdentifier: pageIdentifier) as! WebinarsViewController
                            
                            NotificationData.clickAction.removeAll()
                            
                            self.navController?.pushViewController(viewcontroller, animated: true)
                            
                        }
                        
                        if(NotificationData.clickAction == "dynamic_smallcap_index")
                        {
                            pageIdentifier  =   "DynamicIndexController"
                            
                            let viewcontroller = storyBoard.instantiateViewController(withIdentifier: pageIdentifier) as! DynamicIndexController
                            
                            viewcontroller.idxtype = "SMALL"
                            NotificationData.clickAction.removeAll()
                            
                            self.navController?.pushViewController(viewcontroller, animated: true)
                            
                        }
                        
                        if(NotificationData.clickAction == "quarterly_sectoral_performance")
                        {
                            pageIdentifier  =   "MultibaggerQuarterlySectorViewController"
                            
                            let viewcontroller = storyBoard.instantiateViewController(withIdentifier: pageIdentifier) as! MultibaggerQuarterlySectorViewController
                            
                            NotificationData.clickAction.removeAll()
                            self.navController?.pushViewController(viewcontroller, animated: true)
                            
                        }
                        
                        if(NotificationData.clickAction == "quarterly_list_dynamic_smallcap_multibaggers")
                        {
                            pageIdentifier  =   "MultibaggerViewController"
                            
                            let viewcontroller = storyBoard.instantiateViewController(withIdentifier: pageIdentifier) as! MultibaggerViewController
                            
                            NotificationData.clickAction.removeAll()
                            
                            self.navController?.pushViewController(viewcontroller, animated: true)
                            
                        }
                        
                        
                        
                        if(NotificationData.clickAction == "gainers_losers_dynamic_smallcap_multibaggers")
                        {
                            pageIdentifier  =   "MultibaggerViewController"
                            
                            let viewcontroller = storyBoard.instantiateViewController(withIdentifier: pageIdentifier) as! MultibaggerViewController
                            
                            NotificationData.clickAction.removeAll()
                            self.navController?.pushViewController(viewcontroller, animated: true)
                            
                        }
                        
                    }
                    
                }
            }
        }
    }
    
    func firebaseTokenAddUpdate(){
        
        let webServices = WebService()
        var param = ""
        if let token = InstanceID.instanceID().token() {
            param  = "platform=iOS&userid=\(User.email)&token_no=\(token)"
            
            webServices.callWebServices(url: Urls.addUpdateFCMToken, methodName: "POST", parameters: param, istoken: true, tokenval: User.token) { (success, jsonDict) in
                
                print(jsonDict)
                if jsonDict.value(forKey: "errmsg") as! String == "" {
                    
                    let jsonValue = jsonDict
                }
            }
        }
    }
    func share_link(controller:UINavigationController)
    {
        
       
        let textToShare = "Download the Value Stocks App now - A must have app for all those invested or interested in Stocks and Mutual funds. You should try it too.\n\n" + "https://www.dynamiclevels.com/app"
        
        //if NSURL(string: http://www.codingexplorer.com/") != nil {
        let objectsToShare = [textToShare]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        
        //New Excluded Activities Code
        activityVC.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.addToReadingList]
        //
        
        // self.presentingViewController(activityVC,animated:true,    completion :nil)
        // activityVC.popoverPresentationController?.sourceView = sender
        controller.present(activityVC, animated: true, completion: nil)
        // }
        
        
        
        
    }
    
    
    
    @IBAction func btn_share(_ sender: Any) {
        
         share_link(controller:navController!)        

        
    }
    
    //MARK: For Hiding Left Slide View
    func hideSlideMenu(navigationController: UINavigationController,viewSize: UIView) {
        
        UIView.animate(withDuration: 0.4) {
            for views in viewSize.subviews {
                if views.tag == 100 {
                    
                    navigationController.navigationBar.frame.origin.x = 0
                    
                    self.nibView.frame.origin.x = -self.nibView!.frame.width
                    AppManager().setStatusBarBackgroundColor()
                    
                }
            }
        }
    }
    
    @IBAction func btnLogIn_Action(_ sender: Any) {
        //normalRegistrationLogOut()
        User.navigation = self.navController!
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        //viewController.navController = User.navigation
        navController?.pushViewController(viewController, animated: true)
        
        
    }
    
    
    @IBAction func btnSignUp_Action(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SignUPScreenViewController") as! SignUpViewController
        navController?.pushViewController(viewController, animated: true)
        
    }
    
    func addGestureRecogniser(){
        let  tapGesture = UITapGestureRecognizer(target: self, action: #selector(MultibaggerLandingViewController.tapAction(sender:)))
            nibView.addGestureRecognizer(tapGesture)
        
        let  swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(MultibaggerLandingViewController.tapAction(sender:)))
        swipeGesture.direction = .left
        nibView.addGestureRecognizer(swipeGesture)
    }
    
    //MARK: GESTURE RECONGISER METHODE
    func tapAction(sender: UITapGestureRecognizer) {
        
        //self.hideSlideMenu(navigationController: navController!, viewSize: nibView)
        //btnHamburger.frame.origin.x -= 250
       // isShowingMenu = false
    }
  
    
    func hideSlideMenu(navigationController: UINavigationController,viewSize: UIView, navigationView: UIView) {
        
        
        UIView.animate(withDuration: 0.4) {
            for views in viewSize.subviews {
                if views.tag == 100 {
                    //navigationController.navigationBar.frame.origin.x = 0
                    navigationView.frame.origin.x = 0
                    self.nibView.frame.origin.x = -self.nibView!.frame.width
                    AppManager().setStatusBarBackgroundColor()
                }
            }
        }
    }
}
