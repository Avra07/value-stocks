//
//  LeftSlideMenu.swift
//  DLevels
//
//  Created by Dynamic-Mac-02 on 02/02/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import Foundation
import UIKit


class MultibaggerRecomended: UIView, UITableViewDelegate, UITableViewDataSource {
    
    var sectorListArray = [NSDictionary]()
    var month = "Per"
    @IBOutlet weak var lblMonthChange: UILabel!
    @IBOutlet weak var lblNodataAvailable: UILabel!
    
    
    @IBOutlet weak var tblSlideMenuLlist: UITableView!
    var navController: UINavigationController?
    var nibView: UIView!
    
    //MARK: Prepare Screen
    func prepareScreenWithView(navigationController: UINavigationController, viewSize: UIView) {
        
        nibView = Bundle.main.loadNibNamed("MultibaggerRecomended", owner: self, options: nil)?[0] as! UIView
        
        navController = navigationController
        
        nibView.tag = 100
        var tempRect = nibView.frame
        print(tempRect)
        
        self.lblNodataAvailable.isHidden = true
        tickerlist()  // Loading data for table
        tempRect =  CGRect(x: 0, y: 64, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        
        viewSize.addSubview(self.nibView)
        
        let sectionIndex = IndexSet(integer: 0)
        tblSlideMenuLlist.reloadSections(sectionIndex, with: .top)
        
        UIView.transition(with: self.nibView, duration: 0.4, options: [.curveEaseIn], animations: {
          
            self.nibView.frame = tempRect
            
        }) { (success) in
            
        }
        
    }
    //MARK: Table View Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(sectorListArray.count)
        return sectorListArray.count
    }
    
    //MARK: Table View Delegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    //MARK: Table View Delegate
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = Bundle.main.loadNibNamed("TableViewCell", owner: nil, options: nil)?[1] as! UITableViewCell
        cell.selectionStyle = .none
        
        cell.backgroundColor = indexPath.row % 2 == 0 ? UIColor(red: 232/255, green: 232/255, blue: 232/255, alpha: 0.4) : UIColor.white
        
        let index = sectorListArray[indexPath.row]
        
        let lblName = cell.viewWithTag(2002) as! UILabel
        lblName.text = index.value(forKey: "EXCHANGE_NAME") as? String
        lblName.adjustsFontSizeToFitWidth = true
        
        
      
        
        let lblPERatio = cell.viewWithTag(2003) as! UILabel
        lblPERatio.text = index.value(forKey: "mch_Rate") as? String
        
        let lblMonth = cell.viewWithTag(2004) as! UILabel
        if month == "Per" {
            lblMonth.text = index.value(forKey: "Per") as? String
        } else if month == "EOD" {
            lblMonth.text = index.value(forKey: "LTP") as? String
        }

        
        
        
        let lblSector = cell.viewWithTag(2005) as! UILabel
        lblSector.text = index.value(forKey: "SECTOR") as? String
        //lblSector.adjustsFontSizeToFitWidth = true
        
        let lblYear = cell.viewWithTag(2006) as! UILabel
        lblYear.text = index.value(forKey: "Trd_Date") as? String
        lblYear.adjustsFontSizeToFitWidth = true
        
        
        return cell
    }
    
    
    //MARK: Getting Data From Array
    func tickerlist()
    {
        let obj = WebService()
        let seg = MultibaggerType.typeOfMultibaggerForPastPer
        let type_param = "segment=\(seg)&year=2014-03-31"
        
        let token = User.token
        
        obj.callWebServices(url: Urls.multibagger_call_history, methodName: "GET", parameters: type_param, istoken: true, tokenval:token ) { (returnValue, jsonData) in
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
               // print("Selected Value From Array is : \(SearchForMultibaggerSector.serchValue)")
                
                let tempArrayDict : [NSDictionary] = jsonData.value(forKey: "response") as! [NSDictionary]
                
                for dictValue in tempArrayDict {
                        self.sectorListArray.append(dictValue)
                }
              
                DispatchQueue.main.async {
                    
                    if self.sectorListArray.count == 0 {
                        self.lblNodataAvailable.isHidden = false
                    } else {
                        self.lblNodataAvailable.isHidden = true
                    }
                 let sectionIndex = IndexSet(integer: 0)
                 self.tblSlideMenuLlist.reloadSections(sectionIndex, with: .automatic)
 
                }
            }
            else
            {
                let alertobj = AppManager()
                
                print(jsonData.value(forKey: "errmsg") as! String)
                
            }
        }
    }
    
    
    
    //MARK: For Hiding Left Slide View
    func hideSlideMenu(navigationController: UINavigationController,viewSize: UIView) {
        
        UIView.animate(withDuration: 0.4) {
            for views in viewSize.subviews {
                if views.tag == 100 {
                    
                    navigationController.navigationBar.frame.origin.x = 0
                    
                    self.nibView.frame.origin.x = -self.nibView!.frame.width
                    
                    self.nibView.removeFromSuperview()
                    AppManager().setStatusBarBackgroundColor()
                    
                }
            }
        }
    }
    
    @IBAction func onClickMonthChange(_ sender: UIButton) {
        
        let actionSheeController = UIAlertController(title:"", message: "", preferredStyle: .actionSheet)
        actionSheeController.view.tintColor = UIColor(red: 247/255, green: 196/255, blue: 58/255, alpha: 1)
        actionSheeController.addAction(UIAlertAction(title: "Per", style: .default, handler: { (success) in
            self.lblMonthChange.text = "Per"
            self.month = "Per"
            UIView.animate(withDuration: 0.4) {
                let sectionIndex = IndexSet(integer: 0)
                self.tblSlideMenuLlist.reloadSections(sectionIndex, with: .top)
            }
        }))
        actionSheeController.addAction(UIAlertAction(title: "EOD", style: .default, handler: { (success) in
            self.month = "EOD"
            self.lblMonthChange.text = "EOD"
            UIView.animate(withDuration: 0.4) {
                let sectionIndex = IndexSet(integer: 0)
                self.tblSlideMenuLlist.reloadSections(sectionIndex, with: .top)
            }
        }))
        
        
        actionSheeController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (success) in
        }) )
        self.navController?.present(actionSheeController, animated: true) {
            
        }
    }
    
    @IBAction func btnRemoveViewClick(_ sender: UIButton) {
        
        UIView.transition(with: self.nibView, duration: 1, options: [.curveEaseInOut], animations: {
            
            self.nibView.frame.origin.y -=  200
            
        }) { (success) in
            self.sectorListArray.removeAll()
            self.nibView.removeFromSuperview()
            
        }
        
    }
    
    
    func hideSlideMenu(navigationController: UINavigationController,viewSize: UIView, navigationView: UIView) {
        
        UIView.animate(withDuration: 0.4) {
            for views in viewSize.subviews {
                if views.tag == 100 {
                    navigationView.frame.origin.x = 0
                    self.nibView.frame.origin.x = -self.nibView!.frame.width
                    AppManager().setStatusBarBackgroundColor()
                }
            }
        }
    }
}
