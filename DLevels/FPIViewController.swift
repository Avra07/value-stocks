//
//  FPIViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-01 on 17/10/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class FPIViewController: UIViewController,UIWebViewDelegate {

    @IBOutlet weak var txtData: UITextView!
    @IBOutlet weak var wvWebView: UIWebView!
    @IBOutlet weak var backBtn: UIButton!
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    var isShowingMenu = false
    var instanceOfLeftSlideMenu = LeftSlideMenu()
    var dataArr:     [NSDictionary] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        wvWebView.delegate = self
        getData()
        addLoader()
        // Do any additional setup after loading the view.
    }
    
    func addLoader() {
        actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0) //CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.gray
        self.view.addSubview(actInd)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        wvWebView.frame.size.height = 1
        wvWebView.frame.size = wvWebView.sizeThatFits(.zero)
        wvWebView.scrollView.isScrollEnabled = false
        self.view.layoutIfNeeded()
    }
    
    //MARK: Getting Data
    func getData()
    {
        let obj = WebService()
         self.actInd.startAnimating()
        obj.callWebServices(url: Urls.NRIFPI, methodName: "GET", parameters: "", istoken: false, tokenval: "") { (returnValue, jsonData) in
            // print(jsonData)
            if jsonData.value(forKey: "errmsg") as! String == "" {
                let respDic = jsonData.value(forKey: "response") as! NSArray
                for dict in respDic {
                    let dictValue = dict as! NSDictionary
                    self.dataArr.append(dictValue)
                }
                DispatchQueue.main.async {
                    print(self.dataArr)
                    
                    if let benefits = self.dataArr[0].value(forKey: "eligibility") as? String {
                        
                        do {
                            let benefitsAttibutedString    = try NSAttributedString(data: benefits.data(using: String.Encoding.utf8)!, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil)
                            
                            self.txtData.attributedText = benefitsAttibutedString
                            self.wvWebView.loadHTMLString(benefits, baseURL: nil)
                            self.actInd.stopAnimating()
                            
                        } catch {
                            self.actInd.stopAnimating()
                            print(error)
                        }
                        
                    }
                }
                
                /*
                 self.webvw?.loadHTMLString(self.dataArr[0].value(forKey: "introduction") as! String!, baseURL: nil) */
                
            }
        }
    }
    
    
    
    //MARK: DDP Button Action
    @IBAction func btnDDPs_Action(_ sender: UIButton) {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "DDPController") as! DDPController
        self.navigationController?.pushViewController(viewController, animated: false)
    }
    
    @IBAction func backBtnClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
