//
//  PerformanceGraphViewController.swift
//  DLevels
//
//  Created by SAIKAT GHOSH on 11/07/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit
import Charts

class PerformanceGraphViewController: UIViewController, ChartViewDelegate {
    
    @IBOutlet var chartView: BarChartView!
    @IBOutlet var navigationTitle: UILabel!
    
    var years = [String]()
    var unitsSold = [Double]()
    
    
    
    var symbol_name = ""
    var stock_param = ""
    var title_text  = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationTitle.text = title_text
        self.navigationTitle.sizeToFit()
        getChartData()
    }
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setChart(dataPoints: [String], values: [Double]) {
        
        var dataEntries: [BarChartDataEntry] = []
        
        chartView.xAxis.labelPosition = .bottom
        chartView.xAxis.drawGridLinesEnabled = false
        chartView.xAxis.drawAxisLineEnabled = false
        
        chartView.rightAxis.enabled = false
        
        chartView.noDataText = "You need to provide data for the chart."
        chartView.animate(xAxisDuration: 2.0, yAxisDuration: 2.0)
        
        
        for i in 0..<dataPoints.count {
            let dataEntry = BarChartDataEntry(x: Double(i), y: values[i] , data: years as AnyObject?)
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = BarChartDataSet(values: dataEntries, label: "Result")
        chartDataSet.colors = [UIColor(red: 252/255, green: 235/255, blue: 173/255, alpha: 1)]
        chartDataSet.highlightColor =  UIColor(red: 248/255, green: 157/255, blue: 28/255, alpha: 1)
        
        
        let chartData = BarChartData(dataSet: chartDataSet)
        chartView.data = chartData
        
        chartView.highlightValue(x: Double(dataPoints.count), dataSetIndex: 0, stackIndex: 0)
        
        
        
        //chartView.highlightValue(x: 0, dataSetIndex: dataEntries.count - 1, stackIndex: 0)
        
        chartView.highlightFullBarEnabled = true
        chartView.xAxis.valueFormatter = IndexAxisValueFormatter(values: years)
        chartView.xAxis.granularity = 1
        chartView.chartDescription?.text = ""
        self.chartView.legend.enabled = false
    }
     
    func getChartData() {
        
        chartView.delegate=self
        let obj = WebService()
        let paremeters = "symbol=\(symbol_name)&param=\(stock_param)"
        var indexval: NSDictionary = NSDictionary()
        
        //Mark: Call Service for Chart Data
        obj.callWebServices(url: Urls.getStockDetailsDataHistory, methodName: "POST", parameters: paremeters, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
            
            print("Json Data is :  \(jsonData)")
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                let tempArray = jsonData.value(forKey: "response") as! NSArray
                if(tempArray.count > 0)
                {
                    for dict in tempArray {
                        var year = ""
                        var results = ""
                        var display = ""
                        
                        
                        indexval = dict as! NSDictionary
                        year  = indexval.value(forKey: "YearEnding") as! String
                        
                        if let dis:String = indexval.value(forKey: "Display") as? String {
                            display = dis
                        }
                        if let res:String = indexval.value(forKey: "Result") as? String {
                            results = res
                        }
                        
                        //results  = indexval.value(forKey: "Result") as! String
                        self.years.append(year)
                        
                        if results == "" {
                           results = "0"
                        }
                        
                        if display == "" {
                            display = "0"
                        }
                        
                        self.unitsSold.append(Double(results)!)
                        
                    }
                
                    // let unitsSold = [20.0, 4.0, 6.0, 3.0, 12.0, 16.0, 4.0, 18.0, 2.0, 4.0, 5.0, 4.0]
                    self.setChart(dataPoints: self.years, values: self.unitsSold)
                    
                }
            }
        }
    }
}

    

