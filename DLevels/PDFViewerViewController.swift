//
//  PDFViewerViewController.swift
//  DLevels
//
//  Created by SAIKAT GHOSH on 08/04/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit


class PDFViewerViewController: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var navBarTitle: UINavigationItem!
    @IBOutlet weak var web: UIWebView!
    var strURL = "" //For get String URL with navigation
    var strTitle = ""
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let label = UILabel(frame: CGRect(x:0, y:0, width:400, height:50))
        label.backgroundColor = .clear
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: 15.0)
        label.textAlignment = .left
        label.textColor = UIColor(hex:"284C5A")
        label.text = strTitle
        self.navBarTitle.titleView = label
        
        
        
        web?.delegate = self// Add this line to set the delegate of webView
        
//        web?.scrollView.maximumZoomScale = 20; // set as you want.
//        web?.scrollView.minimumZoomScale = 1; // set as you want.
        
        
//        web?.scrollView.zoomScale = 2;
//        web?.scrollView.zoomScale = 1;
        
        addLoader()
    }
    override func viewWillAppear(_ animated: Bool) {
        AppManager().setStatusBarBackgroundColor()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if let url = URL(string: strURL.encodeUrl()) {
            let request = URLRequest(url: url as URL)
            self.web.loadRequest(request)
        }
        
        
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        self.actInd.startAnimating()
    }
    
    func webViewDidFinishLoad(_ webView : UIWebView) {
       
        
        
//        let contentSize:CGSize = self.web.scrollView.contentSize
//        let viewSize:CGSize = self.view.bounds.size
//        
//        let rw = viewSize.width / contentSize.width
//        
//        self.web.scrollView.minimumZoomScale = rw
//        self.web.scrollView.maximumZoomScale = rw
//        self.web.scrollView.zoomScale = rw
        
        
        
        
        self.actInd.stopAnimating()
    }
    
    
    
    
    func addLoader() {
        actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0) //CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.gray
        self.view.addSubview(actInd)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onClick_back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension String
{
    func encodeUrl() -> String
    {
        return self.addingPercentEncoding( withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
    }
    func decodeUrl() -> String
    {
        return self.removingPercentEncoding!
    }
    
}
