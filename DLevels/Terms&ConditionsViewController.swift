//
//  Terms&ConditionsViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-01 on 23/05/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class Terms_ConditionsViewController: UIViewController {
    
    @IBOutlet weak var btnHamburger: UIButton!
    @IBOutlet weak var webView: UIWebView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let url = URL(string: "https://www.dynamiclevels.com/vsapp-html/vs-terms-and-conditions.html")
        webView.loadRequest(URLRequest(url: url!))
        
    }
    
    @IBAction func btnback(_ sender: Any) {
        
		self.navigationController?.popViewController(animated: false)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
