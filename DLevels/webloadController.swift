//
//  webloadController.swift
//  DLevels
//
//  Created by SAIKAT GHOSH on 25/08/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class webloadController: UIViewController {
    
    @IBOutlet weak var weboutlet: UIWebView!
    var infostring : String = ""
    @IBOutlet weak var lbltitle: UILabel!
    @IBAction func backbutton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    func loadUrl() {
        weboutlet.loadHTMLString(infostring, baseURL: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        loadUrl()
        // Do any additional setup after loading the view.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
