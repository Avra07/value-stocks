//
//  PopUp_ViewController.swift
//  DLevels
//
//  Created by SAIKAT GHOSH on 24/08/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class PopUp_ViewController: UIViewController {
    
    @IBOutlet weak var btnOk: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnCross_Action(_ sender: UIButton) {
        dismissCurrentPage()
    }
    
    @IBAction func btnOk_Action(_ sender: UIButton) {
        dismissCurrentPage()
        
        UIApplication.shared.openURL(NSURL(string: "https://www.dynamiclevels.com/en/vs-premium-benefits/\(User.token)")! as URL)
        
    }
    
    //MARK: DISMISS CURRENT PAGE
    private func dismissCurrentPage(){
        dismiss(animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
