//
//  MFSpecificPageViewController.swift
//  DLevels
//
//  Created by Dlevels-MacMini1 on 29/05/18.
//  Copyright © 2018 Dynamic-Mac-01. All rights reserved.
//

import UIKit
class customMutualFundCell : UITableViewCell{
    
    @IBOutlet weak var lblVal1: UILabel!
    @IBOutlet weak var lblVal2: UILabel!
}
class MFSpecificPageViewController: UIViewController,UIPopoverPresentationControllerDelegate, UIAdaptivePresentationControllerDelegate {

    @IBOutlet weak var btnBackground: UIButton!
    @IBOutlet weak var popOverViewContainer: NSLayoutConstraint!
    
    
    @IBOutlet weak var viewPopover: UIView!
    
    @IBOutlet weak var navbarTitle: UINavigationItem!
    
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var lblFundName: UILabel!
    @IBOutlet weak var lblDLRecomm: UILabel!
    
    @IBOutlet weak var tblFundamentals: UITableView!
    
    @IBOutlet weak var btnCompareWithOtherScheme: UIButton!
    
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    var fundDetailsDict         =     [NSDictionary]()
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    
    var Category = ""
    var Fund = ""
    var FundName = ""
    var scmCode     = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppManager().setStatusBarBackgroundColor()
        
        addLoader()
        actInd.startAnimating()
        
        let label = UILabel(frame: CGRect(x:0, y:0, width:400, height:50))
        label.backgroundColor = .clear
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: 15.0)
        label.textAlignment = .left
        label.textColor = UIColor(hex:"284C5A")
        label.text = Fund
        self.navbarTitle.titleView = label
        btnCompareWithOtherScheme.layer.cornerRadius = 6
        btnCompareWithOtherScheme.layer.borderWidth = 1
        //popOverViewContainer.constant = -400
        viewPopover.isHidden = true
        viewPopover.layer.cornerRadius = 6
        
        getFundDetails(fund: Fund)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func addLoader() {
        actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0) //CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.gray
        self.view.addSubview(actInd)
    }
    
    @IBAction func btnBackgroundClose(_ sender: Any) {
        //popOverViewContainer.constant = -400
         viewPopover.isHidden = true
        UIView.animate(withDuration: 0.1, animations: {
            self.view.layoutIfNeeded()
            self.btnBackground.alpha = 0
        }, completion: nil)
    }
    func getFundDetails(fund : String)
    {
        self.actInd.startAnimating()
        
        let obj = WebService()
        
        let category  =  Category
        let fund      =  fund.replacingOccurrences(of: " ", with: "%20").replacingOccurrences(of: "&", with: "%26")
       
        
        let parameters  = "category=\(category)&fund=\(fund)"
        
        obj.callWebServices(url: Urls.mfgetFundDetails, methodName: "POST", parameters: parameters, istoken: true, tokenval: User.token) {(returnValue, jsonData) in
            
            print("Json Data New is :  \(jsonData)")
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                DispatchQueue.main.async(execute: {
                    self.fundDetailsDict.removeAll()
                    let tempArray = jsonData.value(forKey: "response") as! [NSDictionary]
                    if(tempArray.count > 0){
                        
                        self.actInd.stopAnimating()
                        self.fundDetailsDict = tempArray
                        
                        for arr in tempArray{
                            if let display = arr.value(forKey: "mfp_Display") as? String{
                                if display == "Category" {
                                    self.FundName = (arr.value(forKey: "ColValue") as? String)!
                                }
                            }
                        }
                        self.tblFundamentals.reloadData()
                        
                    }else{
                        DispatchQueue.main.async {
                            self.actInd.stopAnimating()
                        }
                    }
                })
            }
            else
            {
                DispatchQueue.main.async {
                    let alertobj = AppManager()
                    self.actInd.stopAnimating()
                    //self.progressHUD.hide()
                    alertobj.showAlert(title: "Error!", message: "Error" , navigationController: self.navigationController!)
                }
            }
            
        }
    }
    func popoverPresentationControllerDidDismissPopover(popoverPresentationController: UIPopoverPresentationController) {
        //do som stuff from the popover
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //segue for the popover configuration window
       
        if segue.identifier == "popOver" {
            if let vc = segue.destination as? UIViewController {
                vc.popoverPresentationController!.delegate = self
                vc.preferredContentSize =   CGSize(width: 300, height: 270)
                let controller = vc.popoverPresentationController
                controller?.delegate = self
                //you could set the following in your storyboard
                controller?.sourceView = self.view
                let rect = CGRect(x: 0, y: 0, width: self.view.frame.width/2, height: self.view.frame.height/2)
                controller?.sourceRect = CGRect(x: rect.midX, y: rect.midY,width: self.view.frame.width/2,height: self.view.frame.height/2)
               // controller?.backgroundColor = UIColor.gray
                controller?.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
                
            }
        }
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        
        return UIModalPresentationStyle.none
    }
    @IBAction func knowBenifitsClick_Action(_ sender: Any) {
        UIView.animate(withDuration: 0.1, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: .curveEaseOut, animations: {
            //self.popOverViewContainer.constant = 0
            self.viewPopover.isHidden = false
            self.btnBackground.alpha = 0.5
        }, completion: nil)
    }
    
    
        
    
    
    @IBAction func searchClick(_ sender: Any) {
        let viewCont = self.storyboard?.instantiateViewController(withIdentifier: "MFSearchViewController") as! MFSearchViewController
        self.navigationController?.pushViewController(viewCont, animated: false)
    }
    
    @IBAction func btnBack_Action(_ sender: Any) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }

    @IBAction func btnCompareWithOthers_Action(_ sender: Any) {
       
        let viewCont = self.storyboard?.instantiateViewController(withIdentifier: "MFFundDetailsViewController") as! MFFundDetailsViewController
        
                viewCont.Category = Category
                viewCont.FundName = FundName
                viewCont.Fund = Fund
                viewCont.isPageFromMSpecific = "T"
        self.navigationController?.pushViewController(viewCont, animated: false)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension MFSpecificPageViewController : UITableViewDataSource, UITableViewDelegate {
    
    public func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 1000
    }
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
        
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier:"cell", for: indexPath) as! customMutualFundCell
        
        
        cell.selectionStyle = .none
        
        cell.backgroundColor = indexPath.row % 2 == 0 ? UIColor(red: 232/255, green: 232/255, blue: 232/255, alpha: 0.4) : UIColor.white
        
        let index = fundDetailsDict[indexPath.row]
        
        if let label1 = index.value(forKey: "mfp_Display") as? String {
            cell.lblVal1.text = label1
        }
        if let label2 = index.value(forKey: "ColValue") as? String {
            cell.lblVal2.text = label2
        }
        
        return cell
        
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if fundDetailsDict.count == 0 {
            self.actInd.stopAnimating()
//            let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
//            noDataLabel.text          = "No data available"
//            noDataLabel.textColor     = UIColor.black
//            noDataLabel.textAlignment = .center
//            tableView.backgroundView  = noDataLabel
//            tableView.separatorStyle  = .none
//            tableView.backgroundView?.isHidden = false
        } else {
            //tableView.separatorStyle = .singleLine
            tableView.backgroundView?.isHidden = true
        }
        
        return fundDetailsDict.count
        
        
        
    }
}
