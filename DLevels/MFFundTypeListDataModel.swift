//
//  MFFundTypeListDataModel.swift
//  DLevels
//
//  Created by Dlevels-MacMini1 on 27/05/18.
//  Copyright © 2018 Dynamic-Mac-01. All rights reserved.
//

import Foundation
class MFFundTypeListDataModel: NSObject {
    
    var FundType:String                 = ""
    var NoOfFunds:Int                   = 0
    var TotalNetAssets:Int              = 0
    var oneMReturn :Decimal             = 0.00
    var oneWAvgReturn :Decimal          = 0.00
    var yrAvgReturn :Decimal            = 0.00
    var threeMReturn :Decimal           = 0.00
    var sixMReturn :Decimal             = 0.00
    var threeYrAvgReturn :Decimal       = 0.00
    var fiveYrAvgReturn :Decimal        = 0.00
    
    
    
    
    
    func getData(paramCategory:String, completion : @escaping ([MFFundsTypeList])->()){
        let param = "category=\(paramCategory)"
        var result = [MFFundsTypeList]()
        
        
        
        // mfc_category, mfc_category_desc, mfc_category_title, mfc_updt_time, mfc_id
        
        let obj = WebService()
        obj.callWebServices(url: Urls.mffundtypelist, methodName: "POST", parameters: param, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
            
            print(jsonData)
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                let tempArray = jsonData.value(forKey: "response") as! [NSDictionary]
                
                for arr in tempArray{
                    if let Fund_Type = arr.value(forKey: "FundType") as? String{
                        self.FundType = Fund_Type
                    }
                    
                    if let No_Of_Funds = arr.value(forKey: "NoOfFunds") as? String{
                        self.NoOfFunds = Int(No_Of_Funds)!
                    }
                    
                    if let Total_Net_Assets = arr.value(forKey: "TotalNetAssets") as? String{
                        self.TotalNetAssets = Int(Total_Net_Assets)!
                    }
                    
                    if let OneW_Avg_Return = arr.value(forKey: "1WkRet") as? String{
                        self.oneWAvgReturn = Decimal((OneW_Avg_Return as NSString).doubleValue)
                    }
                    if let OneM_Avg_Return = arr.value(forKey: "1MRet") as? String{
                        self.oneMReturn = Decimal((OneM_Avg_Return as NSString).doubleValue)
                    }
                    if let ThreeM_Avg_Return = arr.value(forKey: "3MRet") as? String{
                        self.threeMReturn = Decimal((ThreeM_Avg_Return as NSString).doubleValue)
                    }
                    if let SixM_Avg_Return = arr.value(forKey: "6MRet") as? String{
                        self.sixMReturn = Decimal((SixM_Avg_Return as NSString).doubleValue)
                    }
                    if let Yr_Avg_Return = arr.value(forKey: "1YrAvgReturn") as? String{
                        self.yrAvgReturn = Decimal((Yr_Avg_Return as NSString).doubleValue)
                    }
                    if let ThreeYr_Avg_Return = arr.value(forKey: "3YrRet") as? String{
                        self.threeYrAvgReturn = Decimal((ThreeYr_Avg_Return as NSString).doubleValue)
                    }
                    if let FiveYr_Avg_Return = arr.value(forKey: "5YrRet") as? String{
                        self.fiveYrAvgReturn = Decimal((FiveYr_Avg_Return as NSString).doubleValue)
                    }
                    
                    let obj_mfFundsTypeList = MFFundsTypeList(FundType:self.FundType,NoOfFunds:self.NoOfFunds,TotalNetAssets:self.TotalNetAssets, OneWReturn: self.oneWAvgReturn, OneMReturn: self.oneMReturn, ThreeMReturn: self.threeMReturn, SixMReturn: self.sixMReturn,YrAvgReturn: self.yrAvgReturn, ThreeYrAvgReturn: self.threeYrAvgReturn, FiveYrAvgReturn: self.fiveYrAvgReturn)
                    
                    result.append(obj_mfFundsTypeList)
                }
            }
            return completion(result)
        }
    }
}
