//
//  UserSession.swift
//  DLevels
//
//  Created by SAIKAT GHOSH on 21/08/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//


import UIKit

class UserSession: NSObject {
    
    
    static let shared = UserSession()
    
    private override init() {
        
    }
    
    
    //MARK: CLEAR INVESTMENT VALUES
    func clearInvestmentValues(){
        
        Funddetails.Fund        =   ""
        Funddetails.ammount     =   ""
        Funddetails.scmCode     =   ""
        Funddetails.FundName    =   ""
        
        LUMPSUMToInvestDescription.LUMSUM_DETAILS.removeAll()
        
        SIPToInvestDescription.scmCode  = ""
        SIPToInvestDescription.fundName                 =   ""
        SIPToInvestDescription.SIP_DAY_OF_INVESTMENT    =   ""
        SIPToInvestDescription.SIP_INVEST_AMOUNT        =   ""
        
    }
    
}

