//
//  MFFundDetailsTableViewCell.swift
//  DLevels
//
//  Created by Dlevels-MacMini1 on 27/05/18.
//  Copyright © 2018 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class MFFundDetailsTableViewCell: UITableViewCell {
    class var identifier: String { return String(describing: self) }
    class var nib: UINib { return UINib(nibName: identifier, bundle: nil) }
    
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblSubCategory: UILabel!
    @IBOutlet weak var lblnetAssets: UILabel!
    @IBOutlet weak var lblAvgReturns: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureWithItem(item: MFFundDetails, colName: String) {
        
        let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]
        let underlineAttributedString = NSAttributedString(string: item.fund!, attributes: underlineAttribute)
        lbltitle.attributedText = underlineAttributedString
        
        lblSubCategory.text = item.subCategory
        let netAssets = "\(item.totalNetAssets ?? 0)"
        //lblnetAssets.text = String(format: "%.2f", Double(netAssets)!)
        lblnetAssets.text = netAssets
        
        if(colName == "1W"){
            let oneWAvgReturns = "\(item.oneWReturn ?? 0.00)"
            lblAvgReturns.text = String(format: "%.2f", Double(oneWAvgReturns)!)
            
        }
        else if(colName == "1M"){
            let AvgReturns = "\(item.oneMReturn ?? 0.00)"
            lblAvgReturns.text = String(format: "%.2f", Double(AvgReturns)!)
            
        }
        else if(colName == "3M"){
            let threeMReturn = "\(item.threeMReturn ?? 0.00)"
            lblAvgReturns.text = String(format: "%.2f", Double(threeMReturn)!)
        }
        else if(colName == "6M"){
            let sixMReturn = "\(item.sixMReturn ?? 0.00)"
            lblAvgReturns.text = String(format: "%.2f", Double(sixMReturn)!)
        }
        else if(colName == "1Y"){
            let yrAvgReturn = "\(item.yrAvgReturn ?? 0.00)"
            lblAvgReturns.text = String(format: "%.2f", Double(yrAvgReturn)!)
        }
        else if(colName == "3Y"){
            
            let threeYrAvgReturn = "\(item.threeYrAvgReturn ?? 0.00)"
            lblAvgReturns.text = String(format: "%.2f", Double(threeYrAvgReturn)!)
        }
        else if(colName == "5Y"){
            let fiveYrAvgReturn = "\(item.fiveYrAvgReturn ?? 0.00)"
            lblAvgReturns.text = String(format: "%.2f", Double(fiveYrAvgReturn)!)
        }
        else
        {
            let oneYReturn = "\(item.yrAvgReturn ?? 0.00)"
            lblAvgReturns.text = String(format: "%.2f", Double(oneYReturn)!)
            
        }
    }
    
}
