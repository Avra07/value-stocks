//
//  WebService.swift
//  DLevels
//
//  Created by Dynamic-Mac-01 on 12/01/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import Foundation

class WebService: NSObject {
    
    func callService(urladdr: String, methodtype: String, parameter: String, istoken: Bool, tokenval: String){
        
       
        
        var request = URLRequest(url: URL(string: urladdr)!)
        request.httpMethod = methodtype
        let postString = parameter
        request.httpBody = postString.data(using: .utf8)
        
        if(istoken)
        {
            request.setValue(tokenval , forHTTPHeaderField: "Authorization")
        }
        
        
        
         URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(error)")
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
            }
            
            let responseString = String(data: data, encoding: .utf8)
            print("responseString = \(responseString)")
            
            

            
        }.resume()
        
        
        //task.resume()
    }
}
