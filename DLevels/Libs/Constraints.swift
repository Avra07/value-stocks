//
//  Constraints.swift
//  DLevels
//
//  Created by Dynamic-Mac-02 on 29/12/16.
//  Copyright © 2016 Dynamic-Mac-01. All rights reserved.
//

import Foundation
import UIKit


struct Referral_Data {
    
    static var sharing_screen_content = ""
    static var whatsapp_telegram_msg = ""
    static var tweet_sms = ""
    static var facebook_feed_title = ""
    static var how_referral_works = ""
    static var how_scratch_card_works = ""
    static  var email_subject = ""
    static  var email_body = ""
    
}




struct mfselectedFilter {
    static var mffilterParameter = [mfFilterResult]()
    static var mfqueryText = ""
    static var mfdispalyQueryArr = [String]()
    static var mfdisplayText = ""
    static var mffilterGroupDataSource = [mffilterGroup]()
}




struct selectedFilter {
    static var filterParameter = [FilterResult]()
    static var queryText = ""
    static var dispalyQueryArr = [String]()
    static var displayText = ""
    static var filterGroupDataSource = [filterGroup]()
}


struct Constants {
    static let someNotification = "TEST"
}

struct StoryBoard {
    static let mf = UIStoryboard(name: "MFStoryboard", bundle: nil)
    static let main = UIStoryboard(name: "Main", bundle: nil)
    
}

struct Subscription_Data {
    static var status = false
    
}



struct Urls {
    
    static let main_url                 =   "https://ws.dlevels.com/"
    static let mfFilter_url             =   "https://ws-mf.dlevels.com/mfund/"
    
    
    
    static var login                    =   main_url + "login"
    static var signUp                   =   main_url + "register"
    static var processLoginDetails      =   main_url + "process-login-details"
    
    static var logOut                   =   main_url + "logout"
    static var getUser                  =   main_url + "get-user"
    
    static var resendOtp                =   main_url + "generate-otp"
    static var accountStatus            =   main_url + "account-status"
    static var checkPhoneOtp            =   main_url + "check-phone-otp"
    static var forGotPassword           =   main_url + "forgot-password"
    static var changepassword           =   main_url + "change-password"
    
    static var generatePhoneOtp         =   main_url + "generate-phone-otp"
    static var generateEmailOtp         =   main_url + "generate-email-otp"
    static var updatePhoneNo            =   main_url + "update-phone"
    static var updateEmail              =   main_url + "update-email"
    static var dashboardTicker          =   main_url + "dashboard-ticker"
    static var onBoarding               =   main_url + "onboarding"
    static var multibaggerlist          =   main_url + "multibagger-list"
    static var datewise_Performence     =   main_url + "multibagger-daywise-performance"
    static var sectorwise_Performence   =   main_url + "multibagger-sector"
    static var sectorwise_Performence_Quarter   =   main_url + "multibagger-sector-quarterly"
    static var multibaggerStatus        =   main_url + "multibagger-stats"
    static var multibagger_call_history =   main_url + "multibagger-call-history"
    static var past_Performance         =   main_url + "multibagger-stats"
    static var sector_Wise_Symbol       =   main_url + "multibagger-sector-wise-symbol"
    static var sector_Wise_Symbol_Quarterly       =   main_url + "multibagger-sector-wise-symbol-quarterly"
    static var multibagger_gainers      =   main_url + "multibagger-gainers"
    static var wellcome_phone           =   main_url + "wellcome-phone"
    static var wellcome_email           =   main_url + "wellcome-email"
    static var chart_data               =   main_url + "chart-data"
    static var correction_report        =   main_url + "correction-report"
    static var live_price               =   main_url + "live-price"
    static var check_version                        =   main_url + "check-version"
    static var webinar_video                        =   main_url + "get-video"
    static var autosearch_stock                     =   main_url + "get-autosearch-stock"
    static var portfolio_checker                    =   main_url + "portfolio-checker"
    
    static var portfolio_checker_new                    =   main_url + "portfolio-checker-new"
    
    static var seminar_ad                           =   main_url + "seminar-ad"
    static var sector_performance_detail            =   main_url + "sector-performance-detail"
    static var country_code                         =   main_url + "country-code"
    static var login_without_emailverification      =   main_url + "login-without-email"
    
    
    static var indexstock               = main_url + "index-stock-v1"
    static var indexfactsheet           = main_url + "index-factsheet-v1"
    
    static var NRIFPI               = main_url + "index-nri-fpi"
    static var PMS                  = main_url + "index-pms-hnis"
    static var DPP                  = main_url + "index-dp-list"
    
    static var NRIFPIPMS            = main_url + "nri-fpi-pms"
    
    static var addUpdateFCMToken    = main_url + "add-update-fcm-token"
    static var getNearLevels        = main_url + "near-levels"
    static var getLadderCombined    = main_url + "ladder-combined"
    
    
    // Mutual Fund
    static var mfcategories     = main_url + "mf-categories"
    static var mffundtypelist   = main_url + "mf-fundtype-list"
    static var mffunddetails    = main_url + "mf-fundlist"
    static var mfdata           = main_url + "mf-data"
    static var mfsearch           = main_url + "mf-search"
    static var mfgetFundDetails = main_url + "fund-details-regular"
    static var mfgetFunds = mfFilter_url + "fund-details-regular"
    
    static var getindexhistory = main_url + "get-index-history"
    static var getpmspdf = main_url + "get-pms-pdf"
	
	static var getstckmarktodaypdf = main_url + "get-smt-pdf"
    
    static var stock_details_report = main_url + "stock-details-report-new"
	
	static var getoireport = main_url + "get-oi-report"
    
    static var getfilterlist = main_url + "get-filter-list-test"
    
    static var getsectorlist = main_url + "get-sector-list"
    static var get_filter_result = main_url + "get-rpt-ace-filter-data"
    
    static var getStockDetailsDataHistory = main_url + "get-stock-details-data-history"
    
    static var get_notification = main_url + "get-notification"
    static var notification_read_status = main_url + "notification-read-status"
    
    static var get_user_subscription = main_url + "get-user-subscription"
    static var get_saved_filter_list = main_url + "get-saved-filter-list"
    static var get_app_banner_data = main_url + "get-app-banner-data"
    
    static var get_strategies = main_url + "get-strategies"
    
    static var get_benifits_details = main_url + "get-benifits-details"
    
    //MFFilter URLs
    static var get_filter_list = mfFilter_url + "get-filter-list"
    static var rpt_filter_results = mfFilter_url + "rpt-filter-results"
    
    static var get_financial_guru = main_url + "get-app-financial-guru"
    
    static var get_guru_filter = main_url + "get-guru-filter"
    
    
    static var get_filter_fund_list = mfFilter_url + "get-filter-fund-list"
    
    static var redeem_subscription_coupon = main_url + "redeem-subscription-coupon"
    
    static var get_video_list = main_url + "get-video-list"
    
    static var get_refer_content = main_url + "get-refer-content"
    
    static var get_user_scratchcard_list = main_url + "get-user-scratch-card-list"
    
    static var update_scratch_card_status = main_url + "update-scratch-card-status"
    
    static var addreferralinvite = main_url + "add-referral-invite"
     static var get_app_subscription_benefits = main_url + "get-app-subscription-benefits"
    
}

struct Url {
    
    
    /*  //MARK: DEV URLS
     static let main_url                 =     "https://dev-mf.dlevels.com/mfund/"
     static let uploadFileUrl            =     "https://dev-mf.dlevels.com/"
     static let file_download_url        =     "https://dev-mf.dlevels.com/user_data/"
     static let ucc_url                  =     "http://mf-ws.dlevels.com/BSEStARMFWEB/"
     static var getpaymentlink           =     "http://mf-ws.dlevels.com/BSEStARMFPGWEB/get-payment-link"
     
     */
    static let dlevelsUrl               =   "https://ws.dlevels.com/" // leave this urls as it is
    
    
    
    //LIVE URLS
    static let main_url                 =   "https://ws-mf.dlevels.com/mfund/"
    static let uploadFileUrl            =   "http://ws-mf.dlevels.com/"
    static let file_download_url        =   "http://ws-mf.dlevels.com/user_data/"
    static let ucc_url                  =   "http://bse-api.mfdirect.com/mf-services/"
    static var getpaymentlink           =   "http://bse-api.mfdirect.com/mf-pg-services/get-payment-link"
    
    static var getPaymentStatus      =   ucc_url + "get-payment-status"
    
    static var updatePaymentStatus      =   main_url + "update-order-payment-status"
    static var pandingPaymentConfir     =   main_url + "pending-payment-confirmation"
    
    static let getBankNameList          =   main_url + "get-bank-name-list"
    static let getBankStateList         =   main_url + "get-bank-state-list"
    static let getBankCityList          =   main_url + "get-bank-city-list"
    static let getBranchkList           =   main_url + "get-bank-branch-list"
    
    static let processOrderCancellation =   main_url + "process-order-cancellation"
    static let processSIPCancellation   =   main_url + "process-sip-cancellation"
    static let getMandateForPendingSign =   main_url + "get-mandates-for-pending-signs"
    static let processInvestMore        =   main_url + "process-additional-purchase"
    
    static let sendMandateEmail         =   main_url + "send-mandate-mail"
    static let sendAlert                =   main_url + "send-alert"
    static var login                    =   main_url + "login"
    static var signUp                   =   main_url + "register"
    static var processLoginDetails      =   main_url + "set-login-log" //"process-login-details"
    static var stock_details_report     = dlevelsUrl + "stock-details-report-new"
    static var logOut                   =   main_url + "logout"
    //static var getUser                  =   main_url + "get-user"
    static var getUser                  =    main_url + "get-user" //uploadFileUrl + "get-user-details-dev"
    static var resendOtp                =   main_url + "generate-otp"
    static var accountStatus            =   main_url + "account-status"
    static var checkPhoneOtp            =   main_url + "check-phone-otp"
    static var forGotPassword           =   main_url + "forgot-password"
    static var changepassword           =   main_url + "change-password"
    
    static var generatePhoneOtp         =   main_url + "generate-phone-otp"
    static var generateEmailOtp         =   main_url + "generate-email-otp"
    static var updatePhoneNo            =   main_url + "update-phone"
    static var updateEmail              =   main_url + "update-email"
    static var dashboardTicker          =   main_url + "dashboard-ticker"
    static var onBoarding               =   main_url + "onboarding"
    static var multibaggerlist          =   main_url + "multibagger-list"
    static var datewise_Performence     =   main_url + "multibagger-daywise-performance"
    static var sectorwise_Performence   =   main_url + "multibagger-sector"
    static var sectorwise_Performence_Quarter   =   main_url + "multibagger-sector-quarterly"
    static var multibaggerStatus        =   main_url + "multibagger-stats"
    static var multibagger_call_history =   main_url + "multibagger-call-history"
    static var past_Performance         =   main_url + "multibagger-stats"
    static var sector_Wise_Symbol       =   main_url + "multibagger-sector-wise-symbol"
    static var sector_Wise_Symbol_Quarterly       =   main_url + "multibagger-sector-wise-symbol-quarterly"
    static var multibagger_gainers      =   main_url + "multibagger-gainers"
    static var wellcome_phone           =   main_url + "wellcome-phone"
    static var wellcome_email           =   main_url + "wellcome-email"
    static var chart_data               =   dlevelsUrl + "chart-data"
    static var correction_report        =   main_url + "correction-report"
    static var live_price               =   main_url + "live-price"
    static var check_version                        =   main_url + "check-version"
    static var webinar_video                        =   main_url + "get-video"
    static var autosearch_stock                     =   dlevelsUrl + "get-autosearch-stock"
    static var portfolio_checker                    =   main_url + "portfolio-checker"
    
    static var portfolio_checker_new                =   dlevelsUrl + "portfolio-checker-new"
    static var getbanner                            =   uploadFileUrl + "get-banner"
    static var seminar_ad                           =   main_url + "seminar-ad"
    static var sector_performance_detail            =   dlevelsUrl + "sector-performance-detail"
    static var country_code                         =   dlevelsUrl + "country-code"
    static var login_without_emailverification      =   main_url + "login-without-email"
    
    
    static var indexstock               = main_url + "index-stock-v1"
    static var indexfactsheet           = main_url + "index-factsheet-v1"
    
    static var NRIFPI               = main_url + "index-nri-fpi"
    static var PMS                  = main_url + "index-pms-hnis"
    static var DPP                  = main_url + "index-dp-list"
    
    static var NRIFPIPMS            = main_url + "nri-fpi-pms"
    
    static var addUpdateFCMToken    = main_url + "add-update-fcm-token"
    static var getNearLevels        = dlevelsUrl + "near-levels"
    static var getLadderCombined    = dlevelsUrl + "ladder-combined"
    
    
    // Mutual Fund
    static var mfcategories     = main_url + "mf-categories"
    static var mffundtypelist   = main_url + "mf-fundtype-list"
    static var mffunddetails    = main_url + "mf-fundlist"
    static var mfdata           = main_url + "mf-data"
    static var mfsearch          = main_url + "mf-search"
    static var mfgetFundDetails = main_url + "mf-fund-details"
    
    static var getindexhistory  = main_url + "get-index-history"
    static var getpmspdf        = main_url + "get-pms-pdf"
    
    static var mffundmanagers   = main_url + "get-report-mf-fund-managers"
    static var mffundhouse   = main_url + "get-report-mf-amcs"
    static var ourfunds   = main_url + "get-rec-fund-list"
    
    static var mffundmanagersSchemes   = main_url + "mf-fund-manager-schemes"
    static var mffundhouseSchemes   = main_url + "get-report-mf-amc-wise-schemes"
    static var mffundholdings   = main_url + "get-mf-holdings"
    
    
    
    // KYC
    static var addedituserdetails      = main_url + "add-edit-user-details"
    static var pandetails              = main_url + "pan-details"
    static var getstaticCodes          = main_url + "get-static-codes"
    static var getaccountdetails       = main_url + "get-account-details"
    static var getuserdetails          = main_url + "get-user-details"
    //static var getuserdetails          = main_url + "get-user-details-dev"
    
    static var multiuploaddocument     = uploadFileUrl + "multi-upload-document"
    static var getpincodedetails       = main_url + "get-pincode-details"
    static var getbankbyifsc           = main_url + "get-bank-by-ifsc"
    static var addeditbankdetails      = main_url + "add-edit-bank-details"
    static var addbankproof            = main_url + "add-bank-proof"
    static var clientucc               = ucc_url  + "client-ucc"
    
    // MF Investment
    static var getschemeinvestmentdetails       = main_url + "get-scheme-investment-details"
    static var processorder                     = main_url + "process-order"
    //    static var getpaymentlink                   = "http://mf-ws.dlevels.com/BSEStARMFPGWEB/get-payment-link"
    
    static let mfDashboard                      = main_url + "get-client-portfolio"
    static let mfTransactionHistory             = main_url + "report-orders-for-app"
    static let mfMySip                          = main_url + "get-sip-report"
    static let mfCancelSip                      = main_url + "process-sip-cancellation"
    static let mfGetAddress                     = main_url + "get-user-details"
    static let mfGetBankAccounts                = main_url + "get-user-bank-list"
    static let reportOrdersForApp               = main_url + "report-orders-for-app"
    static let getSchemeRedemptionDetails       = main_url + "get-scheme-redemption-details"
    static let mfRedemption                     = main_url + "process-redemption"
    static let mfGetBankAccountsDetails         = main_url + "get-report-mandate"
    
    static let callBackRequest                      = main_url + "set-call-back-request"
    static let mfgetnavticker                       = main_url + "get-nav-ticker"
    static let mfriskprofile                        = main_url + "get-rp"
    static let mfriskpassessmentquestionsanswer     = main_url + "get-rpq"
    static let mfgetschemenavhistory                = main_url + "get-scheme-nav-history"
    static let mfsaverskassessment                  = main_url + "prc-rp"
    static let mfaddmandate                         = ucc_url  + "add-mandate"
    static let mfaddbank                            = ucc_url  + "add-new-bank"
    static let mfsetdefaultbank                     = ucc_url  + "set-default-bank"
    static let mfupdatedefaultmandate               = main_url  + "update-default-mandate"
    static let mfupdatemandatessign                   = main_url  + "update-mandates-sign"
    static let setfcmtoken                           = main_url  + "set-fcm-token"
    
    static let getnotification                      = main_url  + "get-notification"
    static let getreadstatus                       = main_url  + "notification-read-status"
    
}


struct DisclaimerUrl {
    static let main_url                 =   "http://www.dynamiclevels.com/vsapp-html/vs-disclaimer.html?"
    static let general                       = main_url  + "general"
    static let stock_research                       = main_url  + "stock_research"
    static let stock_screener                       = main_url  + "stock_screener"
    static let value_stocks                       = main_url  + "value_stocks"
    static let sectoral_analysis                       = main_url  + "sectoral_analysis"
    static let mf_screener                       = main_url  + "mf_screener"
    static let guru_s_strategy                       = main_url  + "guru_s_strategy"
    static let nifty_open_interest                       = main_url  + "nifty_open_interest"
    static let mf_research                       = main_url  + "mf_research"
  
}
struct terms {
    static let main_url                 =   "http://www.dynamiclevels.com/vsapp-html/vs-terms-and-conditions.html?"
      static let termscomdition                       = main_url  + "terms-and-conditions"
}

struct Funddetails {
    static var Category     = ""
    static var Fund         = ""
    static var FundName     = ""
    static var scmCode      = ""
    static var ammount      = ""
    static var isInvestMore = false
}




struct Colors {
    static var customBlueColor = UIColor(hex: "004662")
    static var lightBlue = UIColor(hex: "41788e")
    static var headerViewColor = UIColor(hex: "DEEAFF")
    static var viewColor = UIColor(hex: "FAFAFA")
    static var btn_readColor = UIColor(hex: "2E86AB")
    static var btn_LearnColor = UIColor(hex: "2E86D9")
}

struct HeaderText {
    static var headertext : String = ""
    static var guru_Desc = [String:String]()
}


struct switchfilteroptions {
    static var lossMark  = ""
    static var lessvol   = ""
}

struct CountryCode {
    static var countryCode   = ""
}

struct MFStaticData {
    
    static var ARNText   = ""
    static var SEBILink1   = ""
    static var SEBILink2   = ""
    static var SEBIText1   = ""
    static var SEBIText2   = ""
    static var SmallCapPerf   = ""
    static var ason_text   = ""
    static var categoryDesc   = ""
    
}

struct SearchForMultibaggerSector {
    
    static var serchValue   = ""
    static var seg_date     = ""
    
}
struct SearchForCurrentMultibagger {
    
    static var serchValue   = ""
    static var instrument_4     = ""
    static var viewcontrollername = ""
    
    
}
struct MultibaggerType {
    
    static var midCap                       =       "MID"
    static var largeCap                     =       "LARGE"
    
    
    static var midCapForHeatmap             =       "MLT-MID"
    static var largeCapForHeatmap           =       "MLT-LARGE"
    
    static var midCapForPastPerformance     =       "Mid%20Cap"
    static var LargeCapForPastPerformance   =       "Large%20Cap"
    
    
    static var typeOfMultibagger            =       ""
    static var typeOfMultibaggerForHeatMap  =       ""
    static var typeOfMultibaggerForPastPer  =       ""
}

struct ViewToHighLight {
    
    static var vwRiskView               =       ""
    static var vwInvestment             =       ""
    
}


struct FactSheet {
    static var characteristics = [String]()
}

struct MultibaggerStats {
    
    static var segResultQtrArr = [String]()
    static var segQtrDateArr = [String]()
    static var segQtrDisplayArr = [String]()
    
    static var segYearDateArr = [String]()
    static var segYearDisplayArr = [String]()
    
    static var multibaggersmallcaplistdata = [NSDictionary]()
    static var multibaggermidcaplistdata = [NSDictionary]()
    static var multibaggerlargecaplistdata = [NSDictionary]()
    
    
    
    
    static var multibaggersmallcapgainerlooserdata = [NSDictionary]()
    static var multibaggersegmentdata = [NSDictionary]()
    static var multibaggerquarterlysegmentdata = [NSDictionary]()
    
    
    static var multibaggersmallcapyearwisedata = [NSDictionary]()
    
    
    static var multibaggerlargecapgainerlooserdata = [NSDictionary]()
    static var multibaggerlargecapyearwisedata = [NSDictionary]()
    
    static var correctionlevel = [String]()
    static var tblitems = [String]()
    
    static var qutrsmallcapindex = [String]()
    static var qutrmidcapindex = [String]()
    static var qutrniftyindex = [String]()
    
    
    static var newqutrsmallcapindex = [String]()
    static var newqutrmidcapindex = [String]()
    static var newqutrniftyindex = [String]()
    
    
    static var yearsmallcapindex = [String]()
    static var yearniftyindex = [String]()
    
    static var qutrperformancetxt = [String]()
    static var yearperformancetxt = [String]()
    
    static var LargeDisplay = [String]()
    static var MidDisplay = [String]()
    static var SmallDisplay = [String]()
    
    static var NseLargeDisplay = [String]()
    static var NseMidDisplay = [String]()
    static var NseSmallDisplay = [String]()
    
    
    
}


struct seminar_ad{
    
    static var url         		= ""
    static var templete        	= ""
	static var ab_button_action = ""
	static var ab_button_txt = ""
	
    
}

struct Phone {
    
    static let iPhone_5s        =   568       //  UIScreen.main.bounds.size.height - 568
    static let iPhone_7s        =   667       // UIScreen.main.bounds.size.height-667
    static let iPhone_7s_Plus   =   736       // UIScreen.main.bounds.size.height-736
}

struct NotificationData {
    static var  data  = NSDictionary()
    static var  app_id  = ""
    static var  category  = ""
    static var  clickAction = ""
	static var  url  = ""
}

struct User {
    
    static var userId         =   ""
    static var socialId         =   ""
    static var firstName        =   ""
    static var lastName         =   ""
    static var email            =   ""
    static var phone            =   ""
    static var socialToken      =   ""
    static var password         =   ""
    static var regThrough       =   ""
    static var token            =   ""
    static var OnboardinString  =   ""
    static var countryCode      =   ""    
    static var isLandingDataLaded = false
    static var firebaseToken = ""
    
    static var isCommingFromSpecificPage = false
    static var instanceOfSectorPerformance = SectorPerformanceDetailsViewController()
    static var tempIndePath: IndexPath!
    static var navigation = UINavigationController()
    
    static var isAlternetColor = false
    
    static var MF_kyc = ""
    static var MF_ucc_status = ""
    static var MF_UserId = ""
    static var ucc = ""
    
    static var referer_code = ""
    static var fundaccount = ""
    static var refererredby = ""
    
}


struct RegThrough {
    
    static var normal           =   "normal"
    static var gmail            =   "gmail"
    static var facebook         =   "facebook"
}

struct PMS_DATA {
    
    static var PMS1  =   ""
    static var PMS2  =   ""
    static var PMS3  =   ""
    
    
}

struct NRI_DATA {
    
    static var NRI1  =   ""
    static var NRI2  =   ""
    static var NRI3  =   ""
    static var NRI_NEW  =   ""
    
}

struct FPI_DATA {
    
    static var FPI1     =   ""
    static var FPI2     =   ""
    static var FPI3     =   ""
    static var FPI_NEW  =   ""
    
    
}

struct Stock_Filters {
     var Sequence             = "";
     var column_name          = "";
     var default_max          = "";
     var default_min          = "";
     var default_operator     = "";
     var default_value        = "";
     var filter_id            = "";
     var filter_name          = "";
     var filter_options       = "";
     var filter_section       = "";
     var filter_short_name    = "";
     var filter_type          = "";
     var is_active            = "";
     var updt_time            = "";
}

struct Stock_Filters_Modified {
     var Sequence             = "";
     var column_name          = "";
     var default_max          = "";
     var default_min          = "";
     var default_operator     = "";
     var default_value        = "";
     var filter_id            = "";
     var filter_name          = "";
     var filter_options       = "";
     var filter_section       = "";
     var filter_short_name    = "";
     var filter_type          = "";
     var is_active            = "";
     var updt_time            = "";
}


struct SIPToInvestDescription {
    
    static var fundName                         = ""
    static var scmCode                          = ""
    
    static var AMC_CODE                         = "";
    static var AMC_NAME                         = "";
    static var SCHEME_CODE                      = "";
    static var SCHEME_ISIN                      = "";
    static var SCHEME_NAME                      = "";
    static var SCHEME_TYPE                      = "";
    static var SIP_DATES                        = "";
    static var SIP_FREQUENCY                    = "";
    static var SIP_INSTALLMENT_GAP              = "0";
    static var SIP_MAXIMUM_GAP                  = "0";
    static var SIP_MAXIMUM_INSTALLMENT_AMOUNT   = "0";
    static var SIP_MAXIMUM_INSTALLMENT_NUMBERS  = "0";
    static var SIP_MINIMUM_GAP                  = "0";
    static var SIP_MINIMUM_INSTALLMENT_AMOUNT   = "0";
    static var SIP_MINIMUM_INSTALLMENT_NUMBERS  = "0";
    static var SIP_MULTIPLIER_AMOUNT            = "0";
    static var SIP_STATUS                       = "0";
    static var SIP_TRANSACTION_MODE             = "";
    
    //static var isInvestMore                     = false
    static var folioNo                          = ""
    
    static var SIP_INVEST_AMOUNT                = "0";
    static var SIP_DAY_OF_INVESTMENT            = "";
    static var SIP_HTML_MESSAGE                 = "";
    static var SIP_DATE                         = ""
}


struct LUMPSUMToInvestDescription {
    
    static var fundName     = ""
    static var scmCode      = ""
    static var SchemeCount  = "0";
    static var schemes      = "0";
    static var multiplier   =  0
    
    static var LUMPSUM_MAXIMUM_AMOUNT = "0";
    static var LUMPSUM_MINIMUM_AMOUNT = "0";
    
    static var LUMPSUM_INVEST_AMOUNT    = "0";
    static var LUMPSUM_HTML_MESSAGE     = "";
    static var LUMSUM_HTML_MESSAGE      = "";
    //  static var LUMSUM_DETAILS           =   [NSArray]()
    static var LUMSUM_DETAILS            =   [NSDictionary]()
}
