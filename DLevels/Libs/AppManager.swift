//
//  AppManager.swift
//  DLevels
//
//  Created by Dynamic-Mac-02 on 28/12/16.
//  Copyright © 2016 Dynamic-Mac-01. All rights reserved.
//

import Foundation
import UIKit

class AppManager: NSObject {
    
    
    class func GetSelectedFilterArr(queryText: String, displayText:String, completion : @escaping (Bool)->()){
        
        selectedFilter.displayText = ""
        var displayCol = [String]()
       
        if displayText != "" {
            selectedFilter.displayText = displayText
            selectedFilter.dispalyQueryArr.removeAll()
            let dispTxt = displayText.replacingOccurrences(of: "|", with: "")
            selectedFilter.dispalyQueryArr = dispTxt.split(separator: ";").map { String($0) }
        }
        
        if displayText != "" {
            let firstPart = displayText.split(separator: ";").map { String($0) }
            // Market Cap|=|Mid Cap
            
            if firstPart.count > 0 {
                for fP in firstPart {
                    let val = fP.split(separator: "|").map { String($0) }
                   displayCol.append(val[0])
                }
                
            }
        }
        
        
        selectedFilter.queryText =  ""
        selectedFilter.queryText = queryText
        
        // "Market Cap|=|Mid Cap;1 MONTH Performance(IN %)|<|0;6 MONTH Performance(IN %)|>|0;Yr Performance (IN %)|>|0;"
        
        // "MkCapType|=|Small Cap;Yearly_PE|<|20;1Y_Change|>|0;52WL_CH|>|5;Sales_G|>|0;Adj_PAT_G|>|0;Price2Op|<|15;NetDebt|<|1500;
        
       // selectedFilter.filterParameter.removeAll()
        
        
        let ePart: [String] =  selectedFilter.queryText.split(separator: ";").map { String($0) }
        var index = 0
        for expart in ePart{
            
            let iPart: [String] =  expart.split(separator: "|").map { String($0) }
            if iPart.count > 0 {
                
                let fresult = FilterResult()
                
                fresult.filter_short_name = displayCol[index]
                
                fresult.filter_column_name = iPart[0]
                
                index = index + 1
                
                if iPart[1] == ">" {
                    fresult.filter_operator = "greater than"
                    fresult.filter_value = iPart[2]
                }
                if iPart[1] == "<" {
                    fresult.filter_operator = "less than"
                    fresult.filter_value = iPart[2]
                }
                if iPart[1] == "=" {
                    fresult.filter_operator = "equals"
                    fresult.filter_value = iPart[2]
                }
                if iPart[1].lowercased() == "between" {
                    fresult.filter_operator = "between"
                    
                    let minMaxValues: [String] = iPart[2].components(separatedBy: "and")
                    if minMaxValues.count > 0 {
                        fresult.filter_min = minMaxValues[0]
                        fresult.filter_max = minMaxValues[1]
                    }
                }
                
                if iPart[1].lowercased() == "like" {
                    fresult.filter_operator = ""
                    fresult.filter_value = iPart[2]
                    fresult.filter_type = "text"
                }
                
                if iPart[1].lowercased() == "find_in_set" {
                    fresult.filter_operator = ""
                    fresult.filter_value = iPart[2]
                    fresult.filter_type = "dropdown"
                }
                
                
                selectedFilter.filterParameter.append(fresult)
            }
        }
        
        completion(true)
    }
    
    
    
    class func GetQueryDisplayText(filterParam: [FilterResult], completion : @escaping (Bool)->()){
        
        selectedFilter.dispalyQueryArr.removeAll()
        selectedFilter.queryText =  ""
        selectedFilter.displayText = ""
        
        if filterParam.count > 0 {
            for param in filterParam {
                if let ftype:String = param.filter_type as String {
                    
                    
                    if ftype == "multi_select_list" {
                        if let mvalue:String = param.filter_value {
                            if mvalue != "" {
                                selectedFilter.dispalyQueryArr.append("\(param.filter_short_name) : \(mvalue)")
                                selectedFilter.queryText.append("\(param.filter_column_name)|FIND_IN_SET|\(mvalue.replacingOccurrences(of: "&", with: "%26"));")
                                
                                selectedFilter.displayText.append("\(param.filter_short_name)|\(param.filter_operator)|\(mvalue);")
                                
                            }
                        }
                    }
                    
                    
                    if ftype == "dropdown" {
                        if let selvalue:String = param.filter_value {
                            if selvalue != "" {
                                
                                selectedFilter.dispalyQueryArr.append("\(param.filter_short_name) : \(selvalue)")
                                selectedFilter.queryText.append("\(param.filter_column_name)|FIND_IN_SET|\(selvalue);")
                                selectedFilter.displayText.append("\(param.filter_short_name)|\(param.filter_operator)|\(selvalue);")
                                
                            }
                        }
                    }
                    
                    if ftype == "text" {
                        if let txtvalue:String = param.filter_value {
                            if txtvalue != "" {
                                let tvalue = "'%" + txtvalue + "%'"
                                selectedFilter.dispalyQueryArr.append("\(param.filter_short_name) : \(txtvalue)")
                                selectedFilter.queryText.append("\(param.filter_column_name)|Like|\(tvalue);")
                                selectedFilter.displayText.append("\(param.filter_short_name)|\(param.filter_operator)|\(tvalue)")
                                
                            }
                        }
                    }
                    
                    if ftype == "" {
                        if let operator_val:String = param.filter_operator {
                            if operator_val != "" {
                                
                                if operator_val == "between" {
                                    if let min_val:String = param.filter_min {
                                        if let max_val:String = param.filter_max {
                                            
                                            if min_val != "" {
                                                if max_val != ""{
                                                    
                                                    selectedFilter.dispalyQueryArr.append("\(param.filter_short_name) between \(min_val) and \(max_val)")
                                                    selectedFilter.queryText.append("\(param.filter_column_name)|between| \(min_val) and \(max_val);")
                                                    selectedFilter.displayText.append("\(param.filter_short_name)|between|\(min_val) and \(max_val);")
                                                }
                                            }
                                        }
                                    }
                                }
                                if operator_val == "equals" {
                                    
                                    if let mod_val:String = param.filter_value {
                                        if mod_val != ""{
                                            selectedFilter.dispalyQueryArr.append("\(param.filter_short_name) = \(mod_val)")
                                            selectedFilter.queryText.append("\(param.filter_column_name)|=|\(mod_val);")
                                            selectedFilter.displayText.append("\(param.filter_short_name)|=|\(mod_val);")
                                        }
                                    }
                                }
                                
                                if operator_val == "greater than" {
                                    
                                    if let mod_val:String = param.filter_value {
                                        if mod_val != ""{
                                            selectedFilter.dispalyQueryArr.append("\(param.filter_short_name) > \(mod_val)")
                                            selectedFilter.queryText.append("\(param.filter_column_name)|>|\(mod_val);")
                                            selectedFilter.displayText.append("\(param.filter_short_name)|>|\(mod_val);")
                                        }
                                    }
                                }
                                
                                if operator_val == "less than" {
                                    
                                    if let mod_val:String = param.filter_value {
                                        if mod_val != ""{
                                            selectedFilter.dispalyQueryArr.append("\(param.filter_short_name) < \(mod_val)")
                                            selectedFilter.queryText.append("\(param.filter_column_name)|<|\(mod_val);")
                                            selectedFilter.displayText.append("\(param.filter_short_name)|<|\(mod_val);")
                                        }
                                    }
                                }
                                print("Filter Query Array : \(selectedFilter.dispalyQueryArr)")
                                print("Filter Query Text : \(selectedFilter.queryText)")
                            }
                        }
                    }
                }
            }
        }
        
        completion(true)
        
    }
    
   /*********************************************************/
    
    class func GetMFSelectedFilterArr(queryText: String, displayText:String, completion : @escaping (Bool)->()){
        
        mfselectedFilter.mfdisplayText = ""
        var displayCol = [String]()
        
        if displayText != "" {
            mfselectedFilter.mfdisplayText = displayText
            mfselectedFilter.mfdispalyQueryArr.removeAll()
            let dispTxt = displayText.replacingOccurrences(of: "|", with: "")
            mfselectedFilter.mfdispalyQueryArr = dispTxt.split(separator: ";").map { String($0) }
        }
        
        if displayText != "" {
            let firstPart = displayText.split(separator: ";").map { String($0) }
            // Market Cap|=|Mid Cap
            
            if firstPart.count > 0 {
                for fP in firstPart {
                    let val = fP.split(separator: "|").map { String($0) }
                    displayCol.append(val[0])
                }
                
            }
        }
        
        
        mfselectedFilter.mfqueryText =  ""
        mfselectedFilter.mfqueryText = queryText
        
        // "Market Cap|=|Mid Cap;1 MONTH Performance(IN %)|<|0;6 MONTH Performance(IN %)|>|0;Yr Performance (IN %)|>|0;"
        
        // "MkCapType|=|Small Cap;Yearly_PE|<|20;1Y_Change|>|0;52WL_CH|>|5;Sales_G|>|0;Adj_PAT_G|>|0;Price2Op|<|15;NetDebt|<|1500;
        
        // selectedFilter.filterParameter.removeAll()
        
        
        let ePart: [String] =  mfselectedFilter.mfqueryText.split(separator: ";").map { String($0) }
        var index = 0
        for expart in ePart{
            
            
            
            let iPart: [String] =  expart.split(separator: "|").map { String($0) }
            if iPart.count > 0 {
                
                let fresult = mfFilterResult()
                
                fresult.filter_short_name = displayCol[index]
                
                fresult.filter_column_name = iPart[0]
                
                index = index + 1
                
                if iPart[1] == ">" {
                    fresult.filter_operator = "greater than"
                    fresult.filter_value = iPart[2]
                }
                if iPart[1] == "<" {
                    fresult.filter_operator = "less than"
                    fresult.filter_value = iPart[2]
                }
                if iPart[1] == "=" {
                    fresult.filter_operator = "equals"
                    fresult.filter_value = iPart[2]
                }
                if iPart[1] == "between" {
                    fresult.filter_operator = "between"
                    
                    let minMaxValues: [String] = iPart[2].components(separatedBy: "and")
                    if minMaxValues.count > 0 {
                        fresult.filter_min = minMaxValues[0]
                        fresult.filter_max = minMaxValues[1]
                    }
                }
                
                if iPart[1] == "LIKE" {
                    fresult.filter_operator = ""
                    fresult.filter_value = iPart[2]
                    fresult.filter_type = "text"
                }
                
                if iPart[1] == "FIND_IN_SET" {
                    fresult.filter_operator = ""
                    fresult.filter_value = iPart[2]
                    fresult.filter_type = "dropdown"
                }
                
                
                mfselectedFilter.mffilterParameter.append(fresult)
            }
        }
        
        completion(true)
    }
    
    
    
    class func GetMFQueryDisplayText(filterParam: [mfFilterResult], completion : @escaping (Bool)->()){
        
        mfselectedFilter.mfdispalyQueryArr.removeAll()
        mfselectedFilter.mfqueryText =  ""
        mfselectedFilter.mfdisplayText = ""
        
        if filterParam.count > 0 {
            for param in filterParam {
                if let ftype:String = param.filter_type as String {
                    
                    
                    if ftype == "multi_select_list" {
                        if let mvalue:String = param.filter_value {
                            if mvalue != "" {
                                mfselectedFilter.mfdispalyQueryArr.append("\(param.filter_short_name) : \(mvalue)")
                                mfselectedFilter.mfqueryText.append("\(param.filter_column_name)|FIND_IN_SET|\(mvalue.replacingOccurrences(of: "&", with: "%26"));")
                                
                                mfselectedFilter.mfdisplayText.append("\(param.filter_short_name)|\(param.filter_operator)|\(mvalue);")
                                
                            }
                        }
                    }
                    
                    
                    if ftype == "dropdown" {
                        if let selvalue:String = param.filter_value {
                            if selvalue != "" {
                                
                                mfselectedFilter.mfdispalyQueryArr.append("\(param.filter_short_name) : \(selvalue)")
                                mfselectedFilter.mfqueryText.append("\(param.filter_column_name)|FIND_IN_SET|\(selvalue);")
                                mfselectedFilter.mfdisplayText.append("\(param.filter_short_name)|\(param.filter_operator)|\(selvalue);")
                                
                            }
                        }
                    }
                    
                    if ftype == "text" {
                        if let txtvalue:String = param.filter_value {
                            if txtvalue != "" {
                                let tvalue = "'%" + txtvalue + "%'"
                                mfselectedFilter.mfdispalyQueryArr.append("\(param.filter_short_name) : \(tvalue)")
                                mfselectedFilter.mfqueryText.append("\(param.filter_column_name)|Like|\(tvalue);")
                                mfselectedFilter.mfdisplayText.append("\(param.filter_short_name)|\(param.filter_operator)|\(tvalue)")
                                
                            }
                        }
                    }
                    
                    if ftype == "" {
                        if let operator_val:String = param.filter_operator {
                            if operator_val != "" {
                                
                                if operator_val == "between" {
                                    if let min_val:String = param.filter_min {
                                        if let max_val:String = param.filter_max {
                                            
                                            if min_val != "" {
                                                if max_val != ""{
                                                    
                                                    mfselectedFilter.mfdispalyQueryArr.append("\(param.filter_short_name) between \(min_val) and \(max_val)")
                                                    mfselectedFilter.mfqueryText.append("\(param.filter_column_name)|between| \(min_val) and \(max_val);")
                                                    mfselectedFilter.mfdisplayText.append("\(param.filter_short_name)|between|\(min_val) and \(max_val);")
                                                }
                                            }
                                        }
                                    }
                                }
                                if operator_val == "equals" {
                                    
                                    if let mod_val:String = param.filter_value {
                                        if mod_val != ""{
                                            mfselectedFilter.mfdispalyQueryArr.append("\(param.filter_short_name) = \(mod_val)")
                                            mfselectedFilter.mfqueryText.append("\(param.filter_column_name)|=|\(mod_val);")
                                            mfselectedFilter.mfdisplayText.append("\(param.filter_short_name)|=|\(mod_val);")
                                        }
                                    }
                                }
                                
                                if operator_val == "greater than" {
                                    
                                    if let mod_val:String = param.filter_value {
                                        if mod_val != ""{
                                            mfselectedFilter.mfdispalyQueryArr.append("\(param.filter_short_name) > \(mod_val)")
                                            mfselectedFilter.mfqueryText.append("\(param.filter_column_name)|>|\(mod_val);")
                                            mfselectedFilter.mfdisplayText.append("\(param.filter_short_name)|>|\(mod_val);")
                                        }
                                    }
                                }
                                
                                if operator_val == "less than" {
                                    
                                    if let mod_val:String = param.filter_value {
                                        if mod_val != ""{
                                            mfselectedFilter.mfdispalyQueryArr.append("\(param.filter_short_name) < \(mod_val)")
                                            mfselectedFilter.mfqueryText.append("\(param.filter_column_name)|<|\(mod_val);")
                                            mfselectedFilter.mfdisplayText.append("\(param.filter_short_name)|<|\(mod_val);")
                                        }
                                    }
                                }
                                print("Filter Query Array : \(mfselectedFilter.mfdispalyQueryArr)")
                                print("Filter Query Text : \(mfselectedFilter.mfqueryText)")
                            }
                        }
                    }
                }
            }
        }
        
        completion(true)
        
    }
    
    /*******************************************************/
    
    
    
    
    class func setPositiveValue(value: Double) -> String {
        //return value >= 0 ? "\(value)" : " - "
        
        if "\(value)" == "-999" || "\(value)" == "-999.0"{
            return "-"
        } else {
            let splitArray = "\(value)".components(separatedBy: ".")
            if splitArray.count > 1 {
                if splitArray[1].count == 1 {
                    return "\(splitArray[0]).\(splitArray[1]  + "0")"
                } else if splitArray[1].count > 1 {
                    return "\(value.rounded(toPlaces: 2))"
                }
            } else {
                return "\(value)"
            }
        }
        
        return "\(value)"
        //   return "\(value)" != "-999.0" ? "\(value)" : " - "
    }
    
    
    func shouldAutorotate() -> Bool {
        if (UIDevice.current.orientation == UIDeviceOrientation.portrait ||
            UIDevice.current.orientation == UIDeviceOrientation.portraitUpsideDown ||
            UIDevice.current.orientation == UIDeviceOrientation.unknown) {
            return true;
        } else {
            return false;
        }
    }
    
    func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }
    
    //MARK: GET ALTERNET COLOR
     class func getAlternetColor()-> String {
        User.isAlternetColor = !User.isAlternetColor
        return User.isAlternetColor ? "E0E5E8" :"F2F3F5"
    }
    
    func halfTextRedColorChange(fullText : String , changeText : String ) -> NSMutableAttributedString {
        let strNumber: NSString = fullText as NSString
        let range = (strNumber).range(of: changeText)
        let attribute = NSMutableAttributedString.init(string: fullText)
        attribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.red , range: range)
        return attribute
    }
    
    func halfTextGreenColorChange(fullText : String , changeText : String ) -> NSMutableAttributedString {
        let strNumber: NSString = fullText as NSString
        let range = (strNumber).range(of: changeText)
        let attribute = NSMutableAttributedString.init(string: fullText)
        attribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.red , range: range)
        return attribute
    }
    
    
    
    func setStatusBarBackgroundColor() {
        
        let color = UIColor.init(colorLiteralRed: 248/255, green: 200/255, blue: 74/255, alpha: 1)
        guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
        statusBar.backgroundColor = color
    }
    
    
    
    // MARK: Validate Email
    func isValidEmail(email:String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
        
    }
    
    
    // MARK: Validate Phone No
    func isValidPhoneNo(phoneNo: String) -> Bool {
        
        let PHONE_REGEX = "^\\d{3}-\\d{3}-\\d{4}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: phoneNo)
        return result
        
    }
    
    //MARK: GET ALTERNET COLOR
    class func getAlternetColor(alternetColor : Bool = false)-> String {
        User.isAlternetColor = !alternetColor
        return User.isAlternetColor ? "EFEFF4" :"CFCFCF"
    }
    
    class func showAlertMessage(title: String, message: String, navigationController: UINavigationController = User.navigation) {
        
        DispatchQueue.main.async(execute: {
            
            let alertViewController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let alertAction = UIAlertAction(title: "OK", style: .default) { (alertAction) in
                
            }
            alertViewController.addAction(alertAction)
            navigationController.present(alertViewController, animated: true, completion: nil)
            
        })
    }
    class func pushToSearchScreen(){
        let loginCont = StoryBoard.mf.instantiateViewController(withIdentifier: "MF_SearchViewController") as! MF_SearchViewController
        User.navigation.pushViewController(loginCont, animated: true)
    }
    
    func showAlert(title: String, message: String, navigationController: UINavigationController) {
        
        
        DispatchQueue.main.async(execute: {
            
            let alertViewController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let alertAction = UIAlertAction(title: "OK", style: .default) { (alertAction) in
                
            }
            alertViewController.addAction(alertAction)
            navigationController.present(alertViewController, animated: true, completion: nil)
            
        })
    }
    
  
    
    // MARK: Dispath After
    class func delay(_ delay:Double, closure:@escaping ()->()) {
        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
    }
    
    /*
     delay(0.4) {
     // do stuff
     }
     */
    
    
    
    
    
    
  /*  func pushTo(viewController: String, navigationController: UINavigationController){
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController")
        //     self.navigationController?.pushViewController(viewController, animated: true)
        
    } */
    
}


struct AppUtility {
    
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {
        
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.orientationLock = orientation
        }
    }
    
    /// OPTIONAL Added method to adjust lock and rotate to the desired orientation
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation:UIInterfaceOrientation) {
        
        self.lockOrientation(orientation)
        
        UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
    }
    
}
