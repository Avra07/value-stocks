//
//  AppDelegate.swift
//  DLevels
//
//  Created by Dynamic-Mac-01 on 23/12/16.
//  Copyright © 2016 Dynamic-Mac-01. All rights reserved.
//

//Avra00000

import UIKit
import Fabric
import Crashlytics
import Firebase
import UserNotifications


import FBSDKLoginKit
import FBSDKCoreKit



@UIApplicationMain
class AppDelegate: UIResponder,  UIApplicationDelegate{
    
    let currencyCode = "inr"
    var window: UIWindow?
    
    var applicationStateString: String {
        if UIApplication.shared.applicationState == .active {
            return "active"
        } else if UIApplication.shared.applicationState == .background {
            return "background"
        }else {
            return "inactive"
        }
    }
    
    var myInterstitial : GADInterstitial?
    
    var orientationLock = UIInterfaceOrientationMask.all
   
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        // Override point for customization after application launch.
        UIApplication.shared.statusBarStyle = .lightContent
        
        UIApplication.shared.isStatusBarHidden = false
        
        
		
        FirebaseApp.configure()
        Fabric.with([Crashlytics.self])
        
        
        //InviteReferrals.setup(withBrandId: 28287, encryptedKey:"B0734EA90A4F2612437CBE2D3E3E167C")
        
         GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        
        
        // [START set_messaging_delegate]
        Messaging.messaging().delegate = self
        // [END set_messaging_delegate]
		
		// Add observer for InstanceID token refresh callback.
		// Define identifier
		let notificationIdentifier: String = "NotificationIdentifier"
		
		NotificationCenter.default.addObserver(self, selector: #selector(self.tokenRefreshNotification), name: NSNotification.Name(rawValue: notificationIdentifier), object: nil)
		
        let fcmDeviceToken = InstanceID.instanceID().token()
		User.firebaseToken = fcmDeviceToken ?? ""
		
		print(fcmDeviceToken)
        

        
        
        // Register for remote notifications. This shows a permission dialog on first run, to
        // show the dialog at a more appropriate time move this registration accordingly.
        // [START register_for_notifications]
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        
        
        application.registerForRemoteNotifications()
        
        // [END register_for_notifications]
        
        // Add observer for InstanceID token refresh callback.
        NotificationCenter.default.addObserver(self,selector: #selector(self.tokenRefreshNotification),
                                               name: .InstanceIDTokenRefresh,
                                               object: nil)
        
        //print(Messaging.messaging().fcmToken!)
        
        
        
        if Messaging.messaging().fcmToken != nil {
            
            let token = Messaging.messaging().fcmToken ?? ""
            // use the following line for Firebase < 4.0.0
            //let token = FIRInstanceID.instanceID().token()
            //print("FCM token: \(token)")
            UserDefaults.standard.setValue(token, forKey: User.firebaseToken)
            
            
        }
        
        
        
        
        
        
        if let userInfo = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] {
            NSLog("[RemoteNotification] applicationState: \(applicationStateString) didFinishLaunchingWithOptions for iOS9: \(userInfo)")
            //TODO: Handle background notification
            NotificationData.data = NSDictionary()
            
            if let aps = (userInfo as AnyObject)["aps"] as? NSDictionary {
                if let alert = aps["alert"] as? NSDictionary {
                    if let body = alert["body"] as? NSString {
                        print(body)
                    }
                }
				
				if let url = (userInfo as AnyObject)["url"] as? NSString {
					NotificationData.url = url as String
				}
				
                if let category = aps["category"] as? NSString {
                    NotificationData.category = category as String
                    
                    //Capture custom fields
                    if let click_action = (userInfo as AnyObject)["click_action"] as? NSString {
                        print("Click Action: ", click_action);
                        NotificationData.clickAction.removeAll()
                        NotificationData.clickAction.append(click_action as String)
                    }
                    
                }
            }
            
        }

        
        
        return true
    }
	
	
    func requestNotificationAuthorization(application: UIApplication) {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions, completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
    }
    
	
    
    func logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        Crashlytics.sharedInstance().setUserEmail("anwesha@dlevels.com")
        Crashlytics.sharedInstance().setUserIdentifier("dynamic123")
        Crashlytics.sharedInstance().setUserName("DLevels")
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        
        AppEvents.activateApp()
        
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
       
       // NotificationData.clickAction.removeAll()
        
        
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
        
        if(NotificationData.clickAction != ""){
        
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        AppEvents.activateApp()
        
        if(NotificationData.category != ""){
            var pageIdentifier = ""
            
            if(NotificationData.category == "customWebView")
            {
                pageIdentifier  =   "CustomPushViewController"
                let viewController = self.window?.rootViewController?.storyboard?.instantiateViewController(withIdentifier: pageIdentifier) as! CustomPushViewController
                
                User.navigation.pushViewController(viewController, animated: false)
                
            }
            
            if(NotificationData.category == "Stock_Market_Today")
            {
                pageIdentifier  =   "StockMarketTodayViewController"
                let viewController = self.window?.rootViewController?.storyboard?.instantiateViewController(withIdentifier: pageIdentifier) as! StockMarketTodayViewController
                
                User.navigation.pushViewController(viewController, animated: false)
                
            }
            
            if(NotificationData.category == "Value_Stocks")
            {
                pageIdentifier  =   "MultibaggerViewController"
                let viewController = self.window?.rootViewController?.storyboard?.instantiateViewController(withIdentifier: pageIdentifier) as! MultibaggerViewController
                
                User.navigation.pushViewController(viewController, animated: false)
                
            }
            
            if(NotificationData.category == "Nifty_Open_Interest")
            {
                pageIdentifier  =   "NiftyOpenInterestViewController"
                let viewController = self.window?.rootViewController?.storyboard?.instantiateViewController(withIdentifier: pageIdentifier) as! NiftyOpenInterestViewController
                
                User.navigation.pushViewController(viewController, animated: false)
                
            }
            if(NotificationData.category == "Dynamic_Indices")
            {
                pageIdentifier  =   "DynamicIndexController"
                let viewController = self.window?.rootViewController?.storyboard?.instantiateViewController(withIdentifier: pageIdentifier) as! DynamicIndexController
                
                User.navigation.pushViewController(viewController, animated: false)
                
            }
            
            if(NotificationData.category == "PMS")
            {
                var viewController = UIViewController()
                pageIdentifier  =   "PMSPDFViewerViewController"
                if #available(iOS 11.0, *) {
                    viewController = self.window?.rootViewController?.storyboard?.instantiateViewController(withIdentifier: pageIdentifier) as! PMSPDFViewerViewController
                } else {
                    // Fallback on earlier versions
                }
                
                User.navigation.pushViewController(viewController, animated: false)
                
            }
            
            if(NotificationData.category == "Webinars")
            {
                var viewController = UIViewController()
                pageIdentifier  =   "WebinarsViewController"
                if #available(iOS 11.0, *) {
                    viewController = self.window?.rootViewController?.storyboard?.instantiateViewController(withIdentifier: pageIdentifier) as! WebinarsViewController
                } else {
                    // Fallback on earlier versions
                }
                
                User.navigation.pushViewController(viewController, animated: false)
                
            }
            
            if(NotificationData.category == "Sector_Performance")
            {
                var viewController = UIViewController()
                pageIdentifier  =   "MultibaggerQuarterlySectorViewController"
                if #available(iOS 11.0, *) {
                    viewController = self.window?.rootViewController?.storyboard?.instantiateViewController(withIdentifier: pageIdentifier) as! MultibaggerQuarterlySectorViewController
                } else {
                    // Fallback on earlier versions
                }
                
                User.navigation.pushViewController(viewController, animated: false)
                
            }
        }
        
       
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        //self.saveContext()
    }
    
    
    
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        
        
        
        //InviteReferrals.openUrl(with: app, url:  url)
        
        let googleDidHandle = GIDSignIn.sharedInstance().handle(url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String, annotation: [UIApplicationOpenURLOptionsKey.annotation])
        
        let facebookDidHandle = ApplicationDelegate.shared.application(app,
                open: url,sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                annotation: options[UIApplicationOpenURLOptionsKey.annotation])
                                        
        
        return googleDidHandle || facebookDidHandle
    }
    
    // MARK: - Google SignIn Methodes
    
    
    func signInWillDispatch(signIn: GIDSignIn!, error: NSError!) {
     
       // myActivityIndicator.stopAnimating()
    }
    
    // Present a view that prompts the user to sign in with Google
    func signIn(signIn: GIDSignIn!,
                presentViewController viewController: UIViewController!) {
    //    self.presentViewController(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func signIn(signIn: GIDSignIn!,
                dismissViewController viewController: UIViewController!) {
      //  self.dismissViewControllerAnimated(true, completion: nil)
    }


    
	
	func tokenRefreshNotification() {
		connectToFcm()
	}
    
	
	func connectToFcm() {
        guard InstanceID.instanceID().token() != nil else {
            return;
        }
        //Messaging.messaging().disconnect()
        Messaging.messaging().connect { (error) in
            if error != nil {
                print("Unable to connect with FCM. \(String(describing: error))")
            } else {
                print("Connected to FCM.")
            }
        }
	}
    
    

    
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return ApplicationDelegate.shared.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        
    }
    
    
    internal var shouldRotate = false
    func application(_ application: UIApplication,
                     supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        
        if (shouldRotate == true){
            return UIInterfaceOrientationMask.allButUpsideDown
        }
        return UIInterfaceOrientationMask.portrait
        
        //return orientationLock //shouldRotate ? .allButUpsideDown : .portrait
    }
    
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    {
        InstanceID.instanceID().setAPNSToken(deviceToken, type: InstanceIDAPNSTokenType.sandbox)
        InstanceID.instanceID().setAPNSToken(deviceToken, type: InstanceIDAPNSTokenType.prod)
    }

}
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    
    // iOS10+, called when presenting notification in foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
		
		
        let userInfo = notification.request.content.userInfo
        print(userInfo)
        
        if let aps = (userInfo as AnyObject)["aps"] as? NSDictionary {
            if let alert = aps["alert"] as? NSDictionary {
                if let body = alert["body"] as? NSString {
                    print(body)
                }
            }
            if let category = aps["category"] as? NSString {
                NotificationData.category = category as String
                
                //Capture custom fields
                if let click_action = (userInfo as AnyObject)["click_action"] as? NSString {
                    print("Click Action: ", click_action);
                    NotificationData.clickAction.removeAll()
                    NotificationData.clickAction.append(click_action as String)
                   
                }
				
				if let url = (userInfo as AnyObject)["url"] as? NSString {
					NotificationData.url = url as String
				}
                
            }
        }
		
		
	
        
        NSLog("[UserNotificationCenter] applicationState: \(applicationStateString) willPresentNotification: \(userInfo)")
        //TODO: Handle foreground notification
        completionHandler([.alert, .badge, .sound])
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        if let aps = (userInfo as AnyObject)["aps"] as? NSDictionary {
            if let alert = aps["alert"] as? NSDictionary {
                if let body = alert["body"] as? NSString {
                    print(body)
                }
            }
            if let category = aps["category"] as? NSString {
                NotificationData.category = category as String
                
                //Capture custom fields
                if let click_action = (userInfo as AnyObject)["click_action"] as? NSString {
                    print("Click Action: ", click_action);
                    NotificationData.clickAction.removeAll()
                    NotificationData.clickAction.append(click_action as String)
                }
                
            }
        }
		
		if let url = (userInfo as AnyObject)["url"] as? NSString {
			NotificationData.url = url as String
		}
        
        if(NotificationData.category != ""){
            var pageIdentifier = ""
            
            if(NotificationData.category == "customWebView")
            {
                pageIdentifier  =   "CustomPushViewController"
                let viewController = self.window?.rootViewController?.storyboard?.instantiateViewController(withIdentifier: pageIdentifier) as! CustomPushViewController
                
                User.navigation.pushViewController(viewController, animated: false)
                
            }
            
            if(NotificationData.category == "Stock_Market_Today")
            {
                pageIdentifier  =   "StockMarketTodayViewController"
                let viewController = self.window?.rootViewController?.storyboard?.instantiateViewController(withIdentifier: pageIdentifier) as! StockMarketTodayViewController
                
                User.navigation.pushViewController(viewController, animated: false)
                
            }
            
            if(NotificationData.category == "Value_Stocks")
            {
                pageIdentifier  =   "MultibaggerViewController"
                let viewController = self.window?.rootViewController?.storyboard?.instantiateViewController(withIdentifier: pageIdentifier) as! MultibaggerViewController
                
                User.navigation.pushViewController(viewController, animated: false)
                
            }
            
            if(NotificationData.category == "Nifty_Open_Interest")
            {
                pageIdentifier  =   "NiftyOpenInterestViewController"
                let viewController = self.window?.rootViewController?.storyboard?.instantiateViewController(withIdentifier: pageIdentifier) as! NiftyOpenInterestViewController
                
                User.navigation.pushViewController(viewController, animated: false)
                
            }
            if(NotificationData.category == "Dynamic_Indices")
            {
                pageIdentifier  =   "DynamicIndexController"
                let viewController = self.window?.rootViewController?.storyboard?.instantiateViewController(withIdentifier: pageIdentifier) as! DynamicIndexController
                
                User.navigation.pushViewController(viewController, animated: false)
                
            }
            
            if(NotificationData.category == "PMS")
            {
                var viewController = UIViewController()
                pageIdentifier  =   "PMSPDFViewerViewController"
                if #available(iOS 11.0, *) {
                    viewController = self.window?.rootViewController?.storyboard?.instantiateViewController(withIdentifier: pageIdentifier) as! PMSPDFViewerViewController
                } else {
                    // Fallback on earlier versions
                }
                
                User.navigation.pushViewController(viewController, animated: false)
                
            }
            
            if(NotificationData.category == "Webinars")
            {
                var viewController = UIViewController()
                pageIdentifier  =   "WebinarsViewController"
                if #available(iOS 11.0, *) {
                    viewController = self.window?.rootViewController?.storyboard?.instantiateViewController(withIdentifier: pageIdentifier) as! WebinarsViewController
                } else {
                    // Fallback on earlier versions
                }
                
                User.navigation.pushViewController(viewController, animated: false)
                
            }
            
            if(NotificationData.category == "Sector_Performance")
            {
                var viewController = UIViewController()
                pageIdentifier  =   "MultibaggerQuarterlySectorViewController"
                if #available(iOS 11.0, *) {
                    viewController = self.window?.rootViewController?.storyboard?.instantiateViewController(withIdentifier: pageIdentifier) as! MultibaggerQuarterlySectorViewController
                } else {
                    // Fallback on earlier versions
                }
                
                User.navigation.pushViewController(viewController, animated: false)
                
            }
        }
    }
    
    
    class func getAppDelegate() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    
    func getStringValueFormattedAsCurrency(value: String) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.currency
        numberFormatter.currencyCode = currencyCode
        numberFormatter.maximumFractionDigits = 2
        
        let formattedValue = numberFormatter.string(from: NumberFormatter().number(from: value)!)
        return formattedValue!
    }
    
    func getDocDir() -> String {
        return NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    }
    
    
   
    // iOS10+, called when received response (default open, dismiss or custom action) for a notification
   func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = response.notification.request.content.userInfo
        
        
        if let aps = (userInfo as AnyObject)["aps"] as? NSDictionary {
            if let alert = aps["alert"] as? NSDictionary {
                if let body = alert["body"] as? NSString {
                    print(body)
                }
            }
            if let category = aps["category"] as? NSString {
                NotificationData.category = category as String
                
                //Capture custom fields
                if let click_action = (userInfo as AnyObject)["click_action"] as? NSString {
                    print("Click Action: ", click_action);
                    NotificationData.clickAction.removeAll()
                    NotificationData.clickAction.append(click_action as String)
                }
                
            }
        }
	
	
	
		
		
        
        completionHandler([.alert, .badge, .sound])
        
    }
    
   
    
}

extension AppDelegate : MessagingDelegate {

        
        // [START refresh_token]
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
            // print("Firebase registration token: \(fcmToken)")
            
            //UserSession.shared.setFcmToken(fToken: fcmToken)
            
             UserDefaults.standard.setValue(Messaging.messaging().fcmToken, forKey: "firebaseToken")
            
            let dataDict:[String: String] = ["token": fcmToken]
            //NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
            // TODO: If necessary send token to application server.
            // Note: This callback is fired at each app startup and whenever a new token is generated.
            
            Messaging.messaging().subscribe(toTopic: "valuestocks_all")
            Messaging.messaging().subscribe(toTopic: "valuestocks_ios")
            Messaging.messaging().subscribe(toTopic: "valuestocks_ios_test")
            
            
        }
        // [END refresh_token]
        
        
        // [START ios_10_data_message]
        // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
        // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
        func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
            print("Received data message: \(remoteMessage.appData)")
        }
        // [END ios_10_data_message]
    }
    
    
    // The callback to handle data message received via FCM for devices running iOS 10 or above.
    func application(received remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        
        User.firebaseToken = fcmToken
		print(fcmToken)
        
        Messaging.messaging().subscribe(toTopic: "valuestocks_all")
        Messaging.messaging().subscribe(toTopic: "valuestocks_ios")
        Messaging.messaging().subscribe(toTopic: "valuestocks_ios_test")
        
		
		
    }
    
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        User.firebaseToken = fcmToken
        NSLog("[RemoteNotification] didRefreshRegistrationToken: \(fcmToken)")
    }


   
    
    
    // iOS9, called when presenting notification in foreground
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        
        
        
        if let aps = (userInfo as AnyObject)["aps"] as? NSDictionary {
            if let alert = aps["alert"] as? NSDictionary {
                if let body = alert["body"] as? NSString {
                    print(body)
                }
            }
            if let category = aps["category"] as? NSString {
                NotificationData.category = category as String
                
                //Capture custom fields
                if let click_action = (userInfo as AnyObject)["click_action"] as? NSString {
                    print("Click Action: ", click_action);
                    NotificationData.clickAction.removeAll()
                    NotificationData.clickAction.append(click_action as String)
                }
                
            }
        }
    }


