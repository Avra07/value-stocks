//
//  AlertView.swift
//  DLevels
//
//  Created by Dynamic-Mac-01 on 12/01/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import Foundation

class AlertView: NSObject {
    
    func showAlert(vwctrlname: UIViewController,titleofalert: String,msg: String )
    {
        let alert = UIAlertController(title: titleofalert, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        vwctrlname.present(alert, animated: true, completion: nil)
    }
    

}
