//
//  MFFundDetailsViewController.swift
//  DLevels
//
//  Created by Dlevels-MacMini1 on 21/05/18.
//  Copyright © 2018 Dynamic-Mac-01. All rights reserved.
//

import UIKit
class MFFundDetailsViewController: UIViewController {
    @IBOutlet weak var navBarTitle: UINavigationItem!
    @IBOutlet weak var dropdown: UIButton!
    @IBOutlet weak var lbllastcol: UILabel!
    @IBOutlet weak var lblnotes: UILabel!
    @IBOutlet weak var lblheading: UILabel!
    private var _category: String!
    private var _fundname: String!
    var Fund: String!
    @IBOutlet weak var tableVw: UITableView!
    @IBOutlet weak var navbar: UINavigationBar!
    
    fileprivate var dataArray = [MFFundDetails]()
    fileprivate var mainDataArray = [MFFundDetails]()
    private let dataSource = MFFundDetailsDataModel()
    var sortFundFlag = "F"
    var isPageFromMSpecific = "F"
    var FundListTitle = ""
    var Category:String {
        get {
            return _category
        }
        set {
            _category = newValue
        }
    }
    
    var FundName:String {
        get {
            return _fundname
        }
        set {
            _fundname = newValue
        }
    }

    var col = ""
    let items = ["1W Return(%)","1M Return(%)", "3M Return(%)", "6M Return(%)","1Y Return(%)", "3Y Return(%)", "5Y Return(%)"]
    let itemsCol = ["1W","1M", "3M", "6M","1Y", "3Y", "5Y"]

    override func viewDidLoad() {
        super.viewDidLoad()
        AppManager().setStatusBarBackgroundColor()
		
		
		let label = UILabel(frame: CGRect(x:0, y:0, width:400, height:50))
        label.backgroundColor = .clear
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: 15.0)
        label.textAlignment = .left
        label.textColor = UIColor(hex:"284C5A")
        label.text = _fundname
        self.navBarTitle.titleView = label
		
		
		
		var index = 0
        for i in itemsCol {
            if(self.col == i){
                self.lbllastcol.text = items[index]
            }
            index = index + 1
        }
        
        var count = 0
        //lblheading.text = 	"Nifty Smallcap Index Performance (1 Yr) : \(MFStaticData.SmallCapPerf) "
            
        tableVw?.register(MFFundDetailsTableViewCell.nib, forCellReuseIdentifier: MFFundDetailsTableViewCell.identifier)
        tableVw?.delegate = self
        tableVw?.dataSource = self
        
        let obj_MFFundDetailsDM = MFFundDetailsDataModel()
        obj_MFFundDetailsDM.getData(Category: _category, FundName: _fundname, completion: { (dataset) in
            self.dataArray = dataset
            self.mainDataArray = dataset
            if self.Fund != nil{
                if dataset.count > 0{
                    count = 0
                    for dict in dataset {
                            if (dict.fund != self.Fund ){
                                count = count + 1
                                
                            }else{
                                break
                            }
                        }
                    self.tableVw.reloadData()
                    let indexpath = IndexPath(row: count, section: 0)
                    self.tableVw.scrollToRow(at: indexpath as IndexPath, at: .middle, animated: true)
                    }
                }
            
            self.lblnotes.text = MFStaticData.categoryDesc
            self.sort()
            
        })
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        tableVw.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    @IBAction func backClick(_ sender: Any) {
       
        //if isPageFromMSpecific == "F"{
            if let nav = self.navigationController {
                nav.popViewController(animated: true)
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        //} else{
//            let viewCont = self.storyboard?.instantiateViewController(withIdentifier: "MFFundsTypeListViewController") as! MFFundsTypeListViewController
//            viewCont.Category = _category
//            viewCont.TitleVal = FundListTitle
//            self.navigationController?.pushViewController(viewCont, animated: false)
//        }
        
    }
    
     //objNameSorted = mainDataArray.sorted(by: {$0.fund! < $1.fund!})
    // objNCountSorted = mainDataArray.sorted (by: {$0.count < $1.count})
    
    
    @IBAction func searchClick(_ sender: Any) {
        
        
        
        let viewCont = self.storyboard?.instantiateViewController(withIdentifier: "MFSearchViewController") as! MFSearchViewController
        self.navigationController?.pushViewController(viewCont, animated: false)
    }
    
    func sort(){
        if self.col == "1W" {                                                 self.dataArray = self.dataArray.sorted(by: {$0.oneWReturn! > $1.oneWReturn!})
        }
        else if self.col == "1M" {
            self.dataArray = self.dataArray.sorted(by: {$0.oneMReturn! > $1.oneMReturn!})
        }
        else if self.col == "3M" {
            self.dataArray = self.dataArray.sorted(by: {$0.threeMReturn! > $1.threeMReturn!});
        }
        else if self.col == "6M" {
            self.dataArray = self.dataArray.sorted(by: {$0.sixMReturn! > $1.sixMReturn!});
        }
        else if self.col == "1Y" {
            self.dataArray = self.dataArray.sorted(by: {$0.yrAvgReturn! > $1.yrAvgReturn!});
        }
        else if self.col == "3Y" {
            self.dataArray = self.dataArray.sorted(by: {$0.threeYrAvgReturn! > $1.threeYrAvgReturn!});
        }
        else if self.col == "5Y" {
            self.dataArray = self.dataArray.sorted(by: {$0.fiveYrAvgReturn! > $1.fiveYrAvgReturn!});
        }
        
        self.tableVw.reloadData()
        self.view.layoutIfNeeded()
    }
    
    
    @IBAction func btndrpdwnClick(_ sender: Any) {
        
        let actionSheeController = UIAlertController(title:"Data Fields", message: "", preferredStyle: .actionSheet)
        
        for i in items {
            actionSheeController.addAction(UIAlertAction(title: i, style: .default, handler: { (success) in
                self.lbllastcol.text = i
                self.col = self.itemsCol[self.items.index(of: i)!]
                UIView.animate(withDuration: 0.4) {
                    
                    self.sort()
                    
                    
                }
            }))
            
        }
        
        actionSheeController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (success) in
        }) )
        
        // mone rakho
        self.present(actionSheeController, animated: false) {
            
        }
    }
    
    @IBAction func btnFundTypeSort_Action(_ sender: Any) {
        
        if sortFundFlag == "F" {
            dataArray = dataArray.sorted (by: {$0.fund! < $1.fund!})
            sortFundFlag = "T"
        }else{
            dataArray = dataArray.sorted (by: {$0.fund! > $1.fund!})
            sortFundFlag = "F"
        }
        tableVw.reloadData()
    }
    
    @IBAction func btnNetAssetFilter_Action(_ sender: Any) {
        
        let actionSheeController = UIAlertController(title:"", message: "", preferredStyle: .actionSheet)
       
        actionSheeController.addAction(UIAlertAction(title: "Total Net Assets(Cr)", style: .default, handler: { (success) in
            UIView.animate(withDuration: 0.4) {
                self.lblheading.text = "Total Net Assets(Cr)"
                self.dataArray = self.mainDataArray
                self.tableVw.reloadData()
                self.view.layoutIfNeeded()
            }
        }))
        
        actionSheeController.addAction(UIAlertAction(title: "Assets > 100(Cr)", style: .default, handler: { (success) in
            UIView.animate(withDuration: 0.4) {
                self.lblheading.text = "Assets > 100(Cr)"
                self.dataArray = self.mainDataArray.filter { $0.totalNetAssets! > Int(100) }
                self.tableVw.reloadData()
                self.view.layoutIfNeeded()
            }
        }))
        
        actionSheeController.addAction(UIAlertAction(title: "Assets > 250(Cr)", style: .default, handler: { (success) in
            UIView.animate(withDuration: 0.4) {
                self.lblheading.text = "Assets > 250(Cr)"
                self.dataArray = self.mainDataArray.filter { $0.totalNetAssets! > Int(250) }
                self.tableVw.reloadData()
                self.view.layoutIfNeeded()
            }
        }))
        
        actionSheeController.addAction(UIAlertAction(title: "Assets > 500(Cr)", style: .default, handler: { (success) in
            UIView.animate(withDuration: 0.4) {
                self.lblheading.text = "Assets > 500(Cr)"
                self.dataArray = self.mainDataArray.filter { $0.totalNetAssets! > Int(500) }
                self.tableVw.reloadData()
                self.view.layoutIfNeeded()
            }
        }))
        
        actionSheeController.addAction(UIAlertAction(title: "Assets > 1000(Cr)", style: .default, handler: { (success) in
            UIView.animate(withDuration: 0.4) {
                self.lblheading.text = "Assets > 1000(Cr)"
                self.dataArray = self.mainDataArray.filter { $0.totalNetAssets! > Int(1000) }
                self.tableVw.reloadData()
                self.view.layoutIfNeeded()
            }
        }))
        
        
        
        actionSheeController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (success) in
        }) )
        
        // mone rakho
        self.present(actionSheeController, animated: false) {
            
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let index:Int = sender as! Int
        if segue.identifier == "showSpecific" {
            let vc = segue.destination as! MFSpecificPageViewController
            vc.Category = _category
            vc.Fund = dataArray[index].fund!
            vc.FundName = _fundname
           
        }
    }

}
extension MFFundDetailsViewController: UITableViewDataSource,UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dataArray.count == 0 {
            
//            let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
//            noDataLabel.text          = "No data available"
//            noDataLabel.textColor     = UIColor.black
//            noDataLabel.textAlignment = .center
//            tableView.backgroundView  = noDataLabel
//            tableView.separatorStyle  = .none
//            tableView.backgroundView?.isHidden = false
        } else {
           // tableView.separatorStyle = .singleLine
            tableView.backgroundView?.isHidden = true
        }
        
        return dataArray.count
    }
    
    public func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 175
    }
//    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        
//        return UITableViewAutomaticDimension
//    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier:
            MFFundDetailsTableViewCell.identifier, for: indexPath) as? MFFundDetailsTableViewCell
        {
            
            cell.selectionStyle = .none
            
            cell.backgroundColor = indexPath.row % 2 == 0 ?  UIColor(red: 215/255, green: 222/255, blue: 225/255, alpha: 1.0) : UIColor(red: 236/255, green: 239/255, blue: 240/255, alpha: 1.0)
            
            
            //cell.layer.cornerRadius = 10.0;
            
            /*cell.backgroundColor = indexPath.row % 2 == 0 ?  UIColor(red: 215/255, green: 222/255, blue: 225/255, alpha: 1.0) : UIColor(red: 236/255, green: 239/255, blue: 240/255, alpha: 1.0)*/
            
            cell.configureWithItem(item: dataArray[indexPath.item], colName: col)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.dequeueReusableCell(withIdentifier:
            MFFundDetailsTableViewCell.identifier, for: indexPath) as? MFFundDetailsTableViewCell
        {
            self.performSegue(withIdentifier: "showSpecific", sender: indexPath.row)
        }
        
        
    }
    
}
