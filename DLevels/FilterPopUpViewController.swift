//
//  FilterPopUpViewController.swift
//  DLevels
//
//  Created by SAIKAT GHOSH on 07/07/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class FilterPopUpViewController: UIViewController {
    var items = [String]()
    var query  = ""
    let reuseIdentifier = "cell"
    
    
    @IBOutlet weak var vwHeightConst: NSLayoutConstraint!
    
    var isComingFromGurus = false
    
    var islossmakingParam = ""
    var islessVolumeParam = ""
    var header = ""
    // test
    
    @IBOutlet weak var colvw: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let layout = UICollectionViewCenterLayout()
        layout.estimatedItemSize = CGSize(width: 240, height: 40)
        colvw.collectionViewLayout = layout
        
        
        vwHeightConst.constant = CGFloat((selectedFilter.dispalyQueryArr.count * 40) + 60)
        
        
        
    }
    
    override func viewWillLayoutSubviews()
    {
        super.viewWillLayoutSubviews()
        
        //recalculate the collection view layout when the view layout changes
        colvw.collectionViewLayout.invalidateLayout()
    }
    
    //MARK: CROSS BUTTON ACTION
    @IBAction func btnCross_Action(_ sender: UIButton) {
        dismissCurrentPage()
    }
    
    @IBAction func btnokey_Action(_ sender: UIButton){
        
        self.dismiss(animated: true) {
            
            if self.isComingFromGurus {
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                
                let vwcntrl = storyBoard.instantiateViewController(withIdentifier: "GurusFilteredStocksViewController") as! GurusFilteredStocksViewController
                
                vwcntrl.heading = "Guru Stocks"
                
                User.navigation.pushViewController(vwcntrl, animated: true)
            
        }else{
                
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                
                let vwcntrl = storyBoard.instantiateViewController(withIdentifier: "FilteredStocksViewController") as! FilteredStocksViewController
                
                vwcntrl.switchDelegate = StockFiltersViewController() as? SwitchDataManageDelegate
                vwcntrl.heading = "Filtered Stocks"
                User.navigation.pushViewController(vwcntrl, animated: true)
            
        }
    }
    }
    
    
    //MARK: DISMISS CURRENT PAGE
    private func dismissCurrentPage(){
        self.dismiss(animated: true) {
        }
    }
}

extension FilterPopUpViewController: UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return selectedFilter.dispalyQueryArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "titleCell",
                                                            for: indexPath) as? RoundedCollectionViewCell else {
                                                                return RoundedCollectionViewCell()
        }
        cell.textLabel.text = selectedFilter.dispalyQueryArr[indexPath.row]
       // cell.textLabel.preferredMaxLayoutWidth = 300
        
        
        return cell
    }
    
    
}




