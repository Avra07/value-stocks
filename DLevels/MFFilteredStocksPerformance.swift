//
//  MFFilteredStocksPerformance.swift
//  DLevels
//
//  Created by SAIKAT GHOSH on 20/08/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//



import Foundation
class MFFilteredStocksPerformance:NSObject {
    
    var _1D             = 0.00
    var _1W             = 0.00
    var _1M             = 0.00
    var _3M             = 0.00
    var _6M             = 0.00
    var _1Y             = 0.00
    var _2Y             = 0.00
    var _3Y             = 0.00
    var _4Y             = 0.00
    var _5Y             = 0.00
    var _10Y            = 0.00
    var Blb_Symbol      = ""
    var Category        = ""
    var CategoryLong    = ""
    var Dispaly_Name    = ""
    var Sector          = ""
    var netAssets       = ""
    var NseCode         = ""
    var LastClose       = 0.00
    var Scm_Code        = ""
    
    override init(){
        
    }
    
    
    init(symbol:String,sector:String,net_Assets:String,display_name:String,nseCode:String,category:String,categoryLong:String,
         _1DRet: Double,_1WRet: Double,_1MRet: Double,_3MRet: Double,_6MRet: Double,_1YRet: Double,_2YRet: Double,
         _3YRet: Double,_4YRet: Double,_5YRet:Double, _10YRet:Double, lastClose: Double, scm_code: String
        ) {
        
        self._1D                    = _1DRet
        self._1W                    = _1WRet
        self._1M                    = _1MRet
        self._3M                    = _3MRet
        self._6M                    = _6MRet
        self._1Y                    = _1YRet
        self._2Y                    = _2YRet
        self._3Y                    = _3YRet
        self._4Y                    = _4YRet
        self._5Y                    = _5YRet
        self._10Y                    = _10YRet
        self.Blb_Symbol             = symbol
        self.Sector                 = sector
        self.Category               = category
        self.CategoryLong           = categoryLong
        self.Dispaly_Name           = display_name
        self.NseCode                = nseCode
        self.LastClose              = lastClose
        self.netAssets              = net_Assets
        self.Scm_Code               = scm_code
    }
    
    func getData(param:String, completion : @escaping ([MFFilteredStocksPerformance])->()){
        
        var urlStr = ""
        
        var result = [MFFilteredStocksPerformance]()
        let obj = WebService()
        
        obj.callWebServices(url: Urls.rpt_filter_results, methodName: "POST", parameters: param, istoken: false, tokenval: "" ) { (success, jsonData) in
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                var tempArray = [Any]()
                
                
                tempArray = jsonData.value(forKey: "response") as! [NSDictionary]
                
                
                
                for arr in tempArray{
                    
                    if let _Blb_Symbol = (arr as AnyObject).value(forKey: "Blb_Symbol") as? String{
                        self.Blb_Symbol = _Blb_Symbol
                    }
                    if let _Category = (arr as AnyObject).value(forKey: "Category") as? String{
                        self.Category = _Category
                    }
                    if let _CategoryLong = (arr as AnyObject).value(forKey: "CategoryLong") as? String{
                        self.CategoryLong = _CategoryLong
                    }
                    if let _Dispaly_Name = (arr as AnyObject).value(forKey: "Dispaly_Name") as? String{
                        self.Dispaly_Name = _Dispaly_Name
                    }
                    if let _NseCode = (arr as AnyObject).value(forKey: "NseCode") as? String{
                        self.NseCode = _NseCode
                    }
                    if let _Sector = (arr as AnyObject).value(forKey: "Sector") as? String{
                        self.Sector = _Sector
                    }
                    
                    if let _NetAssets = (arr as AnyObject).value(forKey: "NetAssets") as? String{
                        self.netAssets = _NetAssets
                    }
                    
                    if let _LastClose = (arr as AnyObject).value(forKey: "LastClose") as? String{
                        self.LastClose = (_LastClose as NSString).doubleValue
                    }
                    
                    if let _LastChange = (arr as AnyObject).value(forKey: "LastChange") as? String{
                        
                        self._1D = (_LastChange as NSString).doubleValue
                    }
                    
                    if let _1WRet = (arr as AnyObject).value(forKey: "1W_Change") as? String{
                        self._1W = (_1WRet as NSString).doubleValue
                    }
                    if let _1MRet = (arr as AnyObject).value(forKey: "1M_Change") as? String{
                        if(_1MRet=="--"){
                            self._1M = -999.00
                        } else{
                            self._1M = (_1MRet as NSString).doubleValue
                        }
                    }
                    if let _3MRet = (arr as AnyObject).value(forKey: "3M_Change") as? String{
                        if(_3MRet=="--"){
                            self._3M = -999.00
                        } else{
                            self._3M = (_3MRet as NSString).doubleValue
                        }
                    }
                    if let _6MRet = (arr as AnyObject).value(forKey: "6M_Change") as? String{
                        if(_6MRet=="--"){
                            self._6M = -999.00
                        } else{
                            self._6M = (_6MRet as NSString).doubleValue
                        }
                    }
                    if let _1YRet = (arr as AnyObject).value(forKey: "1Y_Change") as? String{
                        if(_1YRet=="--"){
                            self._1Y = -999.00
                        } else{
                            self._1Y = (_1YRet as NSString).doubleValue
                        }
                    }
                    
                    if let _2YRet = (arr as AnyObject).value(forKey: "2Y_Change") as? String{
                        if(_2YRet=="--"){
                            self._2Y = -999.00
                        } else{
                            self._2Y = (_2YRet as NSString).doubleValue
                        }
                    }
                    if let _3YRet = (arr as AnyObject).value(forKey: "3Y_Change") as? String{
                        if(_3YRet=="--"){
                            self._3Y = -999.00
                        } else{
                            self._3Y = (_3YRet as NSString).doubleValue
                        }
                    }
                    
                    if let _4YRet = (arr as AnyObject).value(forKey: "4Y_Change") as? String{
                        if(_4YRet=="--"){
                            self._4Y = -999.00
                        } else{
                            self._4Y = (_4YRet as NSString).doubleValue
                        }
                    }
                    if let _5YRet = (arr as AnyObject).value(forKey: "5Y_Change") as? String{
                        if(_5YRet=="--"){
                            self._5Y = -999.00
                        } else{
                            self._5Y = (_5YRet as NSString).doubleValue
                        }
                    }
                    
                    if let _10YRet = (arr as AnyObject).value(forKey: "10Y_Change") as? String{
                        if(_10YRet=="--"){
                            self._10Y = -999.00
                        } else{
                            self._10Y = (_10YRet as NSString).doubleValue
                        }
                    }
                    
                    if let _scmCode = (arr as AnyObject).value(forKey: "Reg_Scm_Code") as? String{
                        self.Scm_Code = _scmCode
                    }
                    
                    
                    
                    let obj_perf = MFFilteredStocksPerformance(symbol: self.Blb_Symbol, sector: self.Sector, net_Assets: self.netAssets,display_name: self.Dispaly_Name, nseCode: self.NseCode,category: self.Category, categoryLong: self.CategoryLong, _1DRet: self._1D, _1WRet: self._1W, _1MRet: self._1M, _3MRet: self._3M, _6MRet: self._6M, _1YRet: self._1Y,_2YRet: self._2Y, _3YRet: self._3Y,_4YRet: self._4Y, _5YRet: self._5Y,_10YRet: self._10Y, lastClose: self.LastClose,scm_code: self.Scm_Code)
                    
                    result.append(obj_perf)
                    
                }
                
                
                completion(result)
                
            }
        }
        
        
        
    }
    
}





