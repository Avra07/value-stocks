//
//  SelectSectorViewController.swift
//  DLevels
//
//  Created by SAIKAT GHOSH on 05/07/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit


// protocol used for sending data back
protocol DataEnteredDelegate: class {
    func userDidEnterInformation(sectorlist: [String], indexPathRow: String, Section: String)
}


class SelectSectorViewController: UIViewController {
    
    // making this a weak variable so that it won't create a strong reference cycle
    weak var sectorDelegate: DataEnteredDelegate?
    
    var sender_indexPathRow = ""
    var sender_section      = ""
    
    var sectorListDict = [NSDictionary]()
    
    
    @IBOutlet weak var tblvwSectorlist: UITableView!
    
    var selectedSector = ""
    var arrSelectedSector = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        getData(completion: { (status) in
            if status == true {
                self.tblvwSectorlist.reloadData()
            } else {
                
            }
        })
    }
    
    @IBAction func btnbackclick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnDoneclick(_ sender: Any) {
        // call this method on whichever class implements our delegate protocol
        sectorDelegate?.userDidEnterInformation(sectorlist: arrSelectedSector, indexPathRow: sender_indexPathRow, Section: sender_section)
        
        // go back to the previous view controller
        self.navigationController?.popViewController(animated: true)
       
    }
    
    //MARK: GET DATA
    func getData (completion : @escaping (Bool)->()) {
        
        let obj = WebService()
        let paremeters = ""
        
        //actInd.startAnimating()
        
        obj.callWebServices(url: Urls.getsectorlist, methodName: "GET", parameters: paremeters, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
            
            print("Json Data is :  \(jsonData)")
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                let tempArray = jsonData.value(forKey: "response") as! [NSDictionary]
                
                for dict in tempArray {
                    self.sectorListDict.append(dict)
                }
                
                
                
                
                return completion(true)
                
            }
        }
    }

}

extension SelectSectorViewController: UITableViewDataSource,UITableViewDelegate {
    
    //MARK: Table View Delegate
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let data = sectorListDict[indexPath.row]
                
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectSectorCell") as! SelectSectorCell
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        if let title = data.value(forKey: "sector") as? String {
            cell.lblTitle.text = title
            
            if arrSelectedSector.count > 0 {
                if arrSelectedSector.contains(title) {
                    cell.imgTick.isHidden = false
                } else {
                    cell.imgTick.isHidden = true
                }
            }else {
                cell.imgTick.isHidden = true
            }
        }
        return cell
    }
    
    public func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.sectorListDict.count
    }
    
    
    //MARK: Table View Delegate
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //getting the current cell from the index path
        let currentCell = tableView.cellForRow(at: indexPath)! as! SelectSectorCell
        
        //getting the text of that cell
        let currentItem = currentCell.lblTitle!.text
        
        
        
        if let index = arrSelectedSector.index(of: currentItem!) {
            arrSelectedSector.remove(at: index)
            currentCell.imgTick.isHidden = true
            currentCell.isSelected = true
        } else {
            arrSelectedSector.append(currentItem!)
            currentCell.imgTick.isHidden = false
            currentCell.isSelected = false
        }
        
    }
    
    
}
