//
//  ForgotPasswordViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-01 on 13/01/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var txtemail: UITextField!
    
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()

    
    
    @IBAction func btnsubmit(_ sender: Any) {
        
        actInd.startAnimating()
        self.view.isUserInteractionEnabled = false
        
        if(AppManager().isValidEmail(email: txtemail.text!))
        {
            
            let obj = WebService()
            let token = ""
            
            obj.callWebServices(url: Urls.forGotPassword, methodName: "POST", parameters: "email=" + txtemail.text!, istoken: false, tokenval: token) { (returnValue, jsonData) in
                
                //print("hhjhjhjjh\(jsonData)")
                
                if(jsonData.value(forKey: "errmsg") as! String == "")
                {
                    
                    self.actInd.stopAnimating()
                    DispatchQueue.main.async(execute: {
                        
                        self.view.isUserInteractionEnabled = true
                        
                        let chngpassvwcontroller = self.storyboard?.instantiateViewController(withIdentifier: "changepassword") as! ChangePasswordViewController
                        
                        chngpassvwcontroller.emailAddr = self.txtemail.text
                        self.navigationController?.pushViewController(chngpassvwcontroller, animated: true)
                    })
                    
                }
                else
                {
                    DispatchQueue.main.async {
                        self.view.isUserInteractionEnabled = true
                        self.actInd.stopAnimating()
                    }
                    
                    let alertobj = AppManager()
                    alertobj.showAlert(title: "Invalid Mail", message: jsonData.value(forKey: "errmsg") as! String, navigationController: self.navigationController!)
                }
            }
        }
        else
        {
            DispatchQueue.main.async {
                self.view.isUserInteractionEnabled = true
                self.actInd.stopAnimating()
            }
            let alertobj = AppManager()
            alertobj.showAlert(title: "Invalid Mail", message: "Email Address should be in correct format.", navigationController: self.navigationController!)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        txtemail.delegate = self
        AppManager().setStatusBarBackgroundColor()
        self.hideKeyboardOnTapOutside()
        
        
        actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0) //CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.gray
        self.view.addSubview(actInd)

        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        actInd.stopAnimating()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: UITextField Delegate Methode to return keyboard on return key press
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.view.endEditing(true)
        return true
    }
    
    
    // Mark: TextField Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
//        if textField == txtemail {
//            
//            UIView.animate(withDuration: 0.3, animations: {
//                self.view.frame.origin.y -= 300
//            })
//        }
    }
    
    // Mark: TextField Delegate
    func textFieldDidEndEditing(_ textField: UITextField) {
        
//        if textField == txtemail {
//            UIView.animate(withDuration: 0.3, animations: {
//                self.view.frame.origin.y += 300
//            })
//        }
    }
    
    
    @IBAction func backButton_Action(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
