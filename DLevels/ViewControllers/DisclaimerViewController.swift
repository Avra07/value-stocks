//
//  DisclaimerViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-02 on 11/01/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class DisclaimerViewController: UIViewController,UIWebViewDelegate {
    
    @IBOutlet weak var vwLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var navBarTitle: UINavigationItem!
    @IBOutlet weak var webView: UIWebView!
    
    var disclaimerUrl : String?
    var disclaimerHeader : String?
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0)
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.gray
        self.view.addSubview(actInd)
        actInd.startAnimating()
        
        webView.delegate = self
        
        let label = UILabel(frame: CGRect(x:0, y:0, width:400, height:50))
        label.backgroundColor = .clear
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: 15.0)
        label.textAlignment = .left
        label.textColor = UIColor(hex:"284C5A")
        label.text = disclaimerHeader
        self.navBarTitle.titleView = label
        AppManager().setStatusBarBackgroundColor()
        
        let url = URL(string: disclaimerUrl!)
        webView.loadRequest(URLRequest(url: url!))
        
        
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        actInd.stopAnimating()
    }
    
    
    
    @IBAction func btnHamburger_Action(_ sender: Any) {
		//let viewCont = self.storyboard?.instantiateViewController(withIdentifier: "DlHomeViewController") as! DlHomeViewController
		//self.navigationController?.pushViewController(viewCont, animated: false)
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
}


