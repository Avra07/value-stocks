  //
//  TechnicalAnalysisViewController.swift
//  DLevels
//
//  Created by MacMini2 on 27/04/18.
//  Copyright © 2018 Dynamic-Mac-01. All rights reserved.
//

import UIKit
class CustomLadderCell: UITableViewCell {
    
    @IBOutlet weak var lblLevelType: UILabel!
    @IBOutlet weak var lblValue: UILabel!
    @IBOutlet weak var lblLevName: UILabel!
    
    
}
  
class StockCell: UITableViewCell {
    
    @IBOutlet weak var lblText: UILabel!
    
  }

  /**************************/
  class ProgressHUD: UIVisualEffectView {
    
    var text: String? {
        didSet {
            label.text = text
        }
    }
    
    let activityIndictor: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
    let label: UILabel = UILabel()
    let blurEffect = UIBlurEffect(style: .light)
    let vibrancyView: UIVisualEffectView
    
    init(text: String) {
        self.text = text
        self.vibrancyView = UIVisualEffectView(effect: UIVibrancyEffect(blurEffect: blurEffect))
        super.init(effect: blurEffect)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.text = ""
        self.vibrancyView = UIVisualEffectView(effect: UIVibrancyEffect(blurEffect: blurEffect))
        super.init(coder: aDecoder)
        self.setup()
    }
    
    func setup() {
        vibrancyView.backgroundColor = UIColor.darkGray
        contentView.addSubview(vibrancyView)
        contentView.addSubview(activityIndictor)
        contentView.addSubview(label)
        activityIndictor.startAnimating()
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        if let superview = self.superview {
            
            let width = superview.frame.size.width  / 1.5 //.3
            let height: CGFloat = 80.0
            self.frame = CGRect(x: superview.frame.size.width / 2 - width / 2,
                                y: superview.frame.height / 2 - height / 2,
                                width: width,
                                height: height)
            vibrancyView.frame = self.bounds
            
            let activityIndicatorSize: CGFloat = 40
            activityIndictor.frame = CGRect(x: 5,
                                            y: height / 2 - activityIndicatorSize / 2,
                                            width: activityIndicatorSize,
                                            height: activityIndicatorSize)
            
            layer.cornerRadius = 8.0
            layer.masksToBounds = true
            label.text = text
            label.textAlignment = NSTextAlignment.center
            label.numberOfLines = 0
            label.lineBreakMode = .byWordWrapping
            
            label.frame = CGRect(x: activityIndicatorSize + 5,
                                 y: 0,
                                 width: width - activityIndicatorSize - 15,
                                 height: height)
            label.textColor = UIColor.white
            label.font = UIFont.boldSystemFont(ofSize: 14)
        }
    }
    
    func show() {
        self.isHidden = false
    }
    
    func hide() {
        self.isHidden = true
    }
  }
  
  
  /***************************/
class TechnicalAnalysisViewController: UIViewController,UITableViewDelegate, UITableViewDataSource,UITextViewDelegate {

    @IBOutlet weak var tblvwresult: UITableView!
    @IBOutlet weak var searchcontroller: UISearchBar!
    @IBOutlet weak var mTblRsltHghtConst: NSLayoutConstraint!
    
    @IBOutlet weak var vwLadderContainer: UIView!
    @IBOutlet weak var lblSymbolName: UILabel!
   
    @IBOutlet weak var lblShowDate: UILabel!
    @IBOutlet weak var viewDateTimePicker: UIView!
    @IBOutlet weak var datePickerView: UIDatePicker!
    @IBOutlet weak var btnDatePickerDone: UIButton!
    
    @IBOutlet weak var btnSearchDatePicker: UIButton!
    @IBOutlet weak var topConstrant: NSLayoutConstraint!
    
    @IBOutlet weak var lblAdm: UILabel!
    @IBOutlet weak var lblVol: UILabel!
    @IBOutlet weak var lblAvgVol: UILabel!
    @IBOutlet weak var lblPDH: UILabel!
    @IBOutlet weak var lblPWH: UILabel!
    @IBOutlet weak var lblPDL: UILabel!
     @IBOutlet weak var lblPWL: UILabel!
    @IBOutlet weak var lblCWH: UILabel!
    @IBOutlet weak var lblCMH: UILabel!
    @IBOutlet weak var lblCWL: UILabel!
    @IBOutlet weak var lblCML: UILabel!
    
    
    @IBOutlet weak var imgPDH: UIImageView!
    @IBOutlet weak var imgPWH: UIImageView!
    @IBOutlet weak var imgPDL: UIImageView!
    @IBOutlet weak var imgPWL: UIImageView!
    @IBOutlet weak var imgCWH: UIImageView!
    @IBOutlet weak var imgCMH: UIImageView!
    @IBOutlet weak var imgCWL: UIImageView!
    @IBOutlet weak var imgCML: UIImageView!
    
    
    @IBOutlet weak var imgDaily: UIImageView!
    @IBOutlet weak var imgWeekly: UIImageView!
    @IBOutlet weak var imgMonthly: UIImageView!
    
    @IBOutlet weak var tblLadder: UITableView!
    
    @IBOutlet weak var tblLadderheightConstraint: NSLayoutConstraint!
    var serchValue = ""
    var instrument_4 = ""
    var response = [NSDictionary]()
    var refDate = ""
    var PrevDayClose = ""
    var daily = ""
    var weekly = "W"
    var monthly = "M"
    var per_typeStr = ""
    var ladderDict         =     [NSDictionary]()
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    
    let progressHUD = ProgressHUD(text: "Please Wait..It usually takes 5 to 6 sec to load")
    override func viewDidLoad() {
        super.viewDidLoad()

        tblvwresult.delegate = self
        	tblvwresult.tableFooterView = UIView()
        //searchcontroller.layer.borderWidth = 1
        // searchcontroller.layer.cornerRadius = 6
        
        datePickerView.maximumDate = NSDate() as Date
        let date = Date()
        let formatter = DateFormatter()
        let formattershow = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formattershow.dateFormat = "dd/MM/yyyy"
        refDate = formatter.string(from: date)
        lblShowDate.text = formattershow.string(from: date)
        //print("\(serchValue) --- \(instrument_4)")
        lblSymbolName.text = "\(instrument_4)"
        //addLoader()
        //actInd.startAnimating()
        loadNearLevels(symbolval: serchValue, refDate: refDate)
       
        self.view.addSubview(progressHUD)
        // All done!
        progressHUD.show()
        //self.view.backgroundColor = UIColor.black
        
        // self.hideKeyboardOnTapOutside()
        
        addDoneButtonOnKeyboard()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override open var shouldAutorotate: Bool{
        return false
    }
    // called whenever text is changed.
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if(searchText.count > 1)
        {
            loadDataForResultView(termval: searchText, pagenameval: "")
        }
        else
        {
            self.response.removeAll()
            self.tblvwresult.reloadData()
        }
        
        if(searchText == "")
        {
            self.response.removeAll()
            self.tblvwresult.reloadData()
            
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        // Stop doing the search stuff
        // and clear the text in the search bar
        searchBar.text = ""
        // Hide the cancel button
        searchBar.showsCancelButton = false
        vwLadderContainer.isHidden = false
        
        self.view.endEditing(true)
        
        
        //searchBar.resignFirstResponder()
        // You could also change the position, frame etc of the searchBar
    }
    func loadDataForResultView(termval: String,pagenameval: String)
    {
        let obj = WebService()
        let tremvalstring = termval.replacingOccurrences(of: " ", with: "%20")
         let parameters = "term=\(tremvalstring)&pageName=\(pagenameval)"
        
         obj.callWebServices(url: Urls.autosearch_stock, methodName: "GET", parameters: parameters, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
            //print("Search List: \(jsonData)")
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                
                self.mTblRsltHghtConst.constant = UIScreen.main.bounds.height - 240 // 240 for keyboard height
                
                let tempArray = jsonData.value(forKey: "response") as! NSArray
                
                self.response.removeAll()
                
                for dict in tempArray {
                    let dictValue = dict as! NSDictionary
                    self.response.append(dictValue)
                }
                
                
                DispatchQueue.main.async {
                    let sectionIndex = IndexSet(integer: 0)
                    self.vwLadderContainer.isHidden = true
                    self.tblvwresult.reloadData()
                    
                }
                
                
                //self.seperatorView.isHidden = false
               // self.tblvwresult.reloadSections(sectionIndex, with: .automatic)
            }
            else
            {
                //let alertobj = AppManager()
                
                // print(jsonData.value(forKey: "errmsg") as! String)
                
            }
        }
    }
    
    func addLoader() {
        actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0) //CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.gray
        self.view.addSubview(actInd)
    }
    @IBAction func dtpDatePicker(_ sender: Any) {
        
        //topConstrant.constant = 390
       
        viewDateTimePicker.isHidden = false
//        datePickerView.isHidden = false
//        btnDatePickerDone.isHidden = false
        datePickerView.addTarget(self, action:#selector(dateChangedInDate(sender:)), for: UIControlEvents.valueChanged)
        
        //datePickerView.addSubview(datePicker)
        
        
    }
    func dateChangedInDate(sender:UIDatePicker){
        
        let dateFormatter = DateFormatter()
        let dateFormatterValue = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy" //yyyy-MM-dd"
        dateFormatterValue.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.string(from: sender.date)
        refDate = dateFormatterValue.string(from: sender.date)
        //print("date selected \(date)")
        //btnSearchDatePicker.setTitle(date, for:.normal)
        lblShowDate.text = date
       
        
    }
    @IBAction func btnDatePickerDone_Click(_ sender: Any) {
//        topConstrant.constant = 20
//        datePickerView.isHidden = true
//        btnDatePickerDone.isHidden = true
       // self.actInd.startAnimating()
        progressHUD.show()
        viewDateTimePicker.isHidden = true
        loadNearLevels(symbolval: serchValue, refDate: refDate)
    }
    func loadNearLevels(symbolval : String, refDate : String)
    {
        let obj = WebService()
        let paremeters = "sec_name=\(symbolval)&ref_date=\(refDate)"
        obj.callWebServices(url: Urls.getNearLevels, methodName: "POST", parameters: paremeters, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
            
           // print("Json Data New is :  \(jsonData)")
            if jsonData.value(forKey: "errmsg") as! String == "" {
                let response = jsonData.value(forKey: "response") as! NSDictionary
                
                if(response.count > 0)
                {
                    DispatchQueue.main.async(execute: {
                        //self.actInd.stopAnimating()
                        self.progressHUD.hide()
                        let levelsval = response.value(forKey: "near_levels") as! NSArray
                        // print("levelsval Data New is :  \(levelsval)")
                        let symval = response.value(forKey: "sym_details") as! NSArray
                       
                        /*******************************************************/
                        //Mark: For Ticker Section and 1st Dataset Data
                        /*******************************************************/
                        if let PDC = nullToNil(value: (levelsval[0] as AnyObject).value(forKey: "P_Close") as? String as AnyObject) {
                            self.PrevDayClose = PDC as! String
                        }
                        if let ADM = nullToNil(value: (levelsval[0] as AnyObject).value(forKey: "ADM") as? String as AnyObject) {
                            self.lblAdm.text = ADM as? String
                        }
                        if let Vol = nullToNil(value: (symval[0] as AnyObject).value(forKey: "vol") as? String as AnyObject) {
                            self.lblVol.text = Vol as? String
                        }
                        if let AvgVol =  nullToNil(value: (symval[0] as AnyObject).value(forKey: "Avg_Vol") as? String as AnyObject) {
                            let avgVolArr = AvgVol.components(separatedBy: ".")
                            self.lblAvgVol.text = avgVolArr[0] as? String
                        }
                        if let PDH = nullToNil(value: (levelsval[0] as AnyObject).value(forKey: "PDH") as? String as AnyObject) {
                            self.lblPDH.text = PDH as? String
                        }
                        if let PDL = nullToNil(value: (levelsval[0] as AnyObject).value(forKey: "PDL") as? String as AnyObject) {
                            self.lblPDL.text = PDL as? String
                        }
                        if let PWH = nullToNil(value: (levelsval[0] as AnyObject).value(forKey: "PWH") as? String as AnyObject) {
                            self.lblPWH.text = PWH as? String
                        }
                        if let PWL = nullToNil(value: (levelsval[0] as AnyObject).value(forKey: "PWL") as? String as AnyObject) {
                            self.lblPWL.text = PWL as? String
                        }

                        if let CWH = nullToNil(value: (levelsval[0] as AnyObject).value(forKey: "CWH") as? String as AnyObject) {
                            self.lblCWH.text = CWH as? String
                        }
                        if let CWL = nullToNil(value: (levelsval[0] as AnyObject).value(forKey: "CWL") as? String as AnyObject) {
                            self.lblCWL.text = CWL as? String
                        }
                        if let CMH = nullToNil(value: (levelsval[0] as AnyObject).value(forKey: "CMH") as? String as AnyObject) {
                            self.lblCMH.text = CMH as? String
                        }
                        if let CML = nullToNil(value: (levelsval[0] as AnyObject).value(forKey: "CML") as? String as AnyObject) {
                            self.lblCML.text = CML as? String
                        }
                         self.getLadder(prevClose : self.PrevDayClose)
                    })
                  
                   

//                    /*******************************************************/
//                    //Mark:  Close Ticker Section and 1st Dataset Data
//                    /*******************************************************/


               }
                
                
            }
            else
            {
                DispatchQueue.main.async {
                    let alertobj = AppManager()
                    //self.actInd.stopAnimating()
                    self.progressHUD.hide()
                    alertobj.showAlert(title: "Error!", message: jsonData.value(forKey: "errmsg") as! String , navigationController: self.navigationController!)
                }
            }
        }
    }
    
    
    
    @IBAction func btnBack_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
//        let viewCont = self.storyboard?.instantiateViewController(withIdentifier: "StockSpecificNewViewController") as! StockSpecificNewViewController
//        self.navigationController?.pushViewController(viewCont, animated: false)
    }
    
    @IBAction func btnSearchShare_Action(_ sender: Any) {
        
        let viewCont = self.storyboard?.instantiateViewController(withIdentifier: "ShareSearchViewController") as! ShareSearchViewController
        self.navigationController?.pushViewController(viewCont, animated: false)
    }
    
    
    @IBAction func btnDaily_Action(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        if(sender.isSelected == true)
        {
            imgDaily.image = #imageLiteral(resourceName: "CheckboxOn")
            daily = "D"
            getLadder(prevClose: self.PrevDayClose)
        }
        else
        {
            imgDaily.image = #imageLiteral(resourceName: "CheckboxOff")
            daily = ""
            getLadder(prevClose: self.PrevDayClose)
        }
    }
    
    @IBAction func btnWeekly_Action(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        if(sender.isSelected == true)
        {
            imgWeekly.image = #imageLiteral(resourceName: "CheckboxOn")
            weekly = "W"
            getLadder(prevClose: self.PrevDayClose)
        }
        else
        {
            imgWeekly.image = #imageLiteral(resourceName: "CheckboxOff")
            weekly = ""
            getLadder(prevClose: self.PrevDayClose)
        }
    }
    
    
    @IBAction func btnMonthly_Action(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        if(sender.isSelected == true)
        {
            imgMonthly.image = #imageLiteral(resourceName: "CheckboxOn")
            monthly = "M"
            getLadder(prevClose: self.PrevDayClose)
        }
        else
        {
            imgMonthly.image = #imageLiteral(resourceName: "CheckboxOff")
            monthly = ""
            getLadder(prevClose: self.PrevDayClose)
        }
    }
    func getLadder(prevClose : String)
    {
        //self.actInd.startAnimating()
        progressHUD.show()
        per_typeStr = ""
        let obj = WebService()
        if daily != ""{
            per_typeStr.append("D|")
        }
        if weekly != ""{
            per_typeStr.append("W|")
        }
        if monthly != ""{
            per_typeStr.append("M|")
        }
        let per_type      =  per_typeStr
        let sec_name      =  serchValue
        let my_price      =  PrevDayClose
        let ref_date      =  refDate
        let display_type  = "1"
        var count = 0
        if per_type == ""{
            DispatchQueue.main.async {
                let alertobj = AppManager()
                //self.actInd.stopAnimating()
                self.progressHUD.hide()
                alertobj.showAlert(title: "", message: "Please select anyone from Daily, Weekly, Monthly" , navigationController: self.navigationController!)
            }
        }else{
            let parameters  = "sec_name=\(sec_name)&my_price=\(my_price)&ref_date=\(ref_date)&per_type=\(per_type)&display_type=\(display_type)"
            
            obj.callWebServices(url: Urls.getLadderCombined, methodName: "POST", parameters: parameters, istoken: true, tokenval: User.token) {(returnValue, jsonData) in
                
               // print("Json Data New is :  \(jsonData)")
               
                if jsonData.value(forKey: "errmsg") as! String == "" {
                    
                     DispatchQueue.main.async(execute: {
                        self.ladderDict.removeAll()
                        let tempArray = jsonData.value(forKey: "response") as! [NSDictionary]
                        self.ladderDict = tempArray
                       // self.tblLadderheightConstraint.constant = CGFloat(self.ladderDict.count * 42)
                         self.tblLadder.reloadData()
                        if(tempArray.count > 1){
                            for dict in tempArray {
                                let dictValue = dict as! NSDictionary
                                if let index = nullToNil(value: (dictValue as AnyObject).value(forKey: "Lev_Name") as! String as AnyObject) {
                                    if (index as! String != "<b>Price</b>" ){
                                        count = count + 1
                                        
                                    }else{
                                        break
                                    }
                                }
                            }
                           
                            //self.actInd.stopAnimating()
                            self.progressHUD.hide()
                            let indexpath = IndexPath(row: count, section: 0)
                            self.tblLadder.scrollToRow(at: indexpath as IndexPath, at: .middle, animated: true)
                        }else{
                            DispatchQueue.main.async {
                                let alertobj = AppManager()
                                // self.actInd.stopAnimating()
                                self.progressHUD.hide()
                                alertobj.showAlert(title: "", message: "No data found. Please try from a leter date." , navigationController: self.navigationController!)
                            }
                        }
                    })
                }
                else
                {
                    DispatchQueue.main.async {
                        let alertobj = AppManager()
                       // self.actInd.stopAnimating()
                        self.progressHUD.hide()
                        alertobj.showAlert(title: "Error!", message: jsonData.value(forKey: "errmsg") as! String , navigationController: self.navigationController!)
                    }
                }
            }
        }
    }
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//
//    }
    public func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
      return UITableViewAutomaticDimension
    }
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 40
    }
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
        
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        if tableView == self.tblvwresult{
            
           
            
            let cell = tableView.dequeueReusableCell(withIdentifier:"stockcell", for: indexPath) as! StockCell
            cell.selectionStyle = .none
            
            let index = response[indexPath.row]
            
            if let INSTRUMENT_2 = index.value(forKey: "INSTRUMENT_2") as? String {
                cell.lblText.text = INSTRUMENT_2
            }
            return cell
        } else{
        
        let cell = tableView.dequeueReusableCell(withIdentifier:"cell", for: indexPath) as! CustomLadderCell
        cell.backgroundColor = indexPath.row % 2 == 0 ? UIColor(red: 232/255, green: 232/255, blue: 232/255, alpha: 0.4) : UIColor.white
        if let levType = nullToNil(value: self.ladderDict[indexPath.row].value(forKey: "fd_Level_Type") as AnyObject) as? String {
            cell.lblLevelType.text = levType
            //cell.lblLevelType.font = UIFont.boldSystemFont(ofSize: 15.0)
          
        }
        if let levValue = nullToNil(value: self.ladderDict[indexPath.row].value(forKey: "fd_value") as AnyObject) as? String {
            cell.lblValue.text = levValue
        }
        if let levName = nullToNil(value: self.ladderDict[indexPath.row].value(forKey: "Lev_Name") as AnyObject) as? String {
            cell.lblLevName.text = levName.replacingOccurrences(of: "<b>", with: "").replacingOccurrences(of: "</b>", with: "")
            if levName == "<b>Price</b>" {
                cell.backgroundColor = UIColor(red: 67/255, green: 67/255, blue: 67/255, alpha: 0.4)
                cell.lblLevelType.isHidden = true
             
            }else{
                cell.lblLevelType.isHidden = false
                
            }
            
        }
       if cell.lblLevelType.text == "R"{
        cell.lblLevelType.textColor = UIColor(hex: "639AE4")
       }else{
        cell.lblLevelType.textColor = UIColor(hex: "D6513C")
        }
        return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        self.view.resignFirstResponder()
//        self.view.endEditing(true)
        searchcontroller.text = ""
        if tableView == tblvwresult {
            //self.actInd.startAnimating()
            progressHUD.show()
            vwLadderContainer.isHidden = false
            serchValue = response[indexPath.row].value(forKey: "Symbol_Name") as! String
            instrument_4 = response[indexPath.row].value(forKey: "INSTRUMENT_2") as! String
            self.response.removeAll()
            mTblRsltHghtConst.constant = 0
                print("Table \(serchValue)")
            //headerViewHeightConstraint.constant = 45
            lblSymbolName.text = "\(instrument_4)"
            loadNearLevels(symbolval: serchValue, refDate: refDate)
            
            dismissKeyboard()
            tblvwresult.reloadData()
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tblvwresult{
            //  mTblRsltHghtConst.constant = CGFloat(56 + (ailmentListArray.count * 50))
            return response.count
        }else {
            return ladderDict.count
        }
    }
    
    func addDoneButtonOnKeyboard() {
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction)
        )
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        searchcontroller.inputAccessoryView = doneToolbar
    }
    
    func doneButtonAction() {
        searchcontroller.resignFirstResponder()
    }
    

}
