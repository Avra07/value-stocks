//
//  ViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-01 on 23/12/16.
//  Copyright © 2016 Dynamic-Mac-01. All rights reserved.
//

import UIKit

import FirebaseInstanceID

extension UITextField {
    func setBottomBorder() {
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.white.cgColor
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
}
class OTPViewController: UIViewController, UITextFieldDelegate,UIAlertViewDelegate {
    
    @IBOutlet weak var lblResendOtp: UILabel!
    @IBOutlet weak var btnChange: UIButton!
    @IBOutlet weak var lbltimer: UILabel!
    @IBOutlet weak var lbltype: UILabel!
    @IBOutlet weak var btnresend: UIButton!
    @IBOutlet weak var txtotp: UITextField!
    @IBOutlet weak var btnsubmit: UIButton!
    @IBOutlet weak var lblVerificationText: UILabel!
    var code = "+91"
    var number = ""
    
    /////popupbox changenumber btn click //////
    @IBOutlet weak var lblemail: UILabel!
    @IBOutlet weak var txt_code: UITextField!
    @IBOutlet weak var txt_phone_number: UITextField!
    @IBOutlet weak var view_popupbox: UIView!
    
    
    var codeTextField: UITextField?;
    var numberTextField: UITextField?;
    var buttonField: UIButton?;
    var imageField: UIImageView?;
    
    
    var emailAddr   : String?
    var emailOld    : String?
    var phoneNum    : String?
    var checkotptype: String!
    
    var counter = 30
    var timer   = Timer()
    
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    
    
    @IBAction func btnChange(_ sender: Any) {
        
       //self.changeButtonClick(screenType: checkotptype)
        self.askForNumber(screenType: checkotptype);
        
}
    @IBAction func btn_cancel(_ sender: Any) {
        view_popupbox.isHidden=true
        btnsubmit.isEnabled=true
        btnresend.isEnabled=true


    }
    
    @IBAction func btn_ok(_ sender: Any) {
        
        //changeButtonClick(screenType: <#String#>)
    }
    
    @IBAction func btnOk(_ sender: Any) {
       // self.changeButtonClick(screenType: checkotptype)
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        view_popupbox.layer.borderWidth = 2.0
        view_popupbox.layer.borderColor = UIColor.gray.cgColor
        view_popupbox.layer.cornerRadius=5.0;
        
//        User.phone = "9007154608"
//        User.countryCode = "+91"
//        User.email = "anwesha@dynamiclevels.com"
//        checkotptype = "phone"
        if(CountryCode.countryCode != ""){
            code = "+\(CountryCode.countryCode)"
                    }
        else{
            code = "+91"
        }
        //self.codeTextField!.text = code

        self.hideKeyboardOnTapOutside()
        lbltype.text = User.phone
        lblemail.text = "and \(User.email)"
        btnresend.isHidden = true
        lblResendOtp.text = "Did not recieve OTP? Please wait \(counter) second(s)"
        
        btnresend.layer.borderColor = UIColor.black.cgColor
        btnresend.layer.borderWidth = 2.0
        
        emailAddr = User.email
        emailOld = User.email
        phoneNum = User.phone
        
        // lbltype.text = "\(User.countryCode) - \(User.phone)"
        lbltype.text = "\(code) - \(User.phone)"
        
        
        timer.invalidate() // just in case this button is tapped multiple times
        // start the timer
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
        
        addLoader()
    }
    
    
    func addLoader() {
        actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0) //CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.gray
        self.view.addSubview(actInd)
    }

    @IBAction func btnresend(_ sender: Any) {
        
        //let newmobileno: Int = { return Int(User.phone)! }()
        //let newmobilenoresend = "\(User.countryCode)|\(User.phone)"
        var newmobilenoresend: String = ""
        newmobilenoresend = (lbltype.text?.replacingOccurrences(of: " - ", with: "|"))!
        let parameters = "email=\(emailAddr!)&phone=\(newmobilenoresend)"
        
        self.resend(urlAddress: Urls.generatePhoneOtp, param: parameters)
        btnresend.isHidden = true
        
        self.counter = 30
        
        self.timer.invalidate() // just in case this button is tapped multiple times
        // start the timer
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.timerAction), userInfo: nil, repeats: true)
        
    }
    
    
    
    @IBAction func backButton_Action(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnsubmit(_ sender: Any)
    {
        actInd.startAnimating()
        let newmobileno: Int64 = Int64(User.phone)!
        let parameters = "email=\(emailAddr!)&phone=\(newmobileno)&otp=\(txtotp.text!)"
        self.checkOTP(urlAddress: Urls.updatePhoneNo, parameters: parameters)
    }
        
}


extension OTPViewController {
    
    
    
    func addDoneButtonOnKeyboard() {
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(OTPViewController.doneButtonAction)
        )
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        txtotp.inputAccessoryView = doneToolbar
    }
    
    func doneButtonAction() {
        txtotp.resignFirstResponder()
    }
    
    func checkOTP(urlAddress: String, parameters: String)
    {
        let obj = WebService()
        let token = ""
        
        //print("the parameters is :  \(parameters)")
        
        
        // let otpval = txtotp.text
        //let parameters = "email=\(User.email)&phone=\(phoneNum)&otp=\(txtotp.text!)"
        
        obj.callWebServices(url: urlAddress, methodName: "POST", parameters: parameters, istoken: false, tokenval: token) { (returnValue, jsonData) in
            
            print("Json Dict is : \(jsonData)")
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                let emailval = "email=\(User.email)"
                obj.callWebServices(url: Urls.wellcome_phone, methodName: "POST", parameters:emailval, istoken: false, tokenval: "") { (returnValue, jsonData) in
                    print("Json Dict is : \(jsonData)")
                    
                    if(User.regThrough == "normal")
                    {
                        User.email = self.emailAddr!
                        self.performLogin(email: User.email, password: User.password, regThrough: User.regThrough)
                    }
                    else
                    {
                        User.password = User.socialToken
                        self.performLogin(email: User.email, password: User.password, regThrough: User.regThrough)
                    }
                    
                }
                
                
            }
            else
            {
                
                let alertobj = AppManager()
                
                DispatchQueue.main.async {
                    self.actInd.stopAnimating()
                    alertobj.showAlert(title: "Error!", message: jsonData.value(forKey: "errmsg") as! String , navigationController: self.navigationController!)
                }
                
                
            }
        }
    }
    
    
    func afterLoginDetails(UserId: String) {
        
        let webServices = WebService()
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        let deviceID = UIDevice.current.identifierForVendor?.uuidString as! String
        
        let param = "user_id=\(UserId)&app=IOS&app_ver=\(version)&device_id=\(deviceID)"
        
        webServices.callWebServices(url: Urls.processLoginDetails, methodName: "POST", parameters: param, istoken: true, tokenval: User.token) { (success, jsonDict) in
            
            
            if jsonDict.value(forKey: "errmsg") as! String == "" {
//                DispatchQueue.main.async {
//                    let sucessViewConroller = self.storyboard?.instantiateViewController(withIdentifier: "DlHomeViewController") as! DlHomeViewController
//                    self.navigationController?.pushViewController(sucessViewConroller, animated: true)
//                }
            } else{
                let appManager = AppManager()
                appManager.showAlert(title: "Error!", message: jsonDict.value(forKey: "errmsg") as! String, navigationController: self.navigationController!)
            }
        }
    }
    
    func getuser(){
        let webServices = WebService()
        webServices.callWebServices(url: Urls.getUser, methodName: "GET", parameters: "", istoken: true, tokenval: User.token) { (success, jsonDict) in
            
            
            if jsonDict.value(forKey: "errmsg") as! String == "" {
                
                let response = jsonDict.value(forKey: "response") as! [NSDictionary]
                let userId      = response[0].value(forKey: "userId") as! String
                
                
                User.userId     = userId
                User.socialId   = userId
                User.firstName = response[0].value(forKey: "firstName") as! String
                User.lastName = response[0].value(forKey: "lastName") as! String
                User.email = response[0].value(forKey: "emailID") as! String
                User.phone = response[0].value(forKey: "phone") as! String
                
                if let mfKyc: String = response[0].value(forKey: "MF_kyc") as? String {
                    User.MF_kyc = mfKyc
                }
                if let fundaccount: String = response[0].value(forKey: "fund_account_id") as? String {
                    User.fundaccount = fundaccount
                }
                if let refererredby: String = response[0].value(forKey: "referred_by") as? String {
                    User.refererredby = refererredby
                }
                if let mfUccStatus: String = response[0].value(forKey: "MF_ucc_status") as? String {
                    User.MF_ucc_status = mfUccStatus
                }
                
                if let MF_UserId: String = response[0].value(forKey: "MF_UserId") as? String {
                    User.MF_UserId = MF_UserId
                }
                
                if let ucc: String = response[0].value(forKey: "ucc") as? String {
                    User.ucc = ucc
                }
                if let country_ph_code: String = response[0].value(forKey: "country_ph_code") as? String {
                    User.countryCode        = country_ph_code
                    CountryCode.countryCode = country_ph_code
                }
                
                self.afterLoginDetails(UserId: userId)
                
                
                self.multibaggerstatload(completion: {
                    self.firebaseTokenAddUpdate()
                    self.nrifpipmsload()
                    
                    self.getContent(completion: { (result) in
                        
                    })
                })
                
                
            }
        }
    }
    
    func getContent(completion : @escaping ([Bool])->()){
        let param = "id=0&status=Y"
        let obj = WebService()
        obj.callWebServices(url: Urls.get_refer_content, methodName: "GET", parameters: param, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
            
            print(jsonData)
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                let tempArray = jsonData.value(forKey: "response") as! [NSDictionary]
                
                for arr in tempArray{
                    
                    if let keyValue = arr.value(forKey: "key") as? String {
                        
                        if keyValue == "sharing_screen" {
                            
                            if let content = arr.value(forKey: "value") as? String {
                                Referral_Data.sharing_screen_content = content
                            }
                            
                        }
                        
                        if keyValue == "whatsapp_telegram_msg" {
                            
                            if let content = arr.value(forKey: "value") as? String {
                                Referral_Data.whatsapp_telegram_msg = content
                            }
                        }
                        
                        if keyValue == "tweet_sms" {
                            
                            if let content = arr.value(forKey: "value") as? String {
                                Referral_Data.tweet_sms = content
                            }
                        }
                        
                        if keyValue == "facebook_feed_title" {
                            
                            if let content = arr.value(forKey: "value") as? String {
                                Referral_Data.facebook_feed_title = content
                            }
                        }
                        
                        if keyValue == "how_referral_works" {
                            
                            if let content = arr.value(forKey: "value") as? String {
                                Referral_Data.how_referral_works = content
                            }
                        }
                        
                        if keyValue == "how_scratch_card_works" {
                            
                            if let content = arr.value(forKey: "value") as? String {
                                Referral_Data.how_scratch_card_works = content
                            }
                        }
                        
                        if keyValue == "email_subject" {
                            
                            if let content = arr.value(forKey: "value") as? String {
                                Referral_Data.email_subject = content
                            }
                        }
                        
                        if keyValue == "email_body" {
                            
                            if let content = arr.value(forKey: "value") as? String {
                                Referral_Data.email_body = content
                            }
                        }
                        
                        
                        
                        
                    }
                }
                
            }
        }
    }
    
    
    
    
    func getSeminarAd(){
        let webServices = WebService()
        webServices.callWebServices(url: Urls.seminar_ad, methodName: "GET", parameters: "", istoken: true, tokenval: User.token) { (success, jsonDict) in
            
            if jsonDict.value(forKey: "errmsg") as! String == "" {
                
                if  let response = jsonDict.value(forKey: "response") as? [NSDictionary] {
                    if let url = response[0].value(forKey: "URL") as? String {
                        seminar_ad.url = url
                        seminar_ad.templete = response[0].value(forKey: "TEMPLATE") as! String
                        
                        
                        seminar_ad.ab_button_action = response[0].value(forKey: "ab_button_action") as! String
                        seminar_ad.ab_button_txt = response[0].value(forKey: "ab_button_txt") as! String
                        
                        
                        if seminar_ad.templete != ""
                        {
                            DispatchQueue.main.async {
                                let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "WebinarAdsViewController") as! WebinarAdsViewController
                                self.navigationController?.pushViewController(viewController, animated: true)
                            }
                        }
                        else
                        {
                            DispatchQueue.main.async{
                                let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "DlHomeViewController") as! DlHomeViewController
                                self.navigationController?.pushViewController(viewController, animated: true)
                                
                            }
                        }
                    }
                }
            } else {
                DispatchQueue.main.async {
                    let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "DlHomeViewController") as! DlHomeViewController
                    self.navigationController?.pushViewController(viewController, animated: true)
                }
            }
        }
    }
    
    
    //MARK: Multibagger State Load
    func multibaggerstatload(completion : @escaping ()->())
    {
        let obj = WebService()
        let paremeters = ""
        
        self.actInd.startAnimating()
        
        obj.callWebServices(url: Urls.multibaggerStatus, methodName: "GET", parameters: paremeters, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                print(jsonData)
                
                let tempArray = jsonData.value(forKey: "response") as! [NSDictionary]
                MultibaggerStats.segResultQtrArr.removeAll()
                
                MultibaggerStats.segQtrDateArr.removeAll()
                MultibaggerStats.segYearDateArr.removeAll()
                
                MultibaggerStats.segQtrDisplayArr.removeAll()
                MultibaggerStats.segYearDisplayArr.removeAll()
                
                MultibaggerStats.qutrniftyindex.removeAll()
                MultibaggerStats.qutrsmallcapindex.removeAll()
                MultibaggerStats.yearniftyindex.removeAll()
                MultibaggerStats.yearsmallcapindex.removeAll()
                MultibaggerStats.qutrmidcapindex.removeAll()
                
                
                MultibaggerStats.newqutrsmallcapindex.removeAll()
                MultibaggerStats.newqutrmidcapindex.removeAll()
                MultibaggerStats.newqutrniftyindex.removeAll()
                
                MultibaggerStats.SmallDisplay.removeAll()
                MultibaggerStats.MidDisplay.removeAll()
                MultibaggerStats.LargeDisplay.removeAll()
                
                MultibaggerStats.NseSmallDisplay.removeAll()
                MultibaggerStats.NseMidDisplay.removeAll()
                MultibaggerStats.NseLargeDisplay.removeAll()
                
                
                for arr in tempArray
                {
                    //segQtrDateArr
                    if(arr.value(forKey: "mdt_Rep_Type") as! String == "QUARTER")
                    {
                        MultibaggerStats.segResultQtrArr.append(arr.value(forKey: "mdt_Result_Qtr") as! String)
                        MultibaggerStats.segQtrDateArr.append(arr.value(forKey: "mdt_Date") as! String)
                        MultibaggerStats.segQtrDisplayArr.append(arr.value(forKey: "mdt_Display") as! String)
                        
                        if let mrdSmallCapPer = arr.value(forKey: "mrd_SmallCap_Per") as? String {
                            MultibaggerStats.qutrsmallcapindex.append(mrdSmallCapPer)
                        }
                        if let mrdMidCapPer = arr.value(forKey: "mrd_MidCap_Per") as? String {
                            MultibaggerStats.qutrmidcapindex.append(mrdMidCapPer)
                        }
                        if let mrdNiftyPer = arr.value(forKey: "mrd_Nifty_Per") as? String  {
                            MultibaggerStats.qutrniftyindex.append(mrdNiftyPer)
                        }
                        if let mdtPerfDt = arr.value(forKey: "mdt_Perf_Dt") as? String  {
                            MultibaggerStats.qutrperformancetxt.append(mdtPerfDt)
                        }
                        
                        if let SmallDisplay = arr.value(forKey: "SmallDisplay") as? String {
                            MultibaggerStats.SmallDisplay.append(SmallDisplay)
                        }
                        if let MidDisplay = arr.value(forKey: "MidDisplay") as? String {
                            MultibaggerStats.MidDisplay.append(MidDisplay)
                        }
                        if let LargeDisplay = arr.value(forKey: "LargeDisplay") as? String  {
                            MultibaggerStats.LargeDisplay.append(LargeDisplay)
                        }
                        
                        if let NseSmallDisplay = arr.value(forKey: "NseSmallDisplay") as? String {
                            MultibaggerStats.NseSmallDisplay.append(NseSmallDisplay)
                        }
                        if let NseMidDisplay = arr.value(forKey: "NseMidDisplay") as? String {
                            MultibaggerStats.NseMidDisplay.append(NseMidDisplay)
                        }
                        if let NseLargeDisplay = arr.value(forKey: "NseLargeDisplay") as? String  {
                            MultibaggerStats.NseLargeDisplay.append(NseLargeDisplay)
                        }
                        
                        
                        
                        if let mrdSmpPer = arr.value(forKey: "SmpPer") as? String {
                            MultibaggerStats.newqutrsmallcapindex.append(mrdSmpPer)
                        }
                        if let mrdMidPer = arr.value(forKey: "MidPer") as? String {
                            MultibaggerStats.newqutrmidcapindex.append(mrdMidPer)
                        }
                        if let mrdLarPer = arr.value(forKey: "LarPer") as? String  {
                            MultibaggerStats.newqutrniftyindex.append(mrdLarPer)
                        }
                        
                        
                    }
                    
                    if(arr.value(forKey: "mdt_Rep_Type") as! String == "YEAR")
                    {
                        MultibaggerStats.segYearDateArr.append(arr.value(forKey: "mdt_Date") as! String)
                        MultibaggerStats.segYearDisplayArr.append(arr.value(forKey: "mdt_Display") as! String)
                        
                        if let mrdSmallCapPer = arr.value(forKey: "mrd_SmallCap_Per") as? String {
                            MultibaggerStats.yearsmallcapindex.append(mrdSmallCapPer)
                        }
                        if let mrdNiftyPer = arr.value(forKey: "mrd_Nifty_Per") as? String {
                            MultibaggerStats.yearniftyindex.append(mrdNiftyPer)
                        }
                        MultibaggerStats.yearperformancetxt.append(arr.value(forKey: "mdt_Perf_Dt") as! String )
                    }
                }
                
                
                let segQtrDateArr = UserDefaults.standard
                segQtrDateArr.set(MultibaggerStats.segQtrDateArr, forKey: "segQtrDateArr")
                
                self.actInd.stopAnimating()
                return completion()
                /*
                 DispatchQueue.main.async {
                 
                 self.nrifpipmsload()
                 
                 }*/
            }
        }
    }
    
    func nrifpipmsload()
    {
        let obj = WebService()
        let data = "ALL"
        let param = "ptype=\(data)"
        
        self.actInd.startAnimating()
        
        obj.callWebServices(url: Urls.NRIFPIPMS, methodName: "GET", parameters: param, istoken: false, tokenval: "") { (returnValue, jsonData) in
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                
                // print(jsonData)
                
                let tempArray = jsonData.value(forKey: "response") as! [NSDictionary]
                
                
                for arr in tempArray
                {
                    
                    if(arr.value(forKey: "p_type") as! String == "PMS")
                    {
                        PMS_DATA.PMS1 = arr.value(forKey: "p_why") as! String
                        PMS_DATA.PMS2 = arr.value(forKey: "p_what") as! String
                        PMS_DATA.PMS3 = arr.value(forKey: "p_how") as! String
                    }
                    
                    if(arr.value(forKey: "p_type") as! String == "NRI")
                    {
                        NRI_DATA.NRI1 = arr.value(forKey: "p_what") as! String
                        NRI_DATA.NRI2 = arr.value(forKey: "p_why") as! String
                        NRI_DATA.NRI3 = arr.value(forKey: "p_how") as! String
                    }
                    
                    if(arr.value(forKey: "p_type") as! String == "FPI")
                    {
                        FPI_DATA.FPI1 = arr.value(forKey: "p_what") as! String
                        FPI_DATA.FPI2 = arr.value(forKey: "p_why") as! String
                        FPI_DATA.FPI3 = arr.value(forKey: "p_how") as! String
                    }
                    
                    if(arr.value(forKey: "p_type") as! String == "FPI NEW")
                    {
                        FPI_DATA.FPI_NEW = arr.value(forKey: "p_what") as! String
                    }
                    
                    if(arr.value(forKey: "p_type") as! String == "NRI NEW")
                    {
                        NRI_DATA.NRI_NEW = arr.value(forKey: "p_what") as! String
                    }
                }
                
                DispatchQueue.main.async {
                    self.actInd.stopAnimating()
                    
                    var pageIdentifier    =   ""
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    
                    if(NotificationData.category == ""){
                        self.getSeminarAd()
                    }
                    else {
                        if(NotificationData.category == "customWebView")
                        {
                            pageIdentifier  =   "CustomPushViewController"
                            
                            let viewcontroller = storyBoard.instantiateViewController(withIdentifier: pageIdentifier) as! CustomPushViewController
                            self.navigationController?.pushViewController(viewcontroller, animated: true)
                            
                        }
                        
                        if(NotificationData.category == "Stock_Market_Today")
                        {
                            pageIdentifier  =   "StockMarketTodayViewController"
                            let viewController = self.storyboard!.instantiateViewController(withIdentifier: pageIdentifier) as! StockMarketTodayViewController
                            
                            User.navigation.pushViewController(viewController, animated: false)
                            
                        }
                        
                        if(NotificationData.category == "Value_Stocks")
                        {
                            pageIdentifier  =   "MultibaggerViewController"
                            let viewController = self.storyboard!.instantiateViewController(withIdentifier: pageIdentifier) as! MultibaggerViewController
                            
                            User.navigation.pushViewController(viewController, animated: false)
                            
                        }
                        
                        if(NotificationData.category == "Nifty_Open_Interest")
                        {
                            pageIdentifier  =   "NiftyOpenInterestViewController"
                            let viewController = self.storyboard!.instantiateViewController(withIdentifier: pageIdentifier) as! NiftyOpenInterestViewController
                            
                            User.navigation.pushViewController(viewController, animated: false)
                            
                        }
                        if(NotificationData.category == "Dynamic_Indices")
                        {
                            pageIdentifier  =   "DynamicIndexController"
                            let viewController = self.storyboard!.instantiateViewController(withIdentifier: pageIdentifier) as! DynamicIndexController
                            
                            User.navigation.pushViewController(viewController, animated: false)
                            
                        }
                        
                        if(NotificationData.category == "PMS")
                        {
                            var viewController = UIViewController()
                            pageIdentifier  =   "PMSPDFViewerViewController"
                            if #available(iOS 11.0, *) {
                                viewController = self.storyboard!.instantiateViewController(withIdentifier: pageIdentifier) as! PMSPDFViewerViewController
                            } else {
                                // Fallback on earlier versions
                            }
                            
                            User.navigation.pushViewController(viewController, animated: false)
                            
                        }
                        
                        if(NotificationData.category == "Webinars")
                        {
                            var viewController = UIViewController()
                            pageIdentifier  =   "WebinarsViewController"
                            if #available(iOS 11.0, *) {
                                viewController = self.storyboard!.instantiateViewController(withIdentifier: pageIdentifier) as! WebinarsViewController
                            } else {
                                // Fallback on earlier versions
                            }
                            
                            User.navigation.pushViewController(viewController, animated: false)
                            
                        }
                        
                        if(NotificationData.category == "Sector_Performance")
                        {
                            var viewController = UIViewController()
                            pageIdentifier  =   "MultibaggerQuarterlySectorViewController"
                            if #available(iOS 11.0, *) {
                                viewController = self.storyboard!.instantiateViewController(withIdentifier: pageIdentifier) as! MultibaggerQuarterlySectorViewController
                            } else {
                                // Fallback on earlier versions
                            }
                            
                            User.navigation.pushViewController(viewController, animated: false)
                            
                        }
                    }
                    
                }
            }
        }
    }
    
    
    func firebaseTokenAddUpdate(){
        
        let webServices = WebService()
        var param = ""
        if let token = InstanceID.instanceID().token() {
            param  = "platform=iOS&userid=\(User.email)&token_no=\(token)"
            
            webServices.callWebServices(url: Urls.addUpdateFCMToken, methodName: "POST", parameters: param, istoken: true, tokenval: User.token) { (success, jsonDict) in
                
                print(jsonDict)
                if jsonDict.value(forKey: "errmsg") as! String == "" {
                    
                    let jsonValue = jsonDict
                }
            }
        }
    }
    
    
    
    
    
    // MARK: Performing Login Action
    func performLogin(email : String, password: String, regThrough: String) {
        
        let appManager = AppManager()
        let webService = WebService()
        
        let parameters = "user_email=\(email)&password=\(password)&logthrough=\(regThrough)"
        
        if appManager.isValidEmail(email: email) {
            
            UserDefaults.standard.set(User.email, forKey: "email")
            UserDefaults.standard.set(User.password, forKey: "password")
            UserDefaults.standard.set(User.regThrough, forKey: "regThrough")
            
            //            webService.callWebServices(url: Urls.login, methodName: "POST", parameters: parameters, istoken: false, tokenval: "", completion: { (success, dictData) in
            webService.callWebServices(url: Urls.login_without_emailverification, methodName: "POST", parameters: parameters, istoken: false, tokenval: "", completion: { (success, dictData) in
                print(dictData)
                if dictData.value(forKey: "error") as! NSInteger == 0 {
                    
                    let tokendic = dictData.value(forKey: "response") as! NSDictionary
                    User.token = (tokendic.value(forKey: "token") as! NSString) as String
                    
                    self.getuser()
                    
                }
                else
                {
                    DispatchQueue.main.async {
                        appManager.showAlert(title: "Error", message: (dictData.value(forKey: "response") as! NSString) as String, navigationController: self.navigationController!)
                    }
                }
            })
            
        } else{           
            appManager.showAlert(title: "Error", message: "Email Id Not Valid! \n ", navigationController: self.navigationController!)
        }
    }
    
    
    func sendEmailOTP(email: String) {
        
        let objWebService = WebService()
        
        objWebService.callWebServices(url: Urls.generateEmailOtp, methodName: "POST", parameters: "email=\(User.email)", istoken: false, tokenval: "") { (success, jsonDict) in
            
            //print(jsonDict)
            
            if jsonDict.value(forKey: "response") as! String == "success" {
                
               DispatchQueue.main.async {
                    let EmailViewController = self.storyboard?.instantiateViewController(withIdentifier: "EmailOTPViewController1") as! EmailOTPViewController
                    
                    
                    self.navigationController?.pushViewController(EmailViewController, animated: true)
                }
                
                
            } else {
                
                DispatchQueue.main.async {
                
                self.actInd.stopAnimating()
                let alert = AppManager()
                alert.showAlert(title: "Error!", message: jsonDict.value(forKey: "errmsg") as! String, navigationController: self.navigationController!)
                }
            }
        }
    }
    
    func resend(urlAddress: String, param: String)
    {
        let obj = WebService()
        let token = ""
        
        //print("email = \(emailaddr)")
        //let parameters = "email=\(emailAddr)&phone=\(phoneNum)"
        
        //print(param)
        
        obj.callWebServices(url: urlAddress, methodName: "POST", parameters: param, istoken: false, tokenval: token) { (returnValue, jsonData) in
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                let alertobj = AppManager()
                
                alertobj.showAlert(title: "Success", message: "OTP Sent Sucessfully.", navigationController: self.navigationController!)
            }
            else
            {
                DispatchQueue.main.async {
                    self.actInd.stopAnimating()
                    let alertobj = AppManager()
                    
                    alertobj.showAlert(title: "Error!", message: jsonData.value(forKey: "errmsg") as! String , navigationController: self.navigationController!)
                }
                
            }
            
        }
        
    }
    
    // must be internal or public.
    func timerAction() {
        if counter > 0 {
            lblResendOtp.text = "Did not recieve OTP? Please wait \(counter) second(s)"
            counter -= 1
        }
        else
        {
            self.btnresend.isHidden = false
            lblResendOtp.text = "Did not recieve OTP?"
            lblResendOtp.textAlignment = .left
            view.layoutIfNeeded()
        }
    }
    
    func phoneEmailChange(type: String, urlstr: String, inputtext: String, countryCode : String)
    {
        let obj = WebService()
        let token = ""
        var parameters_val = ""
        
        
        //let newmobileno: Int64 = Int64(inputtext)!
        let newmobileno = "\(countryCode)|\(inputtext)"

       // let newcountryCode: Int64 = Int64(countryCode)!
        //let newmobileno: Int = { return Int(inputtext)! }()
        parameters_val = "email=\(User.email)&phone=\(newmobileno)"
        self.actInd.startAnimating()
        
        obj.callWebServices(url: urlstr, methodName: "POST", parameters: parameters_val, istoken: false, tokenval: token) { (returnValue, jsonData) in
            
            
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                self.phoneNum = inputtext
                User.phone = inputtext
                
                
                DispatchQueue.main.async {
                    
                    self.lbltype.text = "\(countryCode) - \(inputtext)"
                    self.actInd.stopAnimating()
                    let alertobj = AppManager()
                    
                    alertobj.showAlert(title: "Sucess", message: "OTP has been Sent in your new mobile no.", navigationController: self.navigationController!)
                    
                }
                
                //print(jsonData)
            }
            else
            {
                DispatchQueue.main.async {
                    
                    self.actInd.stopAnimating()
                    let alertobj = AppManager()
                    
                    alertobj.showAlert(title: "Error!", message: jsonData.value(forKey: "errmsg") as! String , navigationController: self.navigationController!)
                }
            }
        }
        
        
    }
    
    func buttonClicked(sender: UIButton!) {
       
        DispatchQueue.main.async(execute: {
           self.dismiss(animated: true, completion: nil)
            let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "CountryCodeSearchViewController") as! CountryCodeSearchViewController
            self.navigationController?.pushViewController(viewController, animated: true)
            
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        if(CountryCode.countryCode != ""){
            code = "+\(CountryCode.countryCode)"
            
            // self.askForNumber(screenType: checkotptype);
            
    }
        else{
            code = "+91"
            
        }
        
       // lbltype.text = "\(code) - \(User.phone)"
        self.codeTextField?.text = code
        self.numberTextField?.text = number
        
    }

    func askForNumber(prefix: String = "", screenType: String){
        
        var urlstring   = Urls.generatePhoneOtp
        var title = "";
        var message = "Enter your mobile number\n\n\n";
        var alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert);
        alert.isModalInPopover = true;
      
        func handleOk(alertView: UIAlertAction!){
            
             code = self.codeTextField!.text!;
             number = self.numberTextField!.text!;
            if(CountryCode.countryCode != ""){
                code = "\(CountryCode.countryCode)"}
            else{
               code = codeTextField!.text!
            }
           
            if (code.characters.count) < 2  {
                //AppManager().showAlert(title: "Invalid Country Code.", message: "", navigationController: (parent?.navigationController)!)
            }
            
            if (number.characters.count) < 10  {
                //AppManager().showAlert(title: "Invalid Phone Number.", message: "", navigationController: self.navigationController!)
            }
                
                
                
            else
            {
                
                self.phoneEmailChange(type: screenType ,urlstr: urlstring, inputtext: number, countryCode: code)
            }
        }
        
        
        let inputFrame = CGRect(x: 0, y: 70, width: 270, height: 25)
        let inputView: UIView = UIView(frame: inputFrame);
        

        let codeButtonFrame = CGRect(x: 20, y: 0, width: 65, height: 25)
        let countryCodeButtonField: UIButton = UIButton(frame: codeButtonFrame);
        countryCodeButtonField.setTitle("", for: .normal)
        countryCodeButtonField.addTarget(self, action:#selector(self.buttonClicked), for: .touchUpInside)
	
      
        //let bottmConstraint = NSLayoutConstraint(
         //   item: triangleDownImage, attribute: .bottom, relatedBy: .equal, toItem: self.bottomLayoutGuide, attribute:.top, multiplier: 1, constant: 20)
        //triangleDownImage.addConstraint(bottmConstraint)
        
        let codeFrame = CGRect(x: 20, y: 0, width: 65, height: 25)
        let countryCodeTextField: UITextField = UITextField(frame: codeFrame);
        countryCodeTextField.placeholder = "Code";
        countryCodeTextField.borderStyle = UITextBorderStyle.roundedRect;
        countryCodeTextField.keyboardType = UIKeyboardType.decimalPad;
        countryCodeTextField.text = code
        
        let triangleDownImageFrame = CGRect(x: countryCodeTextField.frame.width , y: countryCodeTextField.frame.height/2, width: 17, height: 15)
        let triangleDownImage: UIImageView = UIImageView(frame: triangleDownImageFrame)
        triangleDownImage.image = #imageLiteral(resourceName: "Triangle_Down")
        
        let numberFrame = CGRect(x: 90, y: 0, width: 170, height: 25)
        let myNumberTextField: UITextField = UITextField(frame: numberFrame);
        myNumberTextField.placeholder = "Mobile No";
        myNumberTextField.borderStyle = UITextBorderStyle.roundedRect;
        myNumberTextField.keyboardType = UIKeyboardType.decimalPad;
        
        self.codeTextField = countryCodeTextField;
        self.numberTextField = myNumberTextField;
        self.buttonField = countryCodeButtonField;
        self.imageField = triangleDownImage;
        
        
        //inputView.addSubview(prefixLabel);
        inputView.addSubview(self.codeTextField!);
        inputView.addSubview(self.numberTextField!);
        inputView.addSubview(self.buttonField!);
        inputView.addSubview(self.imageField!);
       
        alert.view.addSubview(inputView);
        
        
        alert.addAction(UIAlertAction(title: "Change", style: UIAlertActionStyle.default, handler:handleOk));
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler:nil));
        
        self.present(alert, animated: true, completion: nil);
        
    }
}



    
    func changeButtonClick(screenType: String )
    {
        /********************************************************************
        
        var urlstring   =   ""
        var titlestr    =   ""
        var msg         =   ""
        
        urlstring   = Urls.generatePhoneOtp
        titlestr    = "Change Mobile No"
        msg         = "Please enter your new Mobile No."
        
        
        
        
 
        //1. Create the alert controller.
        let alert = UIAlertController(title: titlestr, message: msg, preferredStyle: .alert)
       // alert.view.tintColor = UIColor(red: 247/255, green: 187/255, blue: 26/255, alpha: 1)
        
       
        
        
        //2. Add the text field. You can configure it however you need.
        
        alert.addTextField { (textField) in
            textField.text = "+91"
            textField.setBottomBorder()
            textField.keyboardType = UIKeyboardType.phonePad;
            
//            textField.frame = CGRect(x: 0, y: 18, width: alert.view.frame.width / 5, height: 30)
            
            var oldFrame = textField.frame
            oldFrame.origin.y = 18
            oldFrame.size.height = 30
            oldFrame.size.width = 25
            textField.frame = oldFrame

            
        }
        
        alert.addTextField { (textField) in
            
            textField.placeholder = "mobile no";
            //textField.layer.addSublayer(border)
            textField.layer.masksToBounds = true
            //border.frame = CGRect(x: 0, y: textField.frame.size.height - width, width:  textField.frame.size.width, height: textField.frame.size.height)
            textField.keyboardType = UIKeyboardType.phonePad;
            
             textField.frame = CGRect(x: textField.frame.origin.x + 5, y: 18, width: 130, height: 30)
        }
        
        // 3. Grab the value from the text field, and print it when the user clicks OK.
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (AAA) in
            
        }))
        
        alert.addAction(UIAlertAction(title: "Verify", style: .default, handler: { [weak alert] (_) in
            
            
            if (alert!.textFields![1].text?.characters.count)! < 10  {
                AppManager().showAlert(title: "Invalid Phone Number.", message: "", navigationController: self.navigationController!)
            }
            else
            {
                
                self.phoneEmailChange(type: screenType ,urlstr: urlstring, inputtext: alert!.textFields![1].text!, countryCode: alert!.textFields![0].text!)
            }
            
            
            
        }))
        // 4. Present the alert.
        self.present(alert, animated: true, completion: nil)
 
        
        
        ********************************************************************/
        
}


