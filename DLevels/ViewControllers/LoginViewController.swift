//
//  LoginViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-02 on 28/12/16.
//  Copyright © 2016 Dynamic-Mac-01. All rights reserved.
//

import Foundation
import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import FirebaseInstanceID
import FirebaseMessaging


class LoginViewController: UIViewController, UITextFieldDelegate, GIDSignInDelegate, GIDSignInUIDelegate{
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    var isShowingMenu = false
    
    @IBOutlet weak var backNavigation: UIBarButtonItem!
    
    @IBOutlet weak var viewLeadingConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var btninfo: UIButton!
    
    

    @IBAction func cross(_ sender: Any) {
        
        actInd.startAnimating()
        User.regThrough = RegThrough.normal
        User.email = "guest@yopmail.com"
        User.password = "123456"
        
        if  Reachability.isInternetAvailable() {
            
            self.performLogin(email: User.email, password: User.password, regThrough: User.regThrough)
            
            
            
        } else {
            
            let appManager = AppManager()
            appManager.showAlert(title: "Error", message: "No Internet Access!", navigationController: self.navigationController!)
        }
    }
    
    
    
    var navController: UINavigationController?
    
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    var x = 0
    var instanceOfLeftSlideMenu = LeftSlideMenu()
    override func viewDidLoad() {
        super.viewDidLoad()
      //  self.hideKeyboardOnTapOutside()
        AppManager().setStatusBarBackgroundColor()
        // Activity Indicator For Temporary
       instanceOfLeftSlideMenu.navController = self.navigationController!
       User.navigation = self.navigationController!
        if User.email == "guest@yopmail.com"{}
        else{
        addLoader()
        }
        txtEmail.delegate = self
        txtPassword.delegate = self
        
        self.btninfo.layer.cornerRadius = self.btninfo.frame.width/2
        self.btninfo.layer.masksToBounds = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(sender:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(sender:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    
    
    
        
        
        
       //txtEmail.text = "sudhanshubharti25@gmail.com"
        //txtPassword.text = "password"
        
//        UserDefaults.standard.set(nil, forKey: "phone")
//        UserDefaults.standard.set(nil, forKey: "email")
//        UserDefaults.standard.set(nil, forKey: "password")
//        UserDefaults.standard.set(nil, forKey: "regThrough")
        
        //print(GIDSignIn.sharedInstance().currentUser)
        
        //GIDSignIn.sharedInstance().signOut()
        //  FBSession.activeSession().closeAndClearTokenInformation()
        ///   let loginmamager =
        // FBSDKLoginManager.logOut(<#T##FBSDKLoginManager#>)
        
        //  let loginManager = FBSDKLoginManager()
        //  loginManager.logOut()
        //  FBSDKLoginManager().logOut()
        
        
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        
//        if((FBSDKAccessToken.current() != nil))
//        {
//
//        }
        
        GIDSignIn.sharedInstance().signInSilently()
        
        self.title = "Let me test this once."
        
    }
    
    
    
    func keyboardWillShow(sender: NSNotification) {
        
        if(x == 0)
        {
            self.view.frame.origin.y -= 50
            x = -50
        }
    }
    
    func keyboardWillHide(sender: NSNotification) {
        if(x < 0)
        {
            self.view.frame.origin.y += 50
            x = 0
        }
    }
    
    func addLoader() {
        actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0) //CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.gray
        self.view.addSubview(actInd)
    }
    
    @IBAction func btnHamburger_Action(_ sender: Any) {
        
     /*  instanceOfLeftSlideMenu.navController = User.navigation
        if !isShowingMenu {
            instanceOfLeftSlideMenu.prepareScreenWithView(navigationController: self.navigationController!, viewSize: self.view)
            viewLeadingConstraint.constant = 250
            
            isShowingMenu = true
           
        } else {
            
            instanceOfLeftSlideMenu.hideSlideMenu(navigationController: self.navigationController!, viewSize: self.view)
            viewLeadingConstraint.constant = 0
            isShowingMenu = false
            //instanceOfLeftSlideMenu.navController = UINavigationController()
        }*/
        
        self.navigationController?.popViewController(animated: false)
    }
    // MARK:
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        actInd.stopAnimating()
        btnLogin.isEnabled = true
        
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        
    }
    
    // MARK:
    override func viewDidAppear(_ animated: Bool) {
        actInd.stopAnimating()
        btnLogin.isEnabled = true
        
        let loginmanager=LoginManager()
        loginmanager.logOut()
    }
    
    
    @IBAction func btnInfoClick(_ sender: Any) {
        
        let appManager = AppManager()
        let msg = "You may login with any of your Dynamic Levels/ Value Stocks/ MFDirect registered Email id or Sign Up to create a new common account."
        appManager.showAlert(title: "", message: msg, navigationController: self.navigationController!)
    }
    
        
    
        
    
    
    @IBAction func forgotPassword_Action(_ sender: UIButton) {
        
        let signUPScreenViewConroller = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
        self.navigationController?.pushViewController(signUPScreenViewConroller, animated: true)
        
        // GIDSignIn.sharedInstance().signOut()
        
    }
    private func prepareScreen() {
        
        print(">>>>>Constriant value >>>>\(Constants.someNotification)")
        
        // Hide keybord on tap outside
        self.hideKeyboardOnTapOutside()
        AppManager().setStatusBarBackgroundColor() // Set status bar color
    }
    
    
    @IBAction func signUpScreen_Action(_ sender: UIButton) {
        
        let signUPScreenViewConroller = self.storyboard?.instantiateViewController(withIdentifier: "SignUPScreenViewController") as! SignUpViewController
        self.navigationController?.pushViewController(signUPScreenViewConroller, animated: true)
        
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if error == nil {
            
            // ***
            User.firstName       =   user.profile.givenName
            User.lastName        =   user.profile.familyName
            User.email           =   user.profile.email
            User.socialId        =   user.userID
            User.userId          =   user.userID
            User.password        =   user.authentication.idToken
            print(" access token is : \(user.authentication.idToken)")
            User.regThrough     = RegThrough.gmail
            // var idToken  = GIDGoogleUser.getauto
            print("Acess token is:  \(user.authentication.accessToken)")
            print(" User Id:  \(user.userID)")
         //    User.firstName
            
            User.socialToken    =   user.authentication.idToken
            
              print(" User name:  \( User.firstName)")
            performLogin(email: User.email, password: User.password, regThrough: User.regThrough)
            
            
        } else{
            print(error.localizedDescription)
        }
    }
    
    
    // MARK: UITextField Delegate Methode to return keyboard on return key press
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
//    func isEmailExist(email : String, regThrough: String) {
//       actInd.startAnimating()
//        let webServiceObj = WebService()
//        
//        webServiceObj.callWebServices(url: Urls.accountStatus, methodName: "POST", parameters: "email=\(email)", istoken: false, tokenval: "") { (success, returnData) in
//            
//            print("Return data is: \(returnData)")
//            
//        }
//        
//    }
    
    // Mark : Checking Existance Of Email
    func isEmailExist(email : String, regthrough: String) {
        actInd.startAnimating()
        
        // self.view.backgroundColor=UIColor.red;
        let webService = WebService()
        let parameters = "email=\(email)"
        
        webService.callWebServices(url: Urls.accountStatus, methodName: "POST", parameters: parameters, istoken: false, tokenval: "") { (status, response) in
            print("Response is:  \(response)")
            
            if response.value(forKeyPath: "errmsg") as! String == ""{
                
                if(regthrough == "gmail")
                {
                    self.performLogin(email: User.email, password: User.password, regThrough: User.regThrough)
                }
                
                if(regthrough == "facebook")
                {
                    self.performLogin(email: User.email, password: User.password, regThrough: User.regThrough)
                }
                
                
            }
            else
            {
                
                
                if(regthrough == "gmail")
                {
                    
                    DispatchQueue.main.async(execute: {
                        
                        let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "PhoneValidationViewController") as! PhoneValidationViewController
                        self.navigationController?.pushViewController(viewController, animated: true)
                    })
                    
                    
                }
                
                if(regthrough == "facebook")
                {
                    
                    DispatchQueue.main.async {
                        
                        let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "PhoneValidationViewController") as! PhoneValidationViewController
                        self.navigationController?.pushViewController(viewController, animated: true)
                    }
                  
                }
            }
        }
        
        
          actInd.stopAnimating()
    }
    

    
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: Login Function
    @IBAction func loginAction(_ sender: UIButton) {
        checkSubscription()
        if Subscription_Data.status != false{
            print("go")
        }else{
        
        actInd.startAnimating()
        User.regThrough = RegThrough.normal
        User.email = txtEmail.text!
        User.password = txtPassword.text!
        
        if  Reachability.isInternetAvailable() {
            
            self.performLogin(email: User.email, password: User.password, regThrough: User.regThrough)
            
            
            
        } else {
            
            let appManager = AppManager()
            appManager.showAlert(title: "Error", message: "No Internet Access!", navigationController: self.navigationController!)
        }
    }
    }
    
    
    func getUser (completion : @escaping ([NSDictionary])->()) {
        
        let obj = WebService()
        
        let paremeters = "user=\(User.userId)"//"user_id=211297"
        
        //actInd.startAnimating()
        
        obj.callWebServices(url: Urls.get_user_subscription, methodName: "POST", parameters: paremeters, istoken: false, tokenval: User.token) { (returnValue, jsonData) in
            
            var result : [NSDictionary]
            result=[NSDictionary]()
            print("getuser value is :\(jsonData)")
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                if jsonData.value(forKey: "response") as? [NSDictionary] != nil{
                    result = jsonData.value(forKey: "response") as! [NSDictionary]
                    return completion(result)
                }
                
                return completion(result)
                
            } else{
                
            }
            
        }
    }
    func checkSubscription() {
        
        getUser(completion: { (myuser) in
            
            if (myuser.count > 0) {
                
                let status = myuser[0].value(forKey: "ps_status") as? String
                
                if status?.lowercased() != "active"{
                    //self.btnNonSubscription.isHidden = false
                    Subscription_Data.status = false
                    
                    //self.btnVideoTourAfterSubscription.isHidden = true
                    //self.btnVideoTourAfterSubscription.isHidden = false
                    /*
                     */
                    //
                }else {
                    //self.btnNonSubscription.isHidden = true
                    Subscription_Data.status = true
                    // self.btnSubscribe.isHidden = false
                    // self.btnVideoTour.isHidden = false
                   // self.btnVideoTourAfterSubscription.isHidden = false
                }
            }else{
                
                // self.btnNonSubscription.isHidden = false
                Subscription_Data.status = false
                
               // self.btnVideoTourAfterSubscription.isHidden = true
                //self.btnVideoTourAfterSubscription.isHidden = false
                
            }
            
        })
    }
    
    //MARK: Performing login Action
    func performLogin(email : String, password: String, regThrough: String) {
        
        let appManager = AppManager()
        let webService = WebService()
        
        let parameters = "user_email=\(email)&password=\(password)&logthrough=\(regThrough)"
        
        if appManager.isValidEmail(email: email) {
            
//            webService.callWebServices(url: Urls.login, methodName: "POST", parameters: parameters, istoken: false, tokenval: "", completion: { (success, dictData) in
//              
            webService.callWebServices(url: Urls.login_without_emailverification, methodName: "POST", parameters: parameters, istoken: false, tokenval: "", completion: { (success, dictData) in
                
                print("Login Return Result: \(dictData)  \n   Wait here...")
                
                UserDefaults.standard.set(User.email, forKey: "email")
                UserDefaults.standard.set(User.password, forKey: "password")
                UserDefaults.standard.set(User.regThrough, forKey: "regThrough")
                
                if dictData.value(forKey: "error") as! NSInteger == 216
                {
                    let errDict = dictData.value(forKey: "errmsg") as! NSDictionary
                    if errDict.value(forKey: "phone") as! Int == 0 {
                        self.actInd.stopAnimating()
                        DispatchQueue.main.async(execute: {
                            User.phone = errDict.value(forKey: "phoneno") as! String
//                            User.countryCode = UserDefaults.standard.value(forKey: "countryCode") as! String
//                            CountryCode.countryCode = UserDefaults.standard.value(forKey: "countryCode") as! String
                            let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                            viewController.checkotptype = "phone"
                            //viewController.isLogin = true
                            
                            self.navigationController?.pushViewController(viewController, animated: true)
                            
                        })
                        
                    }
                    else if errDict.value(forKey: "email") as! Int == 0
                    {
                        
                        self.actInd.stopAnimating()
                        DispatchQueue.main.async(execute: {
                            self.verifyEmail(email: User.email, phone: "")
                        })
                    }
                    
                }
                else if dictData.value(forKey: "error") as! NSInteger == 0
                {
                    self.actInd.stopAnimating()
                    
                    
                    //print(dictData)
                    let responce = dictData.value(forKey: "response") as! NSDictionary
                    User.token = responce.value(forKey: "token") as! String
                    self.getUser()
                    
                    
//                    if responce.value(forKey: "onboarding") as! Int == 1 {
//                        DispatchQueue.main.async(execute: {
//
//                            User.token = responce.value(forKey: "token") as! String
//
//                            self.getUser()
//                        })
//                    } else {
//
//                        DispatchQueue.main.async(execute: {
//
//                            let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "OnboardingViewController") as! OnboardingViewController
//                            self.navigationController?.pushViewController(viewController, animated: true)
//                        })
//                    }
                    
                }
                    
                 else if dictData.value(forKey: "error") as! NSInteger == 211
                {
                    
                    UserDefaults.standard.set("", forKey: "email")
                    UserDefaults.standard.set("", forKey: "password")
                    UserDefaults.standard.set("", forKey: "regThrough")
                    
                    
                    DispatchQueue.main.async(execute: {
                        self.actInd.stopAnimating()
                        
                        appManager.showAlert(title: "Error", message: "Wrong EmailId/Password!", navigationController: self.navigationController!)
                    })
                    
                    
                    if(User.regThrough == "gmail")
                    {
                        
                        DispatchQueue.main.async(execute: {
                            
                            let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "PhoneValidationViewController") as! PhoneValidationViewController
                            viewController.isLogin = false
                            
                            self.navigationController?.pushViewController(viewController, animated: true)
                            
                        })
                    }
                }
                    
                else
                {
                    let appManager = AppManager()
                    
                    DispatchQueue.main.async(execute: {
                        self.actInd.stopAnimating()
                        
                    })
                    
                    UserDefaults.standard.set("", forKey: "email")
                    UserDefaults.standard.set("", forKey: "password")
                    UserDefaults.standard.set("", forKey: "regThrough")
                    
                    if(User.regThrough == "gmail")
                    {
                        GIDSignIn.sharedInstance().signOut()
                    }
                    
                    appManager.showAlert(title: "Error", message: dictData.value(forKey: "errmsg") as! String, navigationController: self.navigationController!)
                }
            })
            
        }
        else
        {
            
            self.actInd.stopAnimating()
            
            appManager.showAlert(title: "Error", message: "Email Id Not Valid! \n ", navigationController: self.navigationController!)
        }
    }
    
    func afterLoginDetails(UserId: String) {
        
        let webServices = WebService()
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        let deviceID = UIDevice.current.identifierForVendor?.uuidString as! String
        
        let param = "user_id=\(UserId)&app=IOS&app_ver=\(version)&device_id=\(deviceID)"
        
        webServices.callWebServices(url: Urls.processLoginDetails, methodName: "POST", parameters: param, istoken: true, tokenval: User.token) { (success, jsonDict) in
            
            
            if jsonDict.value(forKey: "errmsg") as! String == "" {
                
            } else{
                let appManager = AppManager()
                appManager.showAlert(title: "Error!", message: jsonDict.value(forKey: "errmsg") as! String, navigationController: self.navigationController!)
            }
        }
    }
    
   
    
    func getSeminarAd(){
        let webServices = WebService()
        webServices.callWebServices(url: Urls.seminar_ad, methodName: "GET", parameters: "", istoken: true, tokenval: User.token) { (success, jsonDict) in
            
            if jsonDict.value(forKey: "errmsg") as! String == "" {
                
                if  let response = jsonDict.value(forKey: "response") as? [NSDictionary] {
                    if let url = response[0].value(forKey: "URL") as? String {
                        seminar_ad.url = url
                        seminar_ad.templete = response[0].value(forKey: "TEMPLATE") as! String
                        
                        seminar_ad.ab_button_action = response[0].value(forKey: "ab_button_action") as! String
                        seminar_ad.ab_button_txt = response[0].value(forKey: "ab_button_txt") as! String
                        
                        
                        if seminar_ad.templete != "" && User.email == "guest@yopmail.com"
                        {
                            DispatchQueue.main.async {
                                let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "WebinarAdsViewController") as! WebinarAdsViewController
                                self.navigationController?.pushViewController(viewController, animated: true)
                            }
                        }
                        else
                        {
                            DispatchQueue.main.async{
                                let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "DlHomeViewController") as! DlHomeViewController
                                self.navigationController?.pushViewController(viewController, animated: true)
                                
                            }
                        }
                    }
                }
            } else {
                DispatchQueue.main.async {
                    let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "DlHomeViewController") as! DlHomeViewController
                    self.navigationController?.pushViewController(viewController, animated: true)
                }
            }
        }
    }
    
    func getUser() {
        
        let webServices = WebService()
        webServices.callWebServices(url: Urls.getUser, methodName: "GET", parameters: "", istoken: true, tokenval: User.token) { (success, jsonDict) in
            
            
            if jsonDict.value(forKey: "errmsg") as! String == "" {
                
                let response = jsonDict.value(forKey: "response") as! [NSDictionary]
                let userId      = response[0].value(forKey: "userId") as! String
                
                User.userId = userId
                User.socialId = userId
                User.firstName = response[0].value(forKey: "firstName") as! String
                User.lastName = response[0].value(forKey: "lastName") as! String
                User.email = response[0].value(forKey: "emailID") as! String
                User.phone = response[0].value(forKey: "phone") as! String
                
                if let referer_code: String = response[0].value(forKey: "referer_code") as? String {
                    User.referer_code = referer_code
                }
                if let fundaccount: String = response[0].value(forKey: "fund_account_id") as? String {
                    User.fundaccount = fundaccount
                }
                if let refererredby: String = response[0].value(forKey: "referred_by") as? String {
                    User.refererredby = refererredby
                }
                
                if let mfKyc: String = response[0].value(forKey: "MF_kyc") as? String {
                    User.MF_kyc = mfKyc
                }
                
                if let mfUccStatus: String = response[0].value(forKey: "MF_ucc_status") as? String {
                    User.MF_ucc_status = mfUccStatus
                }
                
                if let MF_UserId: String = response[0].value(forKey: "MF_UserId") as? String {
                    User.MF_UserId = MF_UserId
                }
                
                if let ucc: String = response[0].value(forKey: "ucc") as? String {
                    User.ucc = ucc
                }
                if let country_ph_code: String = response[0].value(forKey: "country_ph_code") as? String {
                    User.countryCode        = country_ph_code
                    CountryCode.countryCode = country_ph_code
                }
                
                
                
                UserDefaults.standard.set(User.email, forKey: "email")
                UserDefaults.standard.set(User.password, forKey: "password")
                UserDefaults.standard.set(User.regThrough, forKey: "regThrough")
                
                User.navigation = self.navigationController!
                
                
                self.afterLoginDetails(UserId: userId)
                
                self.multibaggerstatload(completion: {
                    self.firebaseTokenAddUpdate()
                    self.nrifpipmsload()
                    
                    self.getContent(completion: { (result) in
                        
                    })
                })
                
                var pageIdentifier = ""
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                /*
                if(NotificationData.clickAction == ""){
                    
                    self.getSeminarAd()
                    
                    
//                    pageIdentifier = "DlHomeViewController"
//                    let viewcontroller = storyBoard.instantiateViewController(withIdentifier: pageIdentifier) as! DlHomeViewController
//
//                    self.navigationController?.pushViewController(viewcontroller, animated: true)
                    return
                }
                else {
                    if(NotificationData.clickAction == "webinars")
                    {
                        pageIdentifier  =   "WebinarsViewController"
                        
                        let viewcontroller = storyBoard.instantiateViewController(withIdentifier: pageIdentifier) as! WebinarsViewController
                        
                        self.navigationController?.pushViewController(viewcontroller, animated: true)
                        return
                    }
                    
                    if(NotificationData.clickAction == "dynamic_smallcap_index")
                    {
                        pageIdentifier  =   "DynamicIndexController"
                        
                        let viewcontroller = storyBoard.instantiateViewController(withIdentifier: pageIdentifier) as! DynamicIndexController
                        
                        viewcontroller.idxtype = "SMALL"
                        
                        self.navigationController?.pushViewController(viewcontroller, animated: true)
                        return
                    }
                    
                    if(NotificationData.clickAction == "quarterly_sectoral_performance")
                    {
                        pageIdentifier  =   "MultibaggerQuarterlySectorViewController"
                        
                        let viewcontroller = storyBoard.instantiateViewController(withIdentifier: pageIdentifier) as! MultibaggerQuarterlySectorViewController
                        
                        
                        self.navigationController?.pushViewController(viewcontroller, animated: true)
                        return
                    }
                    
                    if(NotificationData.clickAction == "quarterly_list_dynamic_smallcap_multibaggers")
                    {
                        pageIdentifier  =   "MultibaggerViewController"
                        
                        let viewcontroller = storyBoard.instantiateViewController(withIdentifier: pageIdentifier) as! MultibaggerViewController
                        
                        
                        
                        self.navigationController?.pushViewController(viewcontroller, animated: true)
                        return
                    }
                    
                    
                    
                    if(NotificationData.clickAction == "gainers_losers_dynamic_smallcap_multibaggers")
                    {
                        pageIdentifier  =   "MultibaggerViewController"
                        
                        let viewcontroller = storyBoard.instantiateViewController(withIdentifier: pageIdentifier) as! MultibaggerViewController
                        
                        
                        
                        self.navigationController?.pushViewController(viewcontroller, animated: true)
                        return
                    }
                    
                }*/
                
            } else{
                let appManager = AppManager()
                appManager.showAlert(title: "Error!", message: jsonDict.value(forKey: "errmsg") as! String, navigationController: self.navigationController!)
            }
        }
        
        
        
    }
    
    
    func getContent(completion : @escaping ([Bool])->()){
        let param = "id=0&status=Y"
        let obj = WebService()
        obj.callWebServices(url: Urls.get_refer_content, methodName: "GET", parameters: param, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
            
            print(jsonData)
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                let tempArray = jsonData.value(forKey: "response") as! [NSDictionary]
                
                for arr in tempArray{
                    
                    if let keyValue = arr.value(forKey: "key") as? String {
                        
                        if keyValue == "sharing_screen" {
                            
                            if let content = arr.value(forKey: "value") as? String {
                                Referral_Data.sharing_screen_content = content
                            }
                            
                        }
                        
                        if keyValue == "whatsapp_telegram_msg" {
                            
                            if let content = arr.value(forKey: "value") as? String {
                                Referral_Data.whatsapp_telegram_msg = content
                            }
                        }
                        
                        if keyValue == "tweet_sms" {
                            
                            if let content = arr.value(forKey: "value") as? String {
                                Referral_Data.tweet_sms = content
                            }
                        }
                        
                        if keyValue == "facebook_feed_title" {
                            
                            if let content = arr.value(forKey: "value") as? String {
                                Referral_Data.facebook_feed_title = content
                            }
                        }
                        
                        if keyValue == "how_referral_works" {
                            
                            if let content = arr.value(forKey: "value") as? String {
                                Referral_Data.how_referral_works = content
                            }
                        }
                        
                        if keyValue == "how_scratch_card_works" {
                            
                            if let content = arr.value(forKey: "value") as? String {
                                Referral_Data.how_scratch_card_works = content
                            }
                        }
                        
                        if keyValue == "email_subject" {
                            
                            if let content = arr.value(forKey: "value") as? String {
                                Referral_Data.email_subject = content
                            }
                        }
                        
                        if keyValue == "email_body" {
                            
                            if let content = arr.value(forKey: "value") as? String {
                                Referral_Data.email_body = content
                            }
                        }
                        
                        
                        
                        
                    }
                }
                
            }
        }
    }
    
    
    func verifyEmail(email: String, phone: String) {
        
        let objWebService = WebService()
        var parameter = ""
        if phone == "" {
            parameter = "email=\(email)"
        } else {
            parameter = "email=\(email)"
        }
        
        objWebService.callWebServices(url: Urls.generateEmailOtp, methodName: "POST", parameters: parameter, istoken: false, tokenval: "") { (success, jsonResult) in
            
            print(jsonResult)   
            
            if jsonResult.value(forKey: "response") as!String == "success" {
                DispatchQueue.main.async(execute: {
                    
                    let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                    viewController.checkotptype = "email"
                    self.navigationController?.pushViewController(viewController, animated: true)
                })
                
            } else {
                
                let appManager = AppManager()
                appManager.showAlert(title: "Error", message: jsonResult.value(forKey: "errmsg") as! String, navigationController: self.navigationController!)
            }
            
            
        }
        
    }
    
   
    
    @IBAction func btnFacebookAction(_ sender: UIButton) {
        
   //     let loginManager = FBSDKLoginManager()
   //     loginManager.logOut()
        
        if Reachability.isInternetAvailable() {
            //actInd.startAnimating()
            
            
            
            let fbLoginManger : LoginManager = LoginManager()
           fbLoginManger.loginBehavior = .browser //.systemAccount
         //  fbLoginManger.loginBehavior = FBSDKLoggingBehaviorGraphAPIDebugWarnin
            
            fbLoginManger.logIn(permissions: ["email"], from: self) { (result, error) in  if error == nil {
                    
                let fbLoginResult : LoginManagerLoginResult = result!
                    
                    if (result?.isCancelled)! {
                        
                       //  [FBSDKProfile enableUpdatesOnAccessTokenChange:YES];
                        print("dssdsd")
                        //Profile.enableUpdates(onAccessTokenChange: true)
                        Profile.enableUpdatesOnAccessTokenChange(true)
                        fbLoginManger.logOut()
                        return
                        
                        
                    }
                    if fbLoginResult.grantedPermissions.contains("email"){
                        if let fbToken = result?.token!.tokenString {
                            print(fbToken)
                            self.getFBUserDate(fbTokenval: fbToken)
                        }
                        
                         //fbLoginManger.logOut()
                    }
                    
                     //self.getFBUserDate()
                }
            }
        } else {
            
            let appManager = AppManager()
            appManager.showAlert(title: "Error", message: "No Internet Access!", navigationController: self.navigationController!)
            
        }
    }

    func getFBUserDate(fbTokenval: String) {
        
        print(" facebook Token id is:  \(AccessToken.current!.tokenString)")
        
        
        
        if AccessToken.current! != nil {
            
            GraphRequest(graphPath: "me", parameters: ["fields":"id, name, first_name, last_name, picture.type(large),email"]).start(completionHandler: { (connection, result, error) in
                
                if error == nil {
                    print(result!)
                    
                    let returnResult : [String: Any] = result as! [String : Any]
                    
                    
                   if returnResult["email"] as? String != "" {
//
                    print(fbTokenval)
//                    var token: String! = ""
//                       token = FBSDKAccessToken.current().tokenString
//
                    //print(token)
                        
                        User.email          =  returnResult["email"] as! String //email
                        User.firstName      =  returnResult["first_name"] as! String//firstName
                        User.lastName       =  returnResult["last_name"] as! String //lastName
                        User.socialToken    =  fbTokenval
                        User.socialId       =  returnResult["id"] as! String
                        User.regThrough     =   RegThrough.facebook
                        
                        User.password = fbTokenval
                        
                        
                        self.isEmailExist(email: User.email, regthrough: User.regThrough)
                        
                        
                    } else {

                        AppManager().showAlert(title: "Error", message: "We are facing some issue to login with your account please try another options.", navigationController: self.navigationController!)

                    }
                
                   
                }
                
            })
        }
    }
    
    // Mark: Textfield Delgates
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == txtPassword {
          
            if UIScreen.main.bounds.height <= CGFloat(Phone.iPhone_5s) {
            
            UIView.animate(withDuration: 0.2, animations: {
                self.view.frame.origin.y -= 100
            })
            }
        }
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if UIScreen.main.bounds.height <= CGFloat(Phone.iPhone_5s) {
            
        if textField == txtPassword {
            UIView.animate(withDuration: 0.2, animations: {
                self.view.frame.origin.y += 100
            })
        }
            
        }
        
        if !(txtEmail.text == "" && txtPassword.text == "") {
            btnLogin.isEnabled = true
        }
    }
    
    
    @IBAction func btnGoogleAction(_ sender: UIButton) {
        
        
        if Reachability.isInternetAvailable() {
            
            GIDSignIn.sharedInstance().signIn()
            //actInd.startAnimating()
            
        } else {
            
            let appManager = AppManager()
            appManager.showAlert(title: "Error", message: "No Internet Access!", navigationController: self.navigationController!)
            
        }
    }
    
    
    //MARK: Multibagger State Load
    func multibaggerstatload(completion : @escaping ()->())
    {
        let obj = WebService()
        let paremeters = ""
        
        self.actInd.startAnimating()
        
        obj.callWebServices(url: Urls.multibaggerStatus, methodName: "GET", parameters: paremeters, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                print(jsonData)
                
                let tempArray = jsonData.value(forKey: "response") as! [NSDictionary]
                MultibaggerStats.segResultQtrArr.removeAll()
                
                MultibaggerStats.segQtrDateArr.removeAll()
                MultibaggerStats.segYearDateArr.removeAll()
                
                MultibaggerStats.segQtrDisplayArr.removeAll()
                MultibaggerStats.segYearDisplayArr.removeAll()
                
                MultibaggerStats.qutrniftyindex.removeAll()
                MultibaggerStats.qutrsmallcapindex.removeAll()
                MultibaggerStats.yearniftyindex.removeAll()
                MultibaggerStats.yearsmallcapindex.removeAll()
                MultibaggerStats.qutrmidcapindex.removeAll()
                
                
                MultibaggerStats.newqutrsmallcapindex.removeAll()
                MultibaggerStats.newqutrmidcapindex.removeAll()
                MultibaggerStats.newqutrniftyindex.removeAll()
                
                MultibaggerStats.SmallDisplay.removeAll()
                MultibaggerStats.MidDisplay.removeAll()
                MultibaggerStats.LargeDisplay.removeAll()
                
                MultibaggerStats.NseSmallDisplay.removeAll()
                MultibaggerStats.NseMidDisplay.removeAll()
                MultibaggerStats.NseLargeDisplay.removeAll()
                
                
                for arr in tempArray
                {
                    //segQtrDateArr
                    if(arr.value(forKey: "mdt_Rep_Type") as! String == "QUARTER")
                    {
                        MultibaggerStats.segResultQtrArr.append(arr.value(forKey: "mdt_Result_Qtr") as! String)
                        MultibaggerStats.segQtrDateArr.append(arr.value(forKey: "mdt_Date") as! String)
                        MultibaggerStats.segQtrDisplayArr.append(arr.value(forKey: "mdt_Display") as! String)
                        
                        if let mrdSmallCapPer = arr.value(forKey: "mrd_SmallCap_Per") as? String {
                            MultibaggerStats.qutrsmallcapindex.append(mrdSmallCapPer)
                        }
                        if let mrdMidCapPer = arr.value(forKey: "mrd_MidCap_Per") as? String {
                            MultibaggerStats.qutrmidcapindex.append(mrdMidCapPer)
                        }
                        if let mrdNiftyPer = arr.value(forKey: "mrd_Nifty_Per") as? String  {
                            MultibaggerStats.qutrniftyindex.append(mrdNiftyPer)
                        }
                        if let mdtPerfDt = arr.value(forKey: "mdt_Perf_Dt") as? String  {
                            MultibaggerStats.qutrperformancetxt.append(mdtPerfDt)
                        }
                        
                        if let SmallDisplay = arr.value(forKey: "SmallDisplay") as? String {
                            MultibaggerStats.SmallDisplay.append(SmallDisplay)
                        }
                        if let MidDisplay = arr.value(forKey: "MidDisplay") as? String {
                            MultibaggerStats.MidDisplay.append(MidDisplay)
                        }
                        if let LargeDisplay = arr.value(forKey: "LargeDisplay") as? String  {
                            MultibaggerStats.LargeDisplay.append(LargeDisplay)
                        }
                        
                        if let NseSmallDisplay = arr.value(forKey: "NseSmallDisplay") as? String {
                            MultibaggerStats.NseSmallDisplay.append(NseSmallDisplay)
                        }
                        if let NseMidDisplay = arr.value(forKey: "NseMidDisplay") as? String {
                            MultibaggerStats.NseMidDisplay.append(NseMidDisplay)
                        }
                        if let NseLargeDisplay = arr.value(forKey: "NseLargeDisplay") as? String  {
                            MultibaggerStats.NseLargeDisplay.append(NseLargeDisplay)
                        }
                        
                        
                        
                        if let mrdSmpPer = arr.value(forKey: "SmpPer") as? String {
                            MultibaggerStats.newqutrsmallcapindex.append(mrdSmpPer)
                        }
                        if let mrdMidPer = arr.value(forKey: "MidPer") as? String {
                            MultibaggerStats.newqutrmidcapindex.append(mrdMidPer)
                        }
                        if let mrdLarPer = arr.value(forKey: "LarPer") as? String  {
                            MultibaggerStats.newqutrniftyindex.append(mrdLarPer)
                        }
                        
                        
                    }
                    
                    if(arr.value(forKey: "mdt_Rep_Type") as! String == "YEAR")
                    {
                        MultibaggerStats.segYearDateArr.append(arr.value(forKey: "mdt_Date") as! String)
                        MultibaggerStats.segYearDisplayArr.append(arr.value(forKey: "mdt_Display") as! String)
                        
                        if let mrdSmallCapPer = arr.value(forKey: "mrd_SmallCap_Per") as? String {
                            MultibaggerStats.yearsmallcapindex.append(mrdSmallCapPer)
                        }
                        if let mrdNiftyPer = arr.value(forKey: "mrd_Nifty_Per") as? String {
                            MultibaggerStats.yearniftyindex.append(mrdNiftyPer)
                        }
                        MultibaggerStats.yearperformancetxt.append(arr.value(forKey: "mdt_Perf_Dt") as! String )
                    }
                }
                
                
                let segQtrDateArr = UserDefaults.standard
                segQtrDateArr.set(MultibaggerStats.segQtrDateArr, forKey: "segQtrDateArr")
                
                self.actInd.stopAnimating()
                return completion()
                /*
                 DispatchQueue.main.async {
                 
                 self.nrifpipmsload()
                 
                 }*/
            }
        }
    }
    
    func nrifpipmsload()
    {
        let obj = WebService()
        let data = "ALL"
        let param = "ptype=\(data)"
        
        self.actInd.startAnimating()
        
        obj.callWebServices(url: Urls.NRIFPIPMS, methodName: "GET", parameters: param, istoken: false, tokenval: "") { (returnValue, jsonData) in
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                
                // print(jsonData)
                
                let tempArray = jsonData.value(forKey: "response") as! [NSDictionary]
                
                
                for arr in tempArray
                {
                    
                    if(arr.value(forKey: "p_type") as! String == "PMS")
                    {
                        PMS_DATA.PMS1 = arr.value(forKey: "p_why") as! String
                        PMS_DATA.PMS2 = arr.value(forKey: "p_what") as! String
                        PMS_DATA.PMS3 = arr.value(forKey: "p_how") as! String
                    }
                    
                    if(arr.value(forKey: "p_type") as! String == "NRI")
                    {
                        NRI_DATA.NRI1 = arr.value(forKey: "p_what") as! String
                        NRI_DATA.NRI2 = arr.value(forKey: "p_why") as! String
                        NRI_DATA.NRI3 = arr.value(forKey: "p_how") as! String
                    }
                    
                    if(arr.value(forKey: "p_type") as! String == "FPI")
                    {
                        FPI_DATA.FPI1 = arr.value(forKey: "p_what") as! String
                        FPI_DATA.FPI2 = arr.value(forKey: "p_why") as! String
                        FPI_DATA.FPI3 = arr.value(forKey: "p_how") as! String
                    }
                    
                    if(arr.value(forKey: "p_type") as! String == "FPI NEW")
                    {
                        FPI_DATA.FPI_NEW = arr.value(forKey: "p_what") as! String
                    }
                    
                    if(arr.value(forKey: "p_type") as! String == "NRI NEW")
                    {
                        NRI_DATA.NRI_NEW = arr.value(forKey: "p_what") as! String
                    }
                }
                
                DispatchQueue.main.async {
                    self.actInd.stopAnimating()
                    
                    var pageIdentifier    =   ""
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    
                    if(NotificationData.category == ""){
                        self.getSeminarAd()
                    }
                    else {
                        if(NotificationData.category == "customWebView")
                        {
                            pageIdentifier  =   "CustomPushViewController"
                            
                            let viewcontroller = storyBoard.instantiateViewController(withIdentifier: pageIdentifier) as! CustomPushViewController
                            self.navigationController?.pushViewController(viewcontroller, animated: true)
                            
                        }
                        
                        if(NotificationData.category == "Stock_Market_Today")
                        {
                            pageIdentifier  =   "StockMarketTodayViewController"
                            let viewController = self.storyboard!.instantiateViewController(withIdentifier: pageIdentifier) as! StockMarketTodayViewController
                            
                            User.navigation.pushViewController(viewController, animated: false)
                            
                        }
                        
                        if(NotificationData.category == "Value_Stocks")
                        {
                            pageIdentifier  =   "MultibaggerViewController"
                            let viewController = self.storyboard!.instantiateViewController(withIdentifier: pageIdentifier) as! MultibaggerViewController
                            
                            User.navigation.pushViewController(viewController, animated: false)
                            
                        }
                        
                        if(NotificationData.category == "Nifty_Open_Interest")
                        {
                            pageIdentifier  =   "NiftyOpenInterestViewController"
                            let viewController = self.storyboard!.instantiateViewController(withIdentifier: pageIdentifier) as! NiftyOpenInterestViewController
                            
                            User.navigation.pushViewController(viewController, animated: false)
                            
                        }
                        if(NotificationData.category == "Dynamic_Indices")
                        {
                            pageIdentifier  =   "DynamicIndexController"
                            let viewController = self.storyboard!.instantiateViewController(withIdentifier: pageIdentifier) as! DynamicIndexController
                            
                            User.navigation.pushViewController(viewController, animated: false)
                            
                        }
                        
                        if(NotificationData.category == "PMS")
                        {
                            var viewController = UIViewController()
                            pageIdentifier  =   "PMSPDFViewerViewController"
                            if #available(iOS 11.0, *) {
                                viewController = self.storyboard!.instantiateViewController(withIdentifier: pageIdentifier) as! PMSPDFViewerViewController
                            } else {
                                // Fallback on earlier versions
                            }
                            
                            User.navigation.pushViewController(viewController, animated: false)
                            
                        }
                        
                        if(NotificationData.category == "Webinars")
                        {
                            var viewController = UIViewController()
                            pageIdentifier  =   "WebinarsViewController"
                            if #available(iOS 11.0, *) {
                                viewController = self.storyboard!.instantiateViewController(withIdentifier: pageIdentifier) as! WebinarsViewController
                            } else {
                                // Fallback on earlier versions
                            }
                            
                            User.navigation.pushViewController(viewController, animated: false)
                            
                        }
                        
                        if(NotificationData.category == "Sector_Performance")
                        {
                            var viewController = UIViewController()
                            pageIdentifier  =   "MultibaggerQuarterlySectorViewController"
                            if #available(iOS 11.0, *) {
                                viewController = self.storyboard!.instantiateViewController(withIdentifier: pageIdentifier) as! MultibaggerQuarterlySectorViewController
                            } else {
                                // Fallback on earlier versions
                            }
                            
                            User.navigation.pushViewController(viewController, animated: false)
                            
                        }
                    }
                    
                }
            }
        }
    }
    
    
    func firebaseTokenAddUpdate(){
        
        let webServices = WebService()
        var param = ""
        if let token = InstanceID.instanceID().token() {
            param  = "platform=iOS&userid=\(User.email)&token_no=\(token)"
            
            webServices.callWebServices(url: Urls.addUpdateFCMToken, methodName: "POST", parameters: param, istoken: true, tokenval: User.token) { (success, jsonDict) in
                
                print(jsonDict)
                if jsonDict.value(forKey: "errmsg") as! String == "" {
                    
                    let jsonValue = jsonDict
                }
            }
        }
    }
    
    
    
}
