//
//  StockSpecificNewViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-02 on 30/05/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import UIKit
import Charts
import GoogleMobileAds

class StockSpecificNewViewController: UIViewController,UITableViewDataSource,UITableViewDelegate , UIWebViewDelegate , ChartViewDelegate, GADBannerViewDelegate{
    
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var navigatinViewHeaderHeight: NSLayoutConstraint!
    // Bottom View Constant
    @IBOutlet weak var botomCosntantWithTableView: NSLayoutConstraint!
    @IBOutlet weak var bottomViewConstant: UIView!
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var chartViewTopConstant: NSLayoutConstraint!
    
    @IBOutlet weak var wvinvestingchart: UIWebView!
    
    
    @IBOutlet weak var vwResearchTitleHeight: NSLayoutConstraint!
    @IBOutlet weak var vwResearchTitle: UIView!
    
    
    @IBOutlet weak var tblcorrection: UITableView!
    @IBOutlet weak var tblsupportresistance: UITableView!
    @IBOutlet weak var tbl_Fundamentals: UITableView!
    @IBOutlet weak var tblYearlyData: UITableView!
    @IBOutlet weak var tblQtrlyData: UITableView!
    @IBOutlet weak var tblAnnualCAGR: UITableView!
    
    @IBOutlet weak var tblQtrCAGR: UITableView!
    
    @IBOutlet weak var tblStockPerformance: UITableView!
    
    @IBOutlet weak var tblSectorPerformance: UITableView!
    
    @IBOutlet weak var tblResearchReport: UITableView!
    var header = ""
    
    
    
    @IBOutlet weak var vwButtomContainer: UIView!
    @IBOutlet weak var vwChartViewHeightCobtraint: NSLayoutConstraint!
    @IBOutlet weak var btnhamburger: UIButton!
    @IBOutlet weak var lbl_categorydesc: UILabel!
    @IBOutlet weak var lbl_norecommdesc: UILabel!
    @IBOutlet weak var lbl_ourrecomm: UILabel!
    
    
    @IBOutlet weak var chartviewheader: UILabel!
    @IBOutlet weak var lbl_ltp: UILabel!
    @IBOutlet weak var lbl_date: UILabel!
    @IBOutlet weak var tblHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var lbl_diff: UILabel!
    @IBOutlet weak var img_up_down: UIImageView!
    
    @IBOutlet weak var lineChartView: LineChartView!
    @IBOutlet weak var tblFundamentalHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var scrollvw: UIScrollView!
    @IBOutlet weak var chartTopHeightRemovalConstant: NSLayoutConstraint!
    @IBOutlet weak var lblYearEndingHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblYearEnding: UILabel!
    @IBOutlet weak var lbl_stockname: UILabel!
    @IBOutlet weak var lbl_sector: UILabel!
    
    @IBOutlet weak var lbl_catg: UILabel!
    @IBOutlet weak var lblMarketCap: UILabel!
    var SymbolCategoryDesc = ""
    var IsMsg = ""
    var fundamentalHeightConstant: CGFloat = 0
    var researchReportHeightConstant: CGFloat = 0
    /***************/
    
    @IBOutlet weak var lblGrowthMsgHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var lblGrowthMsg: UILabel!
    @IBOutlet weak var bottomSpaceToAnualCAGRConstant: NSLayoutConstraint!
    @IBOutlet weak var topSpaceToCagrConstant: NSLayoutConstraint!
    @IBOutlet weak var lblcagrHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblCagrMsg: UILabel!
    
    var introductionDict = [NSDictionary]()
    var principal1Dict = [NSDictionary]()
    var principal2Dict = [NSDictionary]()
    var principal3Dict = [NSDictionary]()
    var principal4Dict = [NSDictionary]()
    var pricePerformanceDict = [NSDictionary]()
    
    
    var fundamentalDict = [NSDictionary]()
    var yearlylDict = [NSDictionary]()
    var qtrlylDict = [NSDictionary]()
    var annualCagrDict = [NSDictionary]()
    var qtrCagrDict = [NSDictionary]()
    var stockPricePerfDict = [NSDictionary]()
    var sectorPricePerfDict = [NSDictionary]()
    var researchReportDict = [NSDictionary]()
    
    @IBOutlet weak var tblYearlyDataHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tblqtrlyDataHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tblAnnualCAGRHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tblQtrCAGRHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tblStockPerformanceHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tblcorrectionHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tblSectorPerformanceHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tblSupportResistanceHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tblResearchReportHeightConstraint: NSLayoutConstraint!
    /****************/
    
    var fundamentalitems = [String]()
    var fundamentalvalue = [String]()
    var fundamentalColName = [String]()
    var items = [String]()
    var correctionlevel = [String]()
    var tblitems = [String]()
    var supreslevel = [String]()
    var months = [String]()
    var values = [String]()
    
    var levelval = [String]()
	
    @IBOutlet weak var lbl_category: UILabel!
	
	
	var dates = [String]()
    var symbolval = ""
    var isLoadedWebView = false
    var sector = ""
    var category = ""
    var seg_date = ""
    var highlightcolor = UIColor(red: 247/255, green: 196/255, blue: 58/255, alpha: 1)
    var categoryFlag = ""
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    
    var count = 0
    
    //MARK: VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.wvinvestingchart.isHidden = true
        bannerView.delegate = self
        self.tblSectorPerformanceHeightConstraint.constant = CGFloat(2000) //getTblSectorPerformanceHeight()
        //Test
        self.bottomView.isHidden = true
        wvinvestingchart.delegate = self
        chartviewheader.text = header
        
        tblcorrection.dataSource = self
        tblcorrection.delegate = self
        tblsupportresistance.estimatedRowHeight = 150
        tblsupportresistance.rowHeight = UITableViewAutomaticDimension
        
        symbolval = SearchForCurrentMultibagger.serchValue.replacingOccurrences(of: " ", with: "%20")
        
        addLoader()
        actInd.startAnimating()
        
        
        getData(symbolval: self.symbolval, completion: { (status) in
            if status == true {
                self.loadDataForPortfolioCheckerNew(symbolval: self.symbolval)
            } else {
                
            }
        })
        
        //loadDataForTickerList(symbolval: symbolval)
        // loadDataForPortfolioChecker(symbolval: symbolval)
        
        lineChartView.noDataText = ""
        
        //MARK : Plese open For Chart
        setChart(symbolval: symbolval)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.shouldRotate = true // or false to disable rotation
        
        webViewTest()
        
        
        // In this case, we instantiate the banner with desired ad size.
        //bannerView = GADBannerView(adSize: kGADAdSizeBanner)
        bannerView.adUnitID = "ca-mb-app-pub-8834194653550774/9862407200"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        
        
    }
    
    
    
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.shouldRotate = false
        
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }
    
    @IBAction func btn_Disclaimer(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let disclaimerController = storyBoard.instantiateViewController(withIdentifier: "DisclaimerViewController") as! DisclaimerViewController
        disclaimerController.disclaimerUrl = DisclaimerUrl.stock_research
        disclaimerController.disclaimerHeader = "Stock Research"
        User.navigation.pushViewController(disclaimerController, animated: true)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.shouldRotate = true
        
        if let viewControllersList = self.navigationController?.viewControllers {
            
            
            for tempView  in viewControllersList {
                if tempView.isKind(of: SectorPerformanceDetailsViewController.self) {
                    
                    count += 1
                    print(count)
                    if count > 1
                    {
                        tempView.removeFromParentViewController()
                    }
                }
            }
        }
        
    }
    
    /*
     func removeNavigationFromSuperView(viewCont: UIViewController) {
     
     if let viewControllersList = self.navigationController?.viewControllers {
     for tempView  in viewControllersList {
     //                if tempView.isKind(of: StockSpecificNewViewController.self) {
     //                    tempView.removeFromParentViewController()
     //                }
     
     if tempView.isKind(of: vi) {
     tempView.removeFromParentViewController()
     }
     }
     }
     } */
    
    //MARK: GATE DATE
    func getData (symbolval: String,completion : @escaping (Bool)->()) {
        let obj = WebService()
        let paremeters = "symbol=\(symbolval)"
        
        //actInd.startAnimating()
        
        obj.callWebServices(url: Urls.stock_details_report, methodName: "POST", parameters: paremeters, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
            
            print("Json Data is :  \(jsonData)")
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                let tempDict = jsonData.value(forKey: "response") as! NSDictionary
                
                if(tempDict.count > 0){
                    
                    self.introductionDict       = tempDict.value(forKey: "introduction")  as! [NSDictionary]
                    self.principal1Dict         = tempDict.value(forKey: "principal1")  as! [NSDictionary]
                    self.principal2Dict         = tempDict.value(forKey: "principal2")  as! [NSDictionary]
                    self.principal3Dict         = tempDict.value(forKey: "principal3")  as! [NSDictionary]
                    self.principal4Dict         = tempDict.value(forKey: "principal4")  as! [NSDictionary]
                    self.pricePerformanceDict   = tempDict.value(forKey: "priceperformance")  as! [NSDictionary]
                    
                    self.researchReportDict =  tempDict.value(forKey: "researchreport")  as! [NSDictionary]
                    
                    
                    
                     return completion(true)
                }
            }
        }
    }
    
    func webViewTest() {
        
        let symbolval = SearchForCurrentMultibagger.serchValue.replacingOccurrences(of: " ", with: "%20")
        
        let url = NSURL(string: "https://www.dynamiclevels.com/charting/mobile_black.html?symbol=\(symbolval)&ctype=Candles&internal=D&style=white")
        //print(url!)
        let requestObj = URLRequest(url: url! as URL)
        wvinvestingchart.loadRequest(requestObj)
        
    }
    //    func UITableView_Auto_Height()
    //    {
    //        if(self.tblsupportresistance.contentSize.height < self.tblsupportresistance.frame.height){
    //            var frame: CGRect = self.tblsupportresistance.frame;
    //            frame.size.height = self.tblsupportresistance.contentSize.height;
    //            self.tblsupportresistance.frame = frame;
    //        }
    //    }
    override public func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        self.navigatinViewHeaderHeight.constant = -20
        
        
        
        if UIDevice.current.orientation.isLandscape {
            self.wvinvestingchart.isHidden = false
            self.wvinvestingchart.alpha = 1
            
            DispatchQueue.main.async {
                
                guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
                statusBar.backgroundColor = UIColor.clear
                
                if !self.isLoadedWebView {
                    self.isLoadedWebView = true
                    self.navigatinViewHeaderHeight.constant = 0
                    self.webViewTest()
                }
                
                /*
                 
                 
                 let viewController = self.storyboard?.instantiateViewController(withIdentifier: "WebViewChartViewController") as! WebViewChartViewController
                 self.navigationController?.pushViewController(viewController, animated: false)*/
            }
            
        } else if UIDevice.current.orientation.isPortrait {
            
            self.wvinvestingchart.isHidden = true
            self.wvinvestingchart.alpha = 0.1
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func addLoader() {
        actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0) //CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.gray
        self.view.addSubview(actInd)
    }
    
    public func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool{
        print("webview asking for permission to start loading")
        
        return true
    }
    
    
    
    
    public func webViewDidStartLoad(_ webView: UIWebView)
    {
        print("webview did start loading")
    }
    
    public func webView(_ webView: UIWebView, didFailLoadWithError error: Error){
        print("webview did fail load with error: \(error)")
        
    }
    
    
    
    
    public func webViewDidFinishLoad(_ webView: UIWebView){
        // print("webview did finish load!")
        
        
    }
    
    
    //MARK: Getting data for Ticker List
    func loadDataForTickerList( symbolval : String)
    {
        
        var diff = ""
        var diffper = ""
        var date = ""
        
        
        let obj = WebService()
        let paremeters = "symbol=\(symbolval)"
        
        //actInd.startAnimating()
        
        obj.callWebServices(url: Urls.live_price, methodName: "GET", parameters: paremeters, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
            
            print("Json Data is  Live Price:  \(jsonData)")
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                let tempArray = jsonData.value(forKey: "response") as! NSArray
                
                if(tempArray.count > 0)
                {
                    
                    let indexval = tempArray[0] as! NSDictionary
                    diff = indexval.value(forKey: "DIFF") as! String
                    diffper = indexval.value(forKey: "DIFF_PER") as! String
                    date = indexval.value(forKey: "UpdtTime") as! String
                    
                    DispatchQueue.main.async(execute: {
                        
                        self.lbl_ltp.text = (indexval.value(forKey: "LastTradedPrice") as! String)
                        self.lbl_ltp.sizeToFit()
                        if ((diff as NSString).floatValue >= 0)
                        {
                            self.lbl_diff.text = "+\(diff)(+\(diffper)%)"
                            self.lbl_diff.textColor = UIColor(red: 29/255, green: 204/255, blue: 146/255, alpha: 1)
                            self.img_up_down.image = #imageLiteral(resourceName: "up")
                            
                            
                        }
                        else
                        {
                            self.lbl_diff.text = "\(diff)(\(diffper)%)"
                            self.lbl_diff.textColor = UIColor.red
                            self.img_up_down.image = #imageLiteral(resourceName: "Down")
                        }
                        self.lbl_diff.sizeToFit()
                        self.lbl_date.text = "As on \(date)"
                        self.lbl_date.sizeToFit()
                        
                        
                    })
                    
                }
                else
                {
                    let alertobj = AppManager()
                    
                    alertobj.showAlert(title: "Error!", message: "No Data Found!" , navigationController: self.navigationController!)
                    
                    DispatchQueue.main.async {
                        
                        self.actInd.stopAnimating()
                    }
                    
                }
                
            }
            else
            {
                let alertobj = AppManager()
                DispatchQueue.main.async {
                    
                    self.actInd.stopAnimating()
                    
                    
                    alertobj.showAlert(title: "Error!", message: jsonData.value(forKey: "errmsg") as! String , navigationController: self.navigationController!)
                    
                }
            }
            
            
        }
    }
    func loadDataForPortfolioCheckerNew(symbolval : String)
    {
        var diff = ""
        var diffper = ""
        var date = ""
        let obj = WebService()
        let paremeters = "sec_name=\(symbolval)"
        obj.callWebServices(url: Urls.portfolio_checker_new, methodName: "POST", parameters: paremeters, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
            
            print("Json Data New is :  \(jsonData)")
            if jsonData.value(forKey: "errmsg") as! String == "" {
                let tempArray = jsonData.value(forKey: "response") as! NSArray
                if(tempArray.count > 0)
                {
                    /*******************************************************/
                    //Mark: For Ticker Section and 1st Dataset Data
                    /*******************************************************/
                    let indexval = tempArray[0] as! NSArray
                    //let indexval2 = tempArray[1] as! NSArray
                    if indexval.count > 0 {
						
						if let inst_4  = (indexval[0] as AnyObject).value(forKey: "INSTRUMENT_2") as? String {
							self.chartviewheader.text = inst_4
							SearchForCurrentMultibagger.instrument_4 = inst_4
						}
                        
						
						
                    diff = (indexval[0] as AnyObject).value(forKey: "LastChangeVal") as! String
                    diffper = (indexval[0] as AnyObject).value(forKey: "LastChange") as! String
                    date = (indexval[0] as AnyObject).value(forKey: "LastCloseDt") as! String
                    if let categoryLong = nullToNil(value: indexval[0] as AnyObject)?.value(forKey: "CategoryLong")  as? String {
                        self.lbl_category.text = categoryLong
                        self.category = categoryLong
                        
                        
                        
                        if self.category == "No Recommendation" {
                            self.lbl_ourrecomm.isHidden = true
                            self.lbl_category.isHidden = true
                            self.lbl_norecommdesc.isHidden = false
                            
                        }else {
                            self.lbl_ourrecomm.isHidden = false
                            self.lbl_category.isHidden = false
                            self.lbl_norecommdesc.isHidden = true
                            
                        }
                        
                        
                        
                        
                        if categoryLong == "EXIT"{
                            self.lbl_category.textColor = UIColor.red
                        } else if (self.category == "BUY"){
                            self.lbl_category.textColor = UIColor(red: 29/255, green: 204/255, blue: 146/255, alpha: 1)
                        }
                    }
                    
                    if let categorydesc = nullToNil(value: indexval[0] as AnyObject)?.value(forKey: "CategoryDesc")  as? String {
                        
                        if self.category == "No Recommendation" {
                            self.lbl_categorydesc.text = ""
                        }else {
                            self.lbl_categorydesc.text = categorydesc
                        }
                        
                       
                        
                        
                    }
                    if let sector = nullToNil(value: indexval[0] as AnyObject)?.value(forKey: "SECTOR")  as? String {
                        let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]
                        let underlineAttributedString = NSAttributedString(string: sector, attributes: underlineAttribute)
                        self.sector = sector
                        self.lbl_sector.attributedText = underlineAttributedString
                        self.lbl_sector.sizeToFit()
                    }
                    if let marketCapType = nullToNil(value: indexval[0] as AnyObject)?.value(forKey: "MarketCapType")  as? String, let marketCapValue = nullToNil(value: indexval[0] as AnyObject)?.value(forKey: "MarketCap")  as? String {
                        //self.lbl_catg.text = marketCapType
                        
                        
                        let mrktCapVal = marketCapType + "\n(\(marketCapValue))"
                        
                        let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]
                        let underlineAttributedString = NSAttributedString(string: mrktCapVal, attributes: underlineAttribute)
                        self.lblMarketCap.attributedText = underlineAttributedString
                        self.lblMarketCap.textColor = UIColor.blue
                        
                        
                        
                    }
                    
                    if let symbolCategory = nullToNil(value: indexval[0] as AnyObject)?.value(forKey: "SymbolCategory")  as? String {
                        let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]
                        let underlineAttributedString = NSAttributedString(string: symbolCategory, attributes: underlineAttribute)
                        self.lbl_catg.attributedText = underlineAttributedString
                        self.lbl_catg.sizeToFit()
                    }
                    if let symbolCategoryDesc = nullToNil(value: indexval[0] as AnyObject)?.value(forKey: "SymbolCategoryDesc")  as? String {
                        self.SymbolCategoryDesc = symbolCategoryDesc
                    }
                    
                    if let INSTRUMENT_2 = nullToNil(value: indexval[0] as AnyObject)?.value(forKey: "INSTRUMENT_2")  as? String {
                        self.lbl_stockname.text = INSTRUMENT_2
                    }
                    if let isMsg = nullToNil(value: indexval[0] as AnyObject)?.value(forKey: "IsMsg")  as? String {
                        self.IsMsg = isMsg
                    }
                    DispatchQueue.main.async(execute: {
                        
                        self.lbl_ltp.text = ((indexval[0] as AnyObject).value(forKey: "LastClose") as! String)
                        self.lbl_ltp.sizeToFit()
                        if ((diff as NSString).floatValue >= 0)
                        {
                            self.lbl_diff.text = "+\(diff)(+\(diffper)%)"
                            self.lbl_diff.textColor = UIColor(red: 29/255, green: 204/255, blue: 146/255, alpha: 1)
                            self.img_up_down.image = #imageLiteral(resourceName: "up") 
                            
                            
                        }
                        else
                        {
                            self.lbl_diff.text = "\(diff)(\(diffper)%)"
                            self.lbl_diff.textColor = UIColor.red
                            self.img_up_down.image = #imageLiteral(resourceName: "Down")
                        }
                        self.lbl_diff.sizeToFit()
                        self.lbl_date.text = "As on \(date)"
                        self.lbl_date.sizeToFit()
                        
                        
                    })
                    }
                    /*******************************************************/
                    //Mark:  Close Ticker Section and 1st Dataset Data
                    /*******************************************************/
                    
                    /*******************************************************/
                    //Mark:  Introduction Section
                    /*******************************************************/
                    
                    self.tblFundamentalHeightConstant.constant = 0 //CGFloat(self.introductionDict.count * 42)
                    self.tbl_Fundamentals.reloadData()
                    
                    /*******************************************************/
                    //Mark:  Close Introduction Section
                    /*******************************************************/
                    
                    
                    /*******************************************************/
                    //Mark:  Principal 1 Section
                    /*******************************************************/
                    self.tblAnnualCAGRHeightConstraint.constant = CGFloat(self.principal1Dict.count * 42)
                    self.tblAnnualCAGR.reloadData()
                    
                    /*******************************************************/
                    //Mark:  Close Principal 1 Section
                    /*******************************************************/
                    
                    /*******************************************************/
                    //Mark:  Principal 2 Section
                    /*******************************************************/
                    self.tblStockPerformanceHeightConstraint.constant = CGFloat(self.principal2Dict.count * 42)
                    self.tblStockPerformance.reloadData()
                    
                    /*******************************************************/
                    //Mark:  Close Principal 2 Section
                    /*******************************************************/
                    
                    /*******************************************************/
                    //Mark:  Principal 3 Section
                    /*******************************************************/
                    self.tblSectorPerformance.reloadData()
                    self.tblSectorPerformanceHeightConstraint.constant = self.principal3Dict.isEmpty ? 80 : self.getTblSectorPerformanceHeight()
                    
                    
                    /*******************************************************/
                    //Mark:  Close Principal 3 Section
                    /*******************************************************/
                    
                    /*******************************************************/
                    //Mark:  Principal 4 Section
                    /*******************************************************/
                    self.tblcorrectionHeightConstraint.constant = CGFloat(self.principal4Dict.count * 42)
                    self.tblcorrection.reloadData()
                    
                    /*******************************************************/
                    //Mark:  Close Principal 4 Section
                    /*******************************************************/
                    
                    /*******************************************************/
                    //Mark:  Price Performance Section
                    /*******************************************************/
                    self.tblsupportresistance.reloadData()
                    self.tblSupportResistanceHeightConstraint.constant = CGFloat(self.pricePerformanceDict.count * 42)
                    
                    
                    /*******************************************************/
                    //Mark:  Close Price Performance Section
                    /*******************************************************/
                    
                    /*******************************************************/
                    //Mark:  Research Report Section
                    /*******************************************************/
                    self.tblResearchReport.reloadData()
                    self.tblResearchReportHeightConstraint.constant = self.researchReportDict.isEmpty ? 0 : CGFloat(self.researchReportDict.count * 42)
                    self.vwResearchTitleHeight.constant = self.researchReportDict.isEmpty ? 0 : 40
                    self.vwResearchTitle.isHidden = self.researchReportDict.isEmpty ? true : false
                    
                    /*******************************************************/
                    //Mark:  Close Research Report Section
                    /*******************************************************/
                    
                    
                }
            }
        }
    }
    
    //MARK: GET TABLE VIEW HEIGHT
    private func getTblSectorPerformanceHeight() -> CGFloat {
        
        var height: CGFloat = 0
        for cell in tblSectorPerformance.visibleCells {
            height += cell.bounds.height
        }
        print("Table Sector Performance Height : \(height)")
        return height
    }
    
    //MARK: Getting data for Corection List
    func loadDataForPortfolioChecker(symbolval : String)
    {
        //let token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL3dzLmRsZXZlbHMuY29tL2xvZ2luIiwiaWF0IjoxNDkxODkzNzg2LCJuYmYiOjE0OTE4OTM3ODYsImp0aSI6ImhCa0ZWdXFrdTFhN25DRFgiLCJzdWIiOjIwOTk4OCwieHBhc3MiOiIkUCRCTHVBdDVrYTBUOGc0N2hiYllHV2huN2VUOVhiMDAwIn0.HdXThhfTuf3uFpCOhTXmjZP_BwcA3q_3jFKTA97y1qc"
        
        var category_desc = ""
        var marketCapType = ""
        var stockname = ""
        var lastyearperf = ""
        var lasqtrperf = ""
        var yearEnding = ""
        var yearlyData = ""
        // var  level_val = ""
        var indexval: NSDictionary = NSDictionary()
        let obj = WebService()
        let paremeters = "symbol=\(symbolval)"
        //print(paremeters)
        //actInd.startAnimating()
        
        obj.callWebServices(url: Urls.portfolio_checker, methodName: "GET", parameters: paremeters, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
            
            print("Json Data is :  \(jsonData)")
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                let tempArray = jsonData.value(forKey: "response") as! NSArray
                if(tempArray.count > 0)
                {
                    
                    
                    DispatchQueue.main.async(execute: {
                        
                        for dict in tempArray {
                            
                            
                            indexval = dict as! NSDictionary
                            category_desc = indexval.value(forKey: "CategoryDesc") as! String
                            self.category = indexval.value(forKey: "CategoryLong") as! String
                            self.sector = indexval.value(forKey: "SECTOR") as! String
                            marketCapType = indexval.value(forKey: "MarketCapType") as! String
                            stockname = indexval.value(forKey: "INSTRUMENT_2") as! String
                            self.seg_date = indexval.value(forKey: "ReportDate") as! String
                            yearlyData = indexval.value(forKey: "YearlyData") as! String
                            
                            if (indexval.value(forKey: "YearEnding") is NSNull){
                                yearEnding = ""
                            }
                            else{
                                yearEnding = indexval.value(forKey: "YearEnding") as! String
                            }
                            
                            
                            self.categoryFlag = indexval.value(forKey: "CategoryFlag") as! String
                            
                            
                            
                            
                            
                            //level_val = indexval.value(forKey: "Level_Val") as! String
                            
                            //                            self.fundamentalitems.append("Average Value")
                            //                            self.fundamentalvalue.append("\(indexval.value(forKey: "Avg_Value") as! String)")
                            //                            self.fundamentalColName.append("Avg_Value")
                            
                            
                            if yearlyData == "N" {
                                self.fundamentalitems.append("PAT")
                            }else{
                                self.fundamentalitems.append("PAT ( \(yearlyData) )")
                            }
                            self.fundamentalvalue.append("\(indexval.value(forKey: "ReportedPAT") as! String) Cr.")
                            self.fundamentalColName.append("ReportedPAT")
                            
                            if yearlyData == "N" {
                                self.fundamentalitems.append("PE Ratio")
                            }
                            else{
                                self.fundamentalitems.append("PE Ratio ( \(yearlyData) )")
                            }
                            self.fundamentalvalue.append("\(indexval.value(forKey: "PERatio") as! String)")
                            self.fundamentalColName.append("PERatio")
                            
                            self.fundamentalitems.append("Market Capitalization")
                            self.fundamentalvalue.append("\(indexval.value(forKey: "MarketCap") as! String) Cr.")
                            self.fundamentalColName.append("MarketCap")
                            
                            self.fundamentalitems.append("Debt to Equity Ratio")
                            self.fundamentalvalue.append("\(indexval.value(forKey: "DERatio") as! String)")
                            self.fundamentalColName.append("DERatio")
                            
                            self.fundamentalitems.append("Dividend Yield")
                            self.fundamentalvalue.append("\(indexval.value(forKey: "DividendYield") as! String)%")
                            self.fundamentalColName.append("DividendYield")
                            
                            self.fundamentalitems.append("Book Value")
                            self.fundamentalvalue.append("\(indexval.value(forKey: "BookValue") as! String)")
                            self.fundamentalColName.append("BookValue")
                            
                            self.fundamentalitems.append("Institutions Holding")
                            self.fundamentalvalue.append("\(indexval.value(forKey: "Institutions") as! String)")
                            self.fundamentalColName.append("Institutions")
                            
                            if  self.sector != "BANKS-PRIVATE AND PSU"{
                                if yearlyData == "N" {
                                    self.fundamentalitems.append("EBITDA")
                                }else{
                                    self.fundamentalitems.append("EBITDA ( \(yearlyData) )")
                                }
                                self.fundamentalvalue.append("\(indexval.value(forKey: "Operating_Profit") as! String) Cr.")
                                self.fundamentalColName.append("Operating_Profit")
                            }
                            self.fundamentalitems.append("Promoter's Pledge")
                            self.fundamentalvalue.append("\(indexval.value(forKey: "PledgedShares") as! String)%")
                            self.fundamentalColName.append("PledgedShares")
                            
                            
                            
                            lastyearperf = "(\(indexval.value(forKey: "LastYear") as! String))"
                            
                            self.fundamentalitems.append("Last Year Performance " + lastyearperf)
                            self.fundamentalvalue.append("\(indexval.value(forKey: "LastYearPerf") as! String)%")
                            self.fundamentalColName.append("LastYearPerf")
                            
                            lasqtrperf = "(\(indexval.value(forKey: "LastQtr") as! String))"
                            self.fundamentalitems.append("Last Quater Performance  " + lasqtrperf)
                            self.fundamentalvalue.append("\(indexval.value(forKey: "LastQtrPerf") as! String)%")
                            self.fundamentalColName.append("LastQtrPerf")
                            
                            self.fundamentalitems.append("Last Year Sector Performance  " + lastyearperf)
                            self.fundamentalvalue.append("\(indexval.value(forKey: "LastYearSectorPerf") as! String)%")
                            self.fundamentalColName.append("LastYearSectorPerf")
                            
                            //print (self.fundamentalitems)
                            //print(self.fundamentalvalue)
                            
                        }
                        //          self.vwButtomContainer.isHidden = false
                        
                        
                        
                        
                        self.lbl_categorydesc.text = category_desc
                        self.lbl_categorydesc.sizeToFit()
                        
                        //let a = self.category.replacingOccurrences(of: "(", with: "\n(")
                        //print("Replace: \(a)")
                        
                        self.lbl_category.text = self.category.replacingOccurrences(of: "(", with: "\n(")
                        let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]
                        let underlineAttributedString = NSAttributedString(string: self.sector, attributes: underlineAttribute)
                        self.lbl_sector.attributedText = underlineAttributedString
                        self.lbl_sector.sizeToFit()
                        
                        //self.lbl_catg.text = marketCapType
                        self.lblMarketCap.text = marketCapType
                        self.lbl_stockname.text = stockname
                        
                        
                        
                        
                        if yearlyData == "N" {
                            self.lblYearEndingHeightConstraint.constant = 40
                            self.lblYearEnding.isHidden = false
                            self.lblYearEnding.text = "Quarterly results for : \(yearEnding)"
                        }
                        else{
                            self.lblYearEnding.isHidden = true
                            self.lblYearEndingHeightConstraint.constant = 0
                        }
                        
                        //CURRENTLY NOT SHOWING DATA
                        self.tblFundamentalHeightConstant.constant = 0 //CGFloat(self.fundamentalitems.count * 42)
                        
                        if(self.category == "EXIT"){
                            self.lbl_category.textColor = UIColor.red
                            
                        }   else if (self.category == "BUY"){
                            self.lbl_category.textColor = UIColor(red: 29/255, green: 204/255, blue: 146/255, alpha: 1)
                        }
                        self.tbl_Fundamentals.reloadData()
                        
                    })
                    
                    
                    self.loadDataForCorrectionList(symbolval: symbolval)
                    
                }
                else
                {
                    DispatchQueue.main.async {
                        self.actInd.stopAnimating()
                        
                        self.tbl_Fundamentals.reloadSections(IndexSet(integer: 0), with: .automatic)
                        
                        let alertobj = AppManager()
                        
                        alertobj.showAlert(title: "Error!", message: "No Data Found!" , navigationController: self.navigationController!)
                        
                    }
                    
                }
                
            }
            else
            {
                DispatchQueue.main.async {
                    let alertobj = AppManager()
                    self.actInd.stopAnimating()
                    alertobj.showAlert(title: "Error!", message: jsonData.value(forKey: "errmsg") as! String , navigationController: self.navigationController!)
                }
            }
            
            
        }
    }
    
    
    
    
    //MARK: Getting data for Corection List
    func loadDataForCorrectionList(symbolval : String)
    {
        //let token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL3dzLmRsZXZlbHMuY29tL2xvZ2luIiwiaWF0IjoxNDkxODkzNzg2LCJuYmYiOjE0OTE4OTM3ODYsImp0aSI6ImhCa0ZWdXFrdTFhN25DRFgiLCJzdWIiOjIwOTk4OCwieHBhc3MiOiIkUCRCTHVBdDVrYTBUOGc0N2hiYllHV2huN2VUOVhiMDAwIn0.HdXThhfTuf3uFpCOhTXmjZP_BwcA3q_3jFKTA97y1qc"
        
        var level_name = ""
        
        var  level_val = ""
        var indexval: NSDictionary = NSDictionary()
        let obj = WebService()
        let paremeters = "symbol=\(symbolval)"
        //print(paremeters)
        //actInd.startAnimating()
        
        obj.callWebServices(url: Urls.correction_report, methodName: "GET", parameters: paremeters, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
            
            //print("Correction List data is :  \(jsonData)")
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                let tempArray = jsonData.value(forKey: "response") as! NSArray
                if(tempArray.count > 0)
                {
                    self.tblitems.removeAll()
                    self.supreslevel.removeAll()
                    
                    DispatchQueue.main.async(execute: {
                        
                        for dict in tempArray {
                            
                            indexval = dict as! NSDictionary
                            level_name = indexval.value(forKey: "cd_Level_Name") as! String
                            level_val = indexval.value(forKey: "Level_Val") as! String
                            //print (level_val)
                            if (level_name  == "Recent High")
                            {
                                self.items.append("\(level_val)")
                                self.correctionlevel.append("\(level_name)")
                            }
                            else if (level_name  == "CORR_10")
                            {
                                self.items.append("\(level_val)")
                                self.correctionlevel.append("10% Correction")
                                
                            }
                            else if (level_name  == "CORR_20")
                            {
                                self.items.append("\(level_val)")
                                self.correctionlevel.append("20% Correction")
                                
                                
                            }
                            else if (level_name  == "CORR_30")
                            {
                                self.items.append("\(level_val)")
                                self.correctionlevel.append("30% Correction")
                                
                            }
                            
                            /*else if (level_name  == "Recent High")
                             {
                             self.tblitems.append("\(level_val)")
                             self.supreslevel.append("\(level_name)")
                             }*/
                            if((level_name != "CORR_10") && (level_name != "CORR_20") && (level_name != "CORR_30") && (level_name != "Recent High")){
                                self.tblitems.append("\(level_val)")
                                self.supreslevel.append("\(level_name)")
                                
                            }
                        }
                        
                        //print (self.tblitems)
                        //print ("Correction Table data: \(self.supreslevel)")
                        
                    //    self.tblHeightConstant.constant = CGFloat(self.supreslevel.count * 40)
                        
                        
                        self.tblcorrection.reloadData()
                        self.tblsupportresistance.reloadData()
                        
                        
                        if(self.category == "EXIT"){
                            
                            // DispatchQueue.main.async {
                            
                            self.bottomView.isHidden = true
                            self.botomCosntantWithTableView.constant =  0
                            
                            
                            //print(self.chartViewTopConstant.constant)
                            //print(self.botomCosntantWithTableView.constant)
                            //print("dkljhsfoaigjsd;lbvlsdkzjvbhlkzdx")
                            // }
                            
                            
                            
                        }
                        else if (self.category == "BUY"){
                            
                            self.bottomView.isHidden = false
                            self.botomCosntantWithTableView.constant =  CGFloat(self.supreslevel.count * 40) + CGFloat(self.correctionlevel.count * 40) + 80
                            
                            //print("Bottom View Height : \(self.botomCosntantWithTableView.constant)")
                            
                        }
                        else{
                            
                            self.bottomView.isHidden = false
                            //print("Bottom View Height : \(self.bottomView.frame.height)")
                            self.botomCosntantWithTableView.constant =  CGFloat(self.supreslevel.count * 40) + CGFloat(self.correctionlevel.count * 40) + 80
                        }
                    })
                    
                }
                else
                {
                    DispatchQueue.main.async {
                        self.actInd.stopAnimating()
                        
                        self.tblcorrection.reloadSections(IndexSet(integer: 0), with: .automatic)
                        
                        let alertobj = AppManager()
                        
                        alertobj.showAlert(title: "Error!", message: "No Data Found!" , navigationController: self.navigationController!)
                    }
                    
                }
                
            }
            else
            {
                DispatchQueue.main.async {
                    let alertobj = AppManager()
                    self.actInd.stopAnimating()
                    alertobj.showAlert(title: "Error!", message: jsonData.value(forKey: "errmsg") as! String , navigationController: self.navigationController!)
                }
            }
            
            
        }
    }
    
    
    
    public func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == tblResearchReport {
            return researchReportDict.isEmpty ? 0 : UITableViewAutomaticDimension
        }
        
        
        return tableView == tblSectorPerformance ? principal3Dict.isEmpty  ? 80 : UITableViewAutomaticDimension :  40 //UITableViewAutomaticDimension
    
    }
    
    //MARK: Table View Delegate
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        
        
        if tableView == tblAnnualCAGR  {
            
            if  let stockParam = self.principal1Dict[indexPath.row].value(forKey: "StockParam") as? String {
                if let display = self.principal1Dict[indexPath.row].value(forKey: "Display") as? String {
                
                if stockParam != "" {
                    let viewController = storyBoard.instantiateViewController(withIdentifier: "PerformanceGraphViewController") as! PerformanceGraphViewController
                    
                    viewController.stock_param  = stockParam
                    viewController.title_text   = "\(display) - \(SearchForCurrentMultibagger.instrument_4)"
                    viewController.symbol_name = SearchForCurrentMultibagger.serchValue.replacingOccurrences(of: " ", with: "%20")
                    User.navigation.pushViewController(viewController, animated: true)
                    
                    }
                }
                
            }
        }
        
        if tableView == tblStockPerformance  {
            
            if  let stockParam = self.principal2Dict[indexPath.row].value(forKey: "StockParam") as? String {
                if let display = self.principal2Dict[indexPath.row].value(forKey: "Display") as? String {
                    
                    if stockParam != "" {
                        let viewController = storyBoard.instantiateViewController(withIdentifier: "PerformanceGraphViewController") as! PerformanceGraphViewController
                        
                        viewController.stock_param  = stockParam
                        viewController.title_text   = "\(display) - \(SearchForCurrentMultibagger.instrument_4)"
                        viewController.symbol_name = SearchForCurrentMultibagger.serchValue.replacingOccurrences(of: " ", with: "%20")
                        User.navigation.pushViewController(viewController, animated: true)
                        
                    }
                }
                
            }
        }
        
        if tableView == tblcorrection  {
            
            if  let stockParam = self.principal4Dict[indexPath.row].value(forKey: "StockParam") as? String {
                if let Header = self.principal4Dict[indexPath.row].value(forKey: "Header") as? String {
                    
                    if stockParam != "" {
                        let viewController = storyBoard.instantiateViewController(withIdentifier: "PerformanceGraphViewController") as! PerformanceGraphViewController
                        
                        viewController.stock_param  = stockParam
                        viewController.title_text   = "\(Header) - \(SearchForCurrentMultibagger.instrument_4)"
                        viewController.symbol_name = SearchForCurrentMultibagger.serchValue.replacingOccurrences(of: " ", with: "%20")
                        User.navigation.pushViewController(viewController, animated: true)
                        
                    }
                }
                
            }
        }
        
        
        if tableView == tblsupportresistance  {
            
            if  let stockParam = self.pricePerformanceDict[indexPath.row].value(forKey: "StockParam") as? String {
                if let display = self.pricePerformanceDict[indexPath.row].value(forKey: "Header") as? String {
                    
                    if stockParam != "" {
                        let viewController = storyBoard.instantiateViewController(withIdentifier: "PerformanceGraphViewController") as! PerformanceGraphViewController
                        
                        viewController.stock_param  = stockParam
                        viewController.title_text   = "\(display) - \(SearchForCurrentMultibagger.instrument_4)"
                        viewController.symbol_name = SearchForCurrentMultibagger.serchValue.replacingOccurrences(of: " ", with: "%20")
                        User.navigation.pushViewController(viewController, animated: true)
                        
                    }
                }
                
            }
        }
        
        
        
        if tableView == self.tblResearchReport {
            if let url = nullToNil(value: self.researchReportDict[indexPath.row].value(forKey: "Filename") as AnyObject) as? String  {
                if let title = nullToNil(value: self.researchReportDict[indexPath.row].value(forKey: "Description") as AnyObject) as? String  {
                    
                    let viewController = self.storyboard!.instantiateViewController(withIdentifier: "PDFViewerViewController") as! PDFViewerViewController
                    viewController.strURL = url
                    viewController.strTitle = title
                    self.navigationController!.pushViewController(viewController, animated: true)
                    
                }
                
            }
        }
    }
    
    
    //MARK: Table View Delegate
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
    }
    
    
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:UITableViewCell?
        
        if tableView == self.tblcorrection {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "correctioncell", for: indexPath)
            cell.selectionStyle = .none
            
            cell.backgroundColor = UIColor(hex: AppManager.getAlternetColor())
                //= indexPath.row % 2 == 0 ? UIColor(red: 232/255, green: 232/255, blue: 232/255, alpha: 0.4) : UIColor.white
            
            let lblHeader = cell.viewWithTag(1001) as! UILabel
            let lblValue = cell.viewWithTag(1002) as! UILabel
            
            
            if let header = nullToNil(value: self.principal4Dict[indexPath.row].value(forKey: "Header") as AnyObject) as? String {
                
                if  let stockParam = self.principal4Dict[indexPath.row].value(forKey: "StockParam") as? String {
                    if stockParam != "" {
                        
                        let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]
                        let underlineAttributedString = NSAttributedString(string: header, attributes: underlineAttribute)
                        lblHeader.attributedText = underlineAttributedString
                        lblHeader.textColor = UIColor.blue
                    }else {
                        lblHeader.text = header
                    }
                }else {
                    lblHeader.text = header
                }
            }
            
            if let value = nullToNil(value: self.principal4Dict[indexPath.row].value(forKey: "_Value") as AnyObject) as? String {
                lblValue.text = value
            }
            
            
            
            
            return cell
            
            
        }
            
        else if(tableView == self.tblsupportresistance)
        {
            cell = tableView.dequeueReusableCell(withIdentifier:"supportcell", for: indexPath)
            cell?.selectionStyle = .none
            
            // DispatchQueue.main.async {
            
            
            
            cell?.backgroundColor = UIColor(hex: AppManager.getAlternetColor())
                //= indexPath.row % 2 == 0 ? UIColor(red: 232/255, green: 232/255, blue: 232/255, alpha: 0.4) : UIColor.white
            
            let lblHeader = cell?.viewWithTag(1003) as! UILabel
            let lblValue = cell?.viewWithTag(1004) as! UILabel
            
            if var header = nullToNil(value: self.pricePerformanceDict[indexPath.row].value(forKey: "Header") as AnyObject) as? String {
                
                
                if let period = nullToNil(value: self.pricePerformanceDict[indexPath.row].value(forKey: "Period") as AnyObject) as? String {
                    
                    
                    if period != "" {
                        header = header + "\n" + period
                    }
                    
                
                let stockParam = self.pricePerformanceDict[indexPath.row].value(forKey: "StockParam") as? String
                
                if stockParam != "" {
                    
                    let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]
                    let underlineAttributedString = NSAttributedString(string: header, attributes: underlineAttribute)
                    lblHeader.attributedText = underlineAttributedString
                    lblHeader.textColor = UIColor.blue
                }else {
                    lblHeader.text = header
                }
                }
            }
            
            if let value = nullToNil(value: self.pricePerformanceDict[indexPath.row].value(forKey: "_Value") as AnyObject) as? String {
                lblValue.text = value
            }
            
            return cell!
            //result = cell
        }
        else if (tableView == tbl_Fundamentals)
        {
            cell = tableView.dequeueReusableCell(withIdentifier:"fundamentalcell", for: indexPath)
            cell?.selectionStyle = .none
            
            // DispatchQueue.main.async {
            
            cell?.backgroundColor = UIColor(hex: AppManager.getAlternetColor())
                //= indexPath.row % 2 == 0 ? UIColor(red: 232/255, green: 232/255, blue: 232/255, alpha: 0.4) : UIColor.white
            
            let lblHeader = cell?.viewWithTag(1005) as! UILabel
            let lblValue = cell?.viewWithTag(1006) as! UILabel
            
            
            if let header = nullToNil(value: self.introductionDict[indexPath.row].value(forKey: "Header") as AnyObject) as? String {
                lblHeader.text = header
            }
            
            if let value = nullToNil(value: self.introductionDict[indexPath.row].value(forKey: "_Value") as AnyObject) as? String {
                lblValue.text = value
            }
           
            return cell!
        }
        else if tableView == tblYearlyData {
            cell = tableView.dequeueReusableCell(withIdentifier:"yearlydatacell", for: indexPath)
            cell?.selectionStyle = .none
            cell?.backgroundColor = UIColor(hex: AppManager.getAlternetColor())
                //= indexPath.row % 2 == 0 ? UIColor(red: 232/255, green: 232/255, blue: 232/255, alpha: 0.4) : UIColor.white
            let lblDisplay = cell?.viewWithTag(2005) as! UILabel
            let lblColName = cell?.viewWithTag(2006) as! UILabel
            let lblColValue = cell?.viewWithTag(2007) as! UILabel
            let lblGrowth = cell?.viewWithTag(2008) as! UILabel
            if let display = nullToNil(value: self.yearlylDict[indexPath.row].value(forKey: "Display") as AnyObject) as? String {
                if display != "Header" {
                    lblDisplay.text = display
                }else{
                    lblDisplay.text = ""
                }
            }
            if let colName = nullToNil(value: self.yearlylDict[indexPath.row].value(forKey: "ColName") as AnyObject) as? String {
                lblColName.text = colName
            }
            if let colValue = nullToNil(value: self.yearlylDict[indexPath.row].value(forKey: "ColValue") as AnyObject) as? String {
                lblColValue.text = colValue
            }
            if let growth = nullToNil(value: self.yearlylDict[indexPath.row].value(forKey: "Growth") as AnyObject) as? String {
                lblGrowth.text = growth
            }
            return cell!
        }
        else if tableView == tblQtrlyData{
            cell = tableView.dequeueReusableCell(withIdentifier:"qtrlydatacell", for: indexPath)
            cell?.selectionStyle = .none
            cell?.backgroundColor = UIColor(hex: AppManager.getAlternetColor())
                //= indexPath.row % 2 == 0 ? UIColor(red: 232/255, green: 232/255, blue: 232/255, alpha: 0.4) : UIColor.white
            let lblDisplay = cell?.viewWithTag(3005) as! UILabel
            let lblColName = cell?.viewWithTag(3006) as! UILabel
            let lblColValue = cell?.viewWithTag(3007) as! UILabel
            let lblGrowth = cell?.viewWithTag(3008) as! UILabel
            if let display = nullToNil(value: self.qtrlylDict[indexPath.row].value(forKey: "Display") as AnyObject) as? String {
                if display != "Header" {
                    lblDisplay.text = display
                }else{
                    lblDisplay.text = ""
                }
            }
            if let colName = nullToNil(value: self.qtrlylDict[indexPath.row].value(forKey: "ColName") as AnyObject) as? String {
                lblColName.text = colName
            }
            if let colValue = nullToNil(value: self.qtrlylDict[indexPath.row].value(forKey: "ColValue") as AnyObject) as? String {
                lblColValue.text = colValue
            }
            if let growth = nullToNil(value: self.qtrlylDict[indexPath.row].value(forKey: "Growth") as AnyObject) as? String {
                lblGrowth.text = growth
            }
            return cell!
        }
        else if tableView == tblAnnualCAGR  {
        
            if  let colName = self.principal1Dict[indexPath.row].value(forKey: "ColName") as? String,
                let colValue = self.principal1Dict[indexPath.row].value(forKey: "ColValue") as? String,
                let display = self.principal1Dict[indexPath.row].value(forKey: "Display") as? String,
                let growth = self.principal1Dict[indexPath.row].value(forKey: "Growth") as? String,
                let stockParam = self.principal1Dict[indexPath.row].value(forKey: "StockParam") as? String {
                
                //IF HEADER CELL
                if display == "Header" {
                    
                    let headerCell = tableView.dequeueReusableCell(withIdentifier: "headerCell", for: indexPath)
                    
                    let lblColValue = headerCell.viewWithTag(101) as! UILabel
                    let lblDisplay = headerCell.viewWithTag(102) as! UILabel
                    let lblGrowth = headerCell.viewWithTag(103) as! UILabel
                    
                    lblColValue.text = colName
                    lblDisplay.text = colValue
                    lblGrowth.text = growth
                    
                    return headerCell
                }
                
                //IF SECTION CELL
                else if display == "Section" {
                    
                    let sectionCell = tableView.dequeueReusableCell(withIdentifier: "sectionCell", for: indexPath)
                    
                    let lblColName = sectionCell.viewWithTag(100) as! UILabel
                    lblColName.text = colName
                    
                    return sectionCell
                    
                }
                
                //IF FOUR COLOMN CELL
                else if colName != "", colValue != ""  {
                    
                    let fourColumnCell = tableView.dequeueReusableCell(withIdentifier: "fourColumnCell", for: indexPath)
                    fourColumnCell.contentView.backgroundColor = UIColor(hex: AppManager.getAlternetColor())
                    
                    let lblColName = fourColumnCell.viewWithTag(100) as! UILabel
                    let lblColValue = fourColumnCell.viewWithTag(101) as! UILabel
                    let lblDisplay = fourColumnCell.viewWithTag(102) as! UILabel
                    let lblGrowth = fourColumnCell.viewWithTag(103) as! UILabel
                    
                    
                    if stockParam != "" {
                        
                        let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]
                        let underlineAttributedString = NSAttributedString(string: display, attributes: underlineAttribute)
                        lblColName.attributedText = underlineAttributedString
                        lblColName.textColor = UIColor.blue
                    }else {
                        lblColName.text = display
                    }
                    
                   
                    
                    //lblColName.text = display
                    lblColValue.text = colName
                    lblDisplay.text = colValue
                    lblGrowth.text = growth
                    
                    return fourColumnCell
                    
                }
                    
                    //IF FOUR COLOMN CELL
                else if colName == "", colValue != "" {
                    
                    let fourColumnCell = tableView.dequeueReusableCell(withIdentifier: "fourColumnCell", for: indexPath)
                    fourColumnCell.contentView.backgroundColor = UIColor(hex: AppManager.getAlternetColor())
                    
                    let lblColName = fourColumnCell.viewWithTag(100) as! UILabel
                    let lblColValue = fourColumnCell.viewWithTag(101) as! UILabel
                    let lblDisplay = fourColumnCell.viewWithTag(102) as! UILabel
                    let lblGrowth = fourColumnCell.viewWithTag(103) as! UILabel
                    
                    
                    lblColValue.text = colName
                    lblDisplay.text = colValue
                    lblGrowth.text = growth
                    
                    return fourColumnCell
                    
                }
                
                //IF TWO VALUE CELL
                else if colName == "", colValue == "" {
                    
                    let twoColumnCell = tableView.dequeueReusableCell(withIdentifier: "twoColumnCell", for: indexPath)
                    twoColumnCell.contentView.backgroundColor = UIColor(hex: AppManager.getAlternetColor())
                    
                    let lblDisplay = twoColumnCell.viewWithTag(100) as! UILabel
                    let lblGrowth = twoColumnCell.viewWithTag(101) as! UILabel
                    
                    lblDisplay.text = display
                    lblGrowth.text = growth
                    
                    return twoColumnCell
                }
            }
            
        }
        else if tableView == tblQtrCAGR {
            cell = tableView.dequeueReusableCell(withIdentifier:"qtrcagrdatacell", for: indexPath)
            cell?.selectionStyle = .none
            cell?.backgroundColor = UIColor(hex: AppManager.getAlternetColor())
                //indexPath.row % 2 == 0 ? UIColor(red: 232/255, green: 232/255, blue: 232/255, alpha: 0.4) : UIColor.white
            let lblDisplay = cell?.viewWithTag(5005) as! UILabel
            let lblColName = cell?.viewWithTag(5006) as! UILabel
            let lblColValue = cell?.viewWithTag(5007) as! UILabel
            let lblGrowth = cell?.viewWithTag(5008) as! UILabel
            if let display = nullToNil(value: self.qtrCagrDict[indexPath.row].value(forKey: "Display") as AnyObject) as? String {
                lblDisplay.text = display
            }
            if let colName = nullToNil(value: self.qtrCagrDict[indexPath.row].value(forKey: "ColName") as AnyObject) as? String {
                lblColName.text = colName
            }
            if let colValue = nullToNil(value: self.qtrCagrDict[indexPath.row].value(forKey: "ColValue") as AnyObject) as? String {
                lblColValue.text = colValue
            }
            if let growth = nullToNil(value: self.qtrCagrDict[indexPath.row].value(forKey: "Growth") as AnyObject) as? String {
                lblGrowth.text = growth
            }
            return cell!
        }
            
        else if tableView == tblResearchReport {
            if researchReportDict.isEmpty {
                
                let comingSoonCell = tableView.dequeueReusableCell(withIdentifier: "noDataCell", for: indexPath)
                
                return comingSoonCell
            }
                
            else {
                cell = tableView.dequeueReusableCell(withIdentifier:"researchreportcell", for: indexPath)
                cell?.selectionStyle = .none
                cell?.backgroundColor = UIColor(hex: AppManager.getAlternetColor())
                //indexPath.row % 2 == 0 ? UIColor(red: 232/255, green: 232/255, blue: 232/255, alpha: 0.4) : UIColor.white
                
                let lblDisplay = cell?.viewWithTag(9005) as! UILabel
                let btnpdf = cell?.viewWithTag(9006) as! UIButton
                researchReportHeightConstant += (cell?.bounds.height)!
                // self.tblSectorPerformanceHeightConstraint.constant = getTblSectorPerformanceHeight()
                if let display = nullToNil(value: self.researchReportDict[indexPath.row].value(forKey: "Description") as AnyObject) as? String {
                    lblDisplay.text = display
                }
//                if let colName = nullToNil(value: self.researchReportDict[indexPath.row].value(forKey: "Filename") as AnyObject) as? String {
//                    lblColName.text = colName
//                }
                
                return cell!
            }
        }
        else if tableView == tblStockPerformance {
            
            
            if  let colName = self.principal2Dict[indexPath.row].value(forKey: "ColName") as? String,
                let colValue = self.principal2Dict[indexPath.row].value(forKey: "ColValue") as? String,
                let display = self.principal2Dict[indexPath.row].value(forKey: "Display") as? String,
                let growth = self.principal2Dict[indexPath.row].value(forKey: "Growth") as? String,
                let stockParam = self.principal2Dict[indexPath.row].value(forKey: "StockParam") as? String {
                
                //IF HEADER CELL
                if display == "Header" {
                    
                    let headerCell = tableView.dequeueReusableCell(withIdentifier: "headerCell", for: indexPath)
                    
                    let lblColValue = headerCell.viewWithTag(101) as! UILabel
                    let lblDisplay = headerCell.viewWithTag(102) as! UILabel
                    let lblGrowth = headerCell.viewWithTag(103) as! UILabel
                    
                    lblColValue.text = colName
                    lblDisplay.text = colValue
                    lblGrowth.text = growth
                    
                    return headerCell
                }
                    
                    //IF SECTION CELL
                else if display == "Section" {
                    
                    let sectionCell = tableView.dequeueReusableCell(withIdentifier: "sectionCell", for: indexPath)
                    
                    let lblColName = sectionCell.viewWithTag(100) as! UILabel
                    lblColName.text = colName
                    print(colName)
                    
                    return sectionCell
                    
                }
                    
                    //IF FOUR COLOMN CELL
                else if colName != "", colValue != "" {
                    
                    let fourColumnCell = tableView.dequeueReusableCell(withIdentifier: "fourColumnCell", for: indexPath)
                    fourColumnCell.contentView.backgroundColor = UIColor(hex: AppManager.getAlternetColor())
                    
                    let lblColName = fourColumnCell.viewWithTag(100) as! UILabel
                    let lblColValue = fourColumnCell.viewWithTag(101) as! UILabel
                    let lblDisplay = fourColumnCell.viewWithTag(102) as! UILabel
                    let lblGrowth = fourColumnCell.viewWithTag(103) as! UILabel
                    
                    
                    if stockParam != "" {
                        
                        let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]
                        let underlineAttributedString = NSAttributedString(string: display, attributes: underlineAttribute)
                        lblColName.attributedText = underlineAttributedString
                        lblColName.textColor = UIColor.blue
                    }else {
                        lblColName.text = display
                    }
                    
                    
                    //lblColName.text = display
                    lblColValue.text = colName
                    lblDisplay.text = colValue
                    lblGrowth.text = growth
                    
                    return fourColumnCell
                }
                    
                    //IF FOUR COLOMN CELL
                else if  colName == "", colValue != "" {
                    
                    let fourColumnCell = tableView.dequeueReusableCell(withIdentifier: "fourColumnCell", for: indexPath)
                    fourColumnCell.contentView.backgroundColor = UIColor(hex: AppManager.getAlternetColor())
                    let lblColName = fourColumnCell.viewWithTag(100) as! UILabel
                    let lblColValue = fourColumnCell.viewWithTag(101) as! UILabel
                    let lblDisplay = fourColumnCell.viewWithTag(102) as! UILabel
                    let lblGrowth = fourColumnCell.viewWithTag(103) as! UILabel
                    
                    
                    if stockParam != "" {
                        
                        let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]
                        let underlineAttributedString = NSAttributedString(string: display, attributes: underlineAttribute)
                        lblColName.attributedText = underlineAttributedString
                        lblColName.textColor = UIColor.blue
                    }else {
                        lblColName.text = display
                    }
                    
                    
                    //lblColName.text = display
                    lblColValue.text = colName
                    lblDisplay.text = colValue
                    lblGrowth.text = growth
                    
                    return fourColumnCell
                }
                    
                //IF TWO VALUE CELL
                else if colName == "", colValue == "" {
                    
                    let twoColumnCell = tableView.dequeueReusableCell(withIdentifier: "twoColumnCell", for: indexPath)
                    twoColumnCell.contentView.backgroundColor = UIColor(hex: AppManager.getAlternetColor())
                    let lblDisplay = twoColumnCell.viewWithTag(100) as! UILabel
                    let lblGrowth = twoColumnCell.viewWithTag(101) as! UILabel
                    
                    
                    if stockParam != "" {
                        
                        let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]
                        let underlineAttributedString = NSAttributedString(string: display, attributes: underlineAttribute)
                        lblDisplay.attributedText = underlineAttributedString
                        lblDisplay.textColor = UIColor.blue
                    }else {
                        lblDisplay.text = display
                    }
                    
                    //lblDisplay.text = display
                    lblGrowth.text = growth
                    
                    return twoColumnCell
                }
            }
            
        }
        else  {
            
            if principal3Dict.isEmpty {
                
                let comingSoonCell = tableView.dequeueReusableCell(withIdentifier: "noDataCell", for: indexPath)
                return comingSoonCell
            }
            
            else {
                cell = tableView.dequeueReusableCell(withIdentifier:"sectorperformancecell", for: indexPath)
                cell?.selectionStyle = .none
                cell?.backgroundColor = UIColor(hex: AppManager.getAlternetColor())
                    //indexPath.row % 2 == 0 ? UIColor(red: 232/255, green: 232/255, blue: 232/255, alpha: 0.4) : UIColor.white
                
                let lblDisplay = cell?.viewWithTag(7005) as! UILabel
                let lblColName = cell?.viewWithTag(7006) as! UILabel
                fundamentalHeightConstant += (cell?.bounds.height)!
               // self.tblSectorPerformanceHeightConstraint.constant = getTblSectorPerformanceHeight()
                if let display = nullToNil(value: self.principal3Dict[indexPath.row].value(forKey: "Header") as AnyObject) as? String {
                    lblDisplay.text = display
                }
                if let colName = nullToNil(value: self.principal3Dict[indexPath.row].value(forKey: "_Value") as AnyObject) as? String {
                    lblColName.text = colName
                }
                
                return cell!
            }
            
        }
        
        return UITableViewCell()
    }
    
    
    //MARK: Table View Delegate
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var count:Int?
        //print(tableView)
        
        if (tableView == self.tblResearchReport) {
            count = self.researchReportDict.isEmpty ? 0
                : self.researchReportDict.count
        }
        
        if (tableView == self.tblcorrection) {
            count = self.principal4Dict.count
        }
        
        if (tableView == self.tblsupportresistance) {
            count = self.pricePerformanceDict.count
        }
        if (tableView == self.tbl_Fundamentals) {
            count = introductionDict.count
        }
        if (tableView == self.tblYearlyData) {
            count = yearlylDict.count
        }
        if (tableView == self.tblQtrlyData) {
            count = qtrlylDict.count
        }
        if (tableView == self.tblAnnualCAGR) {
            count = principal1Dict.count
        }
        if (tableView == self.tblQtrCAGR) {
            count = 0 //qtrCagrDict.count
        }
        if (tableView == self.tblStockPerformance) {
            count = principal2Dict.count
        }
        if (tableView == self.tblSectorPerformance) {
            count =  principal3Dict.isEmpty ? 1 : principal3Dict.count
        }
        return count!
    }
    
    override public func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        print(UIDevice.current.orientation.isPortrait)
    }
    
    
    func setChart(symbolval : String) {
        
        lineChartView.delegate=self
        
        let obj = WebService()
        let paremeters = "symbol=\(symbolval)"
        var result = ""
        var linechartDataSet = LineChartDataSet()
        
        //Mark: Call Service for Chart Data
        obj.callWebServices(url: Urls.chart_data, methodName: "GET", parameters: paremeters, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
            
            //print("Json Data is :  \(jsonData)")
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                DispatchQueue.main.async {
                    
                    let tempArray = jsonData.value(forKey: "response") as! NSArray
                    for dict in tempArray {
                        let dictValue = dict as! NSDictionary
                        result = dictValue.value(forKey: "DISPLAY_DATE") as! String
                        self.months.append(result)
                        self.values.append(dictValue.value(forKey: "Close") as! String)
                    }
                    
                    
                    //print(self.months)
                    //barChartView.noDataText = "You need to provide data for the chart."
                    
                    var dataEntries: [ChartDataEntry] = []
                    
                    for i in 0..<self.months.count {
                        
                        // let dataEntry = ChartDataEntry(x: Double(i), y: values[i])
                        let dataEntry = ChartDataEntry(x: Double(i), y: Double(self.values[i])!)
                        dataEntries.append(dataEntry)
                    }
                    
                    linechartDataSet = LineChartDataSet(values: dataEntries, label: "Data Uploaded daily at 8:30 pm")
                    linechartDataSet.axisDependency = .left
                    linechartDataSet.drawCirclesEnabled = false
                    linechartDataSet.drawValuesEnabled = false
                    linechartDataSet.circleColors = [NSUIColor.white]
                    linechartDataSet.setCircleColor(UIColor.white)
                    // linechartDataSet.circleRadius = 0.0 // the radius of the node circle
                    //linechartDataSet.fillAlpha = 65 / 255.0
                    linechartDataSet.fillColor = UIColor.white
                    linechartDataSet.highlightColor = UIColor.white
                    linechartDataSet.drawCircleHoleEnabled = false
                    linechartDataSet.colors = [UIColor.black]
                    
                    self.lineChartView.xAxis.valueFormatter = IndexAxisValueFormatter(values: self.months)
                    self.lineChartView.xAxis.granularity = 1
                    self.lineChartView.xAxis.labelPosition = .bottom
                    self.lineChartView.rightAxis.enabled = false
                    self.lineChartView.data?.setDrawValues(false)
                    self.lineChartView.leftAxis.drawGridLinesEnabled = true
                    self.lineChartView.xAxis.drawGridLinesEnabled = true
                    self.lineChartView.legend.enabled = false
                    self.lineChartView.chartDescription?.text = ""
                    
                    var dataSets : [LineChartDataSet] = [LineChartDataSet]()
                    dataSets.append(linechartDataSet)
                    let data: LineChartData = LineChartData(dataSets: dataSets)
                    
                    data.setValueTextColor(UIColor.red)
                    self.lineChartView.data = data
                    self.actInd.stopAnimating()
                    
                }
                
                
            }
        }
        
        /* END Chart Section */
    }
    
    
    @IBAction func btn_CategoryClick_Action(_ sender: Any) {
         AppManager().showAlert(title: "", message: self.SymbolCategoryDesc , navigationController: self.navigationController!)
    }
    
    @IBAction func btnClickSector(_ sender: Any) {
        
        SearchForMultibaggerSector.serchValue = sector
        //SearchForCurrentMultibagger.viewcontrollername = "MultibaggerSectorList"
        User.isCommingFromSpecificPage = true
        // self.navigationController?.popViewController(animated: false)
        
        DispatchQueue.main.async {
            let viewCont = self.storyboard?.instantiateViewController(withIdentifier: "SectorPerformanceDetailsViewController") as! SectorPerformanceDetailsViewController
            self.navigationController?.pushViewController(viewCont, animated: false)
        }
    }
    
    
    @IBAction func btn_Hamberger(_ sender: Any) {
		
		if (SearchForCurrentMultibagger.viewcontrollername == "CustomPushViewController"){
			
			let viewController = self.storyboard?.instantiateViewController(withIdentifier: "CustomPushViewController") as! CustomPushViewController
			
			self.navigationController?.pushViewController(viewController, animated: false)
			
		} else {
			 self.navigationController?.popViewController(animated: false)
		}
		
		
        //DispatchQueue.main.async {
        
        /* if(SearchForCurrentMultibagger.viewcontrollername == "MultibaggerList"){
         //let viewController = self.storyboard?.instantiateViewController(withIdentifier: "MultibaggerViewController") as! MultibaggerViewController
         //self.navigationController?.pushViewController(viewController, animated: false)
         self.navigationController?.popViewController(animated: false)
         }
         
         if (SearchForCurrentMultibagger.viewcontrollername == "YearWiseMultibagger"){
         
         //let viewController = self.storyboard?.instantiateViewController(withIdentifier: "PastPerformenceViewController") as! PastPerformenceViewController
         
         //self.navigationController?.pushViewController(viewController, animated: false)
         self.navigationController?.popViewController(animated: false)
         }
         
         if (SearchForCurrentMultibagger.viewcontrollername == "Search"){
         
         //let viewController = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
         
         //self.navigationController?.pushViewController(viewController, animated: false)
         self.navigationController?.popViewController(animated: false)
         }*/
        
        // }
    }
    
    @IBAction func btnMarketCapGraph_Action(_ sender: Any) {
        
         
         let storyBoard = UIStoryboard(name: "Main", bundle: nil)
         
         let viewController = storyBoard.instantiateViewController(withIdentifier: "PerformanceGraphViewController") as! PerformanceGraphViewController
         
         viewController.stock_param  = "mcap"
         viewController.title_text   = "Market Cap - \(SearchForCurrentMultibagger.instrument_4)"
         viewController.symbol_name = SearchForCurrentMultibagger.serchValue.replacingOccurrences(of: " ", with: "%20")
         User.navigation.pushViewController(viewController, animated: true)
        
        
    }
    
    
    
    @IBAction func btnTechnicalAnalysis_Action(_ sender: Any) {
        
        let viewCont = self.storyboard?.instantiateViewController(withIdentifier: "TechnicalAnalysisViewController") as! TechnicalAnalysisViewController
        viewCont.serchValue = symbolval
        viewCont.instrument_4 = SearchForCurrentMultibagger.instrument_4
        self.navigationController?.pushViewController(viewCont, animated: false)
    }
    
    
}

