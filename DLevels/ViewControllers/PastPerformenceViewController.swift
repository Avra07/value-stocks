//
//  DashBoardViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-02 on 25/01/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import Foundation
import UIKit

class PastPerformenceViewController: UIViewController, UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var lblnoofstocks: UILabel!
    @IBOutlet weak var lblHeader: UILabel!
    
    @IBOutlet weak var lblindextype: UILabel!
    @IBOutlet weak var segmentVwSpace: UIView!
        
    @IBOutlet weak var btnChangeMonths: UIButton!
    @IBOutlet weak var tickerlistview: UITableView!
    @IBOutlet weak var bottomScrollView: UIScrollView!
    
   
    @IBOutlet weak var navigationView: UIView!
    
    @IBOutlet weak var btnHamberger: UIButton!
    
    @IBOutlet weak var btnMutibaggerCount: UIButton!
    @IBOutlet weak var lblMultibaggerList: UILabel!
    
    @IBOutlet weak var img_sort_button: UIButton!
 
    @IBOutlet weak var lblindexval: UILabel!
    
    @IBOutlet weak var lblviewdate: UILabel!
    let names   = ["Multibagger List","Gainers/Losers","Sector Performance","Year Wise Multibagger"]
    let hint    = ["Multibagger List","Gainers/Losers","Sector Performance","Year Wise Multibagger"]
    
    
    var segmentData = [NSDictionary]()
    var items = [String]()
    var segmentarr = [String]()
    var arrRepType   =  [String]()
    
   
    
    var typeOfMutibagger = ""
    
    var instanceOfMultibaggerRecomended = MultibaggerRecomended()
    var isShowingMenu = false
    
    var alldata         =     [NSDictionary]()
    var response        =     [NSDictionary]()
    var arrLargeCap     =     [NSDictionary]()
    var arrSmallCap     =     [NSDictionary]()
    
    var flag_sort:Int = 1
    var sortedBydata : NSArray = []
    var tablevwbindarr = [NSDictionary]()
    var col = "Current Trailing PE"
  
    var highlightcolor = UIColor(red: 247/255, green: 196/255, blue: 58/255, alpha: 1)
    
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        prepareScrollView()
        
        self.tickerlistview.estimatedRowHeight = 75;
        //self.tickerlistview.rowHeight = UITableViewAutomaticDimension;
        self.tickerlistview.layoutMargins = UIEdgeInsets.zero
        self.tickerlistview.separatorInset = UIEdgeInsets.zero
        
        self.tickerlistview.setNeedsLayout()
        self.tickerlistview.layoutIfNeeded()
        
        if MultibaggerType.typeOfMultibagger == MultibaggerType.midCap
        {
            lblHeader.text = "Small Cap/Mid Cap Multibaggers"
        }
        else {
            lblHeader.text = "Large Cap Multibaggers"
        }
        
        self.segmenmtDataLoad()
        
        loadAllData()
        
        
        
        
        tickerlistview.tableFooterView = UIView()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.shouldRotate = false // or false to disable rotation

        
        addLoader()
        actInd.startAnimating()
      
        
        bottomScrollView.setContentOffset(CGPoint(x: 360, y: 0), animated: false)
    }

    
    override func viewWillAppear(_ animated: Bool) {
        
        tickerlistview.estimatedRowHeight = 1000
        tickerlistview.rowHeight = UITableViewAutomaticDimension
        
        
        let range = Range(uncheckedBounds: (lower: 0, upper: self.tickerlistview.numberOfSections))
        self.tickerlistview.reloadSections(IndexSet(integersIn: range), with: .none)
        
        }
    @IBAction func btn_sort(_ sender: Any) {
        
        self.response = self.response.reversed()
        
        
        let sectionIndex = IndexSet(integer: 0)
        self.tickerlistview.reloadSections(sectionIndex, with: .none)
        self.view.layoutIfNeeded()
        
        
       
    }
    func firstTimeDataLoad()
    {
        self.loadDataForMultibaggerList(dateval: segmentarr[0], index: 0)
    
    }
    
    
    func addLoader() {
        actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0) //CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.gray
        self.view.addSubview(actInd)
    }
    
    //MARK: Segment Controll Value Changed
    func segmentedControlValueChanged(segment: UISegmentedControl) {
        
        
//        self.loadDataForMultibaggerList(dateval: (segmentarr[segment.selectedSegmentIndex] as NSString) as String);
//        
//        
//        UIView.animate(withDuration: 0.2) {
//            
//            self.actInd.stopAnimating()
//            self.tickerlistview.reloadData()
//            //self.tickerlistview.reloadSections(sectionIndex, with: .automatic)
//        }

        
        
        
    }
    
     //MARK: 3 Months Button Action
    @IBAction func btnTableHeaderAction(_ sender: UIButton) {
        
        let actionSheeController = UIAlertController(title:"Data Fields", message: "", preferredStyle: .actionSheet)
        actionSheeController.view.tintColor = UIColor(red: 247/255, green: 196/255, blue: 58/255, alpha: 1)
        
        actionSheeController.addAction(UIAlertAction(title: "Current Trailing PE", style: .default, handler: { (success) in
            self.btnChangeMonths.setTitle("Trailing PE", for: .normal)
            self.col = "Current Trailing PE"
            UIView.animate(withDuration: 0.4) {
                let sectionIndex = IndexSet(integer: 0)
                self.tickerlistview.reloadSections(sectionIndex, with: .none)
                self.view.layoutIfNeeded()
            }
        }))
       
        actionSheeController.addAction(UIAlertAction(title: "EPS", style: .default, handler: { (success) in
            self.col = "EPS"
            self.btnChangeMonths.setTitle("EPS", for: .normal)
            UIView.animate(withDuration: 0.4) {
                let sectionIndex = IndexSet(integer: 0)
                self.tickerlistview.reloadSections(sectionIndex, with: .none)
                self.view.layoutIfNeeded()
            }
        }))
        
        actionSheeController.addAction(UIAlertAction(title: "Avg Value(in Cr)", style: .default, handler: { (success) in
            self.col = "Avg Value(in Cr)"
            self.btnChangeMonths.setTitle("Avg Value", for: .normal)
            UIView.animate(withDuration: 0.4) {
                let sectionIndex = IndexSet(integer: 0)
                self.tickerlistview.reloadSections(sectionIndex, with: .none)
                self.view.layoutIfNeeded()
            }
        }))
        
        actionSheeController.addAction(UIAlertAction(title: "PAT(in Cr)", style: .default, handler: { (success) in
          
            self.col = "PAT(in Cr)"
            self.btnChangeMonths.setTitle("PAT(in Cr)", for: .normal)
            UIView.animate(withDuration: 0.4) {
                let sectionIndex = IndexSet(integer: 0)
                self.tickerlistview.reloadSections(sectionIndex, with: .none)
                self.view.layoutIfNeeded()
            }
        }) )
        
        actionSheeController.addAction(UIAlertAction(title: "Institution", style: .default, handler: { (success) in
            
            self.col = "Institution"
            self.btnChangeMonths.setTitle("Institution", for: .normal)
            UIView.animate(withDuration: 0.4) {
                let sectionIndex = IndexSet(integer: 0)
                self.tickerlistview.reloadSections(sectionIndex, with: .none)
                self.view.layoutIfNeeded()
            }
        }) )
        
        actionSheeController.addAction(UIAlertAction(title: "Pledge", style: .default, handler: { (success) in
            
            self.col = "Pledge"
            self.btnChangeMonths.setTitle("Pledge", for: .normal)
            UIView.animate(withDuration: 0.4) {
                let sectionIndex = IndexSet(integer: 0)
                self.tickerlistview.reloadSections(sectionIndex, with: .none)
                self.view.layoutIfNeeded()
            }
        }) )


        
        
        actionSheeController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (success) in
        }) )
        
        
        self.present(actionSheeController, animated: true) { 
            
        }
        
    }
    
    func removeDuplicates(array: [String]) -> [String] {
        var encountered = Set<String>()
        var result: [String] = []
        for value in array {
            if encountered.contains(value) {
                // Do not add a duplicate element.
            }
            else {
                // Add value to the set.
                encountered.insert(value)
                // ... Append the value.
                result.append(value)
            }
        }
        return result
    }
    
    
    func segmenmtDataLoad()
    {
        
        segmentarr = MultibaggerStats.segYearDateArr
        arrRepType = MultibaggerStats.segYearDisplayArr
        
        
        //        print(segmentarr)
        //        print(arrRepType)
        
        
        self.buttonViewLoad()
        
        
        
    }
    
    
    
    
    
    //MARK: Creating Segment View
    func segmentViewLoad()
    {
        
        //print("segment---------\(arrRepType)")
        
        let segmentedControl = UISegmentedControl(items: arrRepType)
        
        segmentedControl.frame = CGRect(x:0 , y: bottomScrollView.frame.origin.x + 5, width: UIScreen.main.bounds.width - 30 , height: 30)
        segmentedControl.addTarget(self, action: #selector(self.segmentedControlValueChanged(segment:)), for: .valueChanged)
        let font = UIFont.systemFont(ofSize: 12)
        segmentedControl.setTitleTextAttributes([NSFontAttributeName: font],
                                                for: .normal)
        // segmentedControl.tintColor   = UIColor(red: 247/255, green: 197/255, blue: 58/255, alpha: 1)
        
        segmentedControl.tintColor   = UIColor(red: 247/255, green: 197/255, blue: 58/255, alpha: 1)
        segmentedControl.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor(red: 40/255, green: 76/255, blue: 90/255, alpha: 1)], for: .selected)
        segmentedControl.backgroundColor = UIColor.white
        segmentedControl.selectedSegmentIndex = 0
        
        segmentVwSpace.addSubview(segmentedControl)
    }
    
    func buttonViewLoad()
    {
        
        var originx = 0
        
        for (index, element) in arrRepType.enumerated() {
            
            //print(segmentVwSpace.frame.width);
            
            let button = UIButton(frame: CGRect(x:originx , y: Int(segmentVwSpace.frame.origin.x-8), width: (Int(segmentVwSpace.frame.width/4) - 4) , height: 40))
            button.backgroundColor = UIColor.white
            button.setTitle(element, for: .normal)
            button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
            button.addTarget(self, action: #selector(buttonAction), for: .touchUpOutside)
            
            
            button.setTitleColor(UIColor(red: 103/255, green: 128/255, blue: 138/255, alpha: 1), for: .normal)
            button.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 12)
            button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
            
            button.layer.borderColor = UIColor(red: 103/255, green: 128/255, blue: 138/255, alpha: 1).cgColor
            button.layer.borderWidth = 1.0
            button.tag = index
            
            if(index == 0)
            {
                button.backgroundColor = self.highlightcolor
            }
            
            
            
            segmentVwSpace.addSubview(button)
            originx  = originx + Int(button.frame.width )
            
            //print(button.frame.width)
        }
        
        
    }
        
    
    
    func buttonAction(sender: UIButton!) {
        
        self.loadDataForMultibaggerList(dateval: (segmentarr[(sender?.tag)!]) as String, index: sender.tag);
        
        
            
            //self.actInd.stopAnimating()
            self.tickerlistview.reloadData()
            //self.tickerlistview.reloadSections(sectionIndex, with: .automatic)
            sender.backgroundColor = self.highlightcolor
        
            if self.segmentVwSpace != nil{
                
                for v in self.segmentVwSpace.subviews{
                    if(v.tag != sender.tag)
                    {
                        v.backgroundColor = UIColor.white
                    }
                }
                }
    }

    
    //MARK: Table View Delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    //MARK: Table View Delegate
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:"tickercell", for: indexPath)
        cell.selectionStyle = .none
        
        
            cell.backgroundColor = indexPath.row % 2 == 0 ?  UIColor(red: 215/255, green: 222/255, blue: 225/255, alpha: 1.0) : UIColor(red: 236/255, green: 239/255, blue: 240/255, alpha: 1.0)
           
            
            let lblMultibagerName = cell.viewWithTag(1000) as! UILabel
            //lblMultibagerName.text = self.response[indexPath.row].value(forKey: "mrd_Instrument_4") as? String
            
            let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]
            let underlineAttributedString = NSAttributedString(string: (self.response[indexPath.row].value(forKey: "mrd_Instrument_4") as? String)!, attributes: underlineAttribute)
            lblMultibagerName.attributedText = underlineAttributedString
            
            let lblSector = cell.viewWithTag(1004) as! UILabel
            lblSector.text = self.response[indexPath.row].value(forKey: "mrd_Sector") as? String
            
            let lblChange = cell.viewWithTag(1003) as! UILabel
            
            
            if self.col == "Current Trailing PE" {
                lblChange.text =  self.response[indexPath.row].value(forKey: "mrd_PE") as? String
            } else if self.col == "EPS" {
                lblChange.text =  self.response[indexPath.row].value(forKey: "mrd_EPS") as? String
            } else if self.col == "Avg Value(in Cr)" {
                lblChange.text =  self.response[indexPath.row].value(forKey: "mrd_Value") as? String
            } else if self.col == "PAT(in Cr)" {
                lblChange.text =  self.response[indexPath.row].value(forKey: "mrd_PAT") as? String
            }else if self.col == "Institution" {
                lblChange.text =  self.response[indexPath.row].value(forKey: "mrd_Institution") as? String
            }else if self.col == "Pledge" {
                lblChange.text =  self.response[indexPath.row].value(forKey: "mrd_Pledge") as? String
            }
            
            
            
            
            let lblPERatio = cell.viewWithTag(1002) as! UILabel // mrd_PERatio
            lblPERatio.text =  self.response[indexPath.row].value(forKey: "mrd_Performance") as? String
            
            
            
            cell.layoutMargins = UIEdgeInsets.zero

        
         return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    
    
    //MARK: Table View Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return response.count
    }
    
    
    
    
    //MARK: Prepare Scroll View
    private func prepareScrollView() {
      	
        bottomScrollView.showsHorizontalScrollIndicator = true
        var fromLeft: CGFloat = 10
        
        for index in 0..<4 {
            
            let button = UIButton()
            button.contentMode       = .scaleAspectFill
            button.setTitle(names[index], for: .normal)
            button.setTitleColor(UIColor(red: 40/255, green: 76/255, blue: 90/255, alpha: 1), for: .normal)
            button.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 18)
            
            button.accessibilityHint = names[index]
           
            button.titleLabel?.lineBreakMode = .byClipping
            button.frame = CGRect(x: fromLeft, y: 0, width: 170, height: 35)
            button.contentHorizontalAlignment = .center
            button.layer.borderColor = UIColor.blue.cgColor
            button.titleLabel?.adjustsFontSizeToFitWidth = true
            
            fromLeft +=  170
            button.addTarget(self, action: #selector(DashBoardViewController.buttonTappedInScrollView(sender:)), for: .touchUpInside)
            
            button.tag = index
            bottomScrollView.addSubview(button)
            let highlightvw = UIView(frame: CGRect(x: button.frame.origin.x, y: button.frame.origin.y + button.frame.height, width: 160, height: 2.5))
            
            highlightvw.tag = index
            
         //   print("--vvvvvv----\(index)")
            if(index == 3)
            {
                
                highlightvw.backgroundColor = highlightcolor
                button.titleLabel?.font = UIFont(name: "Helvetica Neue Bold", size: 12)
                button.titleLabel?.adjustsFontSizeToFitWidth = true
                
            }
            else
            {
                highlightvw.backgroundColor = UIColor.white
                button.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 14)
                
            }
            
            bottomScrollView.addSubview(highlightvw)
            bottomScrollView.contentSize = CGSize(width: fromLeft, height: 1)
        }
        
        self.view.layoutIfNeeded()
    }
    
    
    func buttonTappedInScrollView(sender: UIButton){
        
    //    print(" You have tapped on: \(sender.accessibilityHint)")
        
        if let page = sender.accessibilityHint {
            
            if page == "MULTIBAGGER LIST" || page == "Multibagger List" {
                
                
                DispatchQueue.main.async {
                    let viewCont = self.storyboard?.instantiateViewController(withIdentifier: "MultibaggerViewController") as! MultibaggerViewController
                    self.navigationController?.pushViewController(viewCont, animated: false)
                }
                
                
            } else if page == "Gainers/Losers" || page == "GAINERS/LOSERS"{
                
                DispatchQueue.main.async {
                    let viewCont = self.storyboard?.instantiateViewController(withIdentifier: "HeatMapViewController") as! HeatMapViewController
                    self.navigationController?.pushViewController(viewCont, animated: false)
                }
                
            }
            else if page == "Year Wise Multibagger" || page == "YEAR WISE MULTIBAGGER" {
                
                DispatchQueue.main.async {
                    let viewCont = self.storyboard?.instantiateViewController(withIdentifier: "PastPerformenceViewController") as! PastPerformenceViewController
                    self.navigationController?.pushViewController(viewCont, animated: false)
                }
            }
            else if page == "Sector Performance" || page == "SECTOR PERFORMANCE" {
                
                DispatchQueue.main.async {
                    let viewCont = self.storyboard?.instantiateViewController(withIdentifier: "MultibaggerSectorViewController") as! MultibaggerSectorViewController
                    self.navigationController?.pushViewController(viewCont, animated: false)
                }
                
                
                
            }
        }
    }
    
    /************************************************************/
    func loadAllData()
    {
        //segmenmtDataLoad()
        
        
        var arrdata = [NSDictionary]()
        
        
        self.actInd.startAnimating()
        
        if MultibaggerType.typeOfMultibagger == MultibaggerType.midCap
        {
            arrdata = MultibaggerStats.multibaggersmallcapyearwisedata
        }
        else
        {
            arrdata = MultibaggerStats.multibaggerlargecapyearwisedata
        }

        
        
        
        if(arrdata.count == 0)
        {
        
        var segmentval = "MLT-LARGE"
        if MultibaggerType.typeOfMultibagger == MultibaggerType.midCap
        {
            segmentval = "MLT-MID"
        }
        
        let typeval = "YEAR"
        
        let obj = WebService()
        let paremeters = "segment=\(segmentval)&type=\(typeval)&date=1900-01-01"
        
        obj.callWebServices(url: Urls.multibaggerlist, methodName: "POST", parameters: paremeters, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
            
            //print("Json Data is :  \(jsonData)")
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                let tempArray = jsonData.value(forKey: "response") as! NSArray
                
                self.alldata.removeAll()
                
                for dict in tempArray {
                    let dictValue = dict as! NSDictionary
                    self.alldata.append(dictValue)
                }
                
                DispatchQueue.main.async(execute: {
                    Thread.current.cancel()
                    self.loadDataForMultibaggerList(dateval: self.segmentarr[0], index: 0)
                    
                    if MultibaggerType.typeOfMultibagger == MultibaggerType.midCap
                    {
                        MultibaggerStats.multibaggersmallcapyearwisedata = self.alldata
                    }
                    else
                    {
                        MultibaggerStats.multibaggerlargecapyearwisedata = self.alldata
                    }
                    
                    
                    
                    self.actInd.stopAnimating()
                    
             
                })
            }
            else
            {
                self.actInd.stopAnimating()
                let alertobj = AppManager()
                alertobj.showAlert(title: "Error!", message: jsonData.value(forKey: "errmsg") as! String , navigationController: self.navigationController!)
            }
            
            }
        }
        else
        {
            if MultibaggerType.typeOfMultibagger == MultibaggerType.midCap
            {
                self.alldata = MultibaggerStats.multibaggersmallcapyearwisedata
            }
            else
            {
                self.alldata = MultibaggerStats.multibaggerlargecapyearwisedata
            }
            DispatchQueue.main.async(execute: {
                Thread.current.cancel()
                self.loadDataForMultibaggerList(dateval: self.segmentarr[0], index: 0)
                
                self.actInd.stopAnimating()
                
            })
        }

        
    }

    /*************************************************************/
    
    //MARK: Getting data for Mulibagger/ TableView List
    func loadDataForMultibaggerList(dateval: String, index: Int)
    {
        var smallcap = ""
        var smallcapval = ""
        var smallcaplbl = ""
        var datevalue = ""
        var datearr =  [String]()
        
        
        //actInd.startAnimating()
        
        self.response.removeAll()
        
        for arr in alldata {
            
            print(arr)
            
            if (arr.value(forKey: "mrd_Date") as! String) == dateval  {
                self.response.append(arr)
            }
        }
        
        let tempArray = response as NSArray
        
        if(tempArray.count > 0)
        {
            
            let indexval = tempArray[0] as! NSDictionary
            
            
            if MultibaggerType.typeOfMultibagger == MultibaggerType.midCap{
                
                smallcap = indexval.value(forKey: "mrd_SmallCap_Per") as! String
                smallcaplbl = "Small Cap Idx Perf : "
                smallcapval = smallcap
            }
            else{
                
                smallcap = indexval.value(forKey: "mrd_Nifty_Per") as! String
                smallcaplbl = "Nifty Idx Perf : "
                smallcapval = smallcap
            }
            
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "yy"
            let result = formatter.string(from: date)
            
            datearr = dateval.components(separatedBy: "-")
            datevalue = String(datearr[0].characters.suffix(2))
            print(datevalue)
            print(result)
            
            
            
            
           self.lblviewdate.text = MultibaggerStats.yearperformancetxt[index]
            

            
                       
            DispatchQueue.main.async(execute: {
                
                self.actInd.stopAnimating()
                self.lblnoofstocks.text = "Total Stocks : \(self.response.count)"
                self.lblindextype.text = smallcaplbl
                self.lblindexval.text = "\(smallcapval)%"
                
                if((smallcapval as NSString).floatValue > 0)
                {
                    self.lblindexval.textColor = UIColor(red: 29/255, green: 204/255, blue: 146/255, alpha: 1)
                }
                else
                {
                    self.lblindexval.textColor = UIColor.red
                }
                
                self.tickerlistview.reloadData()
                //self.tickerlistview.reloadSections(IndexSet(integer: 0), with: .none)
                
            })
            
        }
        
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        SearchForCurrentMultibagger.serchValue = response[indexPath.row].value(forKey: "mrd_Security") as! String
        let srchval  =  SearchForCurrentMultibagger.serchValue
        SearchForCurrentMultibagger.instrument_4 = response[indexPath.row].value(forKey: "mrd_Instrument_4") as! String
         SearchForCurrentMultibagger.viewcontrollername = "YearWiseMultibagger"
        
        
        
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "StockSpecificNewViewController") as! StockSpecificNewViewController
        self.navigationController?.pushViewController(viewController, animated: true)
        
        
    }

    @IBAction func btnSearchAction(_ sender: Any) {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    //MARK: Multibagger count Label Click
    @IBAction func onClickMultibaggerCount(_ sender: UIButton) {
        
        instanceOfMultibaggerRecomended.prepareScreenWithView(navigationController: self.navigationController!,viewSize: self.view)
    }


    @IBAction func onClick_Hamberger(_ sender: UIButton) {
        
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "MultibaggerLandingViewController") as! MultibaggerLandingViewController
        self.navigationController?.pushViewController(viewController, animated: false)
        
        
        //self.navigationController?.popViewController(animated: true)
        
    }
    
}
    
    

