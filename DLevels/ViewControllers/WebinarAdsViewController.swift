//
//  WebinarAdsViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-02 on 01/06/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class WebinarAdsViewController: UIViewController {

    @IBOutlet weak var btnCallNowOrRegister: UIButton!
    @IBOutlet weak var ivwebinarad: UIImageView!
    override func viewDidLoad() {
		
        btnCallNowOrRegister.setTitle(seminar_ad.ab_button_txt, for: .normal)
		self.navigationController?.navigationBar.isHidden = true
        super.viewDidLoad()
        loadWebinarad()
        AppManager().setStatusBarBackgroundColor()
    }

	@IBAction func btn_click_link(_ sender: Any) {
		
		
		if seminar_ad.ab_button_action == "call" {
			
			let static_part  = "telprompt://"
			let dynamic_part = seminar_ad.url
			
			
			let url = URL(string: "\(static_part)\(dynamic_part)")!
			if #available(iOS 10.0, *) {
				UIApplication.shared.open(url, options: [:], completionHandler: nil)
			} else {
				UIApplication.shared.openURL(url)
			}
			
			//UIApplication.shared.openURL(NSURL(string: seminar_ad.url)! as URL)
		}else {
			UIApplication.shared.openURL(NSURL(string: seminar_ad.url)! as URL)
		}
		
		
//        let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "MFHomeViewController") as! MFHomeViewController
//        self.navigationController?.pushViewController(viewController, animated: true)
		
    }
    @IBAction func btn_close(_ sender: Any) {
                       
//        let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "MultibaggerLandingViewController") as! MultibaggerLandingViewController
//        self.navigationController?.pushViewController(viewController, animated: true)
        
        
        let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "DlHomeViewController") as! DlHomeViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
   
    func loadWebinarad(){
        
        DispatchQueue.main.async {
            let urlpath = seminar_ad.templete
            //        ivwebinarad.downloadedFrom(link: urlpath)
            self.ivwebinarad.downloadedFrom(link: urlpath, contentMode: .scaleToFill)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
