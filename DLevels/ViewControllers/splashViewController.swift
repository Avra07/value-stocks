//
//  splashViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-01 on 28/12/16.
//  Copyright © 2016 Dynamic-Mac-01. All rights reserved.
//

import UIKit
import FirebaseInstanceID

class splashViewController: UIViewController{
    
   // @IBOutlet weak var tblStocks: UITableView!
    var arr : NSArray?    
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    
    
    
    
    //MARK: VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true
        
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        print(version)
        
        if User.email == "guest@yopmail.com" {
            normalRegistrationLogOut(completion: {
                self.actInd.stopAnimating()
                self.checkversion(ver: Double(version)!)
            })
        }else {
            self.actInd.stopAnimating()
            checkversion(ver: Double(version)!)
        }
        
        
		
		
		// AppManager().showAlert(title: "Message", message: NotificationData.url, navigationController: self.navigationController!)
		
		
		
        
		
        
    }
    
    
    func share_link()
    {
        let url = URL(string: "https://itunes.apple.com/us/app/dlevels/id1204153729?ls=1&mt=8")!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    func checkversion(ver : Double)
    {
        let webService = WebService()
        let appManager = AppManager()
        let device_type = "IOS"
        let parameters = "version=\(ver)&type=\(device_type)"
        
        webService.callWebServices(url: Urls.check_version, methodName: "GET", parameters: parameters, istoken: false, tokenval: "", completion: { (success, dictData) in
            // print(dictData);
            
            if dictData.value(forKey: "error") as! NSInteger == 0 {
                
                let response = dictData.value(forKey: "response") as! [NSDictionary]
                let forced_val = response[0].value(forKey: "FORCED") as! String
                let msg = response[0].value(forKey: "MESSAGE") as! String
                
                let status_val = response[0].value(forKey: "STATUS_ID") as! String
                
                if(status_val == "1")
                {
                    DispatchQueue.main.async(execute: {
                    
                    if(forced_val == "1")
                    {
                        let alertController = UIAlertController(title: "Notice", message: msg, preferredStyle: .alert)
                        
                        // Create OK button
                        let OKAction = UIAlertAction(title: "Update", style: .default) { (action:UIAlertAction!) in
                            
                            // Code in this block will trigger when OK button tapped.
                            self.share_link()
                            
                        }
                        alertController.addAction(OKAction)
                        
                        // Present Dialog message
                        self.present(alertController, animated: true, completion:nil)
                    }
                    else
                    {
                        let alertController = UIAlertController(title: "Notice", message: msg, preferredStyle: .alert)
                        
                        // Create OK button
                        let OKAction = UIAlertAction(title: "Update", style: .default) { (action:UIAlertAction!) in
                            
                            // Code in this block will trigger when OK button tapped.
                            self.share_link()
                            
                        }
                        alertController.addAction(OKAction)
                        
                        // Create Cancel button
                        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                            
                            
                            self.checklogin()
                        }
                        alertController.addAction(cancelAction)
                        
                        // Present Dialog message
                        self.present(alertController, animated: true, completion:nil)
                        
                        
                        
                        }
                    });
                    
                }
                else
                {
                     DispatchQueue.main.async(execute: {
                        self.checklogin()
                    })
                }
                
                
                
            }
            else {
                
                appManager.showAlert(title: "Error", message: dictData.value(forKey: "errmsg") as! String, navigationController: self.navigationController!)
            }
        })
        
    }
    
       
   
    
    
    func checklogin()
    {
        
        if let email = UserDefaults.standard.value(forKey: "email") as? String{
            print("Email Id is: \(email)")
            User.email = email
            if let password = UserDefaults.standard.value(forKey: "password") as? String{
                //print("password is : \(password)")
                
                if let regThrough = UserDefaults.standard.value(forKey: "regThrough") as? String{
                    
                    //print(regThrough)
                    if(email != "")
                    {
                        self.performLogin(email: email, password: password, regThrough: regThrough)
                    }
                    else
                    {
                       moveToLogin()
                    }
                } else {
                    
                    moveToLogin()
                }
            } else {
                
                moveToLogin()
            }
        } else {
            
            moveToLogin()
        }
        
    }
    
    //MARK: reset Login if already Logged In with support@dynamiclevels.com
    func normalRegistrationLogOut(completion : @escaping ()->()) {
        
        let webService = WebService()
        webService.callWebServices(url: Urls.logOut, methodName: "POST", parameters: "", istoken: true, tokenval: User.token, completion: { (success, jsonResult) in
                
            UserDefaults.standard.set(nil, forKey: "phone")
            UserDefaults.standard.set(nil, forKey: "email")
            UserDefaults.standard.set(nil, forKey: "password")
            UserDefaults.standard.set(nil, forKey: "regThrough")
        })
        
        return completion()
    }
    
  
    
    //MARK: Move To Login View Controller
    func moveToLogin() {
        
       let viewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(viewController, animated: true)
        
        // For Temporary for Apple
        
        //self.performLogin(email: "support@dynamiclevels.com", password: "dynamic123", regThrough: "normal")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        addLoader()
        actInd.startAnimating()
        
    }
    //MARK: Performing login Action
    func performLogin(email : String, password: String, regThrough: String) {
        
        let appManager = AppManager()
        let webService = WebService()
        
        let parameters = "user_email=\(email)&password=\(password)&logthrough=\(regThrough)"
        
        if appManager.isValidEmail(email: email) {
            
            webService.callWebServices(url: Urls.login_without_emailverification, methodName: "POST", parameters: parameters, istoken: false, tokenval: "", completion: { (success, dictData) in
                
                // print("Login Return Result: \(dictData)  \n   Wait here...")
                if dictData.value(forKey: "error") as! NSInteger == 216 {
                    
                    let errDict = dictData.value(forKey: "errmsg") as! NSDictionary
                    if errDict.value(forKey: "phone") as! Int == 0 {
                        User.phone = errDict.value(forKey: "phoneno") as! String
                        User.countryCode = errDict.value(forKey: "country_code") as! String
                        
                        User.regThrough = regThrough
                        User.password = password
                        
                        CountryCode.countryCode = errDict.value(forKey: "country_code") as! String
                        self.actInd.stopAnimating()
                        DispatchQueue.main.async(execute: {                          
                            
                            let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                            viewController.checkotptype = "phone"
                            self.navigationController?.pushViewController(viewController, animated: true)
                            
                        })
                        
                    } else if errDict.value(forKey: "email") as! Int == 0 {
                        
                        self.actInd.stopAnimating()
                        DispatchQueue.main.async(execute: {
                            self.verifyEmail(email: User.email, phone: "")
                        })
                    }
                    
                } else if dictData.value(forKey: "error") as! NSInteger == 0 {
                    self.actInd.stopAnimating()
                    
                    
                    // print(dictData)
                    let responce = dictData.value(forKey: "response") as! NSDictionary
                    User.token = responce.value(forKey: "token") as! String
                    self.getUser()
                    
                    
                    //                    if responce.value(forKey: "onboarding") as! Int == 1 {
                    //                        DispatchQueue.main.async(execute: {
                    //
                    //                            User.token = responce.value(forKey: "token") as! String
                    //
                    //                            self.getUser()
                    //                        })
                    //                    } else {
                    //
                    //                        DispatchQueue.main.async(execute: {
                    //
                    //                            let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "OnboardingViewController") as! OnboardingViewController
                    //                            self.navigationController?.pushViewController(viewController, animated: true)
                    //                        })
                    //                    }
                    
                } else {
                    let appManager = AppManager()
                    
                    
                        self.actInd.stopAnimating()
                        self.moveToLogin()
                   
                    
//                    appManager.showAlert(title: "Error", message: dictData.value(forKey: "errmsg") as! String, navigationController: self.navigationController!)
                }
            })
            
        } else{
            
            self.actInd.stopAnimating()
            
            appManager.showAlert(title: "Error", message: "Email Id Not Valid! \n ", navigationController: self.navigationController!)
        }
    }
    
    func getSeminarAd(){
        
        let webServices = WebService()
        webServices.callWebServices(url: Urls.seminar_ad, methodName: "GET", parameters: "", istoken: true, tokenval: User.token) { (success, jsonDict) in
            
            if jsonDict.value(forKey: "errmsg") as! String == "" {
                
                if  let response = jsonDict.value(forKey: "response") as? [NSDictionary] {
                    if let url = response[0].value(forKey: "URL") as? String {
                        seminar_ad.url = url
                        seminar_ad.templete = response[0].value(forKey: "TEMPLATE") as! String
						
						seminar_ad.ab_button_action = response[0].value(forKey: "ab_button_action") as! String
						seminar_ad.ab_button_txt = response[0].value(forKey: "ab_button_txt") as! String
                        
                        
                            if seminar_ad.templete != ""
                            {
                                DispatchQueue.main.async {
                                    let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "WebinarAdsViewController") as! WebinarAdsViewController
                                    self.navigationController?.pushViewController(viewController, animated: true)
                                }
                            }
                            else{
                                DispatchQueue.main.async {
                                    let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "DlHomeViewController") as! DlHomeViewController
                                    self.navigationController?.pushViewController(viewController, animated: true)
                                    
                                }
                            }
                    }
                }
            } else {
                DispatchQueue.main.async {
                    let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "DlHomeViewController") as! DlHomeViewController
                    self.navigationController?.pushViewController(viewController, animated: true)
                }
            }
        }
    }
    
    
    
    
    //MARK: Multibagger State Load
    func multibaggerstatload(completion : @escaping ()->())
    {
        let obj = WebService()
        let paremeters = ""
        
        self.actInd.startAnimating()
        
        obj.callWebServices(url: Urls.multibaggerStatus, methodName: "GET", parameters: paremeters, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                 print(jsonData)
                
                let tempArray = jsonData.value(forKey: "response") as! [NSDictionary]
                MultibaggerStats.segResultQtrArr.removeAll()
                
                MultibaggerStats.segQtrDateArr.removeAll()
                MultibaggerStats.segYearDateArr.removeAll()
                
                MultibaggerStats.segQtrDisplayArr.removeAll()
                MultibaggerStats.segYearDisplayArr.removeAll()
                
                MultibaggerStats.qutrniftyindex.removeAll()
                MultibaggerStats.qutrsmallcapindex.removeAll()
                MultibaggerStats.yearniftyindex.removeAll()
                MultibaggerStats.yearsmallcapindex.removeAll()
                MultibaggerStats.qutrmidcapindex.removeAll()
                
                
                MultibaggerStats.newqutrsmallcapindex.removeAll()
                MultibaggerStats.newqutrmidcapindex.removeAll()
                MultibaggerStats.newqutrniftyindex.removeAll()
                
                MultibaggerStats.SmallDisplay.removeAll()
                MultibaggerStats.MidDisplay.removeAll()
                MultibaggerStats.LargeDisplay.removeAll()
                
                MultibaggerStats.NseSmallDisplay.removeAll()
                MultibaggerStats.NseMidDisplay.removeAll()
                MultibaggerStats.NseLargeDisplay.removeAll()
                
                
                for arr in tempArray
                {
                    //segQtrDateArr
                    if(arr.value(forKey: "mdt_Rep_Type") as! String == "QUARTER")
                    {
                        MultibaggerStats.segResultQtrArr.append(arr.value(forKey: "mdt_Result_Qtr") as! String)
                        MultibaggerStats.segQtrDateArr.append(arr.value(forKey: "mdt_Date") as! String)
                        MultibaggerStats.segQtrDisplayArr.append(arr.value(forKey: "mdt_Display") as! String)
                        
                        if let mrdSmallCapPer = arr.value(forKey: "mrd_SmallCap_Per") as? String {
                            MultibaggerStats.qutrsmallcapindex.append(mrdSmallCapPer)
                        }
                        if let mrdMidCapPer = arr.value(forKey: "mrd_MidCap_Per") as? String {
                            MultibaggerStats.qutrmidcapindex.append(mrdMidCapPer)
                        }
                        if let mrdNiftyPer = arr.value(forKey: "mrd_Nifty_Per") as? String  {
                            MultibaggerStats.qutrniftyindex.append(mrdNiftyPer)
                        }
                        if let mdtPerfDt = arr.value(forKey: "mdt_Perf_Dt") as? String  {
                            MultibaggerStats.qutrperformancetxt.append(mdtPerfDt)
                        }
                        
                        if let SmallDisplay = arr.value(forKey: "SmallDisplay") as? String {
                            MultibaggerStats.SmallDisplay.append(SmallDisplay)
                        }
                        if let MidDisplay = arr.value(forKey: "MidDisplay") as? String {
                            MultibaggerStats.MidDisplay.append(MidDisplay)
                        }
                        if let LargeDisplay = arr.value(forKey: "LargeDisplay") as? String  {
                            MultibaggerStats.LargeDisplay.append(LargeDisplay)
                        }
                        
                        if let NseSmallDisplay = arr.value(forKey: "NseSmallDisplay") as? String {
                            MultibaggerStats.NseSmallDisplay.append(NseSmallDisplay)
                        }
                        if let NseMidDisplay = arr.value(forKey: "NseMidDisplay") as? String {
                            MultibaggerStats.NseMidDisplay.append(NseMidDisplay)
                        }
                        if let NseLargeDisplay = arr.value(forKey: "NseLargeDisplay") as? String  {
                            MultibaggerStats.NseLargeDisplay.append(NseLargeDisplay)
                        }
                        
                        
                        
                        if let mrdSmpPer = arr.value(forKey: "SmpPer") as? String {
                            MultibaggerStats.newqutrsmallcapindex.append(mrdSmpPer)
                        }
                        if let mrdMidPer = arr.value(forKey: "MidPer") as? String {
                            MultibaggerStats.newqutrmidcapindex.append(mrdMidPer)
                        }
                        if let mrdLarPer = arr.value(forKey: "LarPer") as? String  {
                            MultibaggerStats.newqutrniftyindex.append(mrdLarPer)
                        }
                        
                        
                    }
                    
                    if(arr.value(forKey: "mdt_Rep_Type") as! String == "YEAR")
                    {
                        MultibaggerStats.segYearDateArr.append(arr.value(forKey: "mdt_Date") as! String)
                        MultibaggerStats.segYearDisplayArr.append(arr.value(forKey: "mdt_Display") as! String)
                        
                        if let mrdSmallCapPer = arr.value(forKey: "mrd_SmallCap_Per") as? String {
                            MultibaggerStats.yearsmallcapindex.append(mrdSmallCapPer)
                        }
                        if let mrdNiftyPer = arr.value(forKey: "mrd_Nifty_Per") as? String {
                            MultibaggerStats.yearniftyindex.append(mrdNiftyPer)
                        }
                        MultibaggerStats.yearperformancetxt.append(arr.value(forKey: "mdt_Perf_Dt") as! String )
                    }
                }
                
                
                let segQtrDateArr = UserDefaults.standard
                segQtrDateArr.set(MultibaggerStats.segQtrDateArr, forKey: "segQtrDateArr")
                
                self.actInd.stopAnimating()
                return completion()
                /*
                DispatchQueue.main.async {
                 
                    self.nrifpipmsload()
                    
                }*/
            }
        }
    }
    
    func nrifpipmsload()
    {
        let obj = WebService()
        let data = "ALL"
        let param = "ptype=\(data)"
        
        self.actInd.startAnimating()
        
        obj.callWebServices(url: Urls.NRIFPIPMS, methodName: "GET", parameters: param, istoken: false, tokenval: "") { (returnValue, jsonData) in
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                
                // print(jsonData)
                
                let tempArray = jsonData.value(forKey: "response") as! [NSDictionary]
                
                
                for arr in tempArray
                {
                    
                    if(arr.value(forKey: "p_type") as! String == "PMS")
                    {
                        PMS_DATA.PMS1 = arr.value(forKey: "p_why") as! String
                        PMS_DATA.PMS2 = arr.value(forKey: "p_what") as! String
                        PMS_DATA.PMS3 = arr.value(forKey: "p_how") as! String
                    }
                    
                    if(arr.value(forKey: "p_type") as! String == "NRI")
                    {
                        NRI_DATA.NRI1 = arr.value(forKey: "p_what") as! String
                        NRI_DATA.NRI2 = arr.value(forKey: "p_why") as! String
                        NRI_DATA.NRI3 = arr.value(forKey: "p_how") as! String
                    }
                    
                    if(arr.value(forKey: "p_type") as! String == "FPI")
                    {
                        FPI_DATA.FPI1 = arr.value(forKey: "p_what") as! String
                        FPI_DATA.FPI2 = arr.value(forKey: "p_why") as! String
                        FPI_DATA.FPI3 = arr.value(forKey: "p_how") as! String
                    }
                    
                    if(arr.value(forKey: "p_type") as! String == "FPI NEW")
                    {
                        FPI_DATA.FPI_NEW = arr.value(forKey: "p_what") as! String
                    }
                    
                    if(arr.value(forKey: "p_type") as! String == "NRI NEW")
                    {
                        NRI_DATA.NRI_NEW = arr.value(forKey: "p_what") as! String
                    }
                }
                
                DispatchQueue.main.async {
                    self.actInd.stopAnimating()
                    
                    var pageIdentifier    =   ""
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    
                    if(NotificationData.category == ""){
                        self.getSeminarAd()
                    }
                    else {
                        if(NotificationData.category == "customWebView")
                        {
                            pageIdentifier  =   "CustomPushViewController"
                            
                            let viewcontroller = storyBoard.instantiateViewController(withIdentifier: pageIdentifier) as! CustomPushViewController
                            self.navigationController?.pushViewController(viewcontroller, animated: true)
                            
                        }
                        
                        if(NotificationData.category == "Stock_Market_Today")
                        {
                            pageIdentifier  =   "StockMarketTodayViewController"
                            let viewController = self.storyboard!.instantiateViewController(withIdentifier: pageIdentifier) as! StockMarketTodayViewController
                            
                            User.navigation.pushViewController(viewController, animated: false)
                            
                        }
                        
                        if(NotificationData.category == "Value_Stocks")
                        {
                            pageIdentifier  =   "MultibaggerViewController"
                            let viewController = self.storyboard!.instantiateViewController(withIdentifier: pageIdentifier) as! MultibaggerViewController
                            
                            User.navigation.pushViewController(viewController, animated: false)
                            
                        }
                        
                        if(NotificationData.category == "Nifty_Open_Interest")
                        {
                            pageIdentifier  =   "NiftyOpenInterestViewController"
                            let viewController = self.storyboard!.instantiateViewController(withIdentifier: pageIdentifier) as! NiftyOpenInterestViewController
                            
                            User.navigation.pushViewController(viewController, animated: false)
                            
                        }
                        if(NotificationData.category == "Dynamic_Indices")
                        {
                            pageIdentifier  =   "DynamicIndexController"
                            let viewController = self.storyboard!.instantiateViewController(withIdentifier: pageIdentifier) as! DynamicIndexController
                            
                            User.navigation.pushViewController(viewController, animated: false)
                            
                        }
                        
                        if(NotificationData.category == "PMS")
                        {
                            var viewController = UIViewController()
                            pageIdentifier  =   "PMSPDFViewerViewController"
                            if #available(iOS 11.0, *) {
                                viewController = self.storyboard!.instantiateViewController(withIdentifier: pageIdentifier) as! PMSPDFViewerViewController
                            } else {
                                // Fallback on earlier versions
                            }
                            
                            User.navigation.pushViewController(viewController, animated: false)
                            
                        }
                        
                        if(NotificationData.category == "Webinars")
                        {
                            var viewController = UIViewController()
                            pageIdentifier  =   "WebinarsViewController"
                            if #available(iOS 11.0, *) {
                                viewController = self.storyboard!.instantiateViewController(withIdentifier: pageIdentifier) as! WebinarsViewController
                            } else {
                                // Fallback on earlier versions
                            }
                            
                            User.navigation.pushViewController(viewController, animated: false)
                            
                        }
                        
                        if(NotificationData.category == "Sector_Performance")
                        {
                            var viewController = UIViewController()
                            pageIdentifier  =   "MultibaggerQuarterlySectorViewController"
                            if #available(iOS 11.0, *) {
                                viewController = self.storyboard!.instantiateViewController(withIdentifier: pageIdentifier) as! MultibaggerQuarterlySectorViewController
                            } else {
                                // Fallback on earlier versions
                            }
                            
                            User.navigation.pushViewController(viewController, animated: false)
                            
                        }
                        
                        
                        
                        
                        
                        
                    }
                    
                }
            }
        }
    }
    
    func afterLoginDetails(UserId: String) {
        
        let webServices = WebService()
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        let deviceID = UIDevice.current.identifierForVendor?.uuidString as! String
        
        let param = "user_id=\(UserId)&app=IOS&app_ver=\(version)&device_id=\(deviceID)"
        
        webServices.callWebServices(url: Urls.processLoginDetails, methodName: "POST", parameters: param, istoken: true, tokenval: User.token) { (success, jsonDict) in
            
            
            if jsonDict.value(forKey: "errmsg") as! String == "" {
                
            } else{
                let appManager = AppManager()
                appManager.showAlert(title: "Error!", message: jsonDict.value(forKey: "errmsg") as! String, navigationController: self.navigationController!)
            }
        }
    }
    
    
    func getUser() {
        
        let webServices = WebService()
        webServices.callWebServices(url: Urls.getUser, methodName: "GET", parameters: "", istoken: true, tokenval: User.token) { (success, jsonDict) in
            
            
            if jsonDict.value(forKey: "errmsg") as! String == "" {
                
                let response = jsonDict.value(forKey: "response") as! [NSDictionary]
                let userId      = response[0].value(forKey: "userId") as! String
                
                //let userId      = response[0].value(forKey: "userId") as! String fund_account_id referred_by
                User.userId     = userId
                User.socialId   = userId
                
                User.firstName  = response[0].value(forKey: "firstName") as! String
                User.lastName   = response[0].value(forKey: "lastName") as! String
                User.email      = response[0].value(forKey: "emailID") as! String
                User.phone      = response[0].value(forKey: "phone") as! String
                
                
                if let referer_code: String = response[0].value(forKey: "referer_code") as? String {
                    User.referer_code = referer_code
                }
                if let fundaccount: String = response[0].value(forKey: "fund_account_id") as? String {
                    User.fundaccount = fundaccount
                }
                if let refererredby: String = response[0].value(forKey: "referred_by") as? String {
                    User.refererredby = refererredby
                }
                
                if let mfKyc: String = response[0].value(forKey: "MF_kyc") as? String {
                    User.MF_kyc = mfKyc
                }
                
                if let mfUccStatus: String = response[0].value(forKey: "MF_ucc_status") as? String {
                    User.MF_ucc_status = mfUccStatus
                }
                
                if let MF_UserId: String = response[0].value(forKey: "MF_UserId") as? String {
                    User.MF_UserId = MF_UserId
                }
                
                if let ucc: String = response[0].value(forKey: "ucc") as? String {
                    User.ucc = ucc
                }
                if let country_ph_code: String = response[0].value(forKey: "country_ph_code") as? String {
                    User.countryCode        = country_ph_code
                    CountryCode.countryCode = country_ph_code
                }
                
                
                User.navigation = self.navigationController!
                
                self.afterLoginDetails(UserId: userId)
                
                self.multibaggerstatload(completion: {
                    self.firebaseTokenAddUpdate()
                    self.nrifpipmsload()
                    self.getContent(completion: { (result) in
                       
                    })
                    
                    
                })
                
                
            } else{
                let appManager = AppManager()
                appManager.showAlert(title: "Error!", message: jsonDict.value(forKey: "errmsg") as! String, navigationController: self.navigationController!)
            }
        }
    }
    
    func firebaseTokenAddUpdate(){
        
        let webServices = WebService()
        var param = ""
        if let token = InstanceID.instanceID().token() {
            param  = "platform=iOS&userid=\(User.email)&token_no=\(token)"
            
            webServices.callWebServices(url: Urls.addUpdateFCMToken, methodName: "POST", parameters: param, istoken: true, tokenval: User.token) { (success, jsonDict) in
                
                print(jsonDict)
                if jsonDict.value(forKey: "errmsg") as! String == "" {
                    
                    let jsonValue = jsonDict
                }
            }
        }
    }
    
    func getContent(completion : @escaping ([Bool])->()){
        let param = "id=0&status=Y"
        let obj = WebService()
        obj.callWebServices(url: Urls.get_refer_content, methodName: "GET", parameters: param, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
            
            print(jsonData)
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                let tempArray = jsonData.value(forKey: "response") as! [NSDictionary]
                
                for arr in tempArray{
                    
                    if let keyValue = arr.value(forKey: "key") as? String {
                        
                        if keyValue == "sharing_screen" {

                            if let content = arr.value(forKey: "value") as? String {
                                Referral_Data.sharing_screen_content = content
                            }

                        }
                        
                        if keyValue == "whatsapp_telegram_msg" {
                            
                            if let content = arr.value(forKey: "value") as? String {
                                Referral_Data.whatsapp_telegram_msg = content
                            }
                        }
                        
                        if keyValue == "tweet_sms" {
                            
                            if let content = arr.value(forKey: "value") as? String {
                                Referral_Data.tweet_sms = content
                            }
                        }
                        
                        if keyValue == "facebook_feed_title" {
                            
                            if let content = arr.value(forKey: "value") as? String {
                                Referral_Data.facebook_feed_title = content
                            }
                        }
                        
                        if keyValue == "how_referral_works" {
                            
                            if let content = arr.value(forKey: "value") as? String {
                                Referral_Data.how_referral_works = content
                            }
                        }
                        
                        if keyValue == "how_scratch_card_works" {
                            
                            if let content = arr.value(forKey: "value") as? String {
                                Referral_Data.how_scratch_card_works = content
                            }
                        }
                        
                        if keyValue == "email_subject" {
                            
                            if let content = arr.value(forKey: "value") as? String {
                                Referral_Data.email_subject = content
                            }
                        }
                        
                        if keyValue == "email_body" {
                            
                            if let content = arr.value(forKey: "value") as? String {
                                Referral_Data.email_body = content
                            }
                        }
                        
                        
                        
                        
                    }
                }
                
            }
        }
    }
    
    
    
    func verifyEmail(email: String, phone: String) {
        
        let objWebService = WebService()
        var parameter = ""
        if phone == "" {
            parameter = "email=\(email)"
        } else {
            parameter = "email=\(email)"
        }
        
        objWebService.callWebServices(url: Urls.generateEmailOtp, methodName: "POST", parameters: parameter, istoken: false, tokenval: "") { (success, jsonResult) in
            
            //print(jsonResult)
            
            if jsonResult.value(forKey: "response") as!String == "success" {
                DispatchQueue.main.async(execute: {
                    
                    let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                    viewController.checkotptype = "email"
                    self.navigationController?.pushViewController(viewController, animated: true)
                })
                
            } else {
                
                let appManager = AppManager()
                appManager.showAlert(title: "Error", message: jsonResult.value(forKey: "errmsg") as! String, navigationController: self.navigationController!)
            }
            
            
        }
        
    }
    
    
    
    
    
    
    func addLoader() {
        actInd.frame = CGRect(x: UIScreen.main.bounds.width/2-30, y: 400, width: 60.0, height: 60.0) //CGRectMake(0.0, 0.0, 40.0, 40.0);
        //actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.gray
        self.view.addSubview(actInd)
        
    }
    
    
    func delay(_ delay:Double, closure:@escaping ()->()) {
        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
    }
    
    
}
