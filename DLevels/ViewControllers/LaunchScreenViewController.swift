
//
//  LaunchScreenViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-02 on 24/01/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import Foundation
import UIKit

class LaunchScreenViewController: UIViewController {
    
    @IBOutlet weak var imgLaunch: UIImageView!
    
    
    override func viewDidLoad() {
        
        imgLaunch.image = UIImage(named: "DL Splash screen-1.gif")
    }
}
