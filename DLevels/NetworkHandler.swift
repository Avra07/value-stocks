//
//  NetworkHandler.swift
//  DLevels
//
//  Created by SAIKAT GHOSH on 21/08/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit
import Foundation
class NetworkHandler: NSObject {
    
    
    
    typealias Completion = (_ dictData: NSDictionary) -> Void
    
    //MARK: HANDLE REQUESTS
    func handleRequest(url: String, methodName: String, parameters: String, isToken: Bool, showLoader: Bool, completion: @escaping CompletionHandler){
        
        //MARK: CHECKING INTERNET CONNECTION
        if Reachability.isInternetAvailable() {
            let obj = WebService()
            
            obj.callWebServices_new(url: url, methodName: methodName, parameters: parameters, istoken: isToken, tokenval: User.token, isShowLoader: showLoader  ) { (success, response) in
                DispatchQueue.main.async {
                    //print(response)
                    DispatchQueue.main.async {
                        if showLoader{KVNProgress.dismiss()}
                        completion(true,response)
                    }
                }
            }
        }
            //IF INTERNET NOT FOUND
        else {
            AppManager.showAlertMessage(title: "error", message: "check error")
        }
        
        
    }
    
    
    /*
     //MARK: REQUEST API ALAMOFIRE
     class func requestApiAlamofire(url: String, methode: String, parameterS: String, showLoader: Bool, completion: @escaping Completion) {
     
     DispatchQueue.main.async {
     
     }
     
     }*/
    
    /*
     //REQUEST API CUSTOM CLASS (WEBSERVICE)
     class func requestApi(url: String, methodeType: String, parameters: String, showLoader: Bool, completion: @escaping Completion) {
     
     WebService.callWebServices_new(url: url, methodName: methodeType, parameters: parameters, istoken: showLoader, tokenval: User.token, isShowLoader: showLoader) { (success, response) in
     
     }
     }*/
    
}









