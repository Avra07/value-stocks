//
//  RecomendationPopUpViewController.swift
//  DLevels
//
//  Created by SAIKAT GHOSH on 02/08/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class RecommendationCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnTick: UIButton!
}

// protocol used for sending data back
protocol SelectedRecommendationDelegate: class {
    
    func recommendationList(recommedList: String)

}


struct Character
{
    var name:String
    //  var otherDetails
    var isSelected:Bool! = false
    init(name:String) {
        self.name = name
    }
}


class RecomendationPopUpViewController: UIViewController {
    
    var allCharacters:[Character] = []
    
    var recommArr = ["All", "Buy Value Stock","No Recommendation","Hold","Risky", "Exit"]
    var arrSelectedSector = [String]()
    var lst = ""
    
    weak var recommendDelegate: SelectedRecommendationDelegate?
    
    @IBOutlet weak var resultsTblVw: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        allCharacters = [Character(name: "All"),Character(name: "Buy Value Stock"),Character(name: "No Recommendation"),Character(name: "Hold"),Character(name: "Risky"),Character(name: "Exit")]
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnCross_Action(_ sender: UIButton) {
        dismissCurrentPage()
    }
    
    @IBAction func btnokey_Action(_ sender: UIButton){
        self.dismiss(animated: true) {
            
            self.lst = ""
            for recomm in self.arrSelectedSector {
                
                var recommSelected = ""
                self.lst.append(recomm)
                self.lst.append(",")
                
            }
            
            self.lst = String(self.lst.dropLast(1))
            //self.lst = self.lst.replacingOccurrences(of: "|", with: "'")
            
            self.recommendDelegate?.recommendationList(recommedList: self.lst)
        }
    }
    
    
    //MARK: DISMISS CURRENT PAGE
    private func dismissCurrentPage(){
        self.dismiss(animated: true) {
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension RecomendationPopUpViewController: UITableViewDataSource,UITableViewDelegate {
    
    //MARK: Table View Delegate
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let data = recommArr[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecommendationCell") as! RecommendationCell
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
    
        cell.lblTitle.text = data
        
        if allCharacters[indexPath.row].isSelected
        {
            cell.accessoryType = .checkmark
        }
        else
        {
            cell.accessoryType = .none
        }
        
        cell.selectionStyle = .none
            
        if arrSelectedSector.count > 0 {
            if arrSelectedSector.contains(data) {
               cell.accessoryType = .checkmark
            } else {
                cell.accessoryType = .none
            }
        }else {
           // allCharacters[indexPath.row].isSelected = true
           // arrSelectedSector = recommArr
        }
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.allCharacters.count
    }
    
    
    //MARK: Table View Delegate
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        //getting the current cell from the index path
//        let currentCell = tableView.cellForRow(at: indexPath)! as! RecommendationCell
//
//        //getting the text of that cell
//        let currentItem = currentCell.lblTitle!.text
//
//            if let index = arrSelectedSector.index(of: currentItem!) {
//                arrSelectedSector.remove(at: index)
//                currentCell.btnTick.setImage(UIImage(named: "CheckboxOff"), for: .normal)
//                currentCell.isSelected = true
//            } else {
//                arrSelectedSector.append(currentItem!)
//                currentCell.btnTick.setImage(UIImage(named: "CheckboxOn"), for: .normal)
//                currentCell.isSelected = false
//            }
        
        
        if indexPath.row == 0
        {
            allCharacters[indexPath.row].isSelected = !allCharacters[indexPath.row].isSelected
            
            arrSelectedSector.removeAll()
            
            for index in allCharacters.indices
            {
                allCharacters[index].isSelected = allCharacters[indexPath.row].isSelected
                if allCharacters[index].isSelected {
                    arrSelectedSector.append(allCharacters[index].name)
                }
            }
        }
        else
        {
            allCharacters[indexPath.row].isSelected = !allCharacters[indexPath.row].isSelected
            
            if let index = arrSelectedSector.index(of: allCharacters[indexPath.row].name) {
                arrSelectedSector.remove(at: index)
                
            } else {
                arrSelectedSector.append(allCharacters[indexPath.row].name)
                
            }
            
            if allCharacters.dropFirst().filter({ $0.isSelected }).count == allCharacters.dropFirst().count
            {
                allCharacters[0].isSelected = true
            }
            else
            {
                allCharacters[0].isSelected = false
            }
        }
        tableView.reloadData()
    }
        
}
    
    


