//
//  SponsoredCodeViewController.swift
//  DLevels
//
//  Created by Shailesh Saraf on 09/09/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit



class SponsoredCodeViewController: UIViewController {
    
    
    @IBOutlet weak var text_input: UITextField!
    @IBOutlet weak var Lblcode: UILabel!
    @IBOutlet weak var Lbl_text: UILabel!
    @IBOutlet weak var imgGift: UIImageView!
    @IBOutlet weak var Vw: UIView!
    
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var headerView: UIView!
    
    var responsecode = ""
    var mResult : [NSDictionary] = []
    var toolbar:UIToolbar = UIToolbar()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        AppManager().setStatusBarBackgroundColor()

        hideKeyboardOnTapOutside()
        // Do any additional setup after loading the view.
        
        Vw.layer.shadowColor = UIColor.gray.cgColor
        Vw.layer.shadowOpacity = 0.5
        Vw.layer.shadowRadius = 3
        Vw.layer.shadowOffset = CGSize(width: 0, height: 3)
        Vw.layer.masksToBounds = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardDidHide, object: nil)
        
        //init toolbar
        toolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 65))
        toolbar.barTintColor = #colorLiteral(red: 0.7812563181, green: 0.8036255836, blue: 0.8297665119, alpha: 1)
        
        //create left side empty space so that done button set on right side
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(UITextField.doneButtonAction))
        
       
        
        
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        
        
        toolbar.sizeToFit()
        text_input.inputAccessoryView = toolbar
        
        
    }
    
    // MARK: Keyboard Notifications
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            if view.frame.origin.y == 0 {
                self.view.frame.origin.y -= 60
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    func doneButtonAction() {
        self.view.endEditing(true)
    }
    
    @IBAction func back_bTn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func okey_btn(_ sender: Any) {
        
        
        if text_input.text != "" {
            getData(completion: { (dataset) in
                self.mResult = dataset
                for i in self.mResult{
                    let response = i.value(forKey: "db_response") as! String
                    self.responsecode = response
                }
                
                if self.responsecode == "Failed" {
                    AppManager.showAlertMessage(title: "Error!", message: "Invaild Sponsored Code!")
                }else{
                    // Declare Alert message
                    let dialogMessage = UIAlertController(title: "Congrats!", message: "Sponsored Code activated sucessfully.Please start using our premium features.", preferredStyle: .alert)
                    // Create OK button with action handler
                    let ok = UIAlertAction(title: "Continue", style: .default, handler: { (action) -> Void in
                        print("Ok button tapped")
                        
                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                        let vwcntrl = storyBoard.instantiateViewController(withIdentifier: "DlHomeViewController") as! DlHomeViewController
                        User.navigation.pushViewController(vwcntrl, animated: true)
                        
                    })
                    
                    //Add OK button to dialog message
                    dialogMessage.addAction(ok)
                    
                    // Present dialog message to user
                    self.present(dialogMessage, animated: true, completion: nil)
                }
                
            })
        }else{
             AppManager.showAlertMessage(title: "Error!", message: "Please enter sponsored code!")
        }
    }
    
    func getData (completion : @escaping ([NSDictionary])->()) {
        
        let obj = WebService()
        var vsCode  = ""
        vsCode = text_input.text!
        var result : [NSDictionary] = [NSDictionary]()
        let paremeters = "user_id=\(User.userId)&coupon=\(vsCode)&app=iOS"
        
        //actInd.startAnimating()
        
        obj.callWebServices(url: Urls.redeem_subscription_coupon, methodName: "POST", parameters: paremeters, istoken: false, tokenval: User.token) { (returnValue, jsonData) in
            
            print("Json Data is :\(jsonData)")
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                if jsonData.value(forKey: "response") as? [NSDictionary] != nil{
                    result = jsonData.value(forKey: "response") as! [NSDictionary]
                    
                    return completion(result)
                }
                
            }
            
        }
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SponsoredCodeViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
    }
}
