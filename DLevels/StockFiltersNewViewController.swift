//
//  StockFiltersNewViewController.swift
//  DLevels
//
//  Created by SAIKAT GHOSH on 25/08/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit



class multiSelectSectorNewCell: UITableViewCell {
    
    
    @IBOutlet weak var lblColName: UILabel!
    @IBOutlet weak var lblSortName: UILabel!
    @IBOutlet weak var btnMultiSelect: UIButton!
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
}

class dropdownNewCell: UITableViewCell {
    
    
    @IBOutlet weak var btnSelect: UIButton!
    @IBOutlet weak var lblColName: UILabel!
    @IBOutlet weak var lblSortName: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
    }
    
    
}

class textfldNewCell: UITableViewCell {
    
    
    @IBOutlet weak var txtvalue: UITextField!
    @IBOutlet weak var lblColName: UILabel!
    @IBOutlet weak var lblSortName: UILabel!
    
    
    
}

class filterlistNewCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnoperater: UIButton!
    @IBOutlet weak var txtvalue: UITextField!
    @IBOutlet weak var txtmin: UITextField!
    @IBOutlet weak var txtmax: UITextField!
    @IBOutlet weak var lblColName: UILabel!
    @IBOutlet weak var lblSortName: UILabel!
    @IBOutlet weak var btninfo: UIButton!
    @IBOutlet weak var imgvwinfo: UIImageView!
    
    func inputBoxShowHide(inputOperator: String) {
        if inputOperator == "between" {
            txtmin.isHidden = false
            txtmax.isHidden = false
            txtvalue.isHidden = true
            
        }else{
            txtmin.isHidden = true
            txtmax.isHidden = true
            txtvalue.isHidden = false
        }
    }
}

class StockFiltersNewViewController: UIViewController, SwitchDataManageDelegate, UITextFieldDelegate, DataEnteredDelegate {
    
    
    func userDidEnterInformation(sectorlist: [String], indexPathRow: String, Section: String) {
        
        if sectorlist.count > 0 {
            arrSelectedSectorFilter = sectorlist
            var secListStr = ""
            
            var secListStr4Query = "("
            var sectorName = ""
            for secName in arrSelectedSectorFilter {
                
                sectorName = "'" + secName + "'"
                secListStr.append(sectorName)
                secListStr.append(",")
                secListStr4Query.append(sectorName)
                secListStr4Query.append(",")
                
            }
            
            secListStr.removeLast()
            secListStr4Query.removeLast()
            
            secListStr.append(")")
            secListStr4Query.append(")")
            
            
            
            self.allFilterList[Int(Section)!][Int(indexPathRow)!].setValue(secListStr4Query, forKey: "modified_value")
            
            self.tblvwFilterListCategoryWise.reloadSections(IndexSet(integer: Int(Section)!), with: .none)
        } else {
            arrSelectedSectorFilter = sectorlist
            
            self.allFilterList[Int(Section)!][Int(indexPathRow)!].setValue("All Sectors", forKey: "modified_value")
            
            self.tblvwFilterListCategoryWise.reloadSections(IndexSet(integer: Int(Section)!), with: .none)
        }
        
        
        
        
    }
    
    
    func switchDataInformation(lossMark: String, lessVol: String) {
        
    }
    
    
    @IBOutlet weak var btnresetfilter: UIButton!
    @IBOutlet weak var btnapplyfilter: UIButton!
    @IBOutlet weak var tblvwFilterListCategoryWise  :UITableView!
    @IBOutlet weak var tblvwCategoryList  :UITableView!
    @IBOutlet weak var tblvwSectorlist: UITableView!
    var ary = [String]()
    var arr = [String]()
    var info_value : String = " "
    
    
    
    var stckFilterDict = [String: String]()
    var stckFilterDictArr = [[String: String]]()
    
    var stckFilterModifiedArr = [[[String: String]]]()
    
    var arrfilter = [String]()
    var strQuery  = ""
    
    
    
    
    var sections = [String]()
    var indxpath: IndexPath = IndexPath()
    
    var arrSelectedSectorFilter = [String]()
    
    var arrayOfNames : [String] = [String]()
    var rowBeingEdited : Int? = nil
    
    var stockFilterDict = [[NSDictionary]]()
    var lossMarketing = "Y"
    var lessVolume = "Y"
    
    var currentindex = 0
    
    
    
    // Variable Decleration
    
    var categoryList  = [String]()
    var allFilterList = [[NSMutableDictionary]]()
    var filterListCategoryWise = [NSMutableDictionary]()
    
    var toolbar:UIToolbar = UIToolbar()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let nib = UINib.init(nibName: "SelectSectorCell", bundle: nil)
        self.tblvwFilterListCategoryWise.register(nib, forCellReuseIdentifier: "SelectSectorCell")
        
        
        btnresetfilter.layer.borderColor = UIColor.black.cgColor
        btnresetfilter.layer.borderWidth = 1
        btnresetfilter.layer.cornerRadius = 4
        
        btnapplyfilter.layer.borderWidth = 1
        btnapplyfilter.layer.cornerRadius = 4
        // Do any additional setup after loading the view.
        
        
        getData(completion: { (status) in
            if status == true {
                self.tblvwCategoryList.reloadData()
            } else {
                
            }
        })
        
        
    }
    func removeDuplicates(array: [String]) -> [String] {
        var encountered = Set<String>()
        var result: [String] = []
        for value in array {
            if encountered.contains(value) {
                // Do not add a duplicate element.
            }
            else {
                // Add value to the set.
                encountered.insert(value)
                // ... Append the value.
                result.append(value)
            }
        }
        return result
    }
    
    
    func gotoSectorListincell(_ sender: UIButton){
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SelectSectorViewController") as! SelectSectorViewController
        
        viewController.sectorDelegate = self
        
        
        viewController.arrSelectedSector = arrSelectedSectorFilter
        
        viewController.sender_section = String(sender.tag)
        viewController.sender_indexPathRow = String(sender.accessibilityValue!)
        
        
        User.navigation.pushViewController(viewController, animated: true)
    }
    
    func dropdownloadincell(_ sender: UIButton){
        
        let actionSheeController = UIAlertController(title:"", message: "", preferredStyle: .actionSheet)
        actionSheeController.view.tintColor = UIColor(red: 247/255, green: 196/255, blue: 58/255, alpha: 1)
        
        
        let dataSource: [String] = (sender.accessibilityHint?.components(separatedBy: ";"))!
        
        for data in dataSource {
            actionSheeController.addAction(UIAlertAction(title: data, style: .default, handler: { (success) in
                
                let indexPath = IndexPath(row: Int(sender.accessibilityValue!)!, section: sender.tag)
                
                let cell = self.tblvwFilterListCategoryWise.cellForRow(at: indexPath) as! dropdownNewCell
                
                if cell.btnSelect.accessibilityIdentifier == "\(Int(sender.accessibilityValue!)!) -\(sender.tag)" {
                    
                    cell.btnSelect.setTitle(data, for: .normal)
                    self.allFilterList[sender.tag][Int(sender.accessibilityValue!)!].setValue(data, forKey: "modified_value")
                    
                    
                }
            }))
        }
        
        actionSheeController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (success) in
        }) )
        User.navigation.present(actionSheeController, animated: true) {
            
        }
        
    }
    
    
    func buttonClicked(sender: UIButton){
        
        let s = UIStoryboard.init(name : "Main" , bundle : nil)
        let obj = s.instantiateViewController(withIdentifier:   "webloadController") as! webloadController
        obj.infostring = sender.accessibilityHint!
        self.navigationController?.pushViewController(obj, animated: true)
        
        
    }
    
    @objc func filterOperatorDrpdwn(_ sender: UIButton) {
        
        let actionSheeController = UIAlertController(title:"Filter Operator", message: "", preferredStyle: .actionSheet)
        
        actionSheeController.view.tintColor = UIColor(red: 247/255, green: 196/255, blue: 58/255, alpha: 1)
        
        actionSheeController.addAction(UIAlertAction(title: "greater than", style: .default, handler: { (success) in
            
            sender.setTitle("greater than", for: .normal)
            
            self.allFilterList[sender.tag][Int(sender.accessibilityValue!)!].setValue("greater than", forKey: "operator")
            
            
            
            //            self.tblvwfilterlist.rectForRow(at: indxpath)
            self.tblvwFilterListCategoryWise.reloadData()
            
            
            
        }))
        
        
        
        actionSheeController.addAction(UIAlertAction(title: "less than", style: .default, handler: { (success) in
            sender.setTitle("less than", for: .normal)
            
            self.allFilterList[sender.tag][Int(sender.accessibilityValue!)!].setValue("less than", forKey: "operator")
            
            //            self.tblvwfilterlist.rectForRow(at: indxpath)
            self.tblvwFilterListCategoryWise.reloadData()
            //self.tblvwFilterListCategoryWise.reloadSections(IndexSet(integer: sender.tag), with: .none)
            
            
        }))
        
        actionSheeController.addAction(UIAlertAction(title: "equals", style: .default, handler: { (success) in
            sender.setTitle("equals", for: .normal)
            
            self.allFilterList[sender.tag][Int(sender.accessibilityValue!)!].setValue("equals", forKey: "operator")
            
            //            self.tblvwfilterlist.rectForRow(at: indxpath)
            //self.tblvwFilterListCategoryWise.reloadSections(IndexSet(integer: sender.tag), with: .none)
            self.tblvwFilterListCategoryWise.reloadData()
            
        }))
        
        
        actionSheeController.addAction(UIAlertAction(title: "between", style: .default, handler: { (success) in
            sender.setTitle("between", for: .normal)
            
            let cell = self.tblvwFilterListCategoryWise.dequeueReusableCell(withIdentifier: "filterlistCell") as! filterlistNewCell
            
            cell.inputBoxShowHide(inputOperator: "between")
            
            // self.allFilterList[sender.tag][Int(sender.accessibilityValue!)!].setValue("between", forKey: "operator")
            print(self.allFilterList.count)
            //print()
            
            self.allFilterList[sender.tag][Int(sender.accessibilityValue!)!].setValue("between", forKey: "operator")
            
            print(self.allFilterList[sender.tag][Int(sender.accessibilityValue!)!])
            
            // self.tblvwFilterListCategoryWise.reloadSections(IndexSet(integer: sender.tag), with: .none)
            
            self.tblvwFilterListCategoryWise.reloadData()
            
        }))
        
        
        
        actionSheeController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (success) in
        }) )
        User.navigation.present(actionSheeController, animated: true) {
            
        }
        
        
    }
    
    
    
    
    
    func getData (completion : @escaping (Bool)->()) {
        
        let obj = WebService()
        let paremeters = ""
        
        //actInd.startAnimating()
        
        obj.callWebServices(url: Urls.getfilterlist, methodName: "GET", parameters: paremeters, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
            
            print("Json Data is :  \(jsonData)")
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                let tempArray = jsonData.value(forKey: "response") as! [NSDictionary]
                
                for dictt in tempArray{
                    self.info_value = dictt.value(forKey: "filter_info") as? String ?? ""
                }
                for dict in tempArray {
                    
                    if let filter_section = dict.value(forKey: "filter_section") as? String{
                        
                        if filter_section == "" {
                            if let filter_name = dict.value(forKey: "filter_name") as? String{
                                self.categoryList.append(filter_name)
                            }
                            
                        }
                        else {
                            self.categoryList.append(filter_section)
                        }
                    }
                }
                
                self.categoryList = self.removeDuplicates(array: self.categoryList)
                self.allFilterList.removeAll()
                var rowcnt = 0
                for category in self.categoryList {
                    
                    
                    
                    for dict in tempArray {
                        
                        if let filter_section = dict.value(forKey: "filter_section") as? String{
                            if filter_section == "" {
                                
                                if let filter_name = dict.value(forKey: "filter_name") as? String{
                                    
                                    if filter_name == category {
                                        
                                        var new_dic = NSMutableDictionary()
                                        new_dic = dict as! NSMutableDictionary
                                        
                                        if let mtype = dict.value(forKey: "filter_type") as? String {
                                            if mtype == "" {
                                                if let doperator = dict.value(forKey: "default_operator") as? String {
                                                    new_dic.setObject(doperator, forKey: "operator" as NSCopying)
                                                }
                                            } else {
                                                new_dic.setObject("", forKey: "operator" as NSCopying)
                                            }
                                        }
                                        
                                        new_dic.setObject("", forKey: "modified_value" as NSCopying)
                                        new_dic.setObject("", forKey: "min_value" as NSCopying)
                                        new_dic.setObject("", forKey: "max_value" as NSCopying)
                                        
                                        
                                        // print(new_dic)
                                        
                                        
                                        
                                        self.filterListCategoryWise.append(new_dic)
                                        // print(self.stockFilterWithFileTypeDict)
                                        rowcnt = rowcnt + 1
                                    }
                                }
                                
                            }else {
                                if filter_section == category {
                                    
                                    var new_dic = NSMutableDictionary()
                                    new_dic = dict as! NSMutableDictionary
                                    
                                    if let mtype = dict.value(forKey: "filter_type") as? String {
                                        if mtype == "" {
                                            if let doperator = dict.value(forKey: "default_operator") as? String {
                                                new_dic.setObject(doperator, forKey: "operator" as NSCopying)
                                            }
                                        } else {
                                            new_dic.setObject("", forKey: "operator" as NSCopying)
                                        }
                                    }
                                    
                                    new_dic.setObject("", forKey: "modified_value" as NSCopying)
                                    new_dic.setObject("", forKey: "min_value" as NSCopying)
                                    new_dic.setObject("", forKey: "max_value" as NSCopying)
                                    
                                    
                                    // print(new_dic)
                                    
                                    
                                    
                                    self.filterListCategoryWise.append(new_dic)
                                    // print(self.stockFilterWithFileTypeDict)
                                    rowcnt = rowcnt + 1
                                }
                            }
                            
                            
                            
                        }
                    }
                    self.allFilterList.append(self.filterListCategoryWise)
                    self.filterListCategoryWise.removeAll()
                }
                
                // self.tblvwHeightConstrant.constant = CGFloat((rowcnt + self.sections.count) * 40)
                
                //  print("Filter List: \(self.stockFilterDict)")
                
                print(self.allFilterList)
                return completion(true)
                
            }
        }
    }
    
    
    
    @IBAction func applyfiltersclick(_ sender: Any) {
        
        
        //print(stockFilterDict)
        
        var filter_col_name = ""
        var filter_sort_name = ""
        
        arrfilter.removeAll()
        strQuery = ""
        
        for sec in stockFilterDict
        {
            for dictn in sec
            {
                if let ftype = dictn.value(forKey: "filter_type") as? String {
                    
                    if let filtersortname: String = dictn.value(forKey: "filter_short_name") as? String {
                        if let filtercol:String   = dictn.value(forKey: "column_name") as? String {
                            
                            filter_sort_name = filtersortname
                            filter_col_name = filtercol
                        }
                    }
                    
                    
                    
                    if ftype == "multi_select_list" {
                        if let mvalue = dictn.value(forKey: "modified_value") as? String {
                            if mvalue != "" {
                                if mvalue != "All Sectors" {
                                    
                                    arrfilter.append("\(filter_sort_name) : \(mvalue)")
                                    strQuery.append("\(filter_col_name)|in|\(mvalue.replacingOccurrences(of: "&", with: "%26"));")
                                }
                            }
                        }
                    }
                    
                    if ftype == "dropdown" {
                        if let selvalue = dictn.value(forKey: "modified_value") as? String {
                            if selvalue != "" {
                                if selvalue != "All" {
                                    
                                    let svalue = "'" + selvalue + "'"
                                    
                                    arrfilter.append("\(filter_sort_name) : \(svalue)")
                                    strQuery.append("\(filter_col_name)|=|\(svalue);")
                                }
                            }
                        }
                    }
                    
                    if ftype == "text" {
                        if let txtvalue = dictn.value(forKey: "modified_value") as? String {
                            if txtvalue != "" {
                                
                                let tvalue = "'%" + txtvalue + "%'"
                                //let tvalue =  txtvalue
                                
                                arrfilter.append("\(filter_sort_name) : \(txtvalue)")
                                strQuery.append("\(filter_col_name)|Like|\(tvalue);")
                            }
                            
                        }
                    }
                    
                    if ftype == "" {
                        if let operator_val = dictn.value(forKey: "operator") as? String {
                            if operator_val != "" {
                                
                                if operator_val == "between" {
                                    if let min_val = dictn.value(forKey: "min_value") as? String {
                                        if let max_val = dictn.value(forKey: "max_value") as? String {
                                            
                                            if min_val != "" {
                                                if max_val != ""{
                                                    arrfilter.append("\(filter_sort_name) between \(min_val) and \(max_val)")
                                                    strQuery.append("\(filter_col_name)|between| \(min_val) and \(max_val);")
                                                }
                                            }
                                        }
                                    }
                                }
                                
                                if operator_val == "greater than" {
                                    if let mod_val = dictn.value(forKey: "modified_value") as? String {
                                        if mod_val != "" {
                                            arrfilter.append("\(filter_sort_name) > \(mod_val)")
                                            strQuery.append("\(filter_col_name)|>|\(mod_val);")
                                        }
                                        
                                    }
                                }
                                if operator_val == "equals" {
                                    if let mod_val = dictn.value(forKey: "modified_value") as? String {
                                        if mod_val != "" {
                                            arrfilter.append("\(filter_sort_name) = \(mod_val)")
                                            strQuery.append("\(filter_col_name)|=|\(mod_val);")
                                        }
                                        
                                    }
                                }
                                
                                if operator_val == "less than" {
                                    if let mod_val = dictn.value(forKey: "modified_value") as? String {
                                        if mod_val != "" {
                                            arrfilter.append("\(filter_sort_name) < \(mod_val)")
                                            strQuery.append("\(filter_col_name)|<|\(mod_val);")
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                
            }
            
            
        }
        
        print(strQuery)
        print(arrfilter)
        
    }
    
    
    
    
    
    
    
    
    
    
    
}
extension StockFiltersNewViewController : UITableViewDelegate , UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if  tableView == tblvwCategoryList{
            return self.categoryList.count
        }
        else{
            
            return self.filterListCategoryWise.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblvwCategoryList{
            
            let obj = tblvwCategoryList.dequeueReusableCell(withIdentifier: "rCell") as! rCell
            let str = self.categoryList[indexPath.row]
            obj.rtvalue.text! = str
            if (indexPath.row == 0 ) {
                obj.backgroundColor = UIColor.white
                obj.rtvalue.backgroundColor = UIColor.white
            } else {
                obj.backgroundColor = UIColor.lightGray
                obj.rtvalue.backgroundColor = UIColor.lightGray
            }
            
            return obj
        }
        else{
            
            // let cell = tblvwFilterListCategoryWise.dequeueReusableCell(withIdentifier: "lCell") as! lCell
            let data = self.filterListCategoryWise[indexPath.row] as NSMutableDictionary
            
            print(data)
            
            if let filtertype = data.value(forKey: "filter_type") as? String {
                
                if filtertype == "multi_select_list" {
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "SelectSectorCell") as! SelectSectorCell
                    cell.awakeFromNib()
                    
                    
                    
                    
                    
                    /*
                     cell.accessibilityHint = "multi_select_list"
                     
                     
                     
                     
                     if let filter_short_name = data.value(forKey: "filter_short_name") as? String {
                     cell.lblSortName.text = filter_short_name
                     }
                     
                     if let column_name = data.value(forKey: "column_name") as? String {
                     cell.lblColName.text = column_name
                     }
                     
                     if arrSelectedSectorFilter.count > 0 {
                     var sectorlist = ""
                     for sector in arrSelectedSectorFilter {
                     if sectorlist != "" {
                     sectorlist.append(sector)
                     sectorlist.append(",")
                     }else {
                     sectorlist.append(sector)
                     sectorlist.append(",")
                     }
                     }
                     
                     cell.btnMultiSelect.setTitle(sectorlist, for: .normal)
                     }else {
                     cell.btnMultiSelect.setTitle("All Sectors", for: .normal)
                     }
                     
                     cell.accessibilityHint = "multiSelectSectorCell"
                     
                     
                     cell.btnMultiSelect.accessibilityIdentifier = "\(indexPath.row) -\(indexPath.section)"
                     cell.btnMultiSelect.accessibilityValue = String(indexPath.row)
                     cell.btnMultiSelect.tag = indexPath.section
                     
                     cell.btnMultiSelect.addTarget(self, action: #selector(self.gotoSectorListincell(_:)), for: .touchUpInside)
                     */
                    return cell
                    
                }
                
                if filtertype == "dropdown" {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "dropdownCell") as! dropdownNewCell
                    
                    cell.accessibilityHint = "dropdown"
                    
                    
                    
                    if let filter_short_name = data.value(forKey: "filter_short_name") as? String {
                        cell.lblSortName.text = filter_short_name
                    }
                    
                    if let column_name = data.value(forKey: "column_name") as? String {
                        cell.lblColName.text = column_name
                    }
                    
                    if let filterOption = data.value(forKey: "filter_options") as? String {
                        cell.btnSelect.accessibilityHint = filterOption
                    }
                    if let mod_val = data.value(forKey: "modified_value") as? String {
                        if mod_val == "" {
                            cell.btnSelect.setTitle("All", for: .normal)
                        }else {
                            cell.btnSelect.setTitle(mod_val, for: .normal)
                        }
                        
                    }
                    
                    cell.btnSelect.accessibilityIdentifier = "\(indexPath.row) -\(indexPath.section)"
                    cell.btnSelect.accessibilityValue = String(indexPath.row)
                    cell.btnSelect.tag = indexPath.section
                    
                    
                    
                    
                    cell.btnSelect.addTarget(self, action: #selector(self.dropdownloadincell(_:)), for: .touchUpInside)
                    
                    
                    return cell
                    
                }
                
                if filtertype == "text" {
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "textfldNewCell") as! textfldNewCell
                    
                    cell.accessibilityHint = "text"
                    
                    
                    
                    if let filter_short_name = data.value(forKey: "filter_short_name") as? String {
                        cell.lblSortName.text = filter_short_name
                    }
                    
                    if let column_name = data.value(forKey: "column_name") as? String {
                        cell.lblColName.text = column_name
                    }
                    
                    
                    if let modified_value = data.value(forKey: "modified_value") as? String {
                        cell.txtvalue.text = modified_value
                    }
                    
                    
                    
                    
                    
                    
                    //cell.txtvalue.text = ""
                    
                    cell.txtvalue.accessibilityHint = String(indexPath.section)
                    cell.txtvalue.tag = indexPath.row
                    cell.txtvalue.delegate = self
                    
                    cell.txtvalue.inputAccessoryView = toolbar
                    
                    cell.txtvalue.layer.borderColor = UIColor.lightGray.cgColor
                    cell.txtvalue.layer.cornerRadius = 8
                    cell.txtvalue.layer.borderWidth = 1
                    
                    return cell
                    
                }
                
                if filtertype == "" {
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "filterlistCell" , for: indexPath) as! filterlistNewCell
                    
                    cell.accessibilityHint = "filterList"
                    let informationbtn = cell.viewWithTag(202) as! UIButton
                    
                    if let title = data.value(forKey: "filter_name") as? String {
                        print(title)
                        cell.lblTitle.text = title
                    }
                    
                    if let default_operator = data.value(forKey: "default_operator") as? String {
                        
                        cell.btnoperater.setTitle(default_operator, for: .normal)
                    }
                    
                    if let filter_short_name = data.value(forKey: "filter_short_name") as? String {
                        cell.lblSortName.text = filter_short_name
                    }
                    
                    if let column_name = data.value(forKey: "column_name") as? String {
                        cell.lblColName.text = column_name
                    }
                    
                    if let modifiedvalue = data.value(forKey: "modified_value") as? String {
                        cell.txtvalue.text = modifiedvalue
                    }
                    
                    
                    if let max_value = data.value(forKey: "max_value") as? String {
                        cell.txtmax.text = max_value
                    }
                    
                    if let min_value = data.value(forKey: "min_value") as? String {
                        cell.txtmin.text = min_value
                    }
                    
                    if let filter_info = data.value(forKey: "filter_info") as? String {
                        if filter_info == ""{
                            cell.imgvwinfo.isHidden = true
                            cell.btninfo.isHidden = true
                        } else {
                            cell.imgvwinfo.isHidden = false
                            cell.btninfo.isHidden = false
                            cell.btninfo.accessibilityHint = filter_info
                            cell.btninfo.addTarget(self, action: #selector(buttonClicked), for:
                                UIControlEvents.touchUpInside)
                        }
                    } else {
                        cell.imgvwinfo.isHidden = true
                        cell.btninfo.isHidden = true
                    }
                    
                    cell.txtvalue.inputAccessoryView = toolbar
                    cell.txtmax.inputAccessoryView = toolbar
                    cell.txtmin.inputAccessoryView = toolbar
                    
                    cell.txtvalue.layer.borderColor = UIColor.lightGray.cgColor
                    cell.txtvalue.layer.cornerRadius = 8
                    cell.txtvalue.layer.borderWidth = 1
                    
                    cell.txtmax.layer.borderColor = UIColor.lightGray.cgColor
                    cell.txtmax.layer.cornerRadius = 8
                    cell.txtmax.layer.borderWidth = 1
                    
                    cell.txtmin.layer.borderColor = UIColor.lightGray.cgColor
                    cell.txtmin.layer.cornerRadius = 8
                    cell.txtmin.layer.borderWidth = 1
                    
                    
                    
                    
                    
                    indxpath = indexPath
                    
                    cell.btnoperater.accessibilityValue = String(indexPath.row)
                    cell.btnoperater.tag = currentindex
                    
                    
                    if let operator_v = data.value(forKey: "operator") as? String {
                        
                        if operator_v == "" {
                            if let default_operator = data.value(forKey: "default_operator") as? String {
                                cell.btnoperater.setTitle(default_operator, for: .normal)
                            }
                        }else {
                            cell.btnoperater.setTitle(operator_v, for: .normal)
                            
                            
                            if operator_v == "greater than" {
                                cell.txtvalue.isHidden = false
                                cell.txtmin.isHidden = true
                                cell.txtmax.isHidden = true
                                
                                
                                
                            }
                            
                            if operator_v == "less than" {
                                cell.txtvalue.isHidden = false
                                cell.txtmin.isHidden = true
                                cell.txtmax.isHidden = true
                                
                                
                            }
                            if operator_v == "equals" {
                                cell.txtvalue.isHidden = false
                                cell.txtmin.isHidden = true
                                cell.txtmax.isHidden = true
                                
                                
                            }
                            
                            
                            if operator_v == "between" {
                                cell.txtvalue.isHidden = true
                                cell.txtmin.isHidden = false
                                cell.txtmax.isHidden = false
                                
                                
                            }
                            
                            
                            
                        }
                        
                    }
                    
                    
                    
                    cell.btnoperater.addTarget(self, action: #selector(self.filterOperatorDrpdwn(_:)), for: .touchUpInside)
                    
                    
                    
                    
                    cell.txtvalue.accessibilityHint = String(indexPath.section)
                    cell.txtvalue.tag = indexPath.row
                    cell.txtvalue.delegate = self
                    cell.txtvalue.accessibilityValue = "value"
                    
                    cell.txtmax.accessibilityHint = String(indexPath.section)
                    cell.txtmax.tag = indexPath.row
                    cell.txtmax.delegate = self
                    cell.txtmax.accessibilityValue = "max"
                    
                    cell.txtmin.accessibilityHint = String(indexPath.section)
                    cell.txtmin.tag = indexPath.row
                    cell.txtmin.delegate = self
                    cell.txtmin.accessibilityValue = "min"
                    
                    
                    return cell
                }
                
                
            }
            
            
            
            
            
            
            return UITableViewCell()
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tblvwCategoryList{
            return 60
        }
        else{
            
            let data = self.filterListCategoryWise[indexPath.row] as NSMutableDictionary
            if let filtertype = data.value(forKey: "filter_type") as? String {
                
                if filtertype == "multi_select_list" {
                    return tableView.frame.height
                } else {
                    return 120
                }
            }
        }
        
        return 120
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == tblvwCategoryList{
            currentindex = indexPath.row
            self.filterListCategoryWise = self.allFilterList[currentindex]
            
            tblvwFilterListCategoryWise.reloadData()
            if tableView == tblvwFilterListCategoryWise{
                let obj = tblvwCategoryList.dequeueReusableCell(withIdentifier: "rCell") as! rCell
                
                
                
            }
            // cell = lighttable.dequeueReusableCell(withIdentifier: "lCell") as! lCell
            
        }
        
        
        
    }
    
    
}

