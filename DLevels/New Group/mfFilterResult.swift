//
//  mfFilterResult.swift
//  DLevels
//
//  Created by Shailesh Saraf on 05/09/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import Foundation
class mfFilterResult:NSObject {
    
    var filter_value = ""
    var filter_min = ""
    var filter_max = ""
    var filter_operator = ""
    var filter_short_name = ""
    var filter_type = ""
    var filter_column_name = ""
    var filter_group_id = ""
    
    override init(){
        
    }
    
    
    init(filterGroupId:String,filterValue:String,filterMin:String,filterMax:String, filterOperator:String,filterShortName:String,filterColumnName:String, filterType:String) {
        
        filter_value = filterValue
        filter_min = filterMin
        filter_max = filterMax
        
        filter_operator = filterOperator
        filter_short_name = filterShortName
        filter_column_name = filterColumnName
        filter_type = filterType
        filter_group_id = filterGroupId
    }
    
}
