//
//  mffilterGroup.swift
//  DLevels
//
//  Created by Shailesh Saraf on 05/09/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import Foundation
class mffilterGroup:NSObject {
    var group_id = ""
    var filter_section = ""
    var filter_count = 0
    
    
    override init(){
        
    }
    
    
    init(groupId:String,filterSection: String,filterCount:Int ) {
        group_id = groupId
        filter_section = filterSection
        filter_count = filterCount
    }
    
    
}
