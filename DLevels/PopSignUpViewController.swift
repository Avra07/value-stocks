//
//  PopSignUpViewController.swift
//  DLevels
//
//  Created by Shailesh Saraf on 11/09/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class PopSignUpViewController: UIViewController {

    @IBOutlet weak var btn_continue: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        btn_continue.addCornerRadius(value: 4.0)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btn_Continue(_ sender: Any) {
        dismissCurrentPage()
        let otpController  =   self.storyboard?.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
        otpController.checkotptype = "phone"
        User.navigation.pushViewController(otpController, animated: true)
    }
    
    private func dismissCurrentPage(){
        dismiss(animated: true, completion: nil)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */

}
