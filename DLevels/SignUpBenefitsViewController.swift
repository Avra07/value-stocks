//
//  SignUpBenefitsViewController.swift
//  DLevels
//
//  Created by MacMini2 on 11/06/18.
//  Copyright © 2018 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class SignUpBenefitsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        AppManager().setStatusBarBackgroundColor()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnSignUp_Action(_ sender: Any) {
        let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "SignUPScreenViewController") as! SignUpViewController
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    @IBAction func btnSignUpLater_Action(_ sender: Any) {
        let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "DlHomeViewController") as! DlHomeViewController
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
