//
//  WebViewChartViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-02 on 03/05/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class WebViewChartViewController: UIViewController {

    @IBOutlet weak var wvinvestingchart: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //let instrument_4 = SearchForCurrentMultibagger.instrument_4
        
        
        // Do any additional setup after loading the view.
        
        // Do any additional setup after loading the view, typically from a nib.
        
        // print(wvinvestingchart.isLoading)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.wvinvestingchart.isHidden = false
        
        let symbolval = SearchForCurrentMultibagger.serchValue.replacingOccurrences(of: " ", with: "%20")
        
        let url = NSURL(string: "https://www.dynamiclevels.com/charting/mobile_black.html?symbol=\(symbolval)&ctype=Candles&internal=D&style=white")
        //print(url!)
        let requestObj = URLRequest(url: url! as URL)
        wvinvestingchart.loadRequest(requestObj)


    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override public func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        if UIDevice.current.orientation.isPortrait {
            

        /*
            AppManager().delay(2, closure: { 
                self.wvinvestingchart.isHidden = true
                _ = self.navigationController?.popViewController(animated: true)
            })*/
            
            
              /*  let viewController = self.storyboard?.instantiateViewController(withIdentifier: "StockSpecificViewController") as! StockSpecificViewController
             self.navigationController?.pushViewController(viewController, animated: false)*/
            
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
