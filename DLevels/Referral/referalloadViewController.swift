//
//  referalloadViewController.swift
//  DLevels
//
//  Created by Admin on 19/09/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class referalloadViewController: UIViewController {
    @IBOutlet weak var webView: UIWebView!
    
    @IBOutlet weak var navview: UIView!
    
    @IBOutlet weak var back: UIButton!
    
    @IBOutlet weak var titlelb: UILabel!
    var contain : String?
    
    var disclaimerHeader : String?
    
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        contain = Referral_Data.how_referral_works
        webView.loadHTMLString(contain!, baseURL: nil)
    }
    
    @IBAction func btn_Invite(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let viewcontroller = storyBoard.instantiateViewController(withIdentifier: "ReferralViewController") as! ReferralViewController
        User.navigation.pushViewController(viewcontroller, animated: false)
    }
    @IBAction func btn_Back(_ sender: Any) {
        User.navigation.popViewController(animated: true)
    }
    
}
