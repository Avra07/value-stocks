//
//  ReferralViewController.swift
//  DLevels
//
//  Created by Dynamic-MacBook-1 on 13/09/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit
import MaterialComponents
import MessageUI
import FBSDKShareKit

class ReferralViewController: UIViewController, MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate, UIWebViewDelegate {
    
    
    @IBOutlet weak var referralCodeVw: UIView!
    @IBOutlet weak var lblReferralCode: UILabel!
    @IBOutlet weak var webvw: UIWebView!
    @IBOutlet weak var btnCopy: UIButton!
    @IBOutlet weak var btnHamburger: UIButton!
    @IBOutlet weak var btnHowItWorks: UIButton!
    @IBOutlet weak var view_Body: UIView!
    
    @IBAction func tc(_ sender: Any) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let viewcontroller = storyBoard.instantiateViewController(withIdentifier: "DisclaimerViewController") as! DisclaimerViewController
        viewcontroller.disclaimerHeader = "Terms & Conditions"
        viewcontroller.disclaimerUrl = terms.termscomdition
        User.navigation.pushViewController(viewcontroller, animated: false)
        
        
        
    }
    @IBOutlet weak var view_Body2: UIView!
    
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    
   
    
    @IBAction func btntohumbrgr(_ sender: Any) {
        User.navigation.popViewController(animated: true)
        
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AppManager().setStatusBarBackgroundColor()
        addLoader()
        let borderColor = UIColor(red:227/255, green:229/255, blue:233/255, alpha: 1)
        
        self.referralCodeVw.layer.borderColor = UIColor(red:181/255, green:186/255, blue:201/255, alpha: 1).cgColor
        self.referralCodeVw.layer.borderWidth = 2
        self.referralCodeVw.layer.cornerRadius = 6
        
        self.view_Body.add_Border(.top, color: borderColor, thickness: 2)
        self.view_Body.add_Border(.right, color: borderColor, thickness: 2)
        self.view_Body.add_Border(.bottom, color: borderColor, thickness: 2)
        
        self.view_Body2.add_Border(.top, color: borderColor, thickness: 2)
        self.view_Body2.add_Border(.right, color: borderColor, thickness: 2)
        self.view_Body2.add_Border(.bottom, color: borderColor, thickness: 2)
        
        self.btnCopy.add_Border(.left, color: UIColor(red:181/255, green:186/255, blue:201/255, alpha: 1), thickness: 2
        )
        
        self.lblReferralCode.text = User.referer_code

        // Do any additional setup after loading the view.
        
    }
    
    
    
    
    
    func addLoader() {
        actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0) //CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.gray
        self.view.addSubview(actInd)
    }
    
    //MARK: WEB VIEW DID FINISH LOAD
    func webViewDidFinishLoad(_ webView: UIWebView) {
        actInd.stopAnimating()
    }
    
    func webViewDidStartLoad(_ webView: UIWebView)
    {
        actInd.startAnimating()
    }
    
    
    @IBAction func copyClick(_ sender: Any) {
        UIPasteboard.general.string = User.referer_code
        self.showToast(message: "Referral Code copied to clipboard")
        
    }
    
    @IBAction func btnHowitWorks(_ sender: Any) {
        
    }
    
    
    @IBAction func shareWhatsApp(_ sender: Any) {
        let msg = Referral_Data.whatsapp_telegram_msg.replacingOccurrences(of: "{referrer code}", with: User.referer_code).replacingOccurrences(of: "{medium}", with: "whatsapp")
        let urlWhats:String = "whatsapp://send?text=\(msg)"
        if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            if let whatsappURL = NSURL(string: urlString) {
                if UIApplication.shared.canOpenURL(whatsappURL as URL) {
                    UIApplication.shared.openURL(whatsappURL as URL)
                } else {
                    // Cannot open whatsapp
                }
            }
        }
        
        
        
    }
    
    @IBAction func shareMessage(_ sender: Any) {
        
        let messageVC = MFMessageComposeViewController()
        
        messageVC.title = "New SMS"
        messageVC.body = Referral_Data.tweet_sms.replacingOccurrences(of: "{referrer code}", with: User.referer_code).replacingOccurrences(of: "{medium}", with: "sms")
        messageVC.recipients = [""]
        messageVC.messageComposeDelegate = self
        
        self.present(messageVC, animated: true, completion: nil)
    }
    
    
    @IBAction func shareGmail(_ sender: Any) {
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
    }
    
    
    @IBAction func shareMessanger(_ sender: Any) {
        
        
        
    }
    
    
    @IBAction func shareFacebook(_ sender: Any) {
        
    }
    
    
    
    @IBAction func shareOthers(_ sender: Any) {
        
        var fileToShare = [AnyObject]()
        fileToShare.append(Referral_Data.email_body.replacingOccurrences(of: "{referrer code}", with: User.referer_code).replacingOccurrences(of: "{medium}", with: "others") as AnyObject )
        
        let activityViewController = UIActivityViewController(activityItems: fileToShare , applicationActivities: nil)
        
        activityViewController.completionWithItemsHandler = {(activityType: UIActivityType?, completed: Bool, returnedItems: [Any]?, error: Error?) in
            if !completed {
                // User canceled
                return
            }
            // User completed activity
            
            self.AddReferralInvite(mediumVal: "sms", completion: { (result) in
                
            })
        }
        
        
        activityViewController.popoverPresentationController?.sourceView = self.view
        present(activityViewController, animated: true, completion: nil)
        
        
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch (result) {
        case .cancelled:
            print("Message was cancelled")
            dismiss(animated: true, completion: nil)
        case .failed:
            print("Message failed")
            dismiss(animated: true, completion: nil)
        case .sent:
            print("Message was sent")
            
            self.AddReferralInvite(mediumVal: "sms", completion: { (result) in
                
            })
            
            dismiss(animated: true, completion: nil)
            
        default:
            break
        }
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients([""])
        mailComposerVC.setSubject(Referral_Data.email_subject)
        mailComposerVC.setMessageBody(Referral_Data.email_body.replacingOccurrences(of: "{referrer code}", with: User.referer_code).replacingOccurrences(of: "{medium}", with: "gmail"), isHTML: true)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        
        let appManager = AppManager()
        appManager.showAlert(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", navigationController: self.navigationController!)
    }
    
    // MARK: MFMailComposeViewControllerDelegate
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        
        
        
        self.AddReferralInvite(mediumVal: "gmail", completion: { (result) in
            
        })
        
        controller.dismiss(animated: true, completion: nil)
        
    }
    
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 140, y: self.view.frame.size.height-100, width: 280, height: 35))
        
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        
        toastLabel.textColor = UIColor.white
        
        toastLabel.textAlignment = .center;
        
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 11.0)
        
        toastLabel.text = message
        
        toastLabel.alpha = 1.0
        
        toastLabel.layer.cornerRadius = 10;
        
        toastLabel.clipsToBounds=true
        
        self.view.addSubview(toastLabel)
        
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            
            toastLabel.alpha = 0.0
            
        }, completion: {(isCompleted) in
            
            toastLabel.removeFromSuperview()
            
        })
        
    }
    

    
    
    
    
    func AddReferralInvite(mediumVal: String, completion : @escaping ([Bool])->()){
        let param = "user_id=\(User.userId)&referer_code=\(User.referer_code)&app=iOS&medium=\(mediumVal)"
        let obj = WebService()
        obj.callWebServices(url: Urls.addreferralinvite, methodName: "POST", parameters: param, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
            
            print(jsonData)
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                let tempArray = jsonData.value(forKey: "response") as! [NSDictionary]
                
                for arr in tempArray{
                    
                   
                }
                
            }
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}


extension UIView {
    func add_Border(_ edge: UIRectEdge, color: UIColor, thickness: CGFloat) {
        let subview = UIView()
        subview.translatesAutoresizingMaskIntoConstraints = false
        subview.backgroundColor = color
        self.addSubview(subview)
        switch edge {
        case .top, .bottom:
            subview.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 0).isActive = true
            subview.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0).isActive = true
            subview.heightAnchor.constraint(equalToConstant: thickness).isActive = true
            if edge == .top {
                subview.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
            } else {
                subview.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
            }
        case .left, .right:
            subview.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
            subview.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
            subview.widthAnchor.constraint(equalToConstant: thickness).isActive = true
            if edge == .left {
                subview.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 0).isActive = true
            } else {
                subview.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0).isActive = true
            }
        default:
            break
        }
    }
}

