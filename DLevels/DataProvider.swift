//
//  DataProvider.swift
//  DLevels
//
//  Created by SAIKAT GHOSH on 21/08/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//


import Foundation
import UIKit
import Alamofire

class DataProvider {
    
    static let shared   =   DataProvider()
    private init(){}
    
    //MARK: GET DASHBOARD DATA
    func getDashboardDetails(completion: @escaping (_ status : Bool, _ dashboardData : [NSDictionary], _ myPortfolioData : [NSDictionary], _ pendingData : [NSDictionary]) -> ()){
        
        let parameter = "client_id=\(User.MF_UserId)&client_code=\(User.ucc)"
        //let parameter = "client_id=1&client_code=MF00000001"
        
        
        
        
        
        
        NetworkHandler().handleRequest(url: Url.mfDashboard, methodName: "POST", parameters: parameter, isToken: true, showLoader: false) { (success, jsonResult) in
            //(url: url, methodName: methodName, parameters: parameters, istoken: isToken, tokenval: User.token )
            //      ACProgressHUD.shared.hideHUD()
            
            var arrDashBoardData = [NSDictionary]()
            var arrMyportFolioData = [NSDictionary]()
            var arrPendingData = [NSDictionary]()
            
            if let errMsg = jsonResult.value(forKey: "errmsg") as? String {
                
                if errMsg == "" {
                    if let response = jsonResult.value(forKey: "response") as? [NSArray] {
                        
                        if let dashboardData = response[0] as? [NSDictionary]{
                            arrDashBoardData = dashboardData
                        }
                        
                        if let myPortfolioData = response[1] as? [NSDictionary]{
                            arrMyportFolioData = myPortfolioData
                        }
                        
                        if let pendingData = response[2] as? [NSDictionary]{
                            arrPendingData = pendingData
                        }
                        
                        completion(true, arrDashBoardData, arrMyportFolioData, arrPendingData)
                        
                    }
                } else {
                    completion(false, arrDashBoardData, arrMyportFolioData, arrPendingData)
                }
            }
            
            completion(false, arrDashBoardData, arrMyportFolioData, arrPendingData)
            
        }
    }
    
    //MARK: GET DASHBOARD DATA
    func getBankAccounts(isPrimary : String ,completion: @escaping (_ status : Bool, _ response : [NSDictionary]) -> ()){
        
        let parameter = "user_id=\(User.MF_UserId)&primary\(isPrimary)"
        //        let parameter = "user_id=1&primary\(isPrimary)"
        
        NetworkHandler().handleRequest(url: Url.mfGetBankAccounts, methodName: "POST", parameters: parameter, isToken: true, showLoader: false) { (success, jsonResult) in
            
            print(jsonResult)
            
            if let errMsg = jsonResult.value(forKey: "errmsg") as? String {
                if errMsg == "" {
                    if let response = jsonResult.value(forKey: "response") as? [NSDictionary] {
                        
                        completion(true, response)
                    }
                } else {
                    completion(false,  [NSDictionary]())
                }
            }
            
        }
    }
    
    
    //MARK: GET DASHBOARD DATA
    func setBankPrimary(bankId : String ,completion: @escaping (_ status : Bool, _ response : String) -> ()){
        
        let parameter = "user_id=\(User.MF_UserId)&bank_id=\(bankId)"
        //        let parameter = "user_id=1&primary\(isPrimary)"
        
        NetworkHandler().handleRequest(url: Url.mfsetdefaultbank, methodName: "POST", parameters: parameter, isToken: true, showLoader: true) { (success, jsonResult) in
            
            print(jsonResult)
            
            if let errMsg = jsonResult.value(forKey: "error_code") as? String {
                if errMsg == "100" {
                    if let response = jsonResult.value(forKey: "msg") as? String {
                        
                        completion(true, response)
                    }
                } else {
                    completion(false,  (jsonResult.value(forKey: "msg") as? String)!)
                }
            }
            
        }
    }
    
    
    //MARK: SET DEFAULT MANDATE
    func setDefaultMandate(mandateID : String ,completion: @escaping (_ status : Bool, _ response : [NSDictionary]) -> ()){
        
        let parameter = "client_code=\(User.ucc)&mandate_id=\(mandateID)"
        //        let parameter = "user_id=1&primary\(isPrimary)"
        
        NetworkHandler().handleRequest(url: Url.mfupdatedefaultmandate, methodName: "POST", parameters: parameter, isToken: true, showLoader: false) { (success, jsonResult) in
            
            print(jsonResult)
            
            if let errMsg = jsonResult.value(forKey: "errmsg") as? String {
                if errMsg == "" {
                    if let response = jsonResult.value(forKey: "response") as? [NSDictionary] {
                        
                        completion(true, response)
                    }
                } else {
                    completion(false,  [NSDictionary]())
                }
            }
            
        }
    }
    
    //MARK: GET Fund Transaction DATA
    //func getFundsTransactionsData(follioNo: String, completion: @escaping (_ status : Bool, _ returnData : [NSDictionary]) -> ()){
    
    //   let parameter = "from=1991-01-01&to=2099-01-01&clientcode=\(User.ucc)&order_type=&order_status=&payment_status=&scheme=\(SIPToInvestDescription.scmCode)&follio=\(follioNo)"
    
    //  NetworkHandler.handleRequest(url: Url.reportOrdersForApp, methodName: "POST", parameters: parameter, isToken: false, showLoader: true) { (success, jsonResult) in
    
    //   var arrData = [NSDictionary]()
    
    //  if let errMsg = jsonResult.value(forKey: "errmsg") as? String {
    
    //      if errMsg == "" {
    //          if let response = jsonResult.value(forKey: "response") as? [NSDictionary]{
    //                arrData = response
    //           completion(true, arrData)
    //            } else {
    //             completion(false, arrData)
    //            }
    //       }
    
    //       completion(false, arrData)
    
    //     }
    //     }
    //  }
    
    //MARK: GET DASHBOARD DATA
    func getTransactionHistory(scheme : String ,completion: @escaping (_ status : Bool, _ response : [NSDictionary]) -> ()){
        
        let parameter = "from=1991-01-01&to=2099-01-01&clientcode=\(User.ucc)&order_type=&order_status=&payment_status="
        NetworkHandler().handleRequest(url: Url.mfTransactionHistory, methodName: "POST", parameters: parameter, isToken: true, showLoader: true) { (success, jsonResult) in
            
            print(jsonResult)
            
            if let errMsg = jsonResult.value(forKey: "errmsg") as? String {
                if errMsg == "" {
                    if let response = jsonResult.value(forKey: "response") as? [NSDictionary] {
                        
                        completion(true, response)
                    }
                } else {
                    completion(false,  [NSDictionary]())
                }
            }
            
        }
    }
    
    //MARK: GET MY SIP
    func getMySIP(completion: @escaping (_ status : Bool, _ response : [NSDictionary]) -> ()){
        
        // let parameter = "client_id=\(User.mfUserId)&client_code=\(User.UCC)"
        let parameter = "client_code=\(User.ucc)&from=1900-00-00&to=3000-00-00"
        
        NetworkHandler().handleRequest(url: Url.mfMySip, methodName: "GET", parameters: parameter, isToken: true, showLoader: true) { (success, jsonResult) in
            
            print(jsonResult)
            
            if let errMsg = jsonResult.value(forKey: "errmsg") as? String {
                if errMsg == "" {
                    if let response = jsonResult.value(forKey: "response") as? [NSDictionary] {
                        completion(true, response)
                    }
                } else {
                    completion(false,  [NSDictionary]())
                }
            }
            
        }
    }
    
    
    //MARK: GET RISK QUESTIONS
    /* func getRiskQuestions(completion: @escaping (_ questions : [RiskQuestion]) -> Void){
     
     NetworkHandler.handleRequest(url: Url.mfriskpassessmentquestionsanswer, methodName: MethodType.post, parameters: "", isToken: false, showLoader: true) { (success, jsonResult) in
     
     if let errmsg = jsonResult.value(forKey: "errmsg") as? String, let response = jsonResult.value(forKey: "response") as? [NSDictionary]{
     
     if errmsg == "" {
     
     var allQuestions = [RiskQuestion]()
     
     if !response.isEmpty {
     
     var rID = "", rQuestion = "", questionOption = ""
     
     for question in response {
     
     if let reqId = question.value(forKey: "rpqID") as? String {
     rID = reqId
     }
     
     if let question = question.value(forKey: "rpqQuesrtion") as? String {
     rQuestion = question
     }
     
     if let options = question.value(forKey: "quesOptions") as? String {
     questionOption = options
     }
     
     
     let questions = RiskQuestion(rpqID: rID, rpqQuesrtion: rQuestion, quesOptions: questionOption, selectedAnswer: "", selectedId: "")
     
     allQuestions.append(questions)
     }
     
     completion(allQuestions)
     
     } else {
     AppManager.showAlertMessage(title: "error", message: Errors.somethingWentWrong)
     }
     
     } else {AppManager.showAlertMessage(title: "error", message: Errors.somethingWentWrong)}
     
     }
     
     
     }
     */
    
    //}
    
    //MARK: SAVE RISK DATA
    func saveRiskData(response: String, completion : @escaping (Bool)->()) {
        let param = "client_code=\(User.ucc)&client_id=\(User.MF_UserId)&ResponseStr=\(response)"
        print(param)
        NetworkHandler().handleRequest(url: Url.mfsaverskassessment, methodName: "POST", parameters: param, isToken: false, showLoader: true ) { (success, jsonData) in
            
            print(jsonData)
            if jsonData.value(forKey: "errmsg") as! String == "" {
                completion(true)
            }else {
                completion(false)
            }
        }
    }
    
    //MARK: GET RISK PROFILE
    /*  func getRiskProfile(completion: @escaping (_ isRiskProfile: Bool, _ status: String, _ message: String)-> Void) {
     
     let parameter = "client_code=\(User.ucc)&client_id=\(User.MF_UserId)"
     
     NetworkHandler.handleRequest(url: Url.mfriskprofile, methodName: MethodType.post, parameters: parameter, isToken: false, showLoader: true) { (success, jsonResult) in
     
     if let errmsg = jsonResult.value(forKey: "errmsg") as? String {
     if errmsg == "" {
     if let response = jsonResult.value(forKey: "response") as? [NSDictionary] {
     if response.isEmpty {
     AppManager.showAlertMessage(title: "error", message: Errors.somethingWentWrong)
     } else {
     if let clientRP = response[0].value(forKey: "clientRP") as? String, let riskDescrip = response[0].value(forKey: "Risk_Desc") as? String {
     if clientRP == "" {
     completion(false, "","")
     } else {
     completion(true, clientRP, riskDescrip)
     }
     } else {
     AppManager.showAlertMessage(title: "error", message: errmsg)
     }
     }
     }
     
     } else {
     AppManager.showAlertMessage(title: "error", message: errmsg)
     }
     
     }
     }
     }
     */
    //MARK: CANCEL SIP
    func cancelSIP(sipId: String, reason: String ,completion: @escaping (_ status : Bool, _ response : NSDictionary) -> ()){
        
        // let parameter = "client=MF00000001&sip_id=\(sipId)&reason=\(reason)"
        let parameter = "client=\(User.ucc)&sip_id=\(sipId)&reason=\(reason == "" ? "Client Cancelled".replacingOccurrences(of: " ", with: "%20"): reason)"
        
        NetworkHandler().handleRequest(url: Url.mfCancelSip, methodName: "POST", parameters: parameter, isToken: true, showLoader: true) { (success, jsonResult) in
            
            print(jsonResult)
            
            if let errMsg = jsonResult.value(forKey: "errmsg") as? String {
                if errMsg == "" {
                    if let response = jsonResult.value(forKey: "response") as? NSDictionary {
                        
                        completion(true, response)
                    }
                } else {
                    completion(false,  NSDictionary())
                }
            }
            
        }
    }
    
    //MARK: GET SCHEME REDEMPTION DETAIS
    /* func getSchemeRedemptionDetails(completion: @escaping (_ status : Bool, _ response : [NSDictionary]) -> ()) {
     // scheme=HDFCMCOG-GR
     let parameter = "scheme=\(User.scheme)"
     NetworkHandler.handleRequest(url: Url.getSchemeRedemptionDetails, methodName: "GET", parameters: parameter, isToken: true, showLoader: true) { (success, jsonResult) in
     print("REDEEM DETAILS = \(jsonResult)")
     
     if success {
     if let errorMsg = jsonResult.value(forKey: "errmsg") as? String, let response = jsonResult.value(forKey: "response") as? [NSDictionary] {
     if errorMsg == "" {
     completion(true, response)
     } else {
     completion(false, [NSDictionary]())
     }
     }
     }
     else {
     completion(false, [NSDictionary]())
     }
     }
     }
     */
    //MARK: SEND MANDATE EMAIL
    func sendMandateEmail(mandateId: String, mandateRegId : String, completion : @escaping (Bool) -> Void) {
        
        let parameter = "mandate_id=\(mandateId)&mandate_regn_id=\(mandateRegId)"
        
        NetworkHandler().handleRequest(url: Url.sendMandateEmail, methodName: "GET", parameters: parameter, isToken: true, showLoader: true) { (success, jsonResult) in
            
            if let errmsg = jsonResult.value(forKey: "errmsg") as? String {
                //  AppManager.showAlertMessage(title: errmsg == "" ? Titles.success : Titles.error, message: errmsg == "" ? "Email sent successfully" : Errors.somethingWentWrong)
                completion(errmsg == "")
            }
            completion(false)
        }
    }
    
    //MARK: SEND PENDING PAYMENT CONFIRMATION
    /* func sendPendingPaymentConfirmation(fundName: String, amount: Int, sipDate:String){
     let parameter = "mail=support@dynamiclevels.com&type=pending-payment-confirmation&name=\(User.firstName)&fund_name=\(fundName)&amount_unit=\(amount)&transaction_date=\(AppManager.getCurrentDate())&send_method=email&mobile_no=\(User.)&pan=&sip_Date=\(sipDate)"
     
     NetworkHandler.handleRequest(url: Url.sendAlert, methodName: MethodType.post, parameters: parameter, isToken: false, showLoader: false) { (success, jsonResult) in
     }
     }
     
     
     //MARK: KYC COMPLETE MAIL
     func sendApprovalMail(){
     let parameter = "mail=\(User.email)&type=account-successfully-verified&name=\(User.firstName)&fund_name=&amount_unit=&transaction_date=&send_method=email&mobile_no=\(User.)&pan="
     
     NetworkHandler.handleRequest(url: Url.sendAlert, methodName: MethodType.post, parameters: parameter, isToken: false, showLoader: false) { (success, jsonResult) in
     }
     }
     
     //account-successfully-verified
     
     //MARK: APPROVAL MAIL
     func sendForReviewMail(){
     let parameter = "mail=\(Contact.approvalMail)&type=informed-for-review-approval&name=\(User.firstName)&fund_name=&amount_unit=&transaction_date=&send_method=email&mobile_no=&pan=\(UserSession.shared.getUserPan())"
     
     NetworkHandler.handleRequest(url: Url.sendAlert, methodName: MethodType.post, parameters: parameter, isToken: false, showLoader: false) { (success, jsonResult) in
     }
     }
     
     //MARK: UPLOAD MANDATE SIGN
     func uploadMandateSign(mandateRegId : String, image : UIImage ,docType: String = "mandate_signature", mandateImgType: String = "signed_form", completion: @escaping(Bool)->Void){
     
     let parameters = ["doc_type" : "\(docType)","client_code" : "\(User.ucc)", "mandate_img_type": "\(mandateImgType)", "mandate_regn_id": "\(mandateRegId)"]
     
     print("Sign Upload parameter is: \(parameters)")
     let imgData = image.jpegData(compressionQuality: 0.2)
     KVNProgress.show(withStatus: Titles.loading)
     
     Alamofire.upload(multipartFormData: { multipartFormData in
     multipartFormData.append(imgData!, withName: "document[0]",fileName: ".JPG", mimeType: "image/jpg")
     for (key, value) in parameters {
     multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
     } //Optional for extra parameters
     
     },
     to: Url.mfupdatemandatessign)
     { (result) in
     switch result {
     case .success(let upload, _, _):
     
     upload.uploadProgress(closure: { (progress) in
     print("Upload Progress: \(progress.fractionCompleted)")
     })
     
     upload.responseJSON { response in
     print(response.result.value)
     
     if let result = response.result.value as? NSDictionary {
     if let response = result.value(forKey: "response") as? NSDictionary {
     
     if let erroCode = response.value(forKey: "error_code") as? String {
     if erroCode == "100" {
     KVNProgress.dismiss()
     completion(true)
     }else {
     AppManager.showAlertMessage(title: "error", message: Errors.somethingWentWrong)
     }
     }
     
     }else {self.showError()}
     }else {self.showError()}
     }
     
     case .failure(let encodingError):
     print(encodingError)
     self.showError()
     }
     }
     
     
     
     }
     
     
     //MARK: SHOW ERROR
     private func showError(){
     KVNProgress.dismiss()
     AppManager.showAlertMessage(title: "error", message: Errors.somethingWentWrong)
     }
     
     //MARK: GET SCHEME REDEMPTION DETAIS
     func getRedemption(reason: String, amount:
     String, allUnits : String,  completion: @escaping (_ status : Bool, _ response : NSDictionary) -> ()) {
     
     // let parameter = "scheme=HDFCMCOG-GR&client=MF00000001&follio=4568527789/01&amount=1000&units=&all_units=N&reason=Dekhte hai"
     
     var parameter = ""
     
     if let scheme = User.arrMyPorfolio.value(forKey: "SchemeCode") as? String, let folioNo = User.arrMyPorfolio.value(forKey: "portfolioFilloNo") as? String {
     
     parameter = "scheme=\(scheme)&client=\(User.ucc)&follio=\(folioNo)&amount=\(allUnits == "Y" ? "" : amount)&units=&all_units=\(allUnits)&reason=\(reason.replacingOccurrences(of: " ", with: "%20"))"
     
     NetworkHandler.handleRequest(url: Url.mfRedemption, methodName: "POST", parameters: parameter, isToken: true, showLoader: true) { (success, jsonResult) in
     print("REDEEM DETAILS = \(jsonResult)")
     
     if success {
     if let errorMsg = jsonResult.value(forKey: "errmsg") as? String {
     if errorMsg == "" {
     completion(true, jsonResult)
     } else {
     completion(false, NSDictionary())
     }
     }
     }
     else {
     completion(false, NSDictionary())
     }
     }
     
     
     }
     
     }
     
     //MARK: GET DASHBOARD DATA
     func getBankAccountsDetails(bankID : String ,completion: @escaping (_ status : Bool, _ response : [NSDictionary]) -> ()){
     
     let parameter = "client_code=\(User.ucc)&client_id=\(User.MF_UserId)&bank_id=\(bankID)"
     
     NetworkHandler.handleRequest(url: Url.mfGetBankAccountsDetails, methodName: "POST", parameters: parameter, isToken: true, showLoader: true) { (success, jsonResult) in
     
     print(jsonResult)
     
     if let errMsg = jsonResult.value(forKey: "errmsg") as? String {
     if errMsg == "" {
     if let response = jsonResult.value(forKey: "response") as? [NSDictionary] {
     
     completion(true, response)
     }
     } else {
     completion(false,  [NSDictionary]())
     }
     }
     
     }
     }
     
     //ADD MANDATE
     func addMandate(bankID : String ,type: String, completion: @escaping (_ status : Bool, _ response : String) -> ()){
     
     let parameter = "client=\(User.ucc)&bank_id=\(bankID)&type=\(type)"
     print(parameter)
     NetworkHandler.handleRequest(url: Url.mfaddmandate, methodName: "POST", parameters: parameter, isToken: true, showLoader: true) { (success, jsonResult) in
     
     print(jsonResult)
     
     if let errMsg = jsonResult.value(forKey: "error_code") as? String {
     if errMsg == "100" {
     if let response = jsonResult.value(forKey: "msg") as? String {
     
     completion(true, response)
     }
     } else {
     completion(false, (jsonResult.value(forKey: "msg") as? String)!)
     }
     }
     
     }
     }
     
     //MARK: UPDATE MANDATE
     func updateMandateSignature(document : UIImage ,clientCode: String, mandateRegnId: String, docType: String, completion: @escaping (_ status : Bool, _ response : [NSDictionary]) -> ()){
     
     let parameter = "document[0]=\(document)&client_code=\(clientCode)&mandate_regn_id=\(mandateRegnId)&doc_type=\(docType)"
     
     print(parameter)
     
     NetworkHandler.handleRequest(url: Url.mfupdatemandatessign, methodName: "POST", parameters: parameter, isToken: true, showLoader: true) { (success, jsonResult) in
     
     print(jsonResult)
     
     if let errMsg = jsonResult.value(forKey: "errmsg") as? String {
     if errMsg == "" {
     if let response = jsonResult.value(forKey: "response") as? [NSDictionary] {
     
     completion(true, response)
     }
     } else {
     completion(false,  [NSDictionary]())
     }
     }
     
     }
     }
     
     
     //MARK: GENERAT OTM
     func generateOTM(bankId : String, type : String ,completion: @escaping(Bool,String)-> Void){
     
     let parameter = "client=\(User.ucc)&bank_id=\(bankId)&type=\(type)"
     
     NetworkHandler.handleRequest(url: Url.mfaddmandate, methodName: MethodType.post, parameters: parameter, isToken: false, showLoader: true) { (success, jsonResult) in
     print("Mandate Response : \(jsonResult)")
     
     if let errorCode = jsonResult.value(forKey: "error_code") as? String {
     if errorCode == "100" {
     
     if let mandate = jsonResult.value(forKey: "msg") as? String {
     completion(true, mandate)
     } else {
     AppManager.showAlertMessage(title:"error", message: Errors.somethingWentWrong)
     }
     
     } else {
     AppManager.showAlertMessage(title: "error", message: Errors.somethingWentWrong)
     }
     
     }
     
     
     }
     }
     */
    //MARK: GET FUND CATEGORIES
    func getFundCategories(completion: @escaping ([MFCategories])-> Void){
        
        var result = [MFCategories]()
        NetworkHandler().handleRequest(url: Url.mfcategories, methodName: "POST", parameters: "", isToken: false, showLoader: true) { (success, jsonData) in
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                let tempArray = jsonData.value(forKey: "response") as! [NSDictionary]
                
                for arr in tempArray{
                    var category = "",  desc =  "", title = ""
                    
                    if let category_data = arr.value(forKey: "mfc_category") as? String{
                        category = category_data
                    }
                    
                    if let category_desc = arr.value(forKey: "mfc_category_desc") as? String{
                        desc = category_desc
                    }
                    
                    if let category_title = arr.value(forKey: "mfc_category_title") as? String{
                        title = category_title
                    }
                    
                    let obj_mfcategories = MFCategories(category: category, title: title, desc: desc)
                    
                    result.append(obj_mfcategories)
                }
                
                completion(result)
                
            } else {
                AppManager.showAlertMessage(title: "error", message: "check connection")
            }
        }
    }
    /*
     //MARK: CANCEL TRANSACTIONS
     func cancelTransactions(orderNo : String, reason : String ,completion : @escaping (Bool)->Void) {
     
     let parameter = "client=\(User.ucc)&order_no=\(orderNo)&reason=\(reason == "" ? "Client Cancelled".replacingOccurrences(of: " ", with: "%20"): reason.replacingOccurrences(of: " ", with: "%20"))"
     
     NetworkHandler.handleRequest(url: Url.processOrderCancellation, methodName: MethodType.post, parameters: parameter, isToken: false, showLoader: true) { (success, jsonResult) in
     
     if let response = jsonResult.value(forKey: "response") as? NSDictionary {
     
     if let errCode = response.value(forKey: "error_code") as? String {
     if errCode == "100" {
     completion(true)
     } else {
     completion(false)
     // AppManager.showAlertMessage(title: Titles.error, message: Errors.somethingWentWrong)
     }
     }
     }
     }
     }
     
     //MARK: CLIENT UCC
     func clientUcc(completion : @escaping (Bool)->()){
     let param = "user_id=\(User.ucc)"
     
     NetworkHandler.handleRequest(url: Url.clientucc, methodName: MethodType.post, parameters: param, isToken: true, showLoader: false) { (success, jsonResult) in
     
     if let errorCode = jsonResult.value(forKey: "error_code") as? String, errorCode == "100"{
     
     DataProvider.shared.sendApprovalMail()
     completion(true)
     
     }else {
     
     if let msg = jsonResult.value(forKey: "msg") as? String {
     AppManager.showAlertMessage(title: "error", message: msg)
     } else {
     AppManager.showAlertMessage(title: "error", message: Errors.somethingWentWrong)
     }
     completion(true)
     }
     }
     
     }
     
     
     func getNomineeDetial(scheme: String) {
     
     
     }
     
     //MARKL GET BANK LIST
     func getBankList(completion : @escaping ([NSDictionary]) -> Void){
     
     NetworkHandler.handleRequest(url: Url.getBankNameList, methodName:"GET", parameters: "", isToken: false, showLoader: true) { (success, jsonResult) in
     
     if let response = jsonResult.value(forKey: "response") as? [NSDictionary], !response.isEmpty {
     completion(response)
     } else {
     AppManager.showAlertMessage(title: "error", message: Errors.somethingWentWrong)
     }
     }
     }
     
     //MARKL GET BANK STATE LIST
     func getBankStateList(bankName : String, completion : @escaping ([NSDictionary]) -> Void){
     let parameter = "bank_name=\(bankName.replacingOccurrences(of: " ", with: "%20"))"
     NetworkHandler.handleRequest(url: Url.getBankStateList, methodName:"GET" , parameters: parameter, isToken: false, showLoader: true) { (success, jsonResult) in
     if let response = jsonResult.value(forKey: "response") as? [NSDictionary], !response.isEmpty {
     completion(response)
     } else {
     AppManager.showAlertMessage(title: "error", message: Errors.somethingWentWrong)
     }
     }
     }
     
     //MARKL GET BANK CITY
     func getBankCityList(bankName : String, state: String ,completion : @escaping ([NSDictionary]) -> Void){
     let parameter = "bank_name=\(bankName.replacingOccurrences(of: " ", with: "%20"))&bank_state=\(state.replacingOccurrences(of: " ", with: "%20"))"
     NetworkHandler.handleRequest(url: Url.getBankCityList, methodName: "GET", parameters: parameter, isToken: false, showLoader: true) { (success, jsonResult) in
     if let response = jsonResult.value(forKey: "response") as? [NSDictionary], !response.isEmpty {
     completion(response)
     } else {
     AppManager.showAlertMessage(title: "error", message: Errors.somethingWentWrong)
     }
     }
     }
     
     //MARKL GET BANK CITY
     func getBranchList(bankName : String, city: String ,completion : @escaping ([NSDictionary]) -> Void){
     let parameter = "bank_name=\(bankName.replacingOccurrences(of: " ", with: "%20"))&bank_city=\(city.replacingOccurrences(of: " ", with: "%20"))"
     NetworkHandler.handleRequest(url: Url.getBranchkList, methodName: "GET", parameters: parameter, isToken: false, showLoader: true) { (success, jsonResult) in
     if let response = jsonResult.value(forKey: "response") as? [NSDictionary], !response.isEmpty {
     completion(response)
     } else {
     AppManager.showAlertMessage(title: "error", message: Errors.somethingWentWrong)
     }
     }
     }
     
     
     //MARK: SAVE FIREBASE TOKEN
     func savefirebaseToken(pltform : String, userid: String , deviceid: String, tokenno: String, completion : @escaping ([NSDictionary]) -> Void){
     let parameter = "platform=\(pltform)&user_id=\(userid)&device_id=\(deviceid)&token_no=\(tokenno)"
     NetworkHandler.handleRequest(url: Url.setfcmtoken, methodName: "POST", parameters: parameter, isToken: false, showLoader: false) { (success, jsonResult) in
     if let response = jsonResult.value(forKey: "response") as? [NSDictionary], !response.isEmpty {
     completion(response)
     } else {
     AppManager.showAlertMessage(title: "error", message: Errors.somethingWentWrong)
     }
     }
     }
     }
     */
}

