//
//  MFCategories.swift
//  DLevels
//
//  Created by Dlevels-MacMini1 on 25/05/18.
//  Copyright © 2018 Dynamic-Mac-01. All rights reserved.
//

import Foundation
class MFCategories: NSObject {
   
    var category: String?
    var title: String?
    var desc: String?
    
    init(category:String,title:String,desc:String) {
        self.title      =   title
        self.desc       =   desc
        self.category   =   category
        
    }
}
