//
//  FundTypeList.swift
//  DLevels
//
//  Created by Dlevels-MacMini1 on 27/05/18.
//  Copyright © 2018 Dynamic-Mac-01. All rights reserved.
//

import Foundation
class MFFundsTypeList: NSObject {
    var FundType: String?
    var NoOfFunds: Int?
    var TotalNetAssets: Int?
    
    
    var oneWReturn: Decimal?
    var oneMReturn: Decimal?
    var yrAvgReturn: Decimal?
    var threeMReturn: Decimal?
    var sixMReturn: Decimal?
    var threeYrAvgReturn: Decimal?
    var fiveYrAvgReturn: Decimal?
    
    init(FundType:String,NoOfFunds:Int,TotalNetAssets:Int, OneWReturn: Decimal,OneMReturn: Decimal,ThreeMReturn: Decimal, SixMReturn: Decimal,YrAvgReturn:Decimal, ThreeYrAvgReturn: Decimal, FiveYrAvgReturn: Decimal) {
        
        self.FundType           =   FundType
        self.NoOfFunds          =   NoOfFunds
        self.TotalNetAssets     =   TotalNetAssets
        self.oneWReturn         =   OneWReturn
        self.oneMReturn         =   OneMReturn
        self.yrAvgReturn        =   YrAvgReturn
        self.threeMReturn       =   ThreeMReturn
        self.sixMReturn         =   SixMReturn
        self.threeYrAvgReturn   =   ThreeYrAvgReturn
        self.fiveYrAvgReturn    =   FiveYrAvgReturn
        
    }
    
    
}
