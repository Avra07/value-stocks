//
//  MFFundsListViewController.swift
//  DLevels
//
//  Created by Dlevels-MacMini1 on 21/05/18.
//  Copyright © 2018 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class MFFundsTypeListViewController: UIViewController {
    
    private var _category: String!
    private var _title: String!
    
    @IBOutlet weak var navBarTitle: UINavigationItem!
    @IBOutlet weak var tableVw: UITableView!
    @IBOutlet weak var navbar: UINavigationBar!
    @IBOutlet weak var lblnotes: UILabel!
    @IBOutlet weak var lbllastcol: UILabel!
    
    @IBOutlet weak var lblltotalnooffunds: UILabel!
    @IBOutlet weak var lbltotalnetassest: UILabel!
    
    
    
    
    fileprivate var dataArray = [MFFundsTypeList]()
    fileprivate var mainDataArray = [MFFundsTypeList]()
    private let dataSource = MFFundTypeListDataModel()
    var sortFundFlag = "F"
    
    var noOfTotalFunds = 0
    var totalNetAssest = 0
    
    @IBOutlet weak var vwHeader: UIView!
    var col = ""
    var Category:String {
        get {
            return self._category
        }
        set {
            self._category = newValue
        }
    }

    var TitleVal:String {
        get {
            return self._title
        }
        set {
            self._title = newValue
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
         AppManager().setStatusBarBackgroundColor()
        let label = UILabel(frame: CGRect(x:0, y:0, width:400, height:50))
        label.backgroundColor = .clear
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: 15.0)
        label.textAlignment = .left
        label.textColor = UIColor(hex:"284C5A")
        label.text = self._title
        self.navBarTitle.titleView = label
        //vwHeader.addDropShadow()
        
        tableVw?.register(MFFundTypeListTableViewCell.nib, forCellReuseIdentifier: MFFundTypeListTableViewCell.identifier)
        tableVw?.delegate = self
        tableVw?.dataSource = self
        
        
        tableVw.tableHeaderView = nil;
        tableVw.tableFooterView = nil;
        
        tableVw.backgroundView?.isHidden = true

        let obj_MFFundTypeListDM = MFFundTypeListDataModel()
        
        obj_MFFundTypeListDM.getData(paramCategory: self._category, completion: { (dataset) in
            self.dataArray = dataset
            self.lblnotes.text = MFStaticData.ason_text.replacingOccurrences(of: "<no>", with: "\(self.dataArray.count)").replacingOccurrences(of: "<name>", with: "\(self._title!)".replacingOccurrences(of: "Top", with: ""))
            self.mainDataArray = dataset
            self.tableVw.reloadData()
        })
        // Do any additional setup after loading the view.
    }
    
   

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        tableVw.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    @IBAction func btnNetAssetFilter_Action(_ sender: Any) {
        let actionSheeController = UIAlertController(title:"", message: "", preferredStyle: .actionSheet)
        
        let items = ["Total Net Assets(Cr)", "Assets > 500(Cr) ", "Assets > 1000(Cr) "]
        
        actionSheeController.addAction(UIAlertAction(title: "Total Net Assets(Cr)", style: .default, handler: { (success) in
            UIView.animate(withDuration: 0.4) {
                
                self.dataArray = self.mainDataArray
                self.tableVw.reloadData()
                self.view.layoutIfNeeded()
            }
        }))
        
        actionSheeController.addAction(UIAlertAction(title: "Assets > 100(Cr)", style: .default, handler: { (success) in
            UIView.animate(withDuration: 0.4) {
                
                self.dataArray = self.mainDataArray.filter { $0.TotalNetAssets! > Int(100) }
                self.tableVw.reloadData()
                self.view.layoutIfNeeded()
            }
        }))
        
        actionSheeController.addAction(UIAlertAction(title: "Assets > 250(Cr)", style: .default, handler: { (success) in
            UIView.animate(withDuration: 0.4) {
                
                self.dataArray = self.mainDataArray.filter { $0.TotalNetAssets! > Int(250) }
                self.tableVw.reloadData()
                self.view.layoutIfNeeded()
            }
        }))
        
        
            actionSheeController.addAction(UIAlertAction(title: "Assets > 500(Cr)", style: .default, handler: { (success) in
                UIView.animate(withDuration: 0.4) {
                    
                    self.dataArray = self.mainDataArray.filter { $0.TotalNetAssets! > Int(500) }
                    self.tableVw.reloadData()
                    self.view.layoutIfNeeded()
                }
            }))
        
        actionSheeController.addAction(UIAlertAction(title: "Assets > 1000(Cr)", style: .default, handler: { (success) in
            UIView.animate(withDuration: 0.4) {
                
                self.dataArray = self.mainDataArray.filter { $0.TotalNetAssets! > Int(1000) }
                self.tableVw.reloadData()
                self.view.layoutIfNeeded()
            }
        }))
       
        
        
        actionSheeController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (success) in
        }) )
        
        // mone rakho
        self.present(actionSheeController, animated: false) {
            
        }
        
    }
    @IBAction func btn1YrAvgSort_Click(_ sender: Any) {
        
//        if sortYrReturnFlag == "F" {
//            dataArray = dataArray.sorted(by: {$0.YrAvgReturn! < $1.YrAvgReturn!})
//            sortYrReturnFlag = "T"
//        }else{
//            dataArray = dataArray.sorted(by: {$0.YrAvgReturn! > $1.YrAvgReturn!})
//            sortYrReturnFlag = "F"
//        }
//        //print(dataArray)
//        tableVw.reloadData()
        let actionSheeController = UIAlertController(title:"Data Fields", message: "", preferredStyle: .actionSheet)
        
        let items = ["1W Return(%)","1M Return(%)", "3M Return(%)", "6M Return(%)","1Y Return(%)", "3Y Return(%)", "5Y Return(%)"]
        let itemsCol = ["1W","1M", "3M", "6M","1Y", "3Y", "5Y"]
        for i in items {
            actionSheeController.addAction(UIAlertAction(title: i, style: .default, handler: { (success) in
                self.lbllastcol.text = i
                self.col = itemsCol[items.index(of: i)!]
                UIView.animate(withDuration: 0.4) {
                    if self.col == "1W" {
                        self.dataArray = self.dataArray.sorted(by: {$0.oneWReturn! > $1.oneWReturn!})
                    }
                    else if self.col == "1M" {
                        self.dataArray = self.dataArray.sorted(by: {$0.oneMReturn! > $1.oneMReturn!})
                    }
                    else if self.col == "3M" {
                        self.dataArray = self.dataArray.sorted(by: {$0.threeMReturn! > $1.threeMReturn!})
                    }
                    else if self.col == "6M" {
                        self.dataArray = self.dataArray.sorted(by: {$0.sixMReturn! > $1.sixMReturn!})
                  }
                    else if self.col == "1Y" {
                        self.dataArray = self.dataArray.sorted(by: {$0.yrAvgReturn! > $1.yrAvgReturn!})
                    }
                    else if self.col == "3Y" {
                        self.dataArray = self.dataArray.sorted(by: {$0.threeYrAvgReturn! > $1.threeYrAvgReturn!})
                    }
                    else if self.col == "5Y" {
                        self.dataArray = self.dataArray.sorted(by: {$0.fiveYrAvgReturn! > $1.fiveYrAvgReturn!})
                    }
                    self.tableVw.reloadData()
                    self.view.layoutIfNeeded()
                }
            }))
            
        }
        
        actionSheeController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (success) in
        }) )
        
        // mone rakho
        self.present(actionSheeController, animated: false) {
            
        }
        
        
    }
    
    
    @IBAction func btnFundTypeSort_Action(_ sender: Any) {
        if sortFundFlag == "F" {
         dataArray = dataArray.sorted (by: {$0.FundType! < $1.FundType!})
         sortFundFlag = "T"
        }else{
            dataArray = dataArray.sorted (by: {$0.FundType! > $1.FundType!})
         sortFundFlag = "F"
        }
        tableVw.reloadData()
    }
    
    @IBAction func backClick(_ sender: Any) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    
    @IBAction func searchClick(_ sender: Any) {
        let viewCont = self.storyboard?.instantiateViewController(withIdentifier: "MFSearchViewController") as! MFSearchViewController
        self.navigationController?.pushViewController(viewCont, animated: false)
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let index:Int = sender as! Int
        if segue.identifier == "showDetails" {
            let vc = segue.destination as! MFFundDetailsViewController
            vc.Category = _category
            vc.FundName = dataArray[index].FundType!
            vc.col = self.col
        }
    }

}

extension MFFundsTypeListViewController: UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier:
            MFFundTypeListTableViewCell.identifier, for: indexPath) as? MFFundTypeListTableViewCell
        {
            cell.selectionStyle = .none
            
            cell.backgroundColor = indexPath.row % 2 == 0 ?  UIColor(red: 215/255, green: 222/255, blue: 225/255, alpha: 1.0) : UIColor(red: 236/255, green: 239/255, blue: 240/255, alpha: 1.0)
            
//            noOfTotalFunds = noOfTotalFunds + dataArray[indexPath.item].NoOfFunds!
//            totalNetAssest = totalNetAssest + dataArray[indexPath.item].TotalNetAssets!
            
            cell.configureWithItem(item: dataArray[indexPath.item],colName: col)
            
//            lbltotalnetassest.text = "\(totalNetAssest)"
//            lblltotalnooffunds.text = "\(noOfTotalFunds)"
            
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.dequeueReusableCell(withIdentifier:
            MFFundTypeListTableViewCell.identifier, for: indexPath) as? MFFundTypeListTableViewCell
        {
            self.performSegue(withIdentifier: "showDetails", sender: indexPath.row)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var numOfSections: Int = 0
        if dataArray.count == 0 {
            
//            let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
//            noDataLabel.text          = "No data available"
//            noDataLabel.textColor     = UIColor.black
//            noDataLabel.textAlignment = .center
//            tableView.backgroundView  = noDataLabel
//            tableView.separatorStyle  = .none
//            tableView.backgroundView?.isHidden = false
        } else {
            numOfSections            = dataArray.count
            //tableView.backgroundView?.isHidden = true
        }
        
        var nooffunds = 0
        var totalNetAssest = 0
        
        for item in mainDataArray {
            nooffunds = nooffunds + Int(item.NoOfFunds!)
            totalNetAssest = totalNetAssest + Int(item.TotalNetAssets!)
        }
        
        lblltotalnooffunds.text = "\(nooffunds)"
       lbltotalnetassest.text = "\(totalNetAssest)"
        return numOfSections
    }
    public func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 900
    }
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    
}
