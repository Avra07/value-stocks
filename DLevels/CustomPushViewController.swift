//
//  CustomPushViewController.swift
//  DLevels
//
//  Created by Rishi Sachdeva on 29/06/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class CustomPushViewController: UIViewController, UIWebViewDelegate {
    var comingfrom = false
	var dataArr:     [NSDictionary] = []
	@IBOutlet weak var webvw: UIWebView!
	var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		AppManager().setStatusBarBackgroundColor()
		
		// print(NotificationData.url)
		getData()
		addLoader()
		// Do any additional setup after loading the view.
		
		
	}
	
	func addLoader() {
		actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0) //CGRectMake(0.0, 0.0, 40.0, 40.0);
		actInd.center = self.view.center
		actInd.hidesWhenStopped = true
		actInd.activityIndicatorViewStyle =
			UIActivityIndicatorViewStyle.gray
		self.view.addSubview(actInd)
	}
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	@IBAction func btnBackClick(_ sender: Any) {
        if comingfrom == false{
		let viewController = self.navigationController?.storyboard?.instantiateViewController(withIdentifier: "DlHomeViewController") as! DlHomeViewController
		
		User.navigation.pushViewController(viewController, animated: false)
        }else{
            User.navigation.popViewController(animated: true)
        }
	}
	
	
	
	//MARK: Getting Data
	func getData()
	{
		webvw.loadRequest(NSURLRequest(url: NSURL(string: NotificationData.url)! as URL) as URLRequest)
		
	}
	//MARK: WEB VIEW DID FINISH LOAD
	func webViewDidFinishLoad(_ webView: UIWebView) {
		actInd.stopAnimating()
	}
	
	func webViewDidStartLoad(_ webView: UIWebView)
	{
		actInd.startAnimating()
	}
	
	
	func getQueryStringParameter(url: String, param: String) -> String? {
		guard let url = URLComponents(string: url) else { return nil }
		return url.queryItems?.first(where: { $0.name == param })?.value
	}
	
	func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
		
		if (request.url?.absoluteString.contains("https://www.dynamiclevels.com/stockspecific"))!   {
			let symbol = getQueryStringParameter(url: (request.url?.absoluteString)!, param: "symbol")
			
			let viewController = self.navigationController?.storyboard?.instantiateViewController(withIdentifier: "StockSpecificNewViewController") as! StockSpecificNewViewController
			
			SearchForCurrentMultibagger.serchValue = symbol!
			SearchForCurrentMultibagger.instrument_4 = ""
			SearchForCurrentMultibagger.viewcontrollername = "CustomPushViewController"			
			
			User.navigation.pushViewController(viewController, animated: false)
			
		}
		return true
	}

}
