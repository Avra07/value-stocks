//
//  RewardsViewController.swift
//  DLevels
//
//  Created by Dynamic-MacBook-1 on 14/09/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class RewardsViewController: UIViewController {
    
    @IBOutlet weak var collecVw: UICollectionView!
    @IBOutlet weak var foot_total_rewards: UILabel!
    @IBOutlet weak var rupees_image: UILabel!
    @IBOutlet weak var rupees_count: UILabel!
    @IBOutlet weak var back_arrow: UIImageView!
    @IBOutlet weak var headerrewards: UILabel!
    @IBOutlet weak var norewards: UILabel!
    @IBOutlet weak var howworks: UIButton!
    
    var rewardamount = 0
    var cardType = ""
    var cardstatus = ""
    var stockname = ""
    var price = ""
    var mcollectVw = [[NSDictionary]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let nib = UINib.init(nibName: "YearlyScratchCollectionViewCell", bundle: nil)
        self.collecVw.register(nib, forCellWithReuseIdentifier: "YearlyScratchCollectionViewCell")
        
        let nib1 = UINib.init(nibName: "yearlyunsratchedCollectionViewCell", bundle: nil)
        self.collecVw.register(nib1, forCellWithReuseIdentifier: "yearlyunsratchedCollectionViewCell")
        
        let nib2 = UINib.init(nibName: "monthlyunscratchCollectionViewCell", bundle: nil)
        self.collecVw.register(nib2, forCellWithReuseIdentifier: "monthlyunscratchCollectionViewCell")
        
        let nib3 = UINib.init(nibName: "monthlyScratchedCollectionViewCell", bundle: nil)
        self.collecVw.register(nib3, forCellWithReuseIdentifier: "monthlyScratchedCollectionViewCell")
        
        // self.rupees_count.text = self.mcollectVw[0][1].value(forKey: "Reward_Amount") as! String
        
        scratcheduser  (completion: { (dataset) in
            
            self.mcollectVw = dataset
            self.rupees_count.text = self.mcollectVw[1][0].value(forKey: "Reward_Amount") as? String ?? "0"
            if self.rupees_count.text == "0"{
                self.norewards.isHidden = false
                self.collecVw.isHidden = true
            }else {
                self.norewards.isHidden = true
                
            }
            
            
            
            self.collecVw.reloadData()
            
        })
    }
    func scratcheduser (completion : @escaping ([[NSDictionary]])->())  {
        
        let obj = WebService()
        let paremeters =  "vsc_user_id=\(User.userId)&date_from=&date_to="
        
        //actInd.startAnimating()User.userId
        
        var result = [[NSDictionary]]()
        
        obj.callWebServices(url: Urls.get_user_scratchcard_list, methodName: "POST", parameters: paremeters, istoken: false, tokenval: User.token) { (returnValue, jsonData) in
            
            print("Json Data is :\(jsonData)")
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                if jsonData.value(forKey: "response") as? [[NSDictionary]] != nil{
                    result = jsonData.value(forKey: "response") as! [[NSDictionary]]
                    
                    
                    
                    return completion(result)
                }
                
            }
            
        }
        
        
    }
    
    @IBAction func btnback(_ sender: UIButton) {
       let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DlHomeViewController") as? DlHomeViewController
        User.navigation.pushViewController(vc!, animated: true)
        
    }
    
}
extension RewardsViewController :UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if self.mcollectVw.count > 0                  {
            return self.mcollectVw[0].count
        }else{
            return 0
        }
        
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellsize = CGSize(width: (collectionView.bounds.width - (3*10))/2, height: 150)
        return cellsize
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let sectionInset = UIEdgeInsetsMake(10, 10, 10, 10)
        return sectionInset
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collecVw{
            let data = self.mcollectVw[0][indexPath.row]
            let card_type = data.value(forKey: "vsc_card_type") as! String
            
            let card_status = data.value(forKey:  "vsc_scratch_status") as! String
            
            let stock_name = data.value(forKey:  "vsc_stock_name") as! String
            
            //let Price = i.value(forKey:  "Reward_Amount")
            //self.price = Price as! String
            
            if card_type.uppercased() == "MONTHLY" && card_status.uppercased() == "SCRATCHED"{
                //UNSCRATCHEDYearly
                let cell = collecVw.dequeueReusableCell(withReuseIdentifier: "monthlyScratchedCollectionViewCell", for: indexPath) as! monthlyScratchedCollectionViewCell
                cell.lblrupeecount.text = (data.value(forKey: "vsc_price") as! String)
                cell.lbltext.text = stock_name
                cell.status.text = (data.value(forKey: "vsc_status") as! String)
                if cell.status.text == "INVALID"{
                    cell.imag2.image = UIImage.init(named: "invalid")
                    
                    
                }else if cell.status.text == "PROCESSING"{
                    cell.imag2.image = UIImage.init(named: "processing")
                }else if cell.status.text == "PROCESSED"{
                    cell.imag2.image = UIImage.init(named: "processed")
                }else{
                    cell.imag2.isHidden = true
                    cell.status.isHidden = true
                }
                cell.awakeFromNib()
                
                
                return cell
            }
            if card_type.uppercased() == "MONTHLY" && card_status.uppercased() == "UNSCRATCHED"{
                //UNSCRATCHEDYearly
                let cell = collecVw.dequeueReusableCell(withReuseIdentifier: "monthlyunscratchCollectionViewCell", for: indexPath) as! monthlyunscratchCollectionViewCell
                cell.lblrupeecount.text = (data.value(forKey: "vsc_price") as! String)
                cell.lbltext.text = stock_name
                cell.awakeFromNib()
                
                
                return cell
            }
            if card_type.uppercased() == "YEARLY" && card_status.uppercased() == "SCRATCHED"{
                
                
                let cell = collecVw.dequeueReusableCell(withReuseIdentifier: "YearlyScratchCollectionViewCell", for: indexPath) as! YearlyScratchCollectionViewCell
                cell.lblrupeecount.text = (data.value(forKey: "vsc_price") as! String)
                cell.lbltext.text = stock_name
                cell.status.text = (data.value(forKey: "vsc_status") as! String)
                if cell.status.text == "INVALID"{
                    cell.imag2.image = UIImage.init(named: "invalid")
                    
                    
                }else if cell.status.text == "PROCESSING"{
                    cell.imag2.image = UIImage.init(named: "processing")
                }else if cell.status.text == "PROCESSED"{
                    cell.imag2.image = UIImage.init(named: "processed")
                }else{
                    cell.imag2.isHidden = true
                    cell.status.isHidden = true
                }
                cell.awakeFromNib()
                
                
                return cell
            }
            if card_type.uppercased() == "YEARLY" && card_status.uppercased() == "UNSCRATCHED"{
                //UNSCRATCHEDYearly
                let cell = collecVw.dequeueReusableCell(withReuseIdentifier: "yearlyunsratchedCollectionViewCell", for: indexPath) as! yearlyunsratchedCollectionViewCell
                cell.lblrupeecount.text = (data.value(forKey: "vsc_price") as! String)
                cell.lbltext.text = stock_name
                cell.awakeFromNib()
                
                
                return cell
            }
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == collecVw{
            let data = self.mcollectVw[0][indexPath.row]
            let card_type = data.value(forKey: "vsc_card_type") as! String
            
            let card_status = data.value(forKey:  "vsc_scratch_status") as! String
            
            let stock_name = data.value(forKey:  "vsc_stock_name") as! String
            if User.fundaccount != "" {
            
        
            if  let guruProfileController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Scratchcard_popup_ViewController") as? Scratchcard_popup_ViewController{
                
                
                if card_type.uppercased() == "YEARLY" && card_status.uppercased() == "UNSCRATCHED"{
                    

                    guruProfileController.rupee = data.value(forKey: "vsc_price") as! String
                    guruProfileController.stockname = stock_name
                    guruProfileController.newImage =  UIImage.init(named: "yearly")
                    guruProfileController.status = card_status
                    guruProfileController.type = card_type.uppercased()
                    
                }
               
                if card_type.uppercased() == "MONTHLY" && card_status.uppercased() == "UNSCRATCHED"{
                    if User.fundaccount != "" {
                    guruProfileController.rupee = data.value(forKey: "vsc_price") as! String
                    guruProfileController.stockname = stock_name
                    guruProfileController.newImage =  UIImage.init(named: "montlly")
                    guruProfileController.status = card_status
                    guruProfileController.type = card_type.uppercased()
                   }else{
                    
                   }
                }
                
                if card_type.uppercased() == "YEARLY" && card_status.uppercased() == "SCRATCHED"{
                    
                    guruProfileController.rupee = data.value(forKey: "vsc_price") as! String
                    guruProfileController.stockname = stock_name
                    guruProfileController.newImage =  UIImage.init(named: "yearly")
                    guruProfileController.status = card_status
                    guruProfileController.type = card_type.uppercased()
                }
                if card_type.uppercased() == "MONTHLY" && card_status.uppercased() == "SCRATCHED"{
                    
                    guruProfileController.rupee = data.value(forKey: "vsc_price") as! String
                    guruProfileController.stockname = stock_name
                    guruProfileController.newImage =  UIImage.init(named: "montlly")
                    guruProfileController.status = card_status
                    guruProfileController.type = card_type.uppercased()
                }
                User.navigation.present(guruProfileController, animated: true, completion: nil)
            }
            }else{
                let guruProfileController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "bankAddViewController") as? bankAddViewController
                User.navigation.present(guruProfileController!, animated: true, completion: nil)
            }
        }
        
        
    }
    
    
}
