//
//  Scratchcard_popup_ViewController.swift
//  DLevels
//
//  Created by Dynamic-MacBook-1 on 14/09/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//
import UIKit
import ScratchCard

class Scratchcard_popup_ViewController: UIViewController, ScratchCardImageViewDelegate {
    
    
    @IBOutlet weak var cross: UIImageView!
    @IBOutlet weak var icon2: UIImageView!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var earnupto: UILabel!
    @IBOutlet weak var earnupto_rs: UILabel!
    @IBOutlet weak var youwon: UILabel!
    @IBOutlet weak var wonrs: UILabel!
    @IBOutlet weak var thisworth: UILabel!
    @IBOutlet weak var sharename: UILabel!
    @IBOutlet weak var vew: UIView!
    @IBOutlet weak var scratchcard: ScratchCardImageView!
    var eraseProgres : Float?
    var ishold = true
    var rupee = ""
    var stockname = ""
    var type = ""
    var status = ""
    var scratchCard: ScratchUIView!
    var newImage: UIImage!
    /*
     
     @IBOutlet weak var scratchCard: ScratchCardImageView!
     
     override func viewDidLoad() {
     super.viewDidLoad()
     
     scratchCard.image = UIImage(color: UIColor.gray, size: scratchCard.frame.size)
     scratchCard.lineType = .square
     scratchCard.lineWidth = 20
     scratchCard.delegate = self
     }
     
     func scratchCardImageViewDidEraseProgress(eraseProgress: Float) {
     
     print(eraseProgress)
     if eraseProgress > 0.3{
     scratchCard.isHidden = true
     print("you won")
     }
     }
     
     
     
     
     
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        self.wonrs.text = "₹" + rupee
        self.sharename.text = stockname
        let tap = UITapGestureRecognizer(target: self, action: #selector(wasTapped(sender:)))
        tap.numberOfTapsRequired = 1 // Default value
        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(tap)
        
        
        scratchcard.image = UIImage(named: "yearly_un")
        scratchcard.lineType = .square
        scratchcard.lineWidth = 20
        scratchcard.delegate = self
        
        
        
        
        if status.uppercased() == "SCRATCHED" && type.uppercased() == "MONTHLY"{
            self.icon.isHidden = true
            scratchcard.image = UIImage(named: "Transparent_Card")
        }
        if status.uppercased() == "SCRATCHED" && type.uppercased() == "YEARLY"{
            self.icon2.isHidden = true
            scratchcard.image = UIImage(named: "Transparent_Card")
        }
        
        if status.uppercased() == "UNSCRATCHED"{
            // vew.isHidden = true
            if type.uppercased() == "MONTHLY"{
                
                scratchcard.image = UIImage(named: "montly_un")
            }else{
                scratchcard.image = UIImage(named: "yearly_un")
                self.earnupto_rs.text = "200-400"
            }
        }else{
            
            vew.isHidden = false
            self.earnupto.isHidden = true
            self.earnupto_rs.isHidden = true
            
        }
        
        
        
        
        /* let lblimg = UIImageView()
         lblimg.image = self.newImage
         lblimg.frame = CGRect(x: 105, y:80, width: 50, height: 50)
         lblimg.isHidden = true
         
         
         let lblrupee = UILabel()
         lblrupee.text  = "₹" + self.rupee
         lblrupee.font =  UIFont(name: "HelveticaNeue-Bold", size: 15.0 )
         lblrupee.frame = CGRect(x: 100, y:150, width: 100, height: 20)
         lblrupee.textColor = UIColor.init(red: 40/256, green: 76/256, blue: 90/256 ,alpha: 1)
         lblrupee.isHidden = true
         
         let lbldetails = UILabel()
         lbldetails.text  = "You Worth One Share Of"
         lbldetails.frame = CGRect(x: 60, y: 160, width: 170, height:60)
         lbldetails.numberOfLines = 0
         lbldetails.font = UIFont(name: "HelveticaNeue", size: 13.0)
         lbldetails.textColor = UIColor.init(red: 40/256, green: 76/256, blue: 90/256 ,alpha: 1)
         lbldetails.isHidden = true
         
         
         let lblstock = UILabel()
         lblstock.text  = self.stockname
         lblstock.frame = CGRect(x: 80, y: 200, width: 300, height: 20)
         lblstock.font = UIFont(name: "HelveticaNeue-Bold", size: 15.0 )
         lblstock.textColor = UIColor.init(red: 40/256, green: 76/256, blue: 90/256 ,alpha: 1)
         lblstock.isHidden = true
         
         scratchCard.addSubview(lblimg)
         scratchCard.addSubview(lblstock)
         scratchCard.addSubview(lbldetails)
         scratchCard.addSubview(lblrupee)
         
         
         
         */
        
        
        
        
        scratchcard.delegate = self
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    //Scratch Ended Event(optional)
    
    
    func showAlert() {
       
        
        earnupto.text = "Congratulations!!"
        earnupto_rs.text = "This cash back reward will be credited to your initial source of payment mode within 5-7 working days"
        
    }
    
    func scratched (completion : @escaping ([[NSDictionary]])->())  {
        
        let obj = WebService()
        let paremeters = "vsc_id=\(User.userId)&scratch_status=SCRATCHED"
        
        //actInd.startAnimating()
        
        var result = [[NSDictionary]]()
        
        obj.callWebServices(url: Urls.update_scratch_card_status, methodName: "POST", parameters: paremeters, istoken: false, tokenval: User.token) { (returnValue, jsonData) in
            
            print("Json Data is :\(jsonData)")
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                if jsonData.value(forKey: "response") as? [[NSDictionary]] != nil{
                    result = jsonData.value(forKey: "response") as! [[NSDictionary]]
                    
                    
                    return completion(result)
                }
                
            }
            
        }
        
        
    }
    
    func scratchCardImageViewDidEraseProgress(eraseProgress: Float) {
        
        print(eraseProgress)
        eraseProgres = eraseProgress
        
        if Float(eraseProgress) > 0.3{
            scratchcard.isHidden = true
            showAlert()
        }
        
        
    }
    
    
    
    
    
    @IBAction func croosyellow(_ sender: Any) {
        
        if ishold == true && status.uppercased() == "UNSCRATCHED" {
            
            if Float(eraseProgres ?? 0) > 0.0 {
                scratchcard.isHidden = true
                
                showAlert()
            }else {
                dismissCurrentPage()
            }
            
            ishold = false
        }else{
            dismissCurrentPage()
        }
    }
    
    @IBAction func btnCross_Action(_ sender: UIButton) {
        
        dismissCurrentPage()
       
        
    }
    
    //MARK: DISMISS CURRENT PAGE
    private func dismissCurrentPage(){
        dismiss(animated: true, completion: nil)
    }
    func wasTapped(sender: UITapGestureRecognizer) {
        print("tapped")
    }
    
    
}
