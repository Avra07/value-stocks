//
//  PrivacyPolicyViewController.swift
//  DLevels
//
//  Created by MacMini2 on 06/06/18.
//  Copyright © 2018 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class PrivacyPolicyViewController: UIViewController {

    @IBOutlet weak var btnHamburger: UIButton!
    @IBOutlet weak var webView: UIWebView!
    
    //var isShowingMenu = false
    //var instanceOfLeftSlideMenu = LeftSlideMenu()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let url = URL(string: "https://www.dynamiclevels.com/vsapp-html/vs-privacy.html")
        webView.loadRequest(URLRequest(url: url!))
        
    }
    
    @IBAction func searchClick(_ sender: Any) {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        self.navigationController?.pushViewController(viewController, animated: false)
    }
    
    
    
    @IBAction func backButton_Action(_ sender: UIButton) {
        
		let viewCont = self.storyboard?.instantiateViewController(withIdentifier: "DlHomeViewController") as! DlHomeViewController
		self.navigationController?.pushViewController(viewCont, animated: false)
        
    }
}
