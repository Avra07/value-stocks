//
//  FilteredStocksViewController.swift
//  DLevels
//
//  Created by SAIKAT GHOSH on 07/07/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit
import MaterialComponents


class CustomResultCell: UITableViewCell {
    
    @IBOutlet weak var mStockLbl: UILabel!
    @IBOutlet weak var mSectorLbl: UILabel!
    @IBOutlet weak var mRecommendationLbl: UILabel!
    
    
    
    @IBOutlet weak var mChangeLbl: UILabel!
}

// protocol used for sending data back
protocol SwitchDataManageDelegate: class {
    func switchDataInformation(lossMark: String, lessVol: String)
}



class FilteredStocksViewController: UIViewController,UITableViewDelegate, UITableViewDataSource, SelectedRecommendationDelegate {
    
    
    
    
    @IBOutlet weak var mResultsTblVw: UITableView!
    @IBOutlet weak var mStocksCountLbl: UILabel!
    @IBOutlet weak var mStocksAvgLbl: UILabel!
    @IBOutlet weak var mReturnPerionBtn: UIButton!
    @IBOutlet weak var mSortByBtn: UIButton!
    @IBOutlet weak var mSortByImg: UIImageView!
    @IBOutlet weak var mNoDataView: UIView!
    
    @IBOutlet weak var lossMakingSwitch: UISwitch!
    @IBOutlet weak var lowVolumeSwitch: UISwitch!
    
    @IBOutlet weak var lblSortBy: UILabel!
    
    
    weak var switchDelegate: SwitchDataManageDelegate?
    
    var islossMaking = "Y"
    var islessVolume = "Y"
    var ourRecommend = ""
    
    @IBOutlet weak var vw: UIView!
    @IBOutlet weak var headingName: UILabel!
    var mResults = [FilteredStocksPerformance]()
    var mOriginalResults = [FilteredStocksPerformance]()
    var mAppliedFilters = [String]()
    var mRecommArr = [String]()
    var mRecommFiltered = ""
    
    var avg_count = 0
    var avg_sum = 0.00
    var stocks_avg = 0.00
    
    var heading = ""
    var selectedRecommendation: String = ""
    var paremeters = ""
    
    
    var col = "1Y %"
    var sort_by = "Stocks(A to Z)"
    var isSortByHtoL = true
    fileprivate var dataArray = [FilteredStocksPerformance]()
    
    
    var invoiceInfo: [String: AnyObject]!
    var invoiceComposer: InvoiceComposer!
    var HTMLContent: String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mNoDataView.isHidden = true
        self.headingName.text = heading
        
        loadtableView()
        
        lossMakingSwitch.addTarget(self, action: #selector(switchChanged), for: UIControlEvents.valueChanged)
        lossMakingSwitch.tag = 1
        
        lowVolumeSwitch.addTarget(self, action: #selector(switchChanged), for: UIControlEvents.valueChanged)
        lowVolumeSwitch.tag = 2
        
        
        
        if islossMaking == "Y" {
            lossMakingSwitch.isOn  = true
        }else {
            lossMakingSwitch.isOn = false
        }
        
        if islessVolume == "Y" {
            lowVolumeSwitch.isOn  = true
        }else {
            lowVolumeSwitch.isOn = false
        }
        
        vw.layer.shadowColor = UIColor.gray.cgColor
        vw.layer.shadowOpacity = 0.5
        vw.layer.shadowRadius = 3
        vw.layer.shadowOffset = CGSize(width: 0, height: 3)
        
        /*getData(completion: {result in
         mResults = result
         self.mResultsTblVw.reloadData()
         self.mStocksCountLbl.text = "\(mResults.count)"
         self.stocks_avg = self.avg_sum // / Double(self.avg_count)
         self.mStocksAvgLbl.text = "\(self.stocks_avg)"
         });*/
        //// Do any additional setup after loading the view.
        
    }
    
    func recommendationList(recommedList: String) {
        ourRecommend = recommedList
        loadtableView()
    }
    
    
    func removeDuplicates(array: [String]) -> [String] {
        var encountered = Set<String>()
        var result: [String] = []
        for value in array {
            if encountered.contains(value) {
                // Do not add a duplicate element.
            }
            else {
                // Add value to the set.
                encountered.insert(value)
                // ... Append the value.
                result.append(value)
            }
        }
        return result
    }
    
    @IBAction func btn_Disclaimer(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let disclaimerController = storyBoard.instantiateViewController(withIdentifier: "DisclaimerViewController") as! DisclaimerViewController
        disclaimerController.disclaimerUrl = DisclaimerUrl.stock_screener
        disclaimerController.disclaimerHeader = "Stock Screener"
        User.navigation.pushViewController(disclaimerController, animated: true)
    }
    
    
    func loadtableView() {
        
        
        let removedSpace = selectedFilter.queryText.replacingOccurrences(of: " ", with: "%20")
        let removedAmp = removedSpace.replacingOccurrences(of: "&", with: "%26")
        
        
        paremeters = "lossMaking=\(islossMaking)&lessVolume=\(islessVolume)&ver=new&recommendation=\(ourRecommend)&param=\(removedAmp)"
        print(paremeters)
        let obj_performance = FilteredStocksPerformance()
        //print(self.fname)
        obj_performance.getData(param: paremeters, completion: { (dataset) in
            
            self.mResults = dataset
            self.mOriginalResults = dataset
            if self.mResults.count == 0{
                self.mNoDataView.isHidden = false
            } else{
                
                if self.ourRecommend == "" {
                    self.mRecommArr.removeAll()
                    for arr in self.mResults {
                        self.mRecommArr.append(arr.CategoryLong)
                    }
                    
                    self.mRecommArr = self.removeDuplicates(array: self.mRecommArr)
                }
                
                
                
                self.mStocksCountLbl.text = "\(self.mResults.count)"
                self.mNoDataView.isHidden = true
                
                self.isSortByHtoL = true
                self.sortByParticular(particular: "Stock")
                
            }
            
            self.setAvg()
            
            self.mResultsTblVw.reloadSections(IndexSet(integer: 0), with: .automatic)
        })
    }
    
    func switchChanged(mySwitch: UISwitch) {
        if mySwitch.isOn {
            if mySwitch.tag == 1 {
                self.islossMaking = "Y"
            }else{
                self.islessVolume = "Y"
            }
            
            loadtableView()
            
        }else {
            confirmSwitch(switchBtn: mySwitch)
        }
        
    }
    
    @IBAction func btn_Home(_ sender: Any) {
        let homeController = self.storyboard?.instantiateViewController(withIdentifier: "DlHomeViewController") as! DlHomeViewController
        self.navigationController?.pushViewController(homeController, animated: false)
    }
    
    
    func confirmSwitch(switchBtn: UISwitch) {
        
        var msg = ""
        if switchBtn.tag == 1 {
            msg = "Are you sure you want to include loss making stocks?"
        }else{
            msg = "Are you sure you want to include low volume stocks?"
        }
        
        // Declare Alert message
        let dialogMessage = UIAlertController(title: "", message: msg, preferredStyle: .alert)
        
        // Create OK button with action handler
        let ok = UIAlertAction(title: "YES", style: .default, handler: { (action) -> Void in
            print("Ok button tapped")
            switchBtn.isOn = false
            
            
            if switchBtn.tag == 1 {
                self.islossMaking = "N"
            }else{
                self.islessVolume = "N"
            }
            
            self.loadtableView()
            
        })
        
        // Create Cancel button with action handlder
        let cancel = UIAlertAction(title: "CANCEL", style: .cancel) { (action) -> Void in
            print("Cancel button tapped")
            switchBtn.isOn = true
        }
        
        //Add OK and Cancel button to dialog message
        dialogMessage.addAction(ok)
        dialogMessage.addAction(cancel)
        
        // Present dialog message to user
        self.present(dialogMessage, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "recommendationList" {
            let secondViewController = segue.destination as! RecomendationPopUpViewController
            secondViewController.recommendDelegate = self
            
            
            if self.ourRecommend != "" {
                
                var data = [String]()
                data = ourRecommend.components(separatedBy: ",")
                secondViewController.arrSelectedSector = data
            }
            
            
            
        }
    }
    
    
    
    @IBAction func filter(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func share(_ sender: Any) {
        
        
        let pdfFilePath = self.mResultsTblVw.exportAsPdfFromTable()
        print(pdfFilePath)
        
        createInvoiceAsHTML()
        
        
        
        
        /*
         UIGraphicsBeginImageContext(view.frame.size)
         view.layer.render(in: UIGraphicsGetCurrentContext()!)
         let image = UIGraphicsGetImageFromCurrentImageContext()
         UIGraphicsEndImageContext()
         
         let text = "Be your own Research Analyst.*\n\nDo your own research by applying filters on 30+ fundamental parameters in a unique \"Filter Tool\" to find your Value Stocks.\n\nDownload the Value Stocks App to apply filters and find your stocks now: https://www.dynamiclevels.com/dlevels-app.php"
         
         var imagesToShare = [AnyObject]()
         imagesToShare.append(!)
         imagesToShare.append(text as AnyObject)
         
         let activityViewController = UIActivityViewController(activityItems: imagesToShare , applicationActivities: nil)
         activityViewController.popoverPresentationController?.sourceView = self.view
         present(activityViewController, animated: true, completion: nil)
         
         */
    }
    
    func getDirectoryPath() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
        
    }
    
    func loadPDFAndShare(documentoPath: String){
        
        let fileManager = FileManager.default
        // let documentoPath = (self.getDirectoryPath() as NSString).appendingPathComponent("tablePdf.pdf")
        
        if fileManager.fileExists(atPath: documentoPath){
            
            
            let text = "Be your own Research Analyst.*\n\nDo your own research by applying filters on 30+ fundamental parameters in a unique \"Filter Tool\" to find your Value Stocks.\n\nDownload the Value Stocks App to apply filters and find your stocks now: https://www.dynamiclevels.com/dlevels-app.php"
            
            var fileToShare = [AnyObject]()
            let fileURL = URL(fileURLWithPath: documentoPath)
            fileToShare.append(fileURL as AnyObject)
            fileToShare.append(text as AnyObject)
            
            let activityViewController = UIActivityViewController(activityItems: fileToShare , applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            present(activityViewController, animated: true, completion: nil)
            
            
        }
        else {
            print("document was not found")
        }
    }
    
    @IBAction func refine(_ sender: Any) {
        
        switchfilteroptions.lossMark = self.islossMaking
        switchfilteroptions.lessvol = self.islessVolume
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func back(_ sender: Any) {
        
        switchfilteroptions.lossMark = self.islossMaking
        switchfilteroptions.lessvol = self.islessVolume
        
        
        //switchDelegate?.switchDataInformation(lossMark: self.islossMaking, lessVol: self.islessVolume)
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sortAction(_ sender: Any) {
        let actionSheeController = UIAlertController(title:"Sort By", message: "", preferredStyle: .actionSheet)
        actionSheeController.view.tintColor = UIColor(red: 247/255, green: 196/255, blue: 58/255, alpha: 1)
        
        actionSheeController.addAction(UIAlertAction(title: "Stocks(A to Z)", style: .default, handler: { (success) in
            self.lblSortBy.text = "Sort By: Stocks "
            self.mSortByImg.image = UIImage(named: "low")
            self.sort_by = "Stocks(A to Z)"
            UIView.animate(withDuration: 0.4) {
                self.sortLogic()
                self.view.layoutIfNeeded()
            }
        }))
        
        actionSheeController.addAction(UIAlertAction(title: "Stocks(Z to A)", style: .default, handler: { (success) in
            //self.mSortByBtn.setTitle("Sort By: Stocks", for: .normal)
            self.lblSortBy.text = "Sort By: Stocks "
            self.mSortByImg.image = UIImage(named: "high")
            self.sort_by = "Stocks(Z to A)"
            UIView.animate(withDuration: 0.4) {
                self.sortLogic()
                self.view.layoutIfNeeded()
            }
        }))
        
        actionSheeController.addAction(UIAlertAction(title: "Sector(A to Z)", style: .default, handler: { (success) in
            //self.mSortByBtn.setTitle("Sort By: Sector", for: .normal)
            self.lblSortBy.text = "Sort By: Sector "
            self.mSortByImg.image = UIImage(named: "low")
            self.sort_by = "Sector(A to Z)"
            UIView.animate(withDuration: 0.4) {
                self.sortLogic()
                self.view.layoutIfNeeded()
            }
        }))
        
        
        actionSheeController.addAction(UIAlertAction(title: "Sector(Z to A)", style: .default, handler: { (success) in
            //self.mSortByBtn.setTitle("Sort By: Sector", for: .normal)
            self.lblSortBy.text = "Sort By: Sector "
            self.mSortByImg.image = UIImage(named: "high")
            self.sort_by = "Sector(Z to A)"
            UIView.animate(withDuration: 0.4) {
                self.sortLogic()
                self.view.layoutIfNeeded()
            }
        }))
        
        actionSheeController.addAction(UIAlertAction(title: "Recommendation(A to Z)", style: .default, handler: { (success) in
            //self.mSortByBtn.setTitle("Sort By: Recommendation", for: .normal)
            self.lblSortBy.text = "Sort By: Recommendation "
            self.mSortByImg.image = UIImage(named: "low")
            self.sort_by = "Recommendation(A to Z)"
            UIView.animate(withDuration: 0.4) {
                self.sortLogic()
                self.view.layoutIfNeeded()
            }
        }))
        
        actionSheeController.addAction(UIAlertAction(title: "Recommendation(Z to A)", style: .default, handler: { (success) in
            //self.mSortByBtn.setTitle("Sort By: Recommendation", for: .normal)
            self.lblSortBy.text = "Sort By: Recommendation "
            self.mSortByImg.image = UIImage(named: "high")
            self.sort_by = "Recommendation(Z to A)"
            UIView.animate(withDuration: 0.4) {
                self.sortLogic()
                self.view.layoutIfNeeded()
            }
        }))
        
        actionSheeController.addAction(UIAlertAction(title: "Change%(High to Low)", style: .default, handler: { (success) in
            //self.mSortByBtn.setTitle("Sort By: Change%", for: .normal)
            self.lblSortBy.text = "Sort By: Change % "
            self.mSortByImg.image = UIImage(named: "low")
            self.sort_by = "Change%(High to Low)"
            UIView.animate(withDuration: 0.4) {
                self.sortLogic()
                self.view.layoutIfNeeded()
            }
        }))
        
        
        actionSheeController.addAction(UIAlertAction(title: "Change%(Low to High)", style: .default, handler: { (success) in
            //self.mSortByBtn.setTitle("Sort By: Change%", for: .normal)
            self.lblSortBy.text = "Sort By: Change % "
            self.mSortByImg.image = UIImage(named: "high")
            self.sort_by = "Change%(Low to High)"
            UIView.animate(withDuration: 0.4) {
                self.sortLogic()
                self.view.layoutIfNeeded()
            }
        }))
        
        
        actionSheeController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (success) in
        }) )
        
        // mone rakho
        self.present(actionSheeController, animated: false) {
            
        }
    }
    
    @IBAction func changePeriod(_ sender: Any) {
        
        let actionSheet = MDCActionSheetController(title: "", message: "")
        
        let dataSource: [String] = ["1D %","1W %","1M %","3M %","6M %","1Y %","2Y %","3Y %","4Y %","5Y %"]
        
        for data in dataSource {
        
            let newaction = MDCActionSheetAction(title: data, image: nil, handler: { (success) in
                self.mReturnPerionBtn.setTitle(data, for: .normal)
                self.col = data
                self.setAvg()
                UIView.animate(withDuration: 0.4) {
                    self.sortLogic()
                    self.view.layoutIfNeeded()
                }
                
            })
                actionSheet.addAction(newaction)
        }
        User.navigation.present(actionSheet, animated: true) {
            
        }
        
        
        /*
        
        
        
        let actionSheeController = UIAlertController(title:"Data Fields", message: "", preferredStyle: .actionSheet)
        actionSheeController.view.tintColor = UIColor(red: 247/255, green: 196/255, blue: 58/255, alpha: 1)
        
        actionSheeController.addAction(UIAlertAction(title: "1D %", style: .default, handler: { (success) in
            self.mReturnPerionBtn.setTitle("1D %", for: .normal)
            self.col = "1D %"
            self.setAvg()
            UIView.animate(withDuration: 0.4) {
                self.sortLogic()
                self.view.layoutIfNeeded()
            }
        }))
        
        actionSheeController.addAction(UIAlertAction(title: "1W %", style: .default, handler: { (success) in
            self.mReturnPerionBtn.setTitle("1W %", for: .normal)
            self.col = "1W %"
            self.setAvg()
            UIView.animate(withDuration: 0.4) {
                self.sortLogic()
                self.view.layoutIfNeeded()
            }
        }))
        
        actionSheeController.addAction(UIAlertAction(title: "1M %", style: .default, handler: { (success) in
            self.mReturnPerionBtn.setTitle("1M %", for: .normal)
            self.col = "1M %"
            self.setAvg()
            UIView.animate(withDuration: 0.4) {
                self.sortLogic()
                self.view.layoutIfNeeded()
            }
        }))
        
        
        actionSheeController.addAction(UIAlertAction(title: "3M %", style: .default, handler: { (success) in
            self.mReturnPerionBtn.setTitle("3M %", for: .normal)
            self.col = "3M %"
            self.setAvg()
            UIView.animate(withDuration: 0.4) {
                self.sortLogic()
                self.view.layoutIfNeeded()
            }
        }))
        
        actionSheeController.addAction(UIAlertAction(title: "6M %", style: .default, handler: { (success) in
            self.mReturnPerionBtn.setTitle("6M %", for: .normal)
            self.col = "6M %"
            self.setAvg()
            UIView.animate(withDuration: 0.4) {
                self.sortLogic()
                self.view.layoutIfNeeded()
            }
        }))
        
        actionSheeController.addAction(UIAlertAction(title: "1Y %", style: .default, handler: { (success) in
            self.mReturnPerionBtn.setTitle("1Y %", for: .normal)
            self.col = "1Y %"
            self.setAvg()
            UIView.animate(withDuration: 0.4) {
                self.sortLogic()
                self.view.layoutIfNeeded()
            }
        }))
        
        actionSheeController.addAction(UIAlertAction(title: "2Y %", style: .default, handler: { (success) in
            self.mReturnPerionBtn.setTitle("2Y %", for: .normal)
            self.col = "2Y %"
            self.setAvg()
            UIView.animate(withDuration: 0.4) {
                self.sortLogic()
                self.view.layoutIfNeeded()
            }
        }))
        
        actionSheeController.addAction(UIAlertAction(title: "3Y %", style: .default, handler: { (success) in
            self.mReturnPerionBtn.setTitle("3Y %", for: .normal)
            self.col = "3Y %"
            self.setAvg()
            UIView.animate(withDuration: 0.4) {
                self.sortLogic()
                self.view.layoutIfNeeded()
            }
        }))
        
        actionSheeController.addAction(UIAlertAction(title: "4Y %", style: .default, handler: { (success) in
            self.mReturnPerionBtn.setTitle("4Y %", for: .normal)
            self.col = "4Y %"
            self.setAvg()
            UIView.animate(withDuration: 0.4) {
                self.sortLogic()
                self.view.layoutIfNeeded()
            }
        }))
        
        
        actionSheeController.addAction(UIAlertAction(title: "5Y %", style: .default, handler: { (success) in
            self.mReturnPerionBtn.setTitle("5Y %", for: .normal)
            self.col = "5Y %"
            self.setAvg()
            UIView.animate(withDuration: 0.4) {
                self.sortLogic()
                self.view.layoutIfNeeded()
            }
        }))
        
        
        actionSheeController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (success) in
        }) )
        
        // mone rakho
        self.present(actionSheeController, animated: false) {
            
        }*/
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        SearchForCurrentMultibagger.serchValue = mResults[indexPath.row].value(forKey: "Blb_Symbol") as! String
        SearchForCurrentMultibagger.instrument_4 = mResults[indexPath.row].value(forKey: "Dispaly_Name") as! String
        SearchForCurrentMultibagger.viewcontrollername = "FilteredStocks"
        
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "StockSpecificNewViewController") as! StockSpecificNewViewController
        self.navigationController?.pushViewController(viewController, animated: false)
        
    }
    
    
    public func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60
        
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return mResults.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:CustomResultCell?
        
        
        cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! CustomResultCell
        cell?.selectionStyle = .none
        
        let stock = mResults[indexPath.row]
        
        /*cell?.mStockLbl.text = stock.value(forKey: "Dispaly_Name") as? String
         cell?.mSectorLbl.text = stock.value(forKey: "Sector") as? String
         cell?.mRecommendationLbl.text = stock.value(forKey: "CategoryLong") as? String*/
        let stockText = stock.Dispaly_Name
        
        let attributedString = NSMutableAttributedString.init(string: stockText)
        
        attributedString.addAttribute(NSUnderlineStyleAttributeName, value: 1, range: NSRange.init(location: 0, length: attributedString.length))
        cell?.mStockLbl.attributedText = attributedString
        //cell?.mStockLbl.text = stock.Dispaly_Name
        cell?.mStockLbl.sizeToFit()
        cell?.mSectorLbl.text = stock.Sector
        cell?.mSectorLbl.sizeToFit()
        cell?.mRecommendationLbl.text = stock.CategoryLong
        var _chng = ""
        /*if self.col == "1D %" {
         _chng = stock.value(forKey: "_1") as? String ?? "--"
         } else if self.col == "1W %" {
         _chng = stock.value(forKey: "1W_Change") as? String ?? "--"
         }  else if self.col == "1M %" {
         _chng = stock.value(forKey: "1M_Change") as? String ?? "--"
         }else if self.col == "3M %" {
         _chng = stock.value(forKey: "3M_Change") as? String ?? "--"
         }else if self.col == "6M %" {
         _chng = stock.value(forKey: "6M_Change") as? String ?? "--"
         }else if self.col == "1Y %" {
         _chng = stock.value(forKey: "1Y_Change") as? String ?? "--"
         }else if self.col == "3Y %" {
         _chng = stock.value(forKey: "3Y_Change") as? String ?? "--"
         }else if self.col == "5Y %" {
         _chng = stock.value(forKey: "3Y_Change") as? String ?? "--"
         }*/
        
        if self.col == "1D %" {
            _chng = "\(stock._1D)"
        } else if self.col == "1W %" {
            _chng = "\(stock._1W)"
        }  else if self.col == "1M %" {
            _chng = "\(stock._1M)"
        }else if self.col == "3M %" {
            _chng = "\(stock._3M)"
        }else if self.col == "6M %" {
            _chng = "\(stock._6M)"
        }else if self.col == "1Y %" {
            _chng = "\(stock._1Y)"
        }else if self.col == "2Y %" {
            _chng = "\(stock._2Y)"
        }else if self.col == "3Y %" {
            _chng = "\(stock._3Y)"
        }else if self.col == "4Y %" {
            _chng = "\(stock._4Y)"
        }else if self.col == "5Y %" {
            _chng = "\(stock._5Y)"
        }
        
        if(_chng=="-999.0"){
            _chng = "--"
        }
        
        if _chng != "--" {
            cell?.mChangeLbl.text = String(format: "%.2f", Double(_chng)!)
        } else {
            cell?.mChangeLbl.text = _chng
        }
        
        
        
        
        if _chng != "--"{
            avg_count = avg_count+1;
            var performance: Double = 0.00
            performance = Double(_chng)!
            avg_sum = avg_sum + performance
            
        }
        
        return cell!
        
    }
    
    
    //MARK: Average FUNCTION
    private func setAvg(){
        avg_count = 0
        avg_sum = 0.00
        stocks_avg = 0.00
        for stock in mResults{
            var _chng = "--"
            if self.col == "1D %" {
                _chng = "\(stock._1D)"
            } else if self.col == "1W %" {
                _chng = "\(stock._1W)"
            }  else if self.col == "1M %" {
                _chng = "\(stock._1M)"
            }else if self.col == "3M %" {
                _chng = "\(stock._3M)"
            }else if self.col == "6M %" {
                _chng = "\(stock._6M)"
            }else if self.col == "1Y %" {
                _chng = "\(stock._1Y)"
            }else if self.col == "2Y %" {
                _chng = "\(stock._2Y)"
            }else if self.col == "3Y %" {
                _chng = "\(stock._3Y)"
            }else if self.col == "4Y %" {
                _chng = "\(stock._4Y)"
            }else if self.col == "5Y %" {
                _chng = "\(stock._5Y)"
            }
            
            if(_chng=="-999.0"){
                _chng = "--"
            }
            
            if _chng != "--"{
                avg_count = avg_count+1;
                var performance: Double = 0.00
                performance = Double(_chng)!
                avg_sum = avg_sum + performance
            }
        }
        
        stocks_avg = avg_sum / Double(avg_count)
        mStocksAvgLbl.text = String(format: "%.2f", stocks_avg) + "%"
    }
    
    
    
    //MARK: SORT FUNCTION
    private func sortLogic(){
        
        if sort_by == "Stocks(A to Z)"{
            isSortByHtoL = true
            sortByParticular(particular: "Stock")
        } else if sort_by == "Stocks(Z to A)"{
            isSortByHtoL = false
            sortByParticular(particular: "Stock")
        } else if sort_by == "Sector(A to Z)"{
            isSortByHtoL = true
            sortByParticular(particular: "Sector")
        } else if sort_by == "Sector(Z to A)"{
            isSortByHtoL = false
            sortByParticular(particular: "Sector")
        } else if sort_by == "Recommendation(A to Z)"{
            isSortByHtoL = true
            sortByParticular(particular: "Recommendation")
        } else if sort_by == "Recommendation(Z to A)"{
            isSortByHtoL = false
            sortByParticular(particular: "Recommendation")
        } else if sort_by == "Change%(High to Low)"{
            isSortByHtoL = true
            sortByChange()
        } else if sort_by == "Change%(Low to High)"{
            isSortByHtoL = false
            sortByChange()
        }
        
    }
    
    //MARK: SORT By Change FUNCTION
    private func sortByChange(){
        
        //let isSortByHtoL = false
        
        if col == "1D %" {
            mResults = isSortByHtoL ? mResults.sorted (by: {$0._1D > $1._1D}) : mResults.sorted (by: {$0._1D < $1._1D})
            self.mResultsTblVw.reloadData()
        } else if col == "1W %" {
            mResults = isSortByHtoL ? mResults.sorted (by: {$0._1W > $1._1W}) : mResults.sorted (by: {$0._1W < $1._1W})
            self.mResultsTblVw.reloadData()
        }else if col == "1M %" {
            mResults = isSortByHtoL ? mResults.sorted (by: {$0._1M > $1._1M}) : mResults.sorted (by: {$0._1M < $1._1M})
            self.mResultsTblVw.reloadData()
        } else if col == "3M %" {
            mResults = isSortByHtoL ? mResults.sorted (by: {$0._3M > $1._3M}) : mResults.sorted (by: {$0._3M < $1._3M})
            self.mResultsTblVw.reloadData()
        } else if col == "6M %" {
            mResults = isSortByHtoL ? mResults.sorted (by: {$0._6M > $1._6M}) : mResults.sorted (by: {$0._6M < $1._6M})
            self.mResultsTblVw.reloadData()
        } else if col == "1Y %" {
            mResults = isSortByHtoL ? mResults.sorted (by: {$0._1Y > $1._1Y}) : mResults.sorted (by: {$0._1Y < $1._1Y})
            self.mResultsTblVw.reloadData()
        } else if col == "2Y %" {
            mResults = isSortByHtoL ? mResults.sorted (by: {$0._2Y > $1._2Y}) : mResults.sorted (by: {$0._2Y < $1._2Y})
            self.mResultsTblVw.reloadData()
        }else if col == "3Y %" {
            mResults = isSortByHtoL ? mResults.sorted (by: {$0._3Y > $1._3Y}) : mResults.sorted (by: {$0._3Y < $1._3Y})
            self.mResultsTblVw.reloadData()
        }else if col == "4Y %" {
            mResults = isSortByHtoL ? mResults.sorted (by: {$0._4Y > $1._4Y}) : mResults.sorted (by: {$0._4Y < $1._4Y})
            self.mResultsTblVw.reloadData()
        }  else if col == "5Y %" {
            mResults = isSortByHtoL ? mResults.sorted (by: {$0._5Y > $1._5Y}) : mResults.sorted (by: {$0._5Y < $1._5Y})
            self.mResultsTblVw.reloadData()
        }
    }
    
    
    //MARK: SORT By Particular FUNCTION
    private func sortByParticular(particular:String){
        
        
        if particular == "Stock" {
            mResults = isSortByHtoL ? mResults.sorted (by: {$0.Dispaly_Name < $1.Dispaly_Name}) : mResults.sorted (by: {$0.Dispaly_Name > $1.Dispaly_Name})
            self.mResultsTblVw.reloadData()
        } else if particular == "Sector" {
            mResults = isSortByHtoL ? mResults.sorted (by: {$0.Sector < $1.Sector}) : mResults.sorted (by: {$0.Sector > $1.Sector})
            self.mResultsTblVw.reloadData()
        }else if particular == "Recommendation" {
            mResults = isSortByHtoL ? mResults.sorted (by: {$0.CategoryLong < $1.CategoryLong}) : mResults.sorted (by: {$0.CategoryLong > $1.CategoryLong})
            self.mResultsTblVw.reloadData()
        }
    }
    
    func createInvoiceAsHTML() {
        invoiceComposer = InvoiceComposer()
        if let invoiceHTML = invoiceComposer.renderInvoice(filtered_by: mAppliedFilters, items: mResults, percent_col: self.col) {
            
            HTMLContent = invoiceHTML
            if let filePath: String = invoiceComposer.exportHTMLContentToPDF(HTMLContent: HTMLContent) {
                
                loadPDFAndShare(documentoPath: filePath)
            }
            
            
            
            
        }
    }
    
    
    
    
    //MARK: GET DATA
    func getData (completion : @escaping ([NSDictionary])->()) {
        
        let obj = WebService()
        
        let paremeters = "lossMaking=Y&lessVolume=Y&param=op2|>|500;"
        
        //actInd.startAnimating()
        
        obj.callWebServices(url: Urls.get_filter_result, methodName: "POST", parameters: paremeters, istoken: false, tokenval: User.token) { (returnValue, jsonData) in
            
            var result : [NSDictionary]
            result=[NSDictionary]()
            print("Json Data is :\(jsonData)")
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                if jsonData.value(forKey: "response") as! [NSDictionary] != nil{
                    result = jsonData.value(forKey: "response") as! [NSDictionary]
                    return completion(result)
                }
                return completion(result)
            } else{
                
            }
            
        }
    }
    
}

//MARK: COLLECTION VIEW DELEGATE AND DATA SOURCE
extension FilteredStocksViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return selectedFilter.dispalyQueryArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let pendingCell = collectionView.dequeueReusableCell(withReuseIdentifier: "filterItem", for: indexPath)
        let contenView = pendingCell.viewWithTag(100) as! UIView
        let filter     = pendingCell.viewWithTag(101) as! UILabel
        
        filter.addCornerRadius(value: 4)
        
        
        
        contenView.addCornerRadius(value: 4)
        
        filter.text = selectedFilter.dispalyQueryArr[indexPath.row]
        
        
        return pendingCell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        /*let sizeDynamic  = ["str" sizeWithFont:[UIFont fontWithName:@"Helvetica Nue" size:14] constrainedToSize:CGSizeMake(CGFLOAT_MAX,CGFLOAT_MAX) lineBreakMode:NSLineBreakByWordWrapping];*/
        
        let myString: NSString = selectedFilter.dispalyQueryArr[indexPath.row] as NSString
        let size: CGSize = myString.size(attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14.0)])
        
        return CGSize(width: size.width+20, height: collectionView.bounds.height)
        /*arrPendings.count == 1 ? CGSize(width: collectionView.bounds.width , height: collectionView.bounds.height) : CGSize(width: collectionView.bounds.width - 20, height: collectionView.bounds.height)*/
    }
    
}

extension UITableView {
    
    
    
    // Export pdf from UITableView and save pdf in drectory and return pdf file path
    func exportAsPdfFromTable() -> String {
        
        let originalBounds = self.bounds
        self.bounds = CGRect(x:originalBounds.origin.x, y: originalBounds.origin.y, width: self.contentSize.width, height: self.contentSize.height)
        let pdfPageFrame = CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.contentSize.height)
        
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, pdfPageFrame, nil)
        UIGraphicsBeginPDFPageWithInfo(pdfPageFrame, nil)
        guard let pdfContext = UIGraphicsGetCurrentContext() else { return "" }
        self.layer.render(in: pdfContext)
        UIGraphicsEndPDFContext()
        self.bounds = originalBounds
        // Save pdf data
        return self.saveTablePdf(data: pdfData)
        
    }
    
    // Save pdf file in document directory
    func saveTablePdf(data: NSMutableData) -> String {
        
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let docDirectoryPath = paths[0]
        let pdfPath = docDirectoryPath.appendingPathComponent("tablePdf.pdf")
        if data.write(to: pdfPath, atomically: true) {
            return pdfPath.path
        } else {
            return ""
        }
    }
}


