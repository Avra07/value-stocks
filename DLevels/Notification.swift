//
//  notification.swift
//  DLevels
//
//  Created by SAIKAT GHOSH on 17/07/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit
import NotificationCenter

class Notification: UIViewController {
    
    @IBOutlet weak var mtbl: UITableView!
    @IBOutlet weak var llbb: UILabel!
    
    var iscomingfrom = true
    var mResult : [NSDictionary] = []
    var mload :[NSDictionary] = []
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getData(completion: { (dataset) in
            self.mResult = dataset

            self.mtbl.reloadData()
        })
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.mtbl.reloadData()
    }
    
    @IBAction func back(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: false)
    }
    
    
    
    
    //MARK: GET DATA
    func getData (completion : @escaping ([NSDictionary])->()) {
        
        let obj = WebService()
        var result : [NSDictionary] = [NSDictionary]()
        let paremeters = "user_id=\(User.userId)"//"user_id=211297"
        
        //actInd.startAnimating()
        
        obj.callWebServices(url: Urls.get_notification, methodName: "GET", parameters: paremeters, istoken: false, tokenval: User.token) { (returnValue, jsonData) in
            
            print("Json Data is :\(jsonData)")
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                if jsonData.value(forKey: "response") as? [NSDictionary] != nil{
                    result = jsonData.value(forKey: "response") as! [NSDictionary]
                    return completion(result)
                }
                
            }
            
        }
        
        
    }
    
    
}

extension Notification: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.mResult.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cel = mtbl.dequeueReusableCell(withIdentifier:"pop") as! nCell
        
        let notification = mResult[indexPath.row]
        
        
        if let _header = notification.value(forKey: "un_header") as? String{
            cel.llbb2.text! = _header
        }
        
        if let un_notification_text = notification.value(forKey: "un_notification_text") as? String{
            cel.llbb3.text! = un_notification_text
        }
        if let un_updt_time = notification.value(forKey: "un_updt_time") as? String{
            cel.llbb4.text! = un_updt_time
        }
        if let user_id = notification.value(forKey: "un_id") as? String{
            
            
            if let read = notification.value(forKey: "readFlag") as? String{
                if read == "Y"{
                    cel.img2.isHidden = true
                }else {
                    cel.img2.isHidden = false
                }
            }
        }
        cel.selectionStyle = .none
        return cel
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        let notification_id: String = (self.mResult[indexPath.row].value(forKey: "un_id") as? String)!
        // print("puja")
        print("id :\(notification_id)")
        // let user_id = "211297"
        let paremeters = "user_id=\(User.userId)&notification=\(notification_id)"
        //let paremeters = "user_id=\(user_id)&notification=15"
        print("param" + paremeters)
        
        
        
        let obj = WebService()
        obj.callWebServices(url: Urls.notification_read_status, methodName: "GET", parameters: paremeters, istoken: false, tokenval: User.token) { (returnValue, jsonData) in
            
            var result : [NSDictionary]
            result=[NSDictionary]()
            print("Json new data :\(jsonData)")
            self.getData(completion: { (dataset) in
                self.mResult = dataset
                
                self.mtbl.reloadData()
            })
        
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                
                if let levelname = self.mResult[indexPath.row].value(forKey: "un_action") as? String{
                    if levelname == "customWebView"
                    {
                        var first = ""
                        var last = ""
                        
                        if let str = self.mResult[indexPath.row].value(forKey: "un_data") as? String {
                            
                            if str.contains("|") {
                                let array = str.components(separatedBy: "|")
                                if array.count > 1 {
                                    first = array[0]
                                    last = array[1]
                                }
                            }else {
                                last = str
                            }
                        }
                        
                        
                        
                            
                        if let lastarry = last.components(separatedBy: ";") as? [String] {
                            if lastarry.count > 1 {
                                let l1 = lastarry[0]
                                let l2 = lastarry[1]
                                NotificationData.url = l2
                            }
                        }
                            
                        
                        
                        
                        let s4 = UIStoryboard.init(name:"Main" , bundle: nil)
                        let obj4 = s4.instantiateViewController(withIdentifier:"CustomPushViewController") as! CustomPushViewController
                        obj4.comingfrom = self.iscomingfrom
                        self.navigationController?.pushViewController(obj4, animated: true)
                    }
                    else
                    {
                        
                        
                        let s3 = UIStoryboard.init(name:"Main" , bundle: nil)
                        let obj3 = s3.instantiateViewController(withIdentifier:"StockMarketTodayViewController") as! StockMarketTodayViewController
                        obj3.commingfrom = self.iscomingfrom
                        self.navigationController?.pushViewController(obj3, animated: true)
                    }
                }
            }
            
        }
        
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
}
