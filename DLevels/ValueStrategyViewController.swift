//
//  ValueStrategyViewController.swift
//  DLevels
//
//  Created by SAIKAT GHOSH on 20/08/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class ValueStrategyCell: UITableViewCell {
    
    @IBOutlet weak var headerText: UILabel!
    @IBOutlet weak var textDesc: UILabel!
    
}

class ValueStrategyViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var navigationText: UILabel!
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getDataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ValueStrategyCell") as! ValueStrategyCell
        cell.headerText.text = getDataArray[indexPath.row].highlightedText
        cell.textDesc.text = getDataArray[indexPath.row].desc
        cell.textDesc.textColor = Colors.customBlueColor
        return cell
    }
    
    
    @IBOutlet weak var displayList: UITableView!
    var getDataArray = [valueStrategy]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getData()
        // Do any additional setup after loading the view.
    }
    
    func getData() {
        getDataArray = []
        displayList.delegate = self
        displayList.dataSource = self
        let obj = WebService()
        
        let parameter = "guru_id=0"
        
        //actInd.startAnimating()
        
        obj.callWebServices(url: Urls.get_strategies, methodName: "GET", parameters: parameter, istoken: false, tokenval: User.token) { (returnValue, jsonData) in
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                let getDataArray = jsonData.value(forKey: "response") as! NSArray
                
                print("Guru_strategies: \(getDataArray)")
                for displayArrayDetail in getDataArray{
                    if let displayDetailDict = displayArrayDetail as? NSDictionary {
                        var strategyData = valueStrategy(highlightedText: "", desc: "")
                        if let highlightText = displayDetailDict.value(forKey: "sm_Name"){
                            strategyData.highlightedText = highlightText as! String
                        }
                        if let descDetail = displayDetailDict.value(forKey: "sm_Desc"){
                            let descString = (descDetail as! String).replacingOccurrences(of: "\\n", with: "\n", options: .literal, range: nil)
                            strategyData.desc = descString
                        }
                        self.getDataArray.append(strategyData)
                        OperationQueue.main.addOperation({
                            self.displayList.reloadData()
                        })
                    }
                }
            }
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        User.navigation.popViewController(animated: true)
    }
}




struct valueStrategy {
    var highlightedText: String
    var desc: String
}
