//
//  MFHomeTableViewCell.swift
//  DLevels
//
//  Created by Dlevels-MacMini1 on 25/05/18.
//  Copyright © 2018 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class MFHomeTableViewCell: UITableViewCell {
    
    class var identifier: String { return String(describing: self) }
    class var nib: UINib { return UINib(nibName: identifier, bundle: nil) }
    
    
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lbldesc: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureWithItem(item: MFCategories) {
        // this method will not be created or used in this example project
        // setImageWithURL(url: item.avatarImageURL)
        lbltitle.text = item.title
        lbldesc.text = item.desc
    }
    
    
}
