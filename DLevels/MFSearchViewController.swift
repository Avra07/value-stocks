//
//  MFSearchViewController.swift
//  DLevels
//
//  Created by Dlevels-MacMini1 on 28/05/18.
//  Copyright © 2018 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class MFSearchViewController: UIViewController {
    
    @IBOutlet weak var searchcontroller: UISearchBar!
    @IBOutlet weak var tblviewresult: UITableView!
    var tremvalstring = ""
    var response = [NSDictionary]()

    override func viewDidLoad() {
        super.viewDidLoad()
        AppManager().setStatusBarBackgroundColor()
        searchcontroller.delegate = self
        searchcontroller.becomeFirstResponder()
        searchcontroller.placeholder = "Search"
        tblviewresult.dataSource = self
        tblviewresult.delegate = self
        
        tblviewresult.estimatedRowHeight = 1000
        tblviewresult.rowHeight = UITableViewAutomaticDimension
        addDoneButtonOnKeyboard()
    }
    func addDoneButtonOnKeyboard() {
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction)
        )
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        searchcontroller.inputAccessoryView = doneToolbar
    }
    
    func doneButtonAction() {
        searchcontroller.resignFirstResponder()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        tblviewresult.dataSource = self
        tblviewresult.delegate = self
        
        tblviewresult.estimatedRowHeight = 1000
        tblviewresult.rowHeight = UITableViewAutomaticDimension
    }
    override func viewDidAppear(_ animated: Bool) {
        searchcontroller.becomeFirstResponder()
    }

    @IBAction func searchClose_Action(_ sender: Any) {
        
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Getting data
    func loadDataForResultView(searchval: String)
    {
        var keyword_Param = ""
        keyword_Param = searchval.replacingOccurrences(of: " ", with: "%20").replacingOccurrences(of: "&", with: "%26")
        
        let obj = WebService()
        let paremeters = "keyword=\(keyword_Param)"
        
        obj.callWebServices(url: Urls.mfsearch, methodName: "GET", parameters: paremeters, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
            
            print(jsonData)
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                let tempArray = jsonData.value(forKey: "response") as! NSArray
                
                self.response.removeAll()
                
                for dict in tempArray {
                    let dictValue = dict as! NSDictionary
                    self.response.append(dictValue)
                }
                
                print("Response count is: \(self.response.count)")
                
                //    DispatchQueue.main.async {
                
                if self.response.count > 0 {
                    self.tblviewresult.reloadData()
                } else {
                    
                    self.response.removeAll()
                    self.tblviewresult.reloadData()
                }
                
                // }
            }
            else
            {
                let alertobj = AppManager()
                alertobj.showAlert(title: "Error!", message: jsonData.value(forKey: "errmsg") as! String , navigationController: self.navigationController!)
            }
            
            
        }
        
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MFSearchViewController: UITableViewDataSource,UITableViewDelegate, UISearchBarDelegate , UINavigationControllerDelegate{
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if(searchText.characters.count > 2)
        {
            loadDataForResultView(searchval: searchText)
        }
        else
        {
            self.response.removeAll()
            self.tblviewresult.reloadData()
        }
        
        if(searchText == "")
        {
            self.response.removeAll()
            self.tblviewresult.reloadData()
            
        }
    }
    
    //MARK: Table View Delegate
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier:"searchcell", for: indexPath)
        cell.selectionStyle = .none
        
        cell.backgroundColor = indexPath.row % 2 == 0 ?  UIColor(red: 215/255, green: 222/255, blue: 225/255, alpha: 1.0) : UIColor(red: 236/255, green: 239/255, blue: 240/255, alpha: 1.0)
        
        let lblfund = cell.viewWithTag(1001) as! UILabel
        let lblcategory = cell.viewWithTag(1002) as! UILabel
        
        if let fund = self.response[indexPath.row].value(forKey: "Fund") {
            lblfund.text = fund as! String
            
        }
        if let category = self.response[indexPath.row].value(forKey: "Category") {
            lblcategory.text = category as! String
            
        }
        
        
        return cell
    }
    
    //MARK: Table View Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if response.count > 0 {
            
            return response.count
        } else {
            
            return 0
        }
        
        //return response.count
    }
    //MARK: Orientati
    func navigationControllerSupportedInterfaceOrientations(_ navigationController: UINavigationController) -> UIInterfaceOrientationMask {
        return .portrait
    }
    
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "MFSpecificPageViewController") as! MFSpecificPageViewController
        
        if let category = response[indexPath.row].value(forKey: "Category") as? String {
            viewController.Category = category
        }
        
        if let fund = response[indexPath.row].value(forKey: "Fund") as? String {
            viewController.Fund = fund
        }

        self.navigationController?.pushViewController(viewController, animated: false)
        
    }
    
    public func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 200
    
    }
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }

}
