//
//  UIColor+KVNContrast.h
//  KVNProgress
//
//  Created by Mohammad Ishtiyak on 20/04/18.
//  Copyright (c) 2014 Tech Exactly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface UIColor (KVNContrast)

- (UIStatusBarStyle)statusBarStyleConstrastStyle;

@end
