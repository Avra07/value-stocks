//
//  UIColor+KVNContrast.m
//  KVNProgress
//
//  Created by Mohammad Ishtiyak on 20/04/18.
//  Copyright (c) 2014 Tech Exactly. All rights reserved.
//

#import "UIColor+KVNContrast.h"

@implementation UIColor (KVNContrast)

- (UIStatusBarStyle)statusBarStyleConstrastStyle
{
	const CGFloat *componentColors = CGColorGetComponents(self.CGColor);
	CGFloat darknessScore = (((componentColors[0] * 255) * 299) + ((componentColors[1] * 255) * 587) + ((componentColors[2] * 255) * 114)) / 1000;
	
	if (darknessScore >= 125) {
		return UIStatusBarStyleDefault;
	}
	
	return UIStatusBarStyleLightContent;
}

@end
