//
//  UIImage+Empty.h
//  KVNProgress
//
//  Created by Mohammad Ishtiyak on 20/04/18.
//  Copyright (c) 2014 Tech Exactly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (KVNEmpty)

+ (UIImage *)emptyImage;

@end
