//
//  FPINewViewController.swift
//  DLevels
//
//  Created by MacMini2 on 05/10/18.
//  Copyright © 2018 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class FPINewViewController: UIViewController {

    var controllerArray : [UIViewController] = []
    
    @IBOutlet weak var webvw: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.nrifpipmsload(completion: {
            self.webvw.loadHTMLString(FPI_DATA.FPI_NEW, baseURL: nil)
        })
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func nrifpipmsload(completion : @escaping ()->())
    {
        let obj = WebService()
        let data = "ALL"
        let param = "ptype=\(data)"
        
        obj.callWebServices(url: Urls.NRIFPIPMS, methodName: "GET", parameters: param, istoken: false, tokenval: "") { (returnValue, jsonData) in
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                
                print(jsonData)
                
                let tempArray = jsonData.value(forKey: "response") as! [NSDictionary]
                
                
                for arr in tempArray
                {
                    
                    if(arr.value(forKey: "p_type") as! String == "PMS")
                    {
                        PMS_DATA.PMS1 = arr.value(forKey: "p_why") as! String
                        PMS_DATA.PMS2 = arr.value(forKey: "p_what") as! String
                        PMS_DATA.PMS3 = arr.value(forKey: "p_how") as! String
                    }
                    
                    if(arr.value(forKey: "p_type") as! String == "NRI")
                    {
                        NRI_DATA.NRI1 = arr.value(forKey: "p_what") as! String
                        NRI_DATA.NRI2 = arr.value(forKey: "p_why") as! String
                        NRI_DATA.NRI3 = arr.value(forKey: "p_how") as! String
                    }
                    
                    if(arr.value(forKey: "p_type") as! String == "FPI")
                    {
                        FPI_DATA.FPI1 = arr.value(forKey: "p_what") as! String
                        FPI_DATA.FPI2 = arr.value(forKey: "p_why") as! String
                        FPI_DATA.FPI3 = arr.value(forKey: "p_how") as! String
                    }
                    
                    if(arr.value(forKey: "p_type") as! String == "FPI NEW")
                    {
                        FPI_DATA.FPI_NEW = arr.value(forKey: "p_what") as! String
                    }
                    
                    if(arr.value(forKey: "p_type") as! String == "NRI NEW")
                    {
                        NRI_DATA.NRI_NEW = arr.value(forKey: "p_what") as! String
                    }
                }
                
                
            }
        }
        
        return completion()
    }

}
