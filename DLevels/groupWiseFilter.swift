//
//  groupWiseFilter.swift
//  DLevels
//
//  Created by Shailesh Saraf on 02/09/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import Foundation
class groupWiseFilter:NSObject {
    var group_id = ""
    var filter_section = ""
    var filter_name = ""
    var filter_type = ""
    var filter_id = ""
    var filter_options = ""
    var filter_default_value = ""
    var filter_default_min = ""
    var filter_default_max = ""
    var filter_default_operator = ""
    var filter_short_name = ""
    var filter_column_name = ""
    var info = ""
    override init(){
        
    }
    
    
    init(groupId:String,filterSection: String,filterName:String,filterType:String,filterId:String,filterOptions:String,filterDefaultValue:String,filterDefaultMin:String,filterDefaultMax:String, filterDefaultOperator:String,filterShortName:String,filterColumnName:String) {
        
        group_id = groupId
        filter_section = filterSection
        filter_name = filterName
        
        filter_type = filterType
        filter_section = filterSection
        filter_options = filterOptions
        
        
        filter_default_value = filterDefaultValue
        filter_default_min = filterDefaultMin
        filter_default_max = filterDefaultMax
        
        filter_default_operator = filterDefaultOperator
        filter_short_name = filterShortName
        filter_column_name = filterColumnName
    }
    
}
