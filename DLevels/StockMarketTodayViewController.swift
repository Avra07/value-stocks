//
//  StockMarketTodayViewController.swift
//  DLevels
//
//  Created by Rishi Sachdeva on 03/05/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class StockMarketTodayViewController: UIViewController, UIWebViewDelegate {

	@IBOutlet weak var navBarTitle: UINavigationItem!
	@IBOutlet weak var web: UIWebView!
	var strURL = "" //For get String URL with navigation
	var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
	var commingfrom = false
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		let label = UILabel(frame: CGRect(x:0, y:0, width:400, height:50))
		label.backgroundColor = .clear
		label.numberOfLines = 2
		label.font = UIFont.boldSystemFont(ofSize: 15.0)
		label.textAlignment = .left
		label.textColor = UIColor(hex:"284C5A")
		label.text = "Stock Market Today"
		self.navBarTitle.titleView = label
		
		
		
		web?.delegate = self// Add this line to set the delegate of webView
		addLoader()
	}
	override func viewWillAppear(_ animated: Bool) {
		AppManager().setStatusBarBackgroundColor()
	}
	
	override func viewDidAppear(_ animated: Bool) {
		
		actInd.startAnimating()
		let webService = WebService()
		let parameters = ""
		
		webService.callWebServices(url: Urls.getstckmarktodaypdf, methodName: "GET", parameters: parameters, istoken: false, tokenval: "", completion: { (success, dictData) in
			
			// print(dictData)
			
			var urlstr = ""
			if dictData.value(forKey: "error") as! NSInteger == 0 {
				
				
				
				let response = dictData.value(forKey: "response") as! String
				let path = "https://api.dynamiclevels.com//rss-feed-images//"
				
				
				let urlstr:String = "\(path)\(response.replacingOccurrences(of: " ", with: "%20"))"
				
				let url = URL(string: urlstr)
				let request = URLRequest(url: url! as URL)
				self.web.loadRequest(request)
				
				
				
				
				/*let pdfView = PDFView(frame: self.vwPDF.frame)
				
				pdfView.translatesAutoresizingMaskIntoConstraints = f
				alse
				
				self.vwPDF.addSubview(pdfView)
				
				pdfView.leadingAnchor.constraint(equalTo: self.vwPDF.safeAreaLayoutGuide.leadingAnchor).isActive = true
				pdfView.trailingAnchor.constraint(equalTo: self.vwPDF.safeAreaLayoutGuide.trailingAnchor).isActive = true
				pdfView.topAnchor.constraint(equalTo: self.vwPDF.safeAreaLayoutGuide.topAnchor).isActive = true
				pdfView.bottomAnchor.constraint(equalTo: self.vwPDF.safeAreaLayoutGuide.bottomAnchor).isActive = true
				
				guard let path = NSURL(string: urlstr) else { return }
				if let document = PDFDocument(url: path as URL) {
				pdfView.document = document
				pdfView.autoScales = true
				self.actInd.stopAnimating()
				}*/
			} else {
				self.actInd.stopAnimating()
			}
		})
	}
	
	func webViewDidFinishLoad(_ webView : UIWebView) {
		self.actInd.stopAnimating()
	}
	
	func addLoader() {
		actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0) //CGRectMake(0.0, 0.0, 40.0, 40.0);
		actInd.center = self.view.center
		actInd.hidesWhenStopped = true
		actInd.activityIndicatorViewStyle =
			UIActivityIndicatorViewStyle.gray
		self.view.addSubview(actInd)
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	@IBAction func onClick_back(_ sender: UIButton) {
        
        if NotificationData.category != "" {
            if commingfrom == false{
            let viewController = self.navigationController?.storyboard?.instantiateViewController(withIdentifier: "DlHomeViewController") as! DlHomeViewController
            
            User.navigation.pushViewController(viewController, animated: false)
            }else{
                 User.navigation.popViewController(animated: true)
                
            }
        }else {
            self.navigationController?.popViewController(animated: true)
        }
        
		
}

}
