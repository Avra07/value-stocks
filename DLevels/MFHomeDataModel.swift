//
//  MFHomeDataModel.swift
//  DLevels
//
//  Created by Dlevels-MacMini1 on 25/05/18.
//  Copyright © 2018 Dynamic-Mac-01. All rights reserved.
//

import Foundation
class MFHomeDataModel:NSObject {
    
    func getText(completion : @escaping (String)->()){
        let result: String = ""
        let obj = WebService()
        obj.callWebServices(url: Urls.mfdata, methodName: "POST", parameters: "", istoken: true, tokenval: User.token) { (returnValue, jsonData) in
            
            print(jsonData)
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                let tempArray = jsonData.value(forKey: "response") as! [NSDictionary]
                for arr in tempArray {
                    
                    if let ARNText = arr.value(forKey: "ARNText") as? String{
                        MFStaticData.ARNText = ARNText
                    }
                    
                    if let SEBILink1 = arr.value(forKey: "SEBILink1") as? String{
                        MFStaticData.SEBILink1 = SEBILink1
                    }
                    if let SEBILink2 = arr.value(forKey: "SEBILink2") as? String{
                        MFStaticData.SEBILink2 = SEBILink2
                    }
                    
                    if let SEBIText1 = arr.value(forKey: "SEBIText1") as? String{
                        MFStaticData.SEBIText1 = SEBIText1
                    }
                    if let SEBIText2 = arr.value(forKey: "SEBIText2") as? String{
                        MFStaticData.SEBIText2 = SEBIText2
                    }
                    
                    if let SmallCapPerf = arr.value(forKey: "SmallCapPerf") as? String{
                        MFStaticData.SmallCapPerf = SmallCapPerf
                    }
                    
                    if let ason_text = arr.value(forKey: "ason_text") as? String{
                        MFStaticData.ason_text = ason_text
                    }
                    
                }
            }
            return completion(result)
            
        }
    }
    
    
    func getData(completion : @escaping ([MFCategories])->()){
        var result = [MFCategories]()
        
        var category:String =  ""
        var desc:String =  ""
        var title:String =  ""
        
    // mfc_category, mfc_category_desc, mfc_category_title, mfc_updt_time, mfc_id
    
        let obj = WebService()
        obj.callWebServices(url: Urls.mfcategories, methodName: "POST", parameters: "", istoken: true, tokenval: User.token) { (returnValue, jsonData) in
            
            print(jsonData)
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                let tempArray = jsonData.value(forKey: "response") as! [NSDictionary]
                
                for arr in tempArray{
                    if let category_data = arr.value(forKey: "mfc_category") as? String{
                        category = category_data
                    }
                    
                    if let category_desc = arr.value(forKey: "mfc_category_desc") as? String{
                        desc = category_desc
                    }
                    
                    if let category_title = arr.value(forKey: "mfc_category_title") as? String{
                        title = category_title
                    }
                    
                    let obj_mfcategories = MFCategories(category: category, title: title, desc: desc)
                        
                        result.append(obj_mfcategories)
                }
            }
            return completion(result)
        }
    }
    
    
}
