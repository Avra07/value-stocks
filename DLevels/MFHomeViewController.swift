//
//  MFHomeViewController.swift
//  DLevels
//
//  Created by Dlevels-MacMini1 on 21/05/18.
//  Copyright © 2018 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class MFHomeViewController: UIViewController {
    
    @IBOutlet weak var vwLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var navBarTitle: UINavigationItem!
    @IBOutlet weak var btnSEBTText1: UIButton!
    @IBOutlet weak var btnSEBILink1HeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnSEBILink2HeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnSEBIText2: UIButton!
    @IBOutlet weak var tableVw: UITableView!
    @IBOutlet weak var navbar: UINavigationBar!
    
    fileprivate var dataArray = [MFCategories]()
    private let dataSource = MFHomeDataModel()
    
    let yourAttributes : [String: Any] = [
        NSFontAttributeName : UIFont.systemFont(ofSize: 14),
        NSForegroundColorAttributeName : UIColor.blue,
        NSUnderlineStyleAttributeName : NSUnderlineStyle.styleSingle.rawValue]
    //.styleDouble.rawValue, .styleThick.rawValue, .styleNone.rawValue
    
    
    var instanceOfLeftSlideMenu = LeftSlideMenu()
    var isShowingMenu = false
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    override func viewDidLoad() {
        super.viewDidLoad()
        let obj_MFHomeDM = MFHomeDataModel()
        AppManager().setStatusBarBackgroundColor()
        addLoader()
        //actInd.startAnimating()
        let label = UILabel(frame: CGRect(x:0, y:0, width:400, height:50))
        label.backgroundColor = .clear
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: 15.0)
        label.textAlignment = .left
        label.textColor = UIColor(hex:"284C5A")
        label.text = "Mutual Funds"
        self.navBarTitle.titleView = label
        tableVw?.register(MFHomeTableViewCell.nib, forCellReuseIdentifier: MFHomeTableViewCell.identifier)
        tableVw?.delegate = self
        tableVw?.dataSource = self
       	tableVw.tableFooterView = UIView()
        obj_MFHomeDM.getText(completion: { (txt) in
            obj_MFHomeDM.getData { (dataset) in
                self.dataArray = dataset
                self.tableVw.reloadData()
                print(MFStaticData.SEBIText1)
                print(MFStaticData.SEBIText2)
                
                let attributeString = NSMutableAttributedString(string: "\(MFStaticData.SEBIText1)",
                    attributes: self.yourAttributes)
                self.btnSEBTText1.setAttributedTitle(attributeString, for: .normal)
                
                
                
                let attribute2String = NSMutableAttributedString(string: "\(MFStaticData.SEBIText2)",
                    attributes: self.yourAttributes)
                self.btnSEBIText2.setAttributedTitle(attribute2String, for: .normal)
                
                
                if MFStaticData.SEBILink1 == "" {
                    self.btnSEBILink1HeightConstraint.constant = 0
                }
                if MFStaticData.SEBILink2 == "" {
                    self.btnSEBILink2HeightConstraint.constant = 0
                }
            }            
        })
        
        
    }
    func addLoader() {
        actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0) //CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.gray
        self.view.addSubview(actInd)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        AppManager().setStatusBarBackgroundColor()
        tableVw.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    
    
    @IBAction func humbergerClick(_ sender: Any) {
        
//        if !isShowingMenu {
//            instanceOfLeftSlideMenu.prepareScreenWithView(navigationController: self.navigationController!, viewSize: self.view)
//           vwLeadingConstraint.constant = 250
//            isShowingMenu = true
//        } else {
//
//            instanceOfLeftSlideMenu.hideSlideMenu(navigationController: self.navigationController!, viewSize: self.view)
//            vwLeadingConstraint.constant = 0
//            isShowingMenu = false
//        }
        
        self.navigationController?.popViewController(animated: true)
        
        
//        let viewCont = self.storyboard?.instantiateViewController(withIdentifier: "DlHomeViewController") as! DlHomeViewController
//        self.navigationController?.pushViewController(viewCont, animated: false)
    }
    
    @IBAction func searchClick(_ sender: Any) {
        let viewCont = self.storyboard?.instantiateViewController(withIdentifier: "MFSearchViewController") as! MFSearchViewController
        self.navigationController?.pushViewController(viewCont, animated: false)
    }
    
    
    @IBAction func btnSebiCerculer_Click(_ sender: Any) {
         UIApplication.shared.openURL(NSURL(string: MFStaticData.SEBILink1)! as URL)
    }
    
    @IBAction func btnSebiCercularLink2_Action(_ sender: Any) {
         UIApplication.shared.openURL(NSURL(string: MFStaticData.SEBILink2)! as URL)
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            let index:Int = sender as! Int
            if segue.identifier == "show" {
                let vc = segue.destination as! MFFundsTypeListViewController
                vc.Category = dataArray[index].category!
                vc.TitleVal    = dataArray[index].title!
            }
        }
}

extension MFHomeViewController: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier:
            MFHomeTableViewCell.identifier, for: indexPath) as? MFHomeTableViewCell
        {
            cell.configureWithItem(item: dataArray[indexPath.item])
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.dequeueReusableCell(withIdentifier:
            MFHomeTableViewCell.identifier, for: indexPath) as? MFHomeTableViewCell
        {
         self.performSegue(withIdentifier: "show", sender: indexPath.row)
        }
        
        
    }
    public func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 1000
    }
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
}






