//
//  StockFiltersViewController.swift
//  DLevels
//
//  Created by SAIKAT GHOSH on 01/07/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit
import MaterialComponents

class GroupCell:UITableViewCell{
    @IBOutlet weak var lblGroupName: UILabel!
    @IBOutlet weak var lblCounter: UILabel!
}


class StockFiltersViewController: UIViewController,UITableViewDataSource,UITableViewDelegate, DataEnteredDelegate, SwitchDataManageDelegate {
   
    
    func userDidEnterInformation(sectorlist: [String], indexPathRow: String, Section: String) {
        
    }
    
    func switchDataInformation(lossMark: String, lessVol: String) {
        
    }
    
   
        
        @IBOutlet weak var tblvwGroup: UITableView!
        @IBOutlet weak var tblvwfilters: UITableView!
        @IBOutlet weak var btnresetfilter: UIButton!
        @IBOutlet weak var btnapplyfilter: UIButton!
        
        var allFilterData = [NSDictionary]()
        var filterListDataSource = [groupWiseFilter]()
        var modifiedFilterList = [FilterResult]()
        
        var isComingFromGurus = false
        
        var currentindex = 0
        
        var toolbar:UIToolbar = UIToolbar()
        var txtview = UITextField()
        var dropdownCellHeight = 0
        
        var sectorListDict = [String]()
        
        override func viewDidLoad() {
            super.viewDidLoad()
            print("Guru Stock Rules : \(selectedFilter.dispalyQueryArr)")
            //  hideKeyboardOnTapOutside()
            
            btnresetfilter.layer.borderColor = UIColor.black.cgColor
            btnresetfilter.layer.borderWidth = 1
            btnresetfilter.layer.cornerRadius = 4
            
            
            btnapplyfilter.layer.borderWidth = 1
            btnapplyfilter.layer.cornerRadius = 4
            
            
            var nib = UINib.init(nibName: "dropdownTableViewCell", bundle: nil)
            self.tblvwfilters.register(nib, forCellReuseIdentifier: "dropdownTableViewCell")
            
            nib = UINib.init(nibName: "operatorFilterTableViewCell", bundle: nil)
            self.tblvwfilters.register(nib, forCellReuseIdentifier: "operatorFilterTableViewCell")
            
            nib = UINib.init(nibName: "multiSelectFilTableViewCell", bundle: nil)
            self.tblvwfilters.register(nib, forCellReuseIdentifier: "multiSelectFilTableViewCell")
            
            nib = UINib.init(nibName: "textFilterTableViewCell", bundle: nil)
            self.tblvwfilters.register(nib, forCellReuseIdentifier: "textFilterTableViewCell")
            
            if isComingFromGurus {
                if selectedFilter.queryText != "" {
                    selectedFilter.filterParameter.removeAll()
                    AppManager.GetSelectedFilterArr(queryText: selectedFilter.queryText, displayText: selectedFilter.displayText, completion: {result in
                        
                        
                    })
                }
            }else{
                selectedFilter.dispalyQueryArr.removeAll()
                selectedFilter.displayText = ""
                selectedFilter.filterParameter.removeAll()
                selectedFilter.queryText = ""
            }
            
            
           //  sdsd
            
            
            // Do any additional setup after loading the view.
            getGroupData(completion: { (status) in
                if status == true {
                    self.tblvwGroup.reloadData()
                    self.getFilterGroupWise(group_id: selectedFilter.filterGroupDataSource[0].group_id, completion: { (status) in
                        
                        self.tblvwfilters.reloadData()
                        let indexPath = IndexPath(row: 0, section: 0)
                        self.tblvwGroup.selectRow(at: indexPath, animated: true, scrollPosition: UITableViewScrollPosition.none)
                        
                    })
                    
                    
                } else {
                    
                }
            })
        }
        
        @IBAction func resetbtnclick(_ sender: Any) {
            
            getGroupData(completion: { (status) in
                if status == true {
                    self.tblvwGroup.reloadData()
                    
                    self.getFilterGroupWise(group_id: selectedFilter.filterGroupDataSource[0].group_id, completion: { (status) in
                        
                        selectedFilter.filterParameter.removeAll()
                        selectedFilter.dispalyQueryArr.removeAll()
                        selectedFilter.displayText = ""
                        selectedFilter.queryText = ""
                        
                        print("The filtered array : \(selectedFilter.dispalyQueryArr)")
                        
                        self.tblvwfilters.reloadData()
                    })
                    
                } else {
                    
                }
            })
        }
        
        @IBAction func applyfiltersclick(_ sender: Any) {
            
            AppManager.GetQueryDisplayText(filterParam: selectedFilter.filterParameter,completion: { (status) in
                if status == true {
                    
                }
            })
            
        }
    
    @IBAction func back(_ sender: Any) {
        
        //switchDelegate?.switchDataInformation(lossMark: self.islossMaking, lessVol: self.islessVolume)
        
        self.navigationController?.popViewController(animated: true)
    }
        
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            
            if segue.identifier == "show" {
                let secondViewController = segue.destination as! FilterPopUpViewController
                secondViewController.header = "Filtered Stock"
                secondViewController.items = selectedFilter.dispalyQueryArr
                secondViewController.query = selectedFilter.queryText
                secondViewController.islessVolumeParam = "Y"
                secondViewController.islossmakingParam = "Y"
                secondViewController.isComingFromGurus = isComingFromGurus
                
            }
        }
        
        
        override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
            if identifier == "show" {
                if selectedFilter.queryText == "" {
                    AppManager().showAlert(title:"Alert", message: "Minimum one filter is compulsory", navigationController: User.navigation)
                    
                    return false
                }
            }
            
            return true
        }
        
        
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
            if tableView == tblvwGroup{
                return selectedFilter.filterGroupDataSource.count
            }else{
                return filterListDataSource.count
            }
            
        }
    
    
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if tableView == tblvwGroup{
                
                let obj = tblvwGroup.dequeueReusableCell(withIdentifier: "GroupCell") as! GroupCell
                let str = selectedFilter.filterGroupDataSource[indexPath.row]
                obj.lblGroupName.text! = str.filter_section
                
                obj.selectionStyle = UITableViewCell.SelectionStyle.none
                
                
                if str.filter_count > 0 {
                    obj.lblCounter.text = String(str.filter_count)
                }else{
                    obj.lblCounter.text = ""
                }
                
                if(obj.isSelected){
                    obj.backgroundColor = UIColor.white
                }else{
                    obj.backgroundColor = UIColor.clear
                }
                
                
                return obj
            }
            
            
            if tableView == tblvwfilters {
                
                let data = self.filterListDataSource[indexPath.row] as groupWiseFilter
                
                if let filtertype:String = data.filter_type {
                    
                    if filtertype == "dropdown" {
                        if data.filter_options != "" {
                            let cell = tableView.dequeueReusableCell(withIdentifier: "dropdownTableViewCell") as! dropdownTableViewCell
                            if let sourceArr: [String] = data.filter_options.split(separator: ";").map({ String($0) }) {
                                cell.dataSource = sourceArr
                                cell.title = data.filter_name
                                cell.lblSortName.text = data.filter_short_name
                                cell.lblColName.text = data.filter_column_name
                                cell.group_id = data.group_id
                                cell.awakeFromNib()
                                
                                dropdownCellHeight = sourceArr.count * 70
                                return cell
                            }
                            
                        }
                    }
                    
                    if filtertype == "multi_select_list" {
                        
                        let cell = tableView.dequeueReusableCell(withIdentifier: "multiSelectFilTableViewCell") as! multiSelectFilTableViewCell
                        sectorData(completion: { (status) in
                            if status == true {
                                cell.dataSource = self.sectorListDict
                                cell.lblSortName.text = data.filter_short_name
                                cell.lblColName.text = data.filter_column_name
                                cell.group_id = data.group_id
                                cell.awakeFromNib()
                            }
                        })
                        return cell
                        
                        
                    }
                    
                    if filtertype == "text" {
                        
                        let cell = tableView.dequeueReusableCell(withIdentifier: "textFilterTableViewCell") as! textFilterTableViewCell
                        cell.lblTitle.text = data.filter_name
                        cell.lblSortName.text = data.filter_short_name
                        cell.lblColName.text = data.filter_column_name
                        cell.group_id = data.group_id
                        
                        for filterresult_Obj in selectedFilter.filterParameter {
                            if filterresult_Obj.filter_column_name == data.filter_column_name {
                                cell.txtValue.text = filterresult_Obj.filter_max
                            }
                        }
                        
                        cell.awakeFromNib()
                        return cell
                        
                        
                    }
                    
                    if filtertype == "" {
                        
                        let cell = tableView.dequeueReusableCell(withIdentifier: "operatorFilterTableViewCell") as! operatorFilterTableViewCell
                        
                        cell.lblTitle.text = data.filter_name
                        cell.btnoperater.setTitle(data.filter_default_operator, for: .normal)
                        cell.lblSortName.text = data.filter_short_name
                        cell.lblColName.text = data.filter_column_name
                        cell.info_str = data.info
                        cell.group_id = data.group_id
                        
                        
                        if data.filter_default_operator == "between" {
                            cell.minmaxvw.isHidden      = false
                            cell.singleviewvw.isHidden  = true
                        }else{
                            cell.minmaxvw.isHidden      = true
                            cell.singleviewvw.isHidden  = false
                        }
                        
                        
                        for filterresult_Obj in selectedFilter.filterParameter {
                            if filterresult_Obj.filter_column_name == data.filter_column_name {
                                
                                cell.btnoperater.setTitle(filterresult_Obj.filter_operator, for: .normal)
                                
                                if filterresult_Obj.filter_operator == "between" {
                                    cell.txtmin.text = filterresult_Obj.filter_min
                                    cell.txtmax.text = filterresult_Obj.filter_max
                                    
                                }else{
                                   cell.txtvalue.text = filterresult_Obj.filter_max
                                }
                                
                            }
                        }
                        
                        cell.awakeFromNib()
                        
                        return cell
                    }
                }
                
            }
            return UITableViewCell()
        }
        
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            if tableView == tblvwGroup{
                
                
                self.tblvwGroup.reloadData()
                print("Filter Tbl View Index \(indexPath)")
                let selectedCell:UITableViewCell = tableView.cellForRow(at: indexPath)!
                selectedCell.isSelected = true
                
                selectedCell.backgroundColor = UIColor.white
                
                
                
                
                
                getFilterGroupWise(group_id: selectedFilter.filterGroupDataSource[indexPath.row].group_id, completion: { (status) in
                    self.tblvwfilters.reloadData()
                })
            }
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            if tableView == tblvwGroup{
                return UITableViewAutomaticDimension
            }
            else{
                
                let data = self.filterListDataSource[indexPath.row] as groupWiseFilter
                if let filtertype = data.value(forKey: "filter_type") as? String {
                    
                    if filtertype == "multi_select_list" {
                        return tableView.frame.height
                    }else if filtertype == "dropdown" {
                        return CGFloat(dropdownCellHeight)
                    }  else {
                        return 90
                    }
                }
            }
            
            return 90
        }
    
    
    /*func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if tableView == tblvwGroup{
            let selectedCell:UITableViewCell = tableView.cellForRow(at: indexPath)!
            selectedCell.contentView.backgroundColor = UIColor.clear
        }
    }*/
    
    
    //colorForCellUnselected is just a var in my class
        
        func toggleMinus(){
            
            // Get text from text field
            if var text = txtview.text {
                if text.isEmpty == false{
                    
                    // Toggle
                    if text.hasPrefix("-") {
                        text = text.replacingOccurrences(of: "-", with: "")
                    }
                    else
                    {
                        text = "-\(text)"
                    }
                    
                    // Set text in text field
                    txtview.text = text
                }
            }
        }
        
        //MARK: GET DATA
        func sectorData (completion : @escaping (Bool)->()) {
            
            let obj = WebService()
            let paremeters = ""
            
            //actInd.startAnimating()
            
            obj.callWebServices(url: Urls.getsectorlist, methodName: "GET", parameters: paremeters, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
                
               print("Json Data is :  \(jsonData)")
                
                if jsonData.value(forKey: "errmsg") as! String == "" {
                    
                    let tempArray = jsonData.value(forKey: "response") as! [NSDictionary]
                    
                    for dict in tempArray {
                        self.sectorListDict.append((dict.value(forKey: "sector") as? String)!)
                    }
                    
                    return completion(true)
                    
                }
            }
        }
        
        func getFilterGroupWise(group_id: String, completion : @escaping (Bool)->()) {
            
            self.filterListDataSource.removeAll()
            
            // print(allFilterData)
            
            for dict in self.allFilterData {
                let obj_grpwsefil = groupWiseFilter()
                // self.info_value = dict.value(forKey: "filter_info") as? String ?? ""
                
                if group_id == dict.value(forKey: "group_id") as? String{
                    obj_grpwsefil.group_id = group_id
                    
                    if let filter_section = dict.value(forKey: "filter_section") as? String{
                        if filter_section == "" {
                            if let filter_name = dict.value(forKey: "filter_name") as? String{
                                obj_grpwsefil.filter_section = filter_name
                            }else{
                                obj_grpwsefil.filter_section = filter_section
                            }
                        }
                    }
                    
                    if let filter_name = dict.value(forKey: "filter_name") as? String{
                        if filter_name != "" {
                            obj_grpwsefil.filter_name = filter_name
                        }
                    }
                    
                    if let filter_options = dict.value(forKey: "filter_options") as? String{
                        if filter_options != "" {
                            obj_grpwsefil.filter_options = filter_options
                        }
                    }
                    
                    if let filter_type = dict.value(forKey: "filter_type") as? String{
                        if filter_type != "" {
                            obj_grpwsefil.filter_type = filter_type
                            
                        }
                    }
                    
                    if let filter_column_name = dict.value(forKey: "column_name") as? String{
                        if filter_column_name != "" {
                            obj_grpwsefil.filter_column_name = filter_column_name
                            
                        }
                    }
                    
                    if let filter_short_name = dict.value(forKey: "filter_short_name") as? String{
                        if filter_short_name != "" {
                            obj_grpwsefil.filter_short_name = filter_short_name
                            
                        }
                    }
                    if let filter_info = dict.value(forKey: "filter_info") as? String{
                        if filter_info != "" {
                            obj_grpwsefil.info = filter_info
                            
                        }
                    }
                    
                    if let filter_default_operator = dict.value(forKey: "default_operator") as? String{
                        if filter_default_operator != "" {
                            obj_grpwsefil.filter_default_operator = filter_default_operator
                        }
                    }
                    self.filterListDataSource.append(obj_grpwsefil)
                }
            }
            
            return completion(true)
            
        }
        
        
        
        func getGroupData (completion : @escaping (Bool)->()) {
            
            let obj = WebService()
            let paremeters = ""
            
            //actInd.startAnimating()
            
            obj.callWebServices(url: Urls.getfilterlist, methodName: "GET", parameters: paremeters, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
                
              print("Json Data is :  \(jsonData)")
                
                if jsonData.value(forKey: "errmsg") as! String == "" {
                    
                    let tempArray = jsonData.value(forKey: "response") as! [NSDictionary]
                    
                    if tempArray.count > 0 {
                        
                        selectedFilter.filterGroupDataSource.removeAll()
                        
                        
                        self.allFilterData = tempArray
                        for dict in tempArray {
                            var isAppend = true
                            let fGroup = filterGroup()
                            // self.info_value = dict.value(forKey: "filter_info") as? String ?? ""
                            
                            if let grpId = dict.value(forKey: "group_id") as? String{
                                
                                fGroup.group_id = grpId
                                if let filter_section = dict.value(forKey: "filter_section") as? String{
                                    if filter_section == "" {
                                        if let filter_name = dict.value(forKey: "filter_name") as? String{
                                            fGroup.filter_section = filter_name
                                        }
                                    }
                                    else {
                                        fGroup.filter_section = filter_section
                                    }
                                }
                            }
                            
                            fGroup.filter_count = 0
                            
                            for arr in selectedFilter.filterGroupDataSource{
                                if arr.group_id == fGroup.group_id {
                                    isAppend = false
                                    break
                                }
                            }
                            
                            if isAppend {
                                selectedFilter.filterGroupDataSource.append(fGroup)
                            }
                            
                            
                        }// loop end
                    }
                    
                    //print(self.filterGroupDataSource)
                    
                    return completion(true)
                    
                }
            }
        }
    }
    
    extension StockFiltersViewController : UITextFieldDelegate {
        
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            textField.resignFirstResponder()
            return true
        }
        
        
        func textFieldDidBeginEditing(_ textField: UITextField) {
            
            //init toolbar
            toolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 65))
            toolbar.barTintColor = #colorLiteral(red: 0.7812563181, green: 0.8036255836, blue: 0.8297665119, alpha: 1)
            txtview = textField
            
            //create left side empty space so that done button set on right side
            let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            
            
            let doneBtn: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(UITextField.doneButtonAction))
            
            let textbox_type = textField.accessibilityValue
            
            if (textbox_type == "value" || textbox_type == "min" || textbox_type == "max") {
                textField.keyboardType = .decimalPad
                
                let minusButton = UIButton()
                minusButton.frame = CGRect(x:0, y:0, width:120, height:40)
                minusButton.setTitle("-", for: .normal)
                minusButton.setTitle("-", for: .highlighted)
                
                minusButton.titleLabel?.textColor = UIColor(hex: "000000")
                
                minusButton.backgroundColor = UIColor.gray
                minusButton.layer.cornerRadius = 4.0
                minusButton.addTarget(self, action: #selector(toggleMinus), for: .touchUpInside)
                
                let leftBarButton = UIBarButtonItem(customView: minusButton)
                toolbar.setItems([flexSpace,leftBarButton,flexSpace, doneBtn], animated: false)
                
                
            }else{
                toolbar.setItems([flexSpace, doneBtn], animated: false)
            }
            
            toolbar.sizeToFit()
            textField.inputAccessoryView = toolbar
        }
        
        
        func textFieldDidEndEditing(_ textField: UITextField) {
            
            print("TextField did end editing method called")
            
            
            // save the text in the map using the stored row in the tag field
            
            let sec = Int(textField.accessibilityHint!)
            let row = Int(textField.tag)
            let textbox_type = textField.accessibilityValue
            
            if textbox_type == "max" {
                //stockFilterDict[sec!][row].setValue(textField.text, forKey: "max_value")
            } else if textbox_type == "min" {
                //stockFilterDict[sec!][row].setValue(textField.text, forKey: "min_value")
            }else {
                // stockFilterDict[sec!][row].setValue(textField.text, forKey: "modified_value")
            }
            //print(stockFilterDict[sec!][row]["modified_value"])
            
        }
}

extension UITextField{
    @IBInspectable var doneAccessory: Bool{
        get{
            return self.doneAccessory
        }
        set (hasDone) {
            if hasDone{
                addDoneButtonOnKeyboard()
            }
        }
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.inputAccessoryView = doneToolbar
    }
    
    func doneButtonAction()
    {
        self.resignFirstResponder()
    }
}

