//
//  InvoiceComposer.swift
//  DLevels
//
//  Created by SAIKAT GHOSH on 31/07/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit
class InvoiceComposer: NSObject {
    
    let pathToInvoiceHTMLTemplate = Bundle.main.path(forResource: "invoice", ofType: "html")
    
    let pathToSingleItemHTMLTemplate = Bundle.main.path(forResource: "single_item", ofType: "html")
    
    let pathToLastItemHTMLTemplate = Bundle.main.path(forResource: "last_item", ofType: "html")
    
    let senderInfo = "Gabriel Theodoropoulos<br>123 Somewhere Str.<br>10000 - MyCity<br>MyCountry"
    
    let dueDate = ""
    
    let paymentMethod = "Wire Transfer"
    
    let logoImageURL = "http://www.appcoda.com/wp-content/uploads/2015/12/blog-logo-dark-400.png"
    
    var invoiceNumber: String!
    
    var pdfFilename: String!
    
    
    override init() {
        super.init()
    }
    
    func mfRenderInvoice(filtered_by : [String], items: [MFFilteredStocksPerformance], percent_col: String) -> String! {
        
        do {
            // Load the invoice HTML template code into a String variable.
            var HTMLContent = try String(contentsOfFile: pathToInvoiceHTMLTemplate!)
            
            // Replace all the placeholders with real values except for the items.
            
            
            // Payment method.
            
            var filterItems = ""
            for i in 0..<filtered_by.count {
                filterItems += filtered_by[i] + "<br>"
            }
            
            HTMLContent = HTMLContent.replacingOccurrences(of: "#FILTER_OPTION#", with: filterItems)
            
            HTMLContent = HTMLContent.replacingOccurrences(of: "#Performance#", with: percent_col)
            
            
            // The invoice items will be added by using a loop.
            var allItems = ""
            
            // For all the items except for the last one we'll use the "single_item.html" template.
            // For the last one we'll use the "last_item.html" template.
            for i in 0..<items.count {
                var itemHTMLContent: String!
                
                // Determine the proper template file.
                if i != items.count - 1 {
                    itemHTMLContent = try String(contentsOfFile: pathToSingleItemHTMLTemplate!)
                }
                else {
                    itemHTMLContent = try String(contentsOfFile: pathToLastItemHTMLTemplate!)
                }
                
                // Replace the description and price placeholders with the actual values.
                itemHTMLContent = itemHTMLContent.replacingOccurrences(of: "#ITEM_DESC#", with: items[i].Dispaly_Name)
                
                // Format each item's price as a currency value.
                
                var _chng = ""
                
                if percent_col == "1D %" {
                    _chng = "\(items[i]._1D)"
                } else if percent_col == "1W %" {
                    _chng = "\(items[i]._1W)"
                }  else if percent_col == "1M %" {
                    _chng = "\(items[i]._1M)"
                }else if percent_col == "3M %" {
                    _chng = "\(items[i]._3M)"
                }else if percent_col == "6M %" {
                    _chng = "\(items[i]._6M)"
                }else if percent_col == "1Y %" {
                    _chng = "\(items[i]._1Y)"
                }else if percent_col == "3Y %" {
                    _chng = "\(items[i]._3Y)"
                }else if percent_col == "5Y %" {
                    _chng = "\(items[i]._5Y)"
                }
                
                if(_chng=="-999.0"){
                    _chng = "--"
                }
                
                if _chng != "--" {
                    itemHTMLContent = itemHTMLContent.replacingOccurrences(of: "#PRICE#", with: String(format: "%.2f", Double(_chng)!))
                } else {
                    itemHTMLContent = itemHTMLContent.replacingOccurrences(of: "#PRICE#", with: _chng)
                }
                
                itemHTMLContent = itemHTMLContent.replacingOccurrences(of: "#OUR_RECOMMENDATION#", with: items[i].CategoryLong)
                
                
                // Add the item's HTML code to the general items string.
                allItems += itemHTMLContent
            }
            
            // Set the items.
            HTMLContent = HTMLContent.replacingOccurrences(of: "#ITEMS#", with: allItems)
            
            // The HTML code is ready.
            return HTMLContent
            
        }
        catch {
            print("Unable to open and use HTML template files.")
        }
        
        return nil
    }
    
    
    func renderInvoice(filtered_by : [String], items: [FilteredStocksPerformance], percent_col: String) -> String! {
        
        do {
            // Load the invoice HTML template code into a String variable.
            var HTMLContent = try String(contentsOfFile: pathToInvoiceHTMLTemplate!)
            
            // Replace all the placeholders with real values except for the items.
            
            
            // Payment method.
            
            var filterItems = ""
            for i in 0..<filtered_by.count {
                filterItems += filtered_by[i] + "<br>"
            }
            
            HTMLContent = HTMLContent.replacingOccurrences(of: "#FILTER_OPTION#", with: filterItems)
            
            HTMLContent = HTMLContent.replacingOccurrences(of: "#Performance#", with: percent_col)
            
            
            // The invoice items will be added by using a loop.
            var allItems = ""
            
            // For all the items except for the last one we'll use the "single_item.html" template.
            // For the last one we'll use the "last_item.html" template.
            for i in 0..<items.count {
                var itemHTMLContent: String!
                
                // Determine the proper template file.
                if i != items.count - 1 {
                    itemHTMLContent = try String(contentsOfFile: pathToSingleItemHTMLTemplate!)
                }
                else {
                    itemHTMLContent = try String(contentsOfFile: pathToLastItemHTMLTemplate!)
                }
                
                // Replace the description and price placeholders with the actual values.
                itemHTMLContent = itemHTMLContent.replacingOccurrences(of: "#ITEM_DESC#", with: items[i].Dispaly_Name)
                
                // Format each item's price as a currency value.
                
                var _chng = ""
                
                if percent_col == "1D %" {
                    _chng = "\(items[i]._1D)"
                } else if percent_col == "1W %" {
                    _chng = "\(items[i]._1W)"
                }  else if percent_col == "1M %" {
                    _chng = "\(items[i]._1M)"
                }else if percent_col == "3M %" {
                    _chng = "\(items[i]._3M)"
                }else if percent_col == "6M %" {
                    _chng = "\(items[i]._6M)"
                }else if percent_col == "1Y %" {
                    _chng = "\(items[i]._1Y)"
                }else if percent_col == "3Y %" {
                    _chng = "\(items[i]._3Y)"
                }else if percent_col == "5Y %" {
                    _chng = "\(items[i]._5Y)"
                }
                
                if(_chng=="-999.0"){
                    _chng = "--"
                }
                
                if _chng != "--" {
                    itemHTMLContent = itemHTMLContent.replacingOccurrences(of: "#PRICE#", with: String(format: "%.2f", Double(_chng)!))
                } else {
                    itemHTMLContent = itemHTMLContent.replacingOccurrences(of: "#PRICE#", with: _chng)
                }
                
                itemHTMLContent = itemHTMLContent.replacingOccurrences(of: "#OUR_RECOMMENDATION#", with: items[i].CategoryLong)
                
                
                // Add the item's HTML code to the general items string.
                allItems += itemHTMLContent
            }
            
            // Set the items.
            HTMLContent = HTMLContent.replacingOccurrences(of: "#ITEMS#", with: allItems)
            
            // The HTML code is ready.
            return HTMLContent
            
        }
        catch {
            print("Unable to open and use HTML template files.")
        }
        
        return nil
    }
    
    
    func exportHTMLContentToPDF(HTMLContent: String)-> String {
        
        let printPageRenderer = CustomPrintPageRenderer()
        
        let printFormatter = UIMarkupTextPrintFormatter(markupText: HTMLContent)
        printPageRenderer.addPrintFormatter(printFormatter, startingAtPageAt: 0)
        
        let pdfData = drawPDFUsingPrintPageRenderer(printPageRenderer: printPageRenderer)
        
        pdfFilename = "\(AppDelegate.getAppDelegate().getDocDir())/Filtered_Data.pdf"
        pdfData?.write(toFile: pdfFilename, atomically: true)
        
        print(pdfFilename)
        return pdfFilename
    }
    
    
    func drawPDFUsingPrintPageRenderer(printPageRenderer: UIPrintPageRenderer) -> NSData! {
        let data = NSMutableData()
        
        UIGraphicsBeginPDFContextToData(data, CGRect.zero, nil)
        for i in 0..<printPageRenderer.numberOfPages {
            UIGraphicsBeginPDFPage()
            printPageRenderer.drawPage(at: i, in: UIGraphicsGetPDFContextBounds())
        }
        
        UIGraphicsEndPDFContext()
        
        return data
    }
    
}

