//
//  MFHoldings.swift
//  DLevels
//
//  Created by SAIKAT GHOSH on 21/08/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//


import UIKit

class MFHoldings: NSObject {
    
    var CD_NSE_Symbol            = ""
    var INSTRUMENT_4             = ""
    var DISPLAY                  = ""
    var SECTOR                   = ""
    var Symbol_Name              = ""
    var MF_No_of_Shares          = ""
    var Perc                     = ""
    var Val                      = ""
    
    
    
    init(CDNSESymbol: String, INSTRUMENT4: String, DISPLAY:String, SECTOR: String, SymbolName: String, MFNoOfShares:String, per : String, val: String) {
        
        self.CD_NSE_Symbol            = CDNSESymbol
        self.INSTRUMENT_4             = INSTRUMENT4
        self.DISPLAY                  = DISPLAY
        self.SECTOR                   = SECTOR
        self.Symbol_Name              = SymbolName
        self.MF_No_of_Shares          = MFNoOfShares
        self.Val                      = val
        self.Perc                     = per
        
        
    }
    
    
    //MARK GET HOLDING DATA
    func getData(paramFund:String, completion : @escaping ([MFHoldings])->()){
        
        let removedSpace = paramFund.replacingOccurrences(of: " ", with: "%20")
        let removedAmp = removedSpace.replacingOccurrences(of: "&", with: "%26")
        
        let param = "fund=\(removedAmp)"
        
        var result = [MFHoldings]()
        
        NetworkHandler().handleRequest(url: Url.mffundholdings, methodName: "POST", parameters: param, isToken: false, showLoader: true ) { (success, jsonData) in
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                
                let tempArray = jsonData.value(forKey: "response") as! [NSDictionary]
                
                for arr in tempArray{
                    
                    if let Fundr = (arr as AnyObject).value(forKey: "CD_NSE_Symbol") as? String{
                        self.CD_NSE_Symbol = Fundr
                    }
                    if let Fundc = (arr as AnyObject).value(forKey: "INSTRUMENT_4") as? String{
                        self.INSTRUMENT_4 = Fundc
                    }
                    if let Fundd = (arr as AnyObject).value(forKey: "DISPLAY") as? String{
                        self.DISPLAY = Fundd
                    }
                    if let Fundh = (arr as AnyObject).value(forKey: "SECTOR") as? String{
                        self.SECTOR = Fundh
                    }
                    
                    if let Fundd = (arr as AnyObject).value(forKey: "Symbol_Name") as? String{
                        self.Symbol_Name = Fundd
                    }
                    if let Fundh = (arr as AnyObject).value(forKey: "MF_No_of_Shares") as? String{
                        self.MF_No_of_Shares = Fundh
                    }
                    
                    if let val = (arr as AnyObject).value(forKey: "Val") as? String {
                        self.Val = val
                    }
                    
                    if let perc = (arr as AnyObject).value(forKey: "Perc") as? String {
                        self.Perc = perc
                    }
                    
                    
                    let objHoldings = MFHoldings(CDNSESymbol: self.CD_NSE_Symbol, INSTRUMENT4: self.INSTRUMENT_4, DISPLAY: self.DISPLAY, SECTOR: self.SECTOR, SymbolName: self.Symbol_Name, MFNoOfShares: self.MF_No_of_Shares, per: self.Perc, val: self.Val)
                    
                    result.append(objHoldings)
                    
                }
                
                completion(result)
                
            }
        }
    }
    
    
}



