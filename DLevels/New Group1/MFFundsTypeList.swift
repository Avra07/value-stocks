//
//  MFFundsTypeList.swift
//  DLevels
//
//  Created by SAIKAT GHOSH on 21/08/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class MF_FundsTypeList: NSObject {
    
    var NoOfFunds: Int?
    var TotalNetAssets: Int?
    
    
    var oneW_AVG = 0.00
    var oneW_AVG_D = 0.00
    var oneW_MAX = 0.00
    var oneW_MAX_D = 0.00
    
    var oneM_AVG = 0.00
    var oneM_AVG_D = 0.00
    var oneM_MAX = 0.00
    var oneM_MAX_D = 0.00
    
    
    var oneY_AVG = 0.00
    var oneY_AVG_D = 0.00
    var oneY_MAX = 0.00
    var oneY_MAX_D = 0.00
    
    var threeM_AVG = 0.00
    var threeM_AVG_D = 0.00
    var threeM_MAX = 0.00
    var threeM_MAX_D = 0.00
    
    var threeY_AVG = 0.00
    var threeY_AVG_D = 0.00
    var threeY_MAX = 0.00
    var threeY_MAX_D = 0.00
    
    var fiveY_AVG = 0.00
    var fiveY_AVG_D = 0.00
    var fiveY_MAX = 0.00
    var fiveY_MAX_D = 0.00
    
    var sixM_AVG =  0.00
    var sixM_AVG_D = 0.00
    var sixM_MAX = 0.00
    var sixM_MAX_D = 0.00
    
    var Cat = ""
    var Description = ""
    var FundType =  ""
    var NetAssetsInCr = 0
    var Schemes = 0
    
    
    init(nooffunds: Int, totalnetassets: Int, onew_avg:  Double, onew_avg_d:  Double, onew_max:  Double, onew_max_d:  Double, onem_avg:  Double, onem_avg_d:  Double, onem_max:  Double, onem_max_d:  Double, oney_avg:  Double, oney_avg_d:  Double, oney_max:  Double, oney_max_d:  Double, threem_avg:  Double, threem_avg_d:  Double, threem_max:  Double, threem_max_d:  Double, threey_avg:  Double, threey_avg_d:  Double, threey_max:  Double, threey_max_d:  Double, fivey_avg:  Double, fivey_avg_d:  Double, fivey_max:  Double, fivey_max_d:  Double, sixm_avg :  Double, sixm_avg_d:  Double, sixm_max:  Double, sixm_max_d:  Double, cat: String, description: String, fundtype: String, netassetsincr : Int, schemes: Int) {
        
        self.NoOfFunds = nooffunds
        self.TotalNetAssets = totalnetassets
        self.oneW_AVG = onew_avg.rounded(toPlaces: 3)
        self.oneW_AVG_D = onew_avg_d.rounded(toPlaces: 3)
        self.oneW_MAX = onew_max.rounded(toPlaces: 3)
        self.oneW_MAX_D = onew_max_d.rounded(toPlaces: 3)
        self.oneM_AVG = onem_avg.rounded(toPlaces: 3)
        self.oneM_AVG_D = onem_avg_d.rounded(toPlaces: 3)
        self.oneM_MAX = onem_max.rounded(toPlaces: 3)
        self.oneM_MAX_D = onem_max_d.rounded(toPlaces: 3)
        self.oneY_AVG = oney_avg.rounded(toPlaces: 3)
        self.oneY_AVG_D = oney_avg_d.rounded(toPlaces: 3)
        self.oneY_MAX = oney_max.rounded(toPlaces: 3)
        self.oneY_MAX_D = oney_max_d.rounded(toPlaces: 3)
        self.threeM_AVG = threem_avg.rounded(toPlaces: 3)
        self.threeM_AVG_D = threem_avg_d.rounded(toPlaces: 3)
        self.threeM_MAX = threem_max.rounded(toPlaces: 3)
        self.threeM_MAX_D = threem_max_d.rounded(toPlaces: 3)
        self.threeY_AVG = threey_avg.rounded(toPlaces: 3)
        self.threeY_AVG_D = threey_avg_d.rounded(toPlaces: 3)
        self.threeY_MAX = threey_max.rounded(toPlaces: 3)
        self.threeY_MAX_D = threey_max_d.rounded(toPlaces: 3)
        self.fiveY_AVG = fivey_avg.rounded(toPlaces: 3)
        self.fiveY_AVG_D = fivey_avg_d.rounded(toPlaces: 3)
        self.fiveY_MAX = fivey_max.rounded(toPlaces: 3)
        self.fiveY_MAX_D = fivey_max_d.rounded(toPlaces: 3)
        self.sixM_AVG = sixm_avg.rounded(toPlaces: 3)
        self.sixM_AVG_D = sixm_avg_d.rounded(toPlaces: 3)
        self.sixM_MAX = sixm_max.rounded(toPlaces: 3)
        self.sixM_MAX_D = sixm_max_d.rounded(toPlaces: 3)
        self.Cat = cat
        self.Description = description
        self.FundType = fundtype
        self.NetAssetsInCr = netassetsincr
        self.Schemes = schemes
    }
    
    
}


