//
//  MF_FundTypeListDataModel.swift
//  DLevels
//
//  Created by SAIKAT GHOSH on 21/08/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import Foundation
class MF_FundTypeListDataModel: NSObject {
var FundType:String                 = ""
var NoOfFunds:Int                   = 0
var TotalNetAssets:Int              = 0
var oneMReturn :Decimal             = 0.00
var oneWAvgReturn :Decimal          = 0.00
var yrAvgReturn :Decimal            = 0.00
var threeMReturn :Decimal           = 0.00
var sixMReturn :Decimal             = 0.00
var threeYrAvgReturn :Decimal       = 0.00
var fiveYrAvgReturn :Decimal        = 0.00



var oneW_AVG = 0.00
var oneW_AVG_D = 0.00
var oneW_MAX = 0.00
var oneW_MAX_D = 0.00

var oneM_AVG = 0.00
var oneM_AVG_D = 0.00
var oneM_MAX = 0.00
var oneM_MAX_D = 0.00


var oneY_AVG = 0.00
var oneY_AVG_D = 0.00
var oneY_MAX = 0.00
var oneY_MAX_D = 0.00


var threeM_AVG = 0.00
var threeM_AVG_D = 0.00
var threeM_MAX = 0.00
var threeM_MAX_D = 0.00

var threeY_AVG = 0.00
var threeY_AVG_D = 0.00
var threeY_MAX = 0.00
var threeY_MAX_D = 0.00

var fiveY_AVG = 0.00
var fiveY_AVG_D = 0.00
var fiveY_MAX = 0.00
var fiveY_MAX_D = 0.00

var sixM_AVG =  0.00
var sixM_AVG_D = 0.00
var sixM_MAX = 0.00
var sixM_MAX_D = 0.00


var Cat = ""
var Description = ""
//var FundType =  ""
var NetAssetsInCr = 0.00
var Schemes = 0








func getData(Type:String, paramCategory:String, completion : @escaping ([MF_FundsTypeList])->()){
    
    var urlStr = ""
    
    if Type == "category" {
        urlStr = Url.mffundtypelist
    }
        
    else if Type == "manager" {
        urlStr = Url.mffundmanagers
    }
        
    else if Type == "house" {
        urlStr = Url.mffundhouse
    }
    else if Type == "ourfunds" {
        urlStr = Url.ourfunds
    }
    
    let param = "category=\(paramCategory)"
    var result = [MF_FundsTypeList]()
    
    NetworkHandler().handleRequest(url: urlStr, methodName: "POST", parameters: param, isToken: false, showLoader: false) { (success, jsonData) in
        
        KVNProgress.dismiss()
        print(jsonData)
        
        if jsonData.value(forKey: "errmsg") as! String == "" {
            
            let tempArray = jsonData.value(forKey: "response") as! [NSDictionary]
            
            for arr in tempArray{
                
                if Type == "category" {
                    if let Fund_Type = arr.value(forKey: "FundType") as? String{
                        self.FundType = Fund_Type
                    }
                }
                
                if Type == "manager" {
                    if let Fund_Type = arr.value(forKey: "mfm_manager") as? String{
                        self.FundType = Fund_Type
                    }
                }
                
                if Type == "house" {
                    if let Fund_Type = arr.value(forKey: "FundHouse") as? String{
                        self.FundType = Fund_Type
                    }
                }
                
                if let desc = arr.value(forKey: "Description") as? String{
                    self.Description = desc
                }
                
                if let schm = arr.value(forKey: "Schemes") as? String{
                    self.Schemes = Int(schm)!
                }
                
                if let NetAInCr = arr.value(forKey: "NetAssetsInCr") as? String{
                    self.NetAssetsInCr = (NetAInCr as NSString).doubleValue
                }
                
                if let OW_AVG = arr.value(forKey: "1W_AVG") as? String{
                    self.oneW_AVG = (OW_AVG as NSString).doubleValue
                }
                if let OW_AVG_D = arr.value(forKey: "1W_AVG_D") as? String{
                    self.oneW_AVG_D = (OW_AVG_D as NSString).doubleValue
                }
                if let OW_MAX = arr.value(forKey: "1W_MAX") as? String{
                    self.oneW_MAX = (OW_MAX as NSString).doubleValue
                }
                if let OW_MAX_D = arr.value(forKey: "1W_MAX_D") as? String{
                    self.oneW_MAX_D = (OW_MAX_D as NSString).doubleValue
                }
                
                if let OM_AVG = arr.value(forKey: "1M_AVG") as? String{
                    self.oneM_AVG = (OM_AVG as NSString).doubleValue
                }
                if let OM_AVG_D = arr.value(forKey: "1M_AVG_D") as? String{
                    self.oneM_AVG_D = (OM_AVG_D as NSString).doubleValue
                }
                if let OM_MAX = arr.value(forKey: "1M_MAX") as? String{
                    self.oneM_MAX = (OM_MAX as NSString).doubleValue
                }
                if let OM_MAX_D = arr.value(forKey: "1M_MAX_D") as? String{
                    self.oneM_MAX_D = (OM_MAX_D as NSString).doubleValue
                }
                
                if let OY_AVG = arr.value(forKey: "1Y_AVG") as? String{
                    self.oneY_AVG = (OY_AVG as NSString).doubleValue
                }
                if let OY_AVG_D = arr.value(forKey: "1Y_AVG_D") as? String{
                    self.oneY_AVG_D = (OY_AVG_D as NSString).doubleValue
                }
                if let OY_MAX = arr.value(forKey: "1Y_MAX") as? String{
                    self.oneY_MAX = (OY_MAX as NSString).doubleValue
                }
                if let OY_MAX_D = arr.value(forKey: "1Y_MAX_D") as? String{
                    self.oneY_MAX_D = (OY_MAX_D as NSString).doubleValue
                }
                
                
                if let TM_AVG = arr.value(forKey: "3M_AVG") as? String{
                    self.threeM_AVG = (TM_AVG as NSString).doubleValue
                }
                if let TM_AVG_D = arr.value(forKey: "3M_AVG_D") as? String{
                    self.threeM_AVG_D = (TM_AVG_D as NSString).doubleValue
                }
                if let TM_MAX = arr.value(forKey: "3M_MAX") as? String{
                    self.threeM_MAX = (TM_MAX as NSString).doubleValue
                }
                if let TM_MAX_D = arr.value(forKey: "3M_MAX_D") as? String{
                    self.threeM_MAX_D = (TM_MAX_D as NSString).doubleValue
                }
                
                if let TY_AVG = arr.value(forKey: "3Y_AVG") as? String{
                    self.threeY_AVG = (TY_AVG as NSString).doubleValue
                }
                if let TY_AVG_D = arr.value(forKey: "3Y_AVG_D") as? String{
                    self.threeY_AVG_D = (TY_AVG_D as NSString).doubleValue
                }
                if let TY_MAX = arr.value(forKey: "3Y_MAX") as? String{
                    self.threeY_MAX = (TY_MAX as NSString).doubleValue
                }
                if let TY_MAX_D = arr.value(forKey: "3Y_MAX_D") as? String{
                    self.threeY_MAX_D = (TY_MAX_D as NSString).doubleValue
                }
                
                if let FY_AVG = arr.value(forKey: "5Y_AVG") as? String{
                    self.fiveY_AVG = (FY_AVG as NSString).doubleValue
                }
                if let FY_AVG_D = arr.value(forKey: "5Y_AVG_D") as? String{
                    self.fiveY_AVG_D = (FY_AVG_D as NSString).doubleValue
                }
                if let FY_MAX = arr.value(forKey: "5Y_MAX") as? String{
                    self.fiveY_MAX = (FY_MAX as NSString).doubleValue
                }
                if let FY_MAX_D = arr.value(forKey: "5Y_MAX_D") as? String{
                    self.fiveY_MAX_D = (FY_MAX_D as NSString).doubleValue
                }
                
                if let SM_AVG = arr.value(forKey: "6M_AVG") as? String{
                    self.sixM_AVG = (SM_AVG as NSString).doubleValue
                }
                if let SM_AVG_D = arr.value(forKey: "6M_AVG_D") as? String{
                    self.sixM_AVG_D = (SM_AVG_D as NSString).doubleValue
                }
                if let SM_MAX = arr.value(forKey: "6M_MAX") as? String{
                    self.sixM_MAX = (SM_MAX as NSString).doubleValue
                }
                if let SM_MAX_D = arr.value(forKey: "6M_MAX_D") as? String{
                    self.sixM_MAX_D = (SM_MAX_D as NSString).doubleValue
                }
                
                
                let obj_mfFundsTypeList = MF_FundsTypeList(nooffunds: self.Schemes, totalnetassets: Int(self.NetAssetsInCr), onew_avg: self.oneW_AVG, onew_avg_d: self.oneW_AVG_D, onew_max: self.oneW_MAX, onew_max_d: self.oneW_MAX_D, onem_avg: self.oneM_AVG, onem_avg_d: self.oneM_AVG_D, onem_max: self.oneM_MAX, onem_max_d: self.oneM_MAX_D, oney_avg: self.oneY_AVG, oney_avg_d: self.oneY_AVG_D, oney_max: self.oneY_MAX, oney_max_d: self.oneY_MAX_D, threem_avg: self.threeM_AVG, threem_avg_d: self.threeM_AVG_D, threem_max: self.threeM_MAX, threem_max_d: self.threeM_MAX_D, threey_avg: self.threeY_AVG, threey_avg_d: self.threeY_AVG_D, threey_max: self.threeY_MAX, threey_max_d: self.threeY_MAX_D, fivey_avg: self.fiveY_AVG, fivey_avg_d: self.fiveY_AVG_D, fivey_max: self.fiveY_MAX, fivey_max_d: self.fiveY_MAX_D, sixm_avg: self.sixM_AVG, sixm_avg_d: self.sixM_AVG_D, sixm_max: self.sixM_MAX, sixm_max_d: self.sixM_MAX_D, cat: self.Cat, description: self.Description, fundtype: self.FundType, netassetsincr: Int(self.NetAssetsInCr), schemes: self.Schemes)
                
                result.append(obj_mfFundsTypeList)
            }
            
            return completion(result)
        } else {
            AppManager.showAlertMessage(title: "error", message: "check network")
        }
    }
}

}
