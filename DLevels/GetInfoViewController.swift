//
//  GetInfoViewController.swift
//  DLevels
//
//  Created by Avra Ghosh on 30/09/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class GetInfoViewController: UIViewController {
    @IBOutlet weak var lbl_Header: UILabel!
    var headerText: String!
    var paramName: String!
    var videoLink: String!
    @IBOutlet weak var lbl_Option: UILabel!
    @IBOutlet weak var lbl_What: UILabel!
    @IBOutlet weak var lbl_Why: UILabel!
    @IBOutlet weak var lbl_How: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lbl_Header.text = headerText
        lbl_Option.text = headerText + "?"
        paramName = headerText
        if headerText == "Stocks Screener"{
            paramName = "Stock Screener"
        }
        if headerText == "Mutual Funds Research"{
            paramName = "MF Research"
        }
        if headerText == "Mutual Funds Screener"{
            paramName = "MF Screener"
        }
        getInfoDetails()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btn_back(_ sender: Any) {
        self.navigationController!.popViewController(animated: true)
    }
    
    @IBAction func btn_VideoPlay(_ sender: Any) {
        UIApplication.shared.openURL(NSURL(string: videoLink!)! as URL)
    }
    @IBAction func btn_login(_ sender: Any) {
        if User.email == "guest@yopmail.com"{
        let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
       
        
        self.navigationController?.pushViewController(viewController, animated: true)
        }else{
           
        }
    }
    
    func getInfoDetails() {
        let obj = WebService()
        let isActive = "Y"
        let parameter = "name=\(paramName!)&active=\(isActive)"
        print("parameter :\(parameter)")
        obj.callWebServices(url: Urls.get_benifits_details, methodName: "POST", parameters: parameter, istoken: false, tokenval: User.token) { (returnValue, jsonData) in
            if jsonData.value(forKey: "errmsg") as! String == "" {
                print("JSON Data \(jsonData)")
                let getBenefitDetails = jsonData.value(forKey: "response") as! NSArray
                for getBenefitDetails in getBenefitDetails{
                    if let getDetailDict = getBenefitDetails as? NSDictionary {
                        var getInfoData = GetInfo(get_How: "", get_What: "", get_Why: "", get_VideoLink: "")
                        if let getHow = getDetailDict.value(forKey: "asbd_How"){
                            getInfoData.get_How = getHow as? String
                        }
                        if let getWhat = getDetailDict.value(forKey: "asbd_What"){
                            getInfoData.get_What = getWhat as? String
                        }
                        if let getWhy = getDetailDict.value(forKey: "asbd_Why"){
                            getInfoData.get_Why = getWhy as? String
                        }
                        if let getVideoLink = getDetailDict.value(forKey: "asbd_Video_Link"){
                            getInfoData.get_VideoLink = getVideoLink as? String
                        }
                        self.videoLink = getInfoData.get_VideoLink
                        self.lbl_What.text = getInfoData.get_What
                        self.lbl_How.text = getInfoData.get_How
                        self.lbl_Why.text = getInfoData.get_Why
                    }
                }
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

struct GetInfo {
    var get_How : String?
    var get_What : String?
    var get_Why : String?
    var get_VideoLink : String?
    
}
