//
//  File.swift
//  DLevels
//
//  Created by SAIKAT GHOSH on 10/07/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//


@IBDesignable
class RoundedView: UIView, Roundable {
    @IBInspectable var isCircle: Bool = false {
        didSet {
            setupLayer()
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            setupLayer()
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            setupLayer()
        }
    }
    
    @IBInspectable var borderColor: UIColor = .clear {
        didSet {
            setupLayer()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setupLayer()
    }
}
