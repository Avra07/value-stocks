//
//  EmailOTPViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-01 on 07/03/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class EmailOTPViewController: UIViewController {

    @IBOutlet weak var lbltype: UILabel!
    @IBOutlet weak var lblemail: UILabel!
    @IBOutlet weak var txtotp: UITextField!
    @IBOutlet weak var btnresend: UIButton!
    @IBOutlet weak var lblResendOtp: UILabel!
    
    @IBOutlet weak var VIEW_CHANGE_NUMBER: UIView!
    @IBOutlet weak var lblVerificationText: UILabel!
    @IBOutlet weak var btn_change: UIButton!
    @IBOutlet weak var txt_number: UITextField!
    
    @IBOutlet weak var TXT_CODE: UITextField!
    @IBOutlet weak var btnsubmit: UIButton!
    
    var emailAddr   : String?
    var emailOld    : String?
    var phoneNum    : String?
    var checkotptype: String!
    
    var counter = 30
    var timer   = Timer()
    
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        btnresend.isHidden = true
        lblResendOtp.text = "Did not recieve OTP? Please wait \(counter) second(s)"
        
        btnresend.layer.borderColor = UIColor.black.cgColor
        btnresend.layer.borderWidth = 2.0
        
        emailAddr = User.email
        emailOld = User.email
        phoneNum = User.phone
        
        checkotptype = "email";
        
        lbltype.text = emailAddr;
        
        timer.invalidate() // just in case this button is tapped multiple times
        // start the timer
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
        
        addLoader()

    }
    @IBAction func btn_cancel(_ sender: Any) {
        
        
        VIEW_CHANGE_NUMBER.isHidden = true;
    }

    
    @IBAction func btn_ok(_ sender: Any) {
        
        
        VIEW_CHANGE_NUMBER.isHidden = true;
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    
    @IBAction func btnsubmit(_ sender: Any) {
        
        actInd.startAnimating()
        
        let parameters = "oldemail=\(emailOld!)&newemail=\(lbltype.text!)&otp=\(txtotp.text!)"
        self.checkOTP(urlAddress: Urls.updateEmail, parameters: parameters)
    }
    
    
    @IBAction func btnresend(_ sender: Any) {
        
        
        
        let parameters = "email=\(lbltype.text!)"
        self.resend(urlAddress: Urls.generateEmailOtp, param: parameters)
        btnresend.isHidden = true
        
        self.counter = 30
        
        self.timer.invalidate() // just in case this button is tapped multiple times
        // start the timer
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.timerAction), userInfo: nil, repeats: true)
    }
    
    
    @IBAction func btn_change(_ sender: Any) {
        
         self.changeButtonClick(screenType: checkotptype)
    }
   
    

   
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension EmailOTPViewController {
    
    func addLoader() {
        actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0) //CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.gray
        self.view.addSubview(actInd)
    }
    
    func checkOTP(urlAddress: String, parameters: String)
    {
        let obj = WebService()
        let token = ""
        
        print("the parameters is: \(parameters)")
        
        
        // let otpval = txtotp.text
        //let parameters = "email=\(User.email)&phone=\(phoneNum)&otp=\(txtotp.text!)"
        
        obj.callWebServices(url: urlAddress, methodName: "POST", parameters: parameters, istoken: false, tokenval: token) { (returnValue, jsonData) in
            
            print("Json Dict is : \(jsonData)")
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
              DispatchQueue.main.async(execute: {
                        self.actInd.stopAnimating()
                
                let emailval = "email=\(User.email)"
                
                obj.callWebServices(url: Urls.wellcome_email, methodName: "POST", parameters:emailval, istoken: false, tokenval: "") { (returnValue, jsonData) in
                    
                    print("Json Dict is : \(jsonData)")
                    
                    
                }
                        let sucessViewConroller = self.storyboard?.instantiateViewController(withIdentifier: "sucessvwctrl") as! SucessViewController
                        self.navigationController?.pushViewController(sucessViewConroller, animated: true)
                    })
                
            }
            else
            {
                
                let alertobj = AppManager()
                
                DispatchQueue.main.async {
                    self.actInd.stopAnimating()
                    alertobj.showAlert(title: "Error!", message: jsonData.value(forKey: "errmsg") as! String , navigationController: self.navigationController!)
                }
                
                
            }
        }
    }
    
    
    func sendEmailOTP(email: String) {
        
        let objWebService = WebService()
        
        objWebService.callWebServices(url: Urls.generateEmailOtp, methodName: "POST", parameters: "email=\(User.email)", istoken: false, tokenval: "") { (success, jsonDict) in
            
            print(jsonDict)
            
            if jsonDict.value(forKey: "response") as! String == "success" {
                
                DispatchQueue.main.async(execute: {
                    
                    self.actInd.stopAnimating()
                    self.lbltype.text = self.emailAddr
                    self.txtotp.text = ""
                    self.checkotptype = "email"
                })
                
            } else {
                
                DispatchQueue.main.async {
                    
                    self.actInd.stopAnimating()
                    let alert = AppManager()
                    alert.showAlert(title: "Error!", message: jsonDict.value(forKey: "errmsg") as! String, navigationController: self.navigationController!)
                }
            }
        }
    }
    
    func resend(urlAddress: String, param: String)
    {
        let obj = WebService()
        let token = ""
        
        //print("email = \(emailaddr)")
        //let parameters = "email=\(emailAddr)&phone=\(phoneNum)"
        
        //print(param)
        
        obj.callWebServices(url: urlAddress, methodName: "POST", parameters: param, istoken: false, tokenval: token) { (returnValue, jsonData) in
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                let alertobj = AppManager()
                
                alertobj.showAlert(title: "Sucess", message: "OTP Send Sucessfully.", navigationController: self.navigationController!)
                
                //self.btnresend.isHidden = true
                
                
                
                
            }
            else
            {
                DispatchQueue.main.async {
                    self.actInd.stopAnimating()
                    let alertobj = AppManager()
                    
                    alertobj.showAlert(title: "Error!", message: jsonData.value(forKey: "errmsg") as! String , navigationController: self.navigationController!)
                }
                
            }
            
        }
        
    }
    
    // must be internal or public.
    func timerAction() {
        if counter > 0 {
            lblResendOtp.text = "Did not recieve OTP? Please wait \(counter) second(s)"
            counter -= 1
        }
        else
        {
            self.btnresend.isHidden = false
            lblResendOtp.text = "Did not recieve OTP?"
            lblResendOtp.textAlignment = .left
            view.layoutIfNeeded()
        }
    }
    
    func EmailChange(type: String, urlstr: String, inputtext: String)
    {
        let obj = WebService()
        let token = ""
        var parameters_val = ""
        
        parameters_val = "email=\(inputtext)"
       
        
        
        obj.callWebServices(url: urlstr, methodName: "POST", parameters: parameters_val, istoken: false, tokenval: token) { (returnValue, jsonData) in
            
            
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                    self.emailAddr = inputtext
                    User.email = inputtext
                    self.lbltype.text = inputtext
                
                
                DispatchQueue.main.async {
                    
                    
                    self.actInd.stopAnimating()
                    let alertobj = AppManager()
                    
                    alertobj.showAlert(title: "Sucess", message: "OTP has been Send in your new \(type)", navigationController: self.navigationController!)
                    
                }
                
                //print(jsonData)
            }
            else
            {
                DispatchQueue.main.async {
                    
                    self.actInd.stopAnimating()
                    let alertobj = AppManager()
                    
                    alertobj.showAlert(title: "Error!", message: jsonData.value(forKey: "errmsg") as! String , navigationController: self.navigationController!)
                }
            }
        }
        
        
    }

    func changeButtonClick(screenType: String)
    {
        var urlstring   =   ""
        var titlestr    =   ""
        var msg         =   ""
        
        urlstring   = Urls.generateEmailOtp
        titlestr    = "Change Email Address"
        msg         = "Please enter your new Email Address."
        
        //1. Create the alert controller.
        let alert = UIAlertController(title: titlestr, message: msg, preferredStyle: .alert)
        alert.view.tintColor = UIColor(red: 247/255, green: 187/255, blue: 26/255, alpha: 1)
       
        
        
        //2. Add the text field. You can configure it however you need.
        alert.addTextField { (textField) in
            textField.text = ""
            textField.keyboardType = .default
            
        }
        
        // 3. Grab the value from the text field, and print it when the user clicks OK.
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (AAA) in
            
        }))
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
            
            let emailaddr = alert!.textFields![0].text! as String
            
            if  AppManager().isValidEmail(email: emailaddr) {
                
                let webService = WebService()
                let parameters = "email=\(emailaddr)"
                
                webService.callWebServices(url: Urls.accountStatus, methodName: "POST", parameters: parameters, istoken: false, tokenval: "") { (status, response) in
                    
                    print("Response is:  \(parameters)")
                    print("Response is:  \(response)")
                    
                    if response.value(forKeyPath: "errmsg") as! String != ""
                    {
                    // Force unwrapping because we know it exists.
                    self.EmailChange(type: screenType ,urlstr: urlstring, inputtext: emailaddr)

                    }
                    else
                    {
                        let alertobj = AppManager()
                        
                        alertobj.showAlert(title: "Error!", message: "Email already Exists." , navigationController: self.navigationController!)
                    }
                }

                
                
                
                
                           } else {
                AppManager().showAlert(title: "Invalid Email Format.", message: "", navigationController: self.navigationController!)
            }
            
        }))
        // 4. Present the alert.
        self.present(alert, animated: true, completion: nil)
    }

    
    
    
    
}


