//
//  GuruStrategiesListViewController.swift
//  DLevels
//
//  Created by SAIKAT GHOSH on 13/08/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class GuruStrategiesListCell: UITableViewCell {
    
    @IBOutlet weak var vwbox: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblFilterList: UILabel!
    @IBOutlet weak var btnApply: UIButton!
    @IBOutlet weak var titlevw: UIView!
}

class GuruFilterListCell: UICollectionViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var vwFilterList: UIView!
}

class GuruStrategiesListViewController: UIViewController {
    
    @IBOutlet weak var tblvwGuruStrategies: UITableView!
    
    var tblvwDataSource = [NSDictionary]()
    var colvwDataSource: [String] = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        getData(completion: { (status) in
            if status == true {
                self.tblvwGuruStrategies.reloadData()
            } else {
                
            }
        })
    }
    
    @IBAction func btnbackclick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func goToFilter(sender: UIButton) {
        
        var filterVal = ""
        var filterDisplay = ""
        
        let data = tblvwDataSource[sender.tag]
        
        if let filters = data.value(forKey: "sf_filters") as? String {
            filterVal = filters
        }
        
        if let filters_display = data.value(forKey: "sf_filters_display") as? String {
            filterDisplay = filters_display
        }
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        
        let viewcontroller = storyBoard.instantiateViewController(withIdentifier: "StockFiltersViewController") as! StockFiltersViewController
        
        AppManager.GetSelectedFilterArr(queryText: filterVal, displayText: filterDisplay, completion: {result in
            if result {
                User.navigation.pushViewController(viewcontroller, animated: true)
            }
            
        })
        
       // viewcontroller.selectedFilter        = filterVal
       // viewcontroller.selectedFilterDisplay = filterDisplay
    }
    
    
    //MARK: GET DATA
    func getData (completion : @escaping (Bool)->()) {
        
        let obj = WebService()
        let paremeters = "user_id=-1"
        
        //actInd.startAnimating()
        
        obj.callWebServices(url: Urls.get_saved_filter_list, methodName: "GET", parameters: paremeters, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
            
            print("Json Data is :  \(jsonData)")
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                let tempArray = jsonData.value(forKey: "response") as! [NSDictionary]
                
                for dict in tempArray {
                    self.tblvwDataSource.append(dict)
                }
                return completion(true)
                
            }
        }
    }
    
    
}

// UICollectionViewDataSource

extension GuruStrategiesListViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return colvwDataSource.count
    }
   
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GuruFilterListCell", for: indexPath) as! GuruFilterListCell
        
        cell.lblTitle.text = colvwDataSource[indexPath.row]
        return cell
    }
    
    
    
    
}

// UICollectionViewDelegate

extension GuruStrategiesListViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}



extension GuruStrategiesListViewController: UITableViewDataSource,UITableViewDelegate {
    
    
    
    
    //MARK: Table View Delegate
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let data = tblvwDataSource[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "GuruStrategiesListCell") as! GuruStrategiesListCell
        let str : NSMutableAttributedString = NSMutableAttributedString(string: "")
        
        
        cell.btnApply.layer.borderWidth = 1
        cell.btnApply.layer.cornerRadius = 4
        

        cell.titlevw.backgroundColor = Colors.headerViewColor
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        if let title = data.value(forKey: "sf_filter_name") as? String {
            cell.lblTitle.text = title
        }
        
        if let desc = data.value(forKey: "sf_filter_desc") as? String {
            cell.lblDesc.text = desc
        }
        
        
        if let filters_display = data.value(forKey: "sf_filters_display") as? String {
            
            let display: String = filters_display.replacingOccurrences(of: "|", with: " ")
            colvwDataSource = display.split(separator: ";").map { String($0) }
        }
        
        if let filters_display = data.value(forKey: "sf_filters_display") as? String {
            
            let display: String = filters_display.replacingOccurrences(of: "|", with: " ")
            colvwDataSource = display.split(separator: ";").map { String($0) }
            
            for arr in colvwDataSource {
                
                let myString = arr
                let myString1 = "\n\n"
                
                
                
                // let myAttribute = [NSBackgroundColorAttributeName : UIColor.init(red: 94.0/255.0, green: 94.0/255.0, blue: 94.0/255.0, alpha: 1.0)]
                
                
                
                let myAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]
                let myAttribute1 = [NSBackgroundColorAttributeName : UIColor.clear]
                
                let range = (myString as NSString).range(of: myString )
                
                let myAttrString = NSMutableAttributedString(string: myString , attributes: myAttribute)
                
                let myAttrString1 = NSMutableAttributedString(string: myString1 , attributes: myAttribute1)
                
                //myAttrString.addAttribute(NSForegroundColorAttributeName, value: UIColor.white, range: range)
                
                str.append(myAttrString)
                str.append(myAttrString1)
                
            }
            
            cell.lblFilterList.attributedText = str
        
        
        
        
        }
         
        
        if let id = data.value(forKey: "sf_id") as? String {
            if Int(id)! > 0 {
                cell.btnApply.isHidden = false
                    cell.btnApply.tag = indexPath.row
                    cell.btnApply.addTarget(self, action: #selector(goToFilter), for: .touchUpInside)
            }else if Int(id) == -1  {
                cell.btnApply.isHidden = true
                //cell.lblFilterList.attributedText = NSMutableAttributedString(string: "")
            }else {
                cell.btnApply.isHidden = true
            }
        }
        return cell
    }
    
    public func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.tblvwDataSource.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
    }
    
    
}
