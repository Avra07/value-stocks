//
//  SelectSectorCell.swift
//  DLevels
//
//  Created by SAIKAT GHOSH on 25/08/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class SelectSectorCell: UITableViewCell{
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgTick: UIImageView!
    
    @IBOutlet weak var tblvwSelectSector: UITableView!
    
    weak var sectorDelegate: DataEnteredDelegate?
    var sender_indexPathRow = ""
    var sender_section      = ""
    
    var sectorListDict = [NSDictionary]()
    
    
    
    
    var selectedSector = ""
    var arrSelectedSector = [String]()
    
    func getData (completion : @escaping (Bool)->()) {
        
        let obj = WebService()
        let paremeters = ""
        
        //actInd.startAnimating()
        
        obj.callWebServices(url: Urls.getsectorlist, methodName: "GET", parameters: paremeters, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
            
            print("Json Data is :  \(jsonData)")
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                let tempArray = jsonData.value(forKey: "response") as! [NSDictionary]
                
                for dict in tempArray {
                    self.sectorListDict.append(dict)
                }
                
                
                
                
                return completion(true)
                
            }
        }
    }
    
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.tblvwSelectSector.dataSource = self
        self.tblvwSelectSector.delegate = self
        
        
        // Initialization code
        getData(completion: { (status) in
            if status == true {
                self.tblvwSelectSector.reloadData()
            } else {
                
            }
        })
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

extension SelectSectorCell: UITableViewDataSource,UITableViewDelegate {
    
    //MARK: Table View Delegate
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let data = sectorListDict[indexPath.row]
        
        
        let cell = Bundle.main.loadNibNamed("SectorListVw", owner: nil, options: nil)?[0] as! UITableViewCell
        cell.selectionStyle = .none
        
        let imageVw = cell.viewWithTag(2001) as? UIImageView
        
        if let title = data.value(forKey: "sector") as? String {
            let lblName = cell.viewWithTag(2002) as! UILabel
            lblName.text = title
            
            if arrSelectedSector.count > 0 {
                if arrSelectedSector.contains(title) {
                    imageVw!.isHidden = false
                } else {
                    imageVw!.isHidden = true
                }
            }else {
                imageVw!.isHidden = true
            }
            
            
        }
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.sectorListDict.count
    }
    
    // MARK: Table View Delegate
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // getting the current cell from the index path
        let currentCell = tableView.cellForRow(at: indexPath) as! UITableViewCell
        
        let lblTitle = currentCell.viewWithTag(2002) as! UILabel
        // getting the text of that cell
        let currentItem = lblTitle.text
        let imageVw = currentCell.viewWithTag(2001) as? UIImageView
        
        if let index = arrSelectedSector.index(of: currentItem!) {
            arrSelectedSector.remove(at: index)
            imageVw!.isHidden = true
            currentCell.isSelected = true
        } else {
            arrSelectedSector.append(currentItem!)
            imageVw!.isHidden = false
            currentCell.isSelected = false
        }
        
    }
    
    
}
