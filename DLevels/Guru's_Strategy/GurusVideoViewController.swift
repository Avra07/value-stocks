//
//  GurusVideoViewController.swift
//  DLevels
//
//  Created by SAIKAT GHOSH on 25/08/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit



class GurusVideoViewController: UIViewController {
    
    @IBOutlet weak var getStock: UIButton!
    @IBOutlet weak var imgvw: UIImageView!
    @IBOutlet weak var imgplay: UIImageView!
    @IBOutlet weak var imgvideo:UIImageView!
     @IBOutlet var videoView: UIView!
     @IBOutlet var withoutVideoView: UIView!
    @IBOutlet weak var titlevideo: UILabel!
    @IBOutlet var vw: UIView!
    @IBOutlet weak var about: UIButton!
    @IBOutlet weak var navbar: UIView!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var searchBtn: UIButton!
    var tittle = ""
    var gid = ""
    var namecell = ""
    var longDesc : String?
    var initial : String?
    var videoTxt : String?
    var videoLink : String?
    var videoImg : String?
    
    var getGuruData = [Guru]()
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    
    @IBAction func video(_ sender: Any) {
        UIApplication.shared.openURL(NSURL(string: videoLink!) as! URL)
    }
    
    
    
    @IBAction func backTO(_ sender: Any) {
        User.navigation.popViewController(animated: true)
        
    }
    @IBAction func backToabout(_ sender: Any) {
        
        User.navigation.popViewController(animated: true)
        /* let strybrd =  UIStoryboard.init(name: "Main", bundle: nil)
         let obj = strybrd.instantiateViewController(withIdentifier: "GuruStrategyViewController") as! GuruStrategyViewController
         
         
         
         let transition:CATransition = CATransition()
         transition.duration = 0.5
         transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
         transition.type = kCATransitionPush
         transition.subtype = kCATransitionFromLeft
         self.navigationController!.view.layer.add(transition, forKey: kCATransition)
         
         self.navigationController?.pushViewController(obj, animated: true)
         
         */
    
    }
   
    
    
    
    var titlename = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        // self.titlevideo.text! = tittle
        actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0) //CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.gray
        self.view.addSubview(actInd)
        actInd.startAnimating()
        getRulesByGuru()
        
    }
    
    func getRulesByGuru(){
        
        let obj = WebService()
        var result = [[NSDictionary]]()
        let paremeters = "financial_guru_id=\(gid)&financial_guru_name=\(namecell)&stype=&is_active=Y"//"user_id=\(User.socialId)"
        
        //actInd.startAnimating()
        
        obj.callWebServices(url: Urls.get_financial_guru, methodName: "POST", parameters: paremeters, istoken: false, tokenval: User.token) { (returnValue, jsonData) in
            
            //result=[[NSDictionary]]
            print("Json Data neww is :\(jsonData)")
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                result = jsonData.value(forKey: "response") as! [[NSDictionary]]
                let results = result[1]
                for res in results {
                    self.videoTxt = res.value(forKey: "gd_video_text") as? String
                    self.videoLink = res.value(forKey:  "gd_video_link") as? String
                    self.videoImg = res.value(forKey:  "gd_video_image") as? String
                 }
                if self.videoLink == ""{
                    self.videoView.isHidden = true
                    self.withoutVideoView.isHidden = false
                }
                else {
                    self.videoView.isHidden = false
                    self.withoutVideoView.isHidden = true
                    self.titlevideo.text = self.videoTxt
                    /*print("Video Image link is \(self.videoImg)")
                    var imageURL : NSURL? = NSURL(string: self.videoImg!)
                    //let imageURL = NSURL(string: self.videoImg!)
                    print("Image URL is \(imageURL)")
                    let imagedData = NSData(contentsOf: imageURL! as URL)
                    self.imgvideo.image = UIImage(data: imagedData as! Data) */
                    
                    let url : NSString = self.videoImg! as NSString
                    let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
                    let imageURL : NSURL = NSURL(string: urlStr as String)!
                    print(imageURL)
                    let imagedData = NSData(contentsOf: imageURL as URL)
                    self.imgvideo.image = UIImage(data: imagedData! as Data)
                }
            }
            self.actInd.stopAnimating()
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

