//
//  GuruStrategyViewController.swift
//  DLevels
//
//  Created by SAIKAT GHOSH on 25/08/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit



class gurusCell : UITableViewCell{
    @IBOutlet weak var lbl1 : UILabel!
    @IBOutlet weak var back_arrow : UIButton!
    @IBOutlet weak var img_Guru: UIImageView!
}




class GuruStrategyViewController: UIViewController {
    
    
    @IBOutlet weak var titlevw: UIView!
    @IBOutlet weak var tbl_GuruHeight: NSLayoutConstraint!
    
    @IBAction func btnbck(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBOutlet weak var logo: UIImageView!
    
    @IBOutlet weak var titlelbl: UILabel!
    
    @IBAction func clrbtn(_ sender: Any) {
    }
    let frame = CGRect(x: 0, y: 0, width: 0, height: 0)
    //var clickBy = "Strategies"
    var filteredData: [NSDictionary] = []
    //@IBOutlet weak var searGuru: UISearchBar!
    //@IBOutlet var strategyvw: UIView!
    
    //@IBOutlet weak var by_Guru: UIButton!
    //@IBOutlet weak var byStrategy: UIButton!
    
    /*@IBAction func Strategybtn(_ sender: Any) {
     guru_vw.backgroundColor = UIColor.clear
     strategyvw.backgroundColor = UIColor(hex: "F7BB1A")
     self.Guru_s_tblvw.tableHeaderView?.isHidden = true
     searGuru.frame = frame
     clickBy = "Strategies"
     
     getStrategies(completion: { (dataset) in
     self.tResult.removeAll()
     self.tResult = dataset
     
     self.Guru_s_tblvw.reloadData()
     // self.tgResult = dataset
     // let name = self.tgResult[IndexPath]
     })
     }
 
    @IBAction func Gurubtn(_ sender: Any) {
        // self.Guru_s_tblvw.tableHeaderView?.isHidden = false
        //searGuru.frame = CGRect(x: 0, y: 0, width: 375, height: 56)
        guru_vw.backgroundColor = UIColor(hex: "F7BB1A")
        strategyvw.backgroundColor = UIColor.clear
        //searGuru.active = true
        
        
        //tResult.removeAll()
        
    }*/
    
    @IBOutlet weak var Guru_s_tblvw: UITableView!
    
    var tResult : [NSDictionary] = []
    var tgResult : [NSDictionary] = []
    var buttonIsSelected = false
    var response = [NSDictionary]()
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.guru_vw.backgroundColor = UIColor(hex: "F7BB1A")
        //self.Guru_s_tblvw.tableHeaderView = nil;
        //searGuru.delegate = self as UISearchBarDelegate
        // searGuru.becomeFirstResponder()
        getFinancialGuru(completion: { (dataset) in
            
            self.tResult = dataset
            self.tbl_GuruHeight.constant = CGFloat(self.tResult.count * 90)
            print("Count : \(self.tResult.count)")
            self.Guru_s_tblvw.reloadData()
            
        })
    }
    
    
    /* func addDoneButtonOnKeyboard() {
     
     let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
     doneToolbar.barStyle       = UIBarStyle.default
     let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
     
     let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction)
     )
     
     var items = [UIBarButtonItem]()
     items.append(flexSpace)
     items.append(done)
     
     doneToolbar.items = items
     doneToolbar.sizeToFit()
     
     searGuru.inputAccessoryView = doneToolbar
     }
     
     @objc func doneButtonAction() {
     // searGuru.resignFirstResponder()
     }
     */
    
    func loadDataForResultView(searchval: String){
        let obj = WebService()
        
        
        let paremeters = "financial_guru_id=0&financial_guru_name=\(searchval)&stype=&is_active=Y"//"user_id=\(User.socialId)"
        
        
        obj.callWebServices(url: Urls.get_financial_guru, methodName: "POST", parameters: paremeters, istoken: false, tokenval: User.token) { (returnValue, jsonData) in
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                if let tempArray = jsonData.value(forKey: "response") as? [NSDictionary] {
                    self.tResult = tempArray.isEmpty ? [NSDictionary]() : tempArray
                    
                    self.Guru_s_tblvw.reloadData()
                    
                }
            } else {
                AppManager.showAlertMessage(title: "errror", message: jsonData.value(forKey: "errmsg") as! String)}
        }
        
    }
    
    
    
    /*    func getStrategies(completion : @escaping ([NSDictionary])->()) {
     
     let obj = WebService()
     
     let paremeters = "user_id=211297"//"user_id=\(User.socialId)"
     
     //actInd.startAnimating()
     
     obj.callWebServices(url: Urls.get_strategies, methodName: "GET", parameters: paremeters, istoken: false, tokenval: User.token) { (returnValue, jsonData) in
     
     var result : [NSDictionary]
     result=[NSDictionary]()
     print("Json Data new is :\(jsonData)")
     
     if jsonData.value(forKey: "errmsg") as! String == "" {
     
     if jsonData.value(forKey: "response") as? [NSDictionary] != nil{
     
     result = jsonData.value(forKey: "response") as! [NSDictionary]
     
     return completion(result)
     }
     return completion(result)
     } else{
     
     }
     
     }
     }
     
     */
    
    
    func getFinancialGuru (completion : @escaping ([NSDictionary])->()) {
        
        let obj = WebService()
        
        let paremeters = "financial_guru_id=0&financial_guru_name=&stype=&is_active=Y"//"user_id=\(User.socialId)"
        
        //actInd.startAnimating()
        
        obj.callWebServices(url: Urls.get_financial_guru, methodName: "POST", parameters: paremeters, istoken: false, tokenval: User.token) { (returnValue, jsonData) in
            
            var result : [NSDictionary]
            result=[NSDictionary]()
            print("Json Data neww is :\(jsonData)")
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                if jsonData.value(forKey: "response") as? [NSDictionary] != nil{
                    result = jsonData.value(forKey: "response") as! [NSDictionary]
                    
                    return completion(result)
                }
                return completion(result)
            } else{
                
            }
            
        }
    }
    
    
    
    
    
    
    
    
    
}
extension GuruStrategyViewController : UITableViewDelegate  , UITableViewDataSource , UISearchBarDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.tResult.count
        
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Guru_s_tblvw.dequeueReusableCell(withIdentifier:"gurusCell") as! gurusCell
        let result  = tResult[indexPath.row]
        
        print(result)
        
        
        if let name = result.value(forKey: "Guru") as? String{
            cell.lbl1.text! = name
            cell.lbl1.text! = cell.lbl1.text!.uppercased()
        }
        
        
        if let guru_imgUrl = result.value(forKey: "GuruProFilePic") as? String{
            let imageUrl:URL = URL(string: guru_imgUrl)!
            
            let imageData:NSData = NSData(contentsOf: imageUrl)!
            
            let image = UIImage(data: imageData as Data)
            
            cell.img_Guru.image = image
            
            cell.img_Guru.contentMode = UIViewContentMode.scaleAspectFit
            
        }
        //else {
        //  if let name = result.value(forKey: "sm_Name") as? String{
        //     cell.lbl1.text! = name
        //   }
        
        
        
        
        cell.selectionStyle = .none
        return cell
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
        
        
    }
    func searchBar(_ searchBar: UISearchBar , textDidChange searchText: String) {
        
        if searchText == "" {
            
            self.Guru_s_tblvw.reloadData()
        } else if searchText.count > 1 {
            
            loadDataForResultView(searchval: searchText)
            
            
            
        }
        
        
        
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
        let guruProfileController = storyboard?.instantiateViewController(withIdentifier: "ProfileHeaderViewController") as? ProfileHeaderViewController
        
        
        //let cell = Guru_s_tblvw.dequeueReusableCell(withIdentifier:"gurusCell") as! gurusCell
        let result  = tResult[indexPath.row]
        
        print(result)
        
        
        let name = result.value(forKey: "Guru")
        
        let g_id = result.value(forKey: "afg_id")
        
        guruProfileController!.gid = g_id as! String
        guruProfileController!.namecell = name as! String
        
        
        User.navigation.pushViewController(guruProfileController!, animated: true)
        
    }
}

