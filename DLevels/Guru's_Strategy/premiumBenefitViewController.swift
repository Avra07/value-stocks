//
//  premiumBenefitViewController.swift
//  DLevels
//
//  Created by Admin on 04/10/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class premiumBenefitViewController: UIViewController {

    @IBOutlet weak var subscribe_btn: UIButton!
    @IBOutlet weak var statictext: UILabel!
    @IBOutlet weak var mtbl : UITableView!
    @IBOutlet weak var midVw: UIView!
    @IBOutlet weak var upperVw: UIView!
    @IBOutlet weak var lbl_premium: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    var mResult : [NSDictionary] = []
    var mload :[NSDictionary] = []
    
    
    
    @IBAction func back_btn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getData(completion: { (dataset) in
            self.mResult = dataset
            
            self.mtbl.reloadData()
        })
        
    }
 
   
    
     //MARK: GET DATA
     func getData (completion : @escaping ([NSDictionary])->()) {
     
     let obj = WebService()
     var result : [NSDictionary] = [NSDictionary]()
     let paremeters = "app_sub_id=0&active=Y"
     
     //actInd.startAnimating()
     
     obj.callWebServices(url: Urls.get_app_subscription_benefits, methodName: "GET", parameters: paremeters, istoken: false, tokenval: User.token) { (returnValue, jsonData) in
     
     print("Json Data is :\(jsonData)")
     
     if jsonData.value(forKey: "errmsg") as! String == "" {
     
     if jsonData.value(forKey: "response") as? [NSDictionary] != nil{
     result = jsonData.value(forKey: "response") as! [NSDictionary]
     return completion(result)
     }
     
     }
     
     }
     
     
     }
    
}
extension premiumBenefitViewController : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return self.mResult.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cel = mtbl.dequeueReusableCell(withIdentifier:"benefitTableViewCell") as! benefitTableViewCell
        
        let benefit = mResult[indexPath.row]
        
        
        if let header = benefit.value(forKey: "asb_Name") as? String{
            cel.titlelb.text! = header
        }
        
        if let text = benefit.value(forKey: "asb_Image_Path") as? String{
           // cel.img.text! = text
            let imageUrl:URL = URL(string: text)!
            print("Image : \(imageUrl)")
            let imageData:NSData = NSData(contentsOf: imageUrl)!
            let image = UIImage(data: imageData as Data)
            cel.img.image = image
            cel.img.contentMode = UIViewContentMode.scaleAspectFit
        }
        if let un_updt_time = benefit.value(forKey: "asb_Desc") as? String{
            cel.desclb.text! = un_updt_time
        }
        if let video = benefit.value(forKey: "asbd_Video_Link") as? String{
            cel.link = video
            
            
        }
        cel.selectionStyle = .none
        return cel
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
        
    }
}
