//
//  GurusProfileViewController.swift
//  DLevels
//
//  Created by SAIKAT GHOSH on 25/08/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
// Avra change


import UIKit
class RuleCell : UITableViewCell{
    
    @IBOutlet weak var rulelbl : UILabel!
    @IBOutlet weak var black_dot: UILabel!
    
    
}


class GurusProfileViewController: UIViewController {
    
    
    @IBOutlet weak var vW : UIView!
    @IBOutlet weak var lbl : UILabel!
    @IBOutlet weak var lbl1 : UILabel!
    @IBOutlet weak var lbl2 : UILabel!
    @IBOutlet weak var lbl_Disclaimer: UILabel!
    
    @IBOutlet weak var lbl3 : UILabel!
    @IBOutlet weak var lbl4 : UILabel!
    @IBOutlet weak var lbl5 : UILabel!
    @IBOutlet weak var lbl7 : UILabel!
    @IBOutlet weak var lbl6 : UILabel!
    @IBOutlet weak var vW1 : UIView!
    @IBOutlet weak var vW2 : UIView!
    @IBOutlet weak var vW3 : UIView!
    var arrfilter = [String]()
    
    @IBOutlet weak var view_disclaimer: UIView!
    @IBOutlet weak var ruleTblVw: UITableView!
    @IBOutlet weak var profilename: UITextField!
    @IBOutlet weak var footvw: UIView!
    @IBOutlet weak var containerVw: UIView!
    @IBOutlet weak var abtvw: UIView!
    @IBOutlet weak var navVw: UIView!
    @IBOutlet weak var about: UIButton!
    @IBOutlet weak var search: UIButton!
    
    @IBOutlet weak var btn_profileRead: UIButton!
    @IBOutlet weak var btn_strategyRead: UIButton!
    @IBOutlet weak var btn_disclaimerRead: UIButton!
    @IBOutlet weak var btn_subStocks: UIButton!
    
    @IBAction func getStocks(_ sender: Any) {
        
        print("Status : \(Subscription_Data.status)")
        if Subscription_Data.status {
            if filterDisplay != "" {
                
                selectedFilter.displayText = filterDispalyGlobal
                let display: String = filterDisplay.replacingOccurrences(of: "|", with: " ")
                arrfilter = display.split(separator: ";").map { String($0) }
                
            }
            
            let strybrd =  UIStoryboard.init(name: "Main", bundle: nil)
            
            let obj = strybrd.instantiateViewController(withIdentifier: "GurusFilteredStocksViewController") as! GurusFilteredStocksViewController
            obj.heading = "Gurus Stocks"
            selectedFilter.queryText = filterVal
            selectedFilter.dispalyQueryArr = arrfilter
            User.navigation.pushViewController(obj, animated: true)
        
        }else{
           
            let strybrd =  UIStoryboard.init(name: "Main", bundle: nil)
            
            let obj = strybrd.instantiateViewController(withIdentifier: "premiumBenefitViewController") as! premiumBenefitViewController
              User.navigation.pushViewController(obj, animated: true)
        }
    }
    
   
    
    @IBOutlet weak var vwRulesHeightConstant: NSLayoutConstraint!
    
    @IBAction func searchguru(_ sender: Any) {
        
        
        let strybrd =  UIStoryboard.init(name: "Main", bundle: nil)
        let obj = strybrd.instantiateViewController(withIdentifier: "GuruStrategyViewController") as! GuruStrategyViewController
        self.navigationController?.pushViewController(obj, animated: true)
        
        
        
    }
    @IBOutlet weak var backbtn: UIButton!
    @IBAction func backbtnaction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func video_Btn(_ sender: Any) {
        
        let strybrd =  UIStoryboard.init(name: "Main", bundle: nil)
        let obj1 = strybrd.instantiateViewController(withIdentifier: "GurusVideoViewController") as! GurusVideoViewController
        self.navigationController?.pushViewController(obj1, animated: true)
        obj1.titlename = profilename.text!
        
        
    }
    @IBOutlet weak var logo: UIImageView!
    
    
    @IBOutlet weak var ftVw: UIView!
    @IBOutlet weak var scrlVw: UIScrollView!
    var ary = [String]()
    var namecell = ""
    var gid = ""
    var listarry = [String]()
    var  arr : [NSDictionary] = []
    var rulearray = [String]()
    var tittle = ""
    var filterDisplay = ""
    var filterDispalyGlobal = ""
    var filterVal = ""
    var longDesc : String?
    var initial : String?
    var longProfile : String?
    var shortProfile : String?
    var longStrategy : String?
    var shortStrategy : String?
    var flag : Int = 1
    var flagStrategy : Int = 1
    var getGuruData = [Guru]()
    var activityIndicator: UIActivityIndicatorView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.profilename.text = ttle
        btn_disclaimerRead.setTitleColor(Colors.btn_readColor, for: UIControlState.normal)
        btn_profileRead.setTitleColor(Colors.btn_readColor, for: UIControlState.normal)
        btn_strategyRead.setTitleColor(Colors.btn_readColor, for: UIControlState.normal)
        
        getFinancialGuru(completion: { (dataset) in
            self.getRulesByGuru()
            
        })
        
        
        vW.layer.shadowColor = UIColor.gray.cgColor
        vW.layer.shadowOpacity = 0.5
        vW.layer.shadowRadius = 3
        vW.layer.shadowOffset = CGSize(width: 0, height: 3)
        vW.layer.masksToBounds = false
        vW1.layer.shadowColor = UIColor.gray.cgColor
        vW1.layer.shadowOpacity = 0.5
        vW1.layer.shadowRadius = 3
        vW1.layer.shadowOffset = CGSize(width: 0, height: 3)
        vW1.layer.masksToBounds = false
        vW2.layer.shadowColor = UIColor.gray.cgColor
        vW2.layer.shadowOpacity = 0.5
        vW2.layer.shadowRadius = 3
        vW2.layer.shadowOffset = CGSize(width: 0, height: 3)
        vW2.layer.masksToBounds = false
        vW3.layer.shadowColor = UIColor.gray.cgColor
        vW3.layer.shadowOpacity = 0.5
        vW3.layer.shadowRadius = 3
        vW3.layer.shadowOffset = CGSize(width: 0, height: 3)
        
        view_disclaimer.layer.masksToBounds = false
        view_disclaimer.layer.shadowColor = UIColor.gray.cgColor
        view_disclaimer.layer.shadowOpacity = 0.5
        view_disclaimer.layer.shadowRadius = 3
        view_disclaimer.layer.shadowOffset = CGSize(width: 0, height: 3)
        view_disclaimer.layer.masksToBounds = false
        footvw.layer.cornerRadius = 4
        footvw.clipsToBounds = true
    }
    
    @IBAction func btn_ReadMore(_ sender: Any) {
        if (flag == 0){
            lbl1.text = shortProfile
            flag = 1
            btn_profileRead.setTitle("Read More", for: UIControlState.normal)
        }
        else if  (flag == 1){
            lbl1.text = longProfile
            flag = 0
            btn_profileRead.setTitle("Read Less", for: UIControlState.normal)
        }
    }
    
    @IBAction func btn_StrategyRead(_ sender: Any) {
        if (flagStrategy == 0){
            lbl3.text = shortStrategy
            flagStrategy = 1
            btn_strategyRead.setTitle("Read More", for: UIControlState.normal)
        }
        else if  (flagStrategy == 1){
            lbl3.text = longStrategy
            flagStrategy = 0
            btn_strategyRead.setTitle("Read Less", for: UIControlState.normal)
        }
    }
    
    @IBAction func btn_DisclaimerRead(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let disclaimerController = storyBoard.instantiateViewController(withIdentifier: "DisclaimerViewController") as! DisclaimerViewController
        disclaimerController.disclaimerUrl = DisclaimerUrl.guru_s_strategy
        disclaimerController.disclaimerHeader = "All Gurus"
        User.navigation.pushViewController(disclaimerController, animated: true)
    }
    
    func getRulesByGuru(){
        
        let obj = WebService()
        var result = [[NSDictionary]]()
        let paremeters = "financial_guru_id=\(gid)&financial_guru_name=\(namecell)&stype=&is_active=Y"//"user_id=\(User.socialId)"
        
        //actInd.startAnimating()
        
        obj.callWebServices(url: Urls.get_financial_guru, methodName: "POST", parameters: paremeters, istoken: false, tokenval: User.token) { (returnValue, jsonData) in
            
            //result=[[NSDictionary]]
            print("Json Data neww is :\(jsonData)")
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                result = jsonData.value(forKey: "response") as! [[NSDictionary]]
                for i in result{
                    for j in i {
                        if let fillter = j.value(forKey: "gd_filters_display") as? String{
                            
                            let array = fillter.components(separatedBy: ";")
                            for arrayElement in array
                            {
                                if (arrayElement != "")
                                {
                                    self.rulearray.append(arrayElement)
                                }
                            }
                            //self.rulearray = array
                        }
                        if let filters = j.value(forKey: "gd_filters") as? String {
                            //let arry = filters.replacingOccurrences(of: "|", with: "") as String
                            self.filterVal = filters
                        }
                        if let filter = j.value(forKey: "gd_filters_display") as? String{
                            let arry = filter.replacingOccurrences(of: "|", with: "") as String
                            self.filterDisplay = arry
                            self.filterDispalyGlobal = filter
                        }
                    }
                }
                
                self.vwRulesHeightConstant.constant = CGFloat(self.rulearray.count * 45)
                self.ruleTblVw.reloadData()
                
            } else{
                
            }
            print("Rule Array : \(self.rulearray)")
        }
    }
    
    func getFinancialGuru (completion : @escaping ([[NSDictionary]])->()) {
        
        let obj = WebService()
        var result = [[NSDictionary]]()
        let paremeters = "financial_guru_id=\(gid)&financial_guru_name=\(namecell)&stype=&is_active=Y"//"user_id=\(User.socialId)"
        
        //actInd.startAnimating()
        
        obj.callWebServices(url: Urls.get_financial_guru, methodName: "POST", parameters: paremeters, istoken: false, tokenval: User.token) { (returnValue, jsonData) in
            
            //result=[[NSDictionary]]
            print("Json Data neww is :\(jsonData)")
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                
                result = jsonData.value(forKey: "response") as! [[NSDictionary]]
                for i in result{
                    for j in i {
                        
                        if let disclaimerText = j.value(forKey: "Disclaimer") as? String{
                            self.lbl_Disclaimer.text = disclaimerText
                        }
                        if let long_prfle = j.value(forKey: "afg_profile") as? String{
                            self.longProfile = long_prfle.replacingOccurrences(of: "\\n", with: "\n", options: .literal, range: nil)
                        }
                        if let short_profile = j.value(forKey: "afg_short_profile") as? String{
                            self.lbl1.text = short_profile
                            self.shortProfile = short_profile
                        }
                        
                        if let long_strat = j.value(forKey: "gd_strategy_desc") as? String{
                            self.longStrategy = long_strat
                        }
                        if let short_strat = j.value(forKey: "gd_strategy_short_desc") as? String{
                            self.lbl3.text = short_strat
                            self.shortStrategy = short_strat
                        }
                        
                        
                        if let surce = j.value(forKey: "gd_source") as? String{
                            self.lbl7.text! = surce
                        }
                        
                        if let fillter = j.value(forKey: "gd_filters_display") as? String{
                            
                            let array = fillter.components(separatedBy: ";")
                            
                        }
                        
                    }
                }
                
                
                
                
            } else{
                
            }
            return completion(result)
            
        }
    }
    
    
}


extension GurusProfileViewController : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("Row Count : \(self.rulearray.count)")
        return self.rulearray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = ruleTblVw.dequeueReusableCell(withIdentifier:"RuleCell") as! RuleCell
        cell.black_dot.layer.cornerRadius = 4.0
        cell.rulelbl.text = self.rulearray[indexPath.row].replacingOccurrences(of: "|", with: " ")
        cell.rulelbl.text = cell.rulelbl.text?.replacingOccurrences(of: "_", with: " ")
        cell.rulelbl.textColor = Colors.customBlueColor
        
        return cell
    }
}

