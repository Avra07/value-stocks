//
//  ProfileHeaderViewController.swift
//  DLevels
//
//  Created by Admin on 27/08/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
// change it

import UIKit


class ProfileHeaderViewController: UIViewController {
    
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var pageHeading: UILabel!
    var pageMenu : CAPSPageMenu!
    var controllerArray : [UIViewController] = []
    var index : Int = 0
    var guruDataDict = [String:String]()
    //var guru_Desc = [String:String]()
    var guruInitial : String?
    var pageHeader : String!
    
    var namecell = ""
    var gid = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pageHeading.text! = namecell
        addingAllView()
        // Do any additional setup after loading the view.
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addingAllView() {
        
        let headerV = self.storyboard!.instantiateViewController(withIdentifier: "GurusProfileViewController") as! GurusProfileViewController
        headerV.title = "ABOUT"
        headerV.longDesc = guruDataDict[headerV.title!]
        headerV.namecell = namecell
        headerV.gid = gid
        headerV.initial = headerV.title
        controllerArray.append(headerV)
        
        
        
        
        
        
        let headerA = self.storyboard!.instantiateViewController(withIdentifier: "GurusVideoViewController") as! GurusVideoViewController
        headerA.title = " VIDEO"
        headerA.longDesc = guruDataDict[headerA.title!]
        headerA.namecell = namecell
        headerA.gid = gid
        headerA.initial = headerA.title
        controllerArray.append(headerA)
        
        
        
        
        
        let parameters: [CAPSPageMenuOption] = [
            .menuItemFont(UIFont(name: "HelveticaNeue-Bold", size: 14.0)!),
            //.menuItemWidthBasedOnTitleTextWidth(true),
            .unselectedMenuItemLabelColor(Colors.customBlueColor),
            .selectedMenuItemLabelColor(Colors.customBlueColor),
            .menuHeight(40.0),
            .menuItemWidth(180.0),
            .centerMenuItems(false)
            
        ]
        
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: containerView.frame.width, height: containerView.frame.height), pageMenuOptions: parameters)
        containerView.backgroundColor = UIColor.white
        containerView.addSubview(pageMenu!.view)
        
        pageMenu.delegate = self as? CAPSPageMenuDelegate
        
        //pageHeading.text = HeaderText.guru_Desc[HeaderText.headertext]
        pageMenu.moveToPage(index)
        
    }
    
    func didMoveToPage(_ controller: UIViewController, index: Int){
        if (index == 0){
            pageHeading.text = HeaderText.guru_Desc["ABOUT"]!
        }
        if (index == 1){
            pageHeading.text = HeaderText.guru_Desc["VIDEO"]!
        }
        
    }
    
    @IBAction func btn_Home(_ sender: Any) {
        let homeController = self.storyboard?.instantiateViewController(withIdentifier: "DlHomeViewController") as! DlHomeViewController
        self.navigationController?.pushViewController(homeController, animated: false)
    }
    
    
    @IBAction func btnBack_Action(_ sender: UIButton) {
        
        User.navigation.popViewController(animated: true)
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
}

