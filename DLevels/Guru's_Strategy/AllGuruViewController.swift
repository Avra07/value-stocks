//
//  AllGuruViewController.swift
//  DLevels
//
//  Created by Dynamic on 23/08/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit


class AllGuruViewController: UIViewController, CAPSPageMenuDelegate {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var pageHeading: UILabel!
    var pageMenu : CAPSPageMenu!
    var controllerArray : [UIViewController] = []
    var index : Int = 0
    var guruDataDict = [String:String]()
    //var guru_Desc = [String:String]()
    var guruInitial : String?
    var pageHeader : String!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pageHeading.text = HeaderText.guru_Desc[guruInitial!]
        addingAllView()
        // Do any additional setup after loading the view.
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addingAllView() {
        
        let headerV = self.storyboard!.instantiateViewController(withIdentifier: "GuruViewController") as! GuruViewController
        headerV.title = "V"
        headerV.longDesc = guruDataDict[headerV.title!]
        headerV.initial = headerV.title
        controllerArray.append(headerV)
        
        let headerA = self.storyboard!.instantiateViewController(withIdentifier: "GuruViewController") as! GuruViewController
        headerA.title = "A"
        headerA.longDesc = guruDataDict[headerA.title!]
        headerA.initial = headerA.title
        controllerArray.append(headerA)
        
        let headerL = self.storyboard!.instantiateViewController(withIdentifier: "GuruViewController") as! GuruViewController
        headerL.title = "L"
        headerL.longDesc = guruDataDict[headerL.title!]
        headerL.initial = headerL.title
        controllerArray.append(headerL)
        
        let headerU = self.storyboard!.instantiateViewController(withIdentifier: "GuruViewController") as! GuruViewController
        headerU.title = "U"
        headerU.longDesc = guruDataDict[headerU.title!]
        headerU.initial = headerU.title
        controllerArray.append(headerU)
        
        let headerE = self.storyboard!.instantiateViewController(withIdentifier: "GuruViewController") as! GuruViewController
        headerE.title = "E"
        headerE.longDesc = guruDataDict[headerE.title!]
        headerE.initial = headerE.title
        controllerArray.append(headerE)
        
        
        
        let parameters: [CAPSPageMenuOption] = [
            .menuItemFont(UIFont(name: "HelveticaNeue-Bold", size: 14.0)!),
            //.menuItemWidthBasedOnTitleTextWidth(true),
            .unselectedMenuItemLabelColor(Colors.customBlueColor),
            .selectedMenuItemLabelColor(Colors.customBlueColor),
            .menuHeight(40.0),
            .menuItemSeparatorWidth(0.0),
            .menuMargin(10.0),
            .menuItemWidth((containerView.frame.width - 20.0)/5),
            .centerMenuItems(true)
            
        ]
        print("Parameters : \(parameters)")
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: containerView.frame.width, height: containerView.frame.height), pageMenuOptions: parameters)
        containerView.backgroundColor = UIColor.white
        containerView.addSubview(pageMenu!.view)
        
        pageMenu.delegate = self
        
        //pageHeading.text = HeaderText.guru_Desc[HeaderText.headertext]
        pageMenu.moveToPage(index)
        
    }
    
    func didMoveToPage(_ controller: UIViewController, index: Int){
        if (index == 0){
            pageHeading.text = HeaderText.guru_Desc["V"]!
        }
        if (index == 1){
            pageHeading.text = HeaderText.guru_Desc["A"]!
        }
        if (index == 2){
            pageHeading.text = HeaderText.guru_Desc["L"]!
        }
        if (index == 3){
            pageHeading.text = HeaderText.guru_Desc["U"]!
        }
        if (index == 4){
            pageHeading.text = HeaderText.guru_Desc["E"]!
        }
    }
    
    
    
    @IBAction func btnBack_Action(_ sender: UIButton) {
        
        // self.navigationController?.popViewController(animated: true)
        let viewCont = self.storyboard?.instantiateViewController(withIdentifier: "DlHomeViewController") as! DlHomeViewController
        self.navigationController?.pushViewController(viewCont, animated: false)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
}

