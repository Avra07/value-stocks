//
//  GuruViewController.swift
//  DLevels
//
//  Created by Dynamic on 23/08/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit



class GuruViewCell: UITableViewCell {
    @IBOutlet weak var img_Guru: UIImageView!
    @IBOutlet weak var lbl_GuruName: UILabel!
    
}

class GuruViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var displayView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var guruList: UITableView!
    
    @IBOutlet weak var lbl_longDesc: UILabel!
    @IBOutlet weak var lbl_Initial: UILabel!
    @IBOutlet weak var lbl_Gurus: UILabel!
    @IBOutlet weak var lbl_displayMsg: UILabel!
    @IBOutlet weak var videoVw: UIView!
    @IBOutlet weak var toknowmore: UILabel!
    @IBOutlet weak var Vwimg: UIImageView!
    @IBAction func toknowLinkvideo(_ sender: Any) {
        
        UIApplication.shared.openURL(NSURL(string: "https://youtu.be/_-LDqMik4mw") as! URL)
        
        
    }
    var longDesc : String?
    var initial : String?
    var getGuruData = [Guru]()
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0) //CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.gray
        self.view.addSubview(actInd)
        actInd.startAnimating()
        HeaderText.headertext = initial!
        self.guruList.separatorColor = UIColor(patternImage: UIImage(named: "dotted_line.png")!)
        lbl_longDesc.text = longDesc
        lbl_longDesc.textColor = Colors.customBlueColor
        
        lbl_Initial.text = initial
        lbl_Initial.textColor = Colors.customBlueColor
        
        lbl_Gurus.textColor = Colors.customBlueColor
        headerView.backgroundColor = Colors.headerViewColor
        displayView.backgroundColor = Colors.viewColor
        //applyShadowOnView()
        
        
        getGuruDetails()
        // Do any additional setup after loading the view.
    }
    
    
    
    
    func applyShadowOnView() {
        
        headerView.layer.shadowColor = UIColor.gray.cgColor
        headerView.layer.shadowOpacity = 0.5
        headerView.layer.shadowRadius = 3
        headerView.layer.shadowOffset = CGSize(width: 0, height: 3)
        headerView.layer.masksToBounds = false
    }
    
    func getGuruDetails(){
        getGuruData = []
        guruList.delegate = self
        guruList.dataSource = self
        let isActive = "Y"
        let obj = WebService()
        
        let parameter = "financial_guru_id=0&financial_guru_name=&stype=\(initial!)&is_active=\(isActive)"
        
        //actInd.startAnimating()
        
        
        
        obj.callWebServices(url: Urls.get_financial_guru, methodName: "POST", parameters: parameter, istoken: false, tokenval: User.token) { (returnValue, jsonData) in
            
        /*obj.callWebServices_new(url: Urls.get_financial_guru, methodName: "POST", parameters: parameter,istoken: true, tokenval: User.token, isShowLoader: true) { (returnValue, jsonData) in */
            print(jsonData)
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                let getGuruData = jsonData.value(forKey: "response") as! NSArray
                
                for guruArrayDetail in getGuruData{
                    if let guruDetailDict = guruArrayDetail as? NSDictionary {
                        var guruData = Guru(guru_Id: "", guru_imgLink: "", guru_Name: "")
                        if let guruId = guruDetailDict.value(forKey: "afg_id"){
                            guruData.guru_Id = guruId as? String
                        }
                        if let guruImage = guruDetailDict.value(forKey: "GuruProFilePic"){
                            guruData.guru_imgLink = guruImage as? String
                        }
                        if let guruName = guruDetailDict.value(forKey: "Guru"){
                            guruData.guru_Name = guruName as? String
                        }
                        self.getGuruData.append(guruData)
                    }
                }
                if getGuruData.count == 0{
                    self.guruList.isHidden = true
                    self.lbl_displayMsg.backgroundColor = .clear
                    self.lbl_displayMsg.numberOfLines = 2
                    self.lbl_displayMsg.font = UIFont(name: "Montserrat", size: 17)
                    self.lbl_displayMsg.textAlignment = .left
                    self.lbl_displayMsg.textColor = UIColor(hex:"284C5A")
                    self.lbl_displayMsg.text = "Coming Soon..."
                }
                else{
                    print("Im inside else")
                    self.messageView.isHidden = true
                    self.guruList.reloadData()
                }
            }
            self.actInd.stopAnimating()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return getGuruData.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GuruCell") as! GuruViewCell
        if(getGuruData[indexPath.row].guru_Name != ""){
            cell.lbl_GuruName.text = getGuruData[indexPath.row].guru_Name
            cell.lbl_GuruName.textColor = Colors.customBlueColor
        }else{}
        print("Guru Name : \(getGuruData[indexPath.row].guru_Name!)")
        let imageUrlString = (getGuruData[indexPath.row].guru_imgLink)!
        if (imageUrlString != ""){
            let imageUrl:URL = URL(string: imageUrlString)!
            print("Image : \(imageUrl)")
            let imageData:NSData = NSData(contentsOf: imageUrl)!
            let image = UIImage(data: imageData as Data)
            cell.img_Guru.image = image
            cell.img_Guru.contentMode = UIViewContentMode.scaleAspectFit
        }else{}
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let guruProfileController = storyboard?.instantiateViewController(withIdentifier: "ProfileHeaderViewController") as? ProfileHeaderViewController
        guruProfileController?.namecell = getGuruData[indexPath.row].guru_Name!
        guruProfileController?.gid = getGuruData[indexPath.row].guru_Id!
        
        User.navigation.pushViewController(guruProfileController!, animated: true)
    }
    
}

struct Guru {
    var guru_Id : String?
    var guru_imgLink : String?
    var guru_Name : String?
    
}

