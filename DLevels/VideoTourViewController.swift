//
//  VideoTourViewController.swift
//  DLevels
//
//  Created by Shailesh Saraf on 09/09/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class CustomVideoTourCell: UITableViewCell {
    
    @IBOutlet weak var imgVideo: UIImageView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var btnVideoLink: UIButton!
    @IBOutlet weak var lblDetails: UILabel!
}

class VideoTourViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var tableViewHeightConstant: NSLayoutConstraint!
    
    var isShowingMenu = false
    var instanceOfLeftSlideMenu = LeftSlideMenu()
    var videodata         =     [NSDictionary]()
    var videolinkdata         =    [String]()
    
    
    
    @IBOutlet weak var btnHamburger: UIButton!
    @IBOutlet weak var webinartbl: UITableView!
    @IBOutlet weak var bottomScrollView: UIScrollView!
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    var isAdShow = true
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        loadVideoData()
        addLoader()
        webinartbl.tableFooterView = UIView()
        
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    
    
    func addLoader() {
        actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0) //CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.gray
        self.view.addSubview(actInd)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tapAction(sender: UITapGestureRecognizer) {
        
        instanceOfLeftSlideMenu.hideSlideMenu(navigationController: self.navigationController!, viewSize: self.view)
        btnHamburger.frame.origin.x -= 250
        isShowingMenu = false
    }
    
    //MARK": Getting Data for table.
    func loadVideoData()
    {
        
        self.actInd.startAnimating()
        
        //        let alertobj = AppManager()
        //        alertobj.showAlert(title: "Error!", message: User.token as! String , navigationController: self.navigationController!)
        
        
        
        
        let obj = WebService()
        let paremeters = "video_id=0"
        
        obj.callWebServices(url: Urls.get_video_list, methodName: "POST", parameters: paremeters, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
            
            //                let alertobj = AppManager()
            //                alertobj.showAlert(title: "Data!", message: "Json Data is :  \(jsonData)" as! String , navigationController: self.navigationController!)
            
            //print("Json Data is :  \(jsonData)")
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                
                self.videodata.removeAll()
                self.videodata = jsonData.value(forKey: "response") as! [NSDictionary]
                
                
                for dict in self.videodata {
                    
                    self.videolinkdata.append(dict.value(forKey: "vl_Link") as! String)
                }
                print("VIDEOLOAD \(self.videolinkdata)")
                
                DispatchQueue.main.async(execute: {
                    //Thread.current.cancel()
                    
                    self.webinartbl.reloadData()
                    self.tableViewHeightConstant.constant = CGFloat(self.videodata.count * 250)
                    self.actInd.stopAnimating()
                    
                })
            }
            else
            {
                
                DispatchQueue.main.async {
                    self.actInd.stopAnimating()
                    let alertobj = AppManager()
                    alertobj.showAlert(title: "Error!", message: jsonData.value(forKey: "errmsg") as! String , navigationController: self.navigationController!)
                }
            }
        }
    }
    
    //MARK: Table View Delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    //MARK: Table View Delegate
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:"CustomVideoTourCell", for: indexPath) as! CustomVideoTourCell
        cell.selectionStyle = .none
        
        cell.lblDate.text = self.videodata[indexPath.row].value(forKey: "vl_Date") as? String
        cell.lblDetails.text = self.videodata[indexPath.row].value(forKey: "vl_Header") as? String
        cell.btnVideoLink.tag = indexPath.row
        cell.btnVideoLink.addTarget(self, action:#selector(playvideo(sender:)), for: UIControlEvents.touchUpInside)
        let imageval = self.videodata[indexPath.row].value(forKey: "vl_Image_Path") as? String
        
        
        //cell.imgVideo.addCornerRadius(value: 12)
        cell.imgVideo.downloadedFrom(link: imageval!)
        
        return cell
    }
    
    func playvideo(sender:UIButton) {
        //let buttonRow = sender.tag
        print(videolinkdata[sender.tag])
        UIApplication.shared.openURL(NSURL(string: videolinkdata[sender.tag])! as URL)
    }
    
    
    //MARK: Table View Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return videodata.count
    }
    
    
    // MARK: - Navigation
    @IBAction func onClickHambergerIcon(_ sender: UIButton) {
        
        if NotificationData.category != "" {
            let viewController = self.navigationController?.storyboard?.instantiateViewController(withIdentifier: "DlHomeViewController") as! DlHomeViewController
            
            User.navigation.pushViewController(viewController, animated: false)
        }else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
}
