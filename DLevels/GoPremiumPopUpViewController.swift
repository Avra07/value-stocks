//
//  GoPremiumPopUpViewController.swift
//  DLevels
//
//  Created by SAIKAT GHOSH on 08/08/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class GoPremiumPopUpViewController: UIViewController {
    
    @IBOutlet weak var go_premium_img: UIImageView!
    @IBOutlet weak var crossout: UIButton!
    @IBOutlet weak var trail_btn: UIButton!
    
    @IBAction func btnCross_Action(_ sender: UIButton) {
        dismissCurrentPage()
    }
    
    //MARK: DISMISS CURRENT PAGE
    private func dismissCurrentPage(){
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnTap(_ sender: Any) {
        
        UIApplication.shared.openURL(NSURL(string: "https://www.dynamiclevels.com/en/my-subscription/\(User.token)")! as URL)
    }
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.clear
        view.isOpaque = false
        // Do any additional setup after loading the view.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
