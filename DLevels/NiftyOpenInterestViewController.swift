//
//  NiftyOpenInterestViewController.swift
//  DLevels
//
//  Created by Rishi Sachdeva on 14/03/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class NiftyOpenInterestCell: UITableViewCell {
	
	@IBOutlet weak var lblDate: UILabel!
	@IBOutlet weak var lblFiiPro: UILabel!
	@IBOutlet weak var lblClient: UILabel!
	@IBOutlet weak var lblNiftClose: UILabel!
}

class NiftyOpenInterestViewController: UIViewController {
	
	@IBOutlet weak var tableVw: UITableView!
	@IBOutlet weak var navBarTitle: UINavigationItem!
	fileprivate var mainDataArray = [NiftyOpenInterest]()
    @IBOutlet weak var btn_disclaimer: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
		AppManager().setStatusBarBackgroundColor()
        // Do any additional setup after loading the view.
		
		
		let label = UILabel(frame: CGRect(x:0, y:0, width:400, height:50))
		label.backgroundColor = .clear
		label.numberOfLines = 2
		label.font = UIFont.boldSystemFont(ofSize: 15.0)
		label.textAlignment = .left
		label.textColor = UIColor(hex:"284C5A")
		label.text = "Nifty Open Interest"
		self.navBarTitle.titleView = label
        btn_disclaimer.setTitleColor(Colors.customBlueColor, for: UIControlState.normal)
		
		
		
		let obj_NiftyOpenInterestDM = NiftOpenInterestDataModel()
		obj_NiftyOpenInterestDM.getData(completion: { (dataset) in
			self.mainDataArray = dataset
			self.tableVw.reloadData()
			
			
		})
    }
	
    @IBAction func btn_Disclaimer(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let disclaimerController = storyBoard.instantiateViewController(withIdentifier: "DisclaimerViewController") as! DisclaimerViewController
        disclaimerController.disclaimerUrl = DisclaimerUrl.nifty_open_interest
        disclaimerController.disclaimerHeader = "Nifty Open Interest"
        User.navigation.pushViewController(disclaimerController, animated: true)
        
    }
    
    
    @IBAction func backClick(_ sender: Any) {
		if let nav = self.navigationController {
			nav.popViewController(animated: true)
		} else {
			self.dismiss(animated: true, completion: nil)
		}
		
	}
}

extension NiftyOpenInterestViewController: UITableViewDataSource,UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		if let cell = tableView.dequeueReusableCell(withIdentifier:
			"NiftyOpenInterestCell", for: indexPath) as? NiftyOpenInterestCell
		{
			cell.selectionStyle = .none
			
			cell.backgroundColor = indexPath.row % 2 == 0 ?  UIColor(red: 215/255, green: 222/255, blue: 225/255, alpha: 1.0) : UIColor(red: 236/255, green: 239/255, blue: 240/255, alpha: 1.0)
			
			cell.lblDate.text = mainDataArray[indexPath.row].date
			cell.lblFiiPro.text = mainDataArray[indexPath.row].fiiPro
			cell.lblClient.text = mainDataArray[indexPath.row].client
			cell.lblNiftClose.text = mainDataArray[indexPath.row].NiftyClose
			
			return cell
		}
		return UITableViewCell()
	}
	
	
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return mainDataArray.count
	}
	public func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
		
		return 900
	}
	public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		
		return UITableViewAutomaticDimension
	}
	
	
}
