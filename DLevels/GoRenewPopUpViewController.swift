//
//  GoRenewPopUpViewController.swift
//  DLevels
//
//  Created by SAIKAT GHOSH on 08/08/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class GoRenewPopUpViewController: UIViewController {
    @IBOutlet weak var btnno: UIButton!
    @IBOutlet weak var btnrenew: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
       
    }
    
    @IBAction func btnCross_Action(_ sender: UIButton) {
        dismissCurrentPage()
    }
    
    //MARK: DISMISS CURRENT PAGE
    private func dismissCurrentPage(){
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func renewTap(_ sender: Any) {
        dismissCurrentPage()
        UIApplication.shared.openURL(NSURL(string: "https://www.dynamiclevels.com/en/my-subscription/\(User.token)")! as URL)
        
        
    }
    
    @IBAction func nothanksTap(_ sender: Any) {
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
