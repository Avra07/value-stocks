//
//  DynamicIndexHomeViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-01 on 07/10/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class DynamicIndexHomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.shouldRotate = false
        // Do any additional setup after loading the view.
        
        appDelegate.shouldRotate = false // or false to disable rotation
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnDetailsClick(_ sender: Any) {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "DynamicIndexController") as! DynamicIndexController
        self.navigationController?.pushViewController(viewController, animated: false)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
