//
//  MFFundDetailsDataModel.swift
//  DLevels
//
//  Created by Dlevels-MacMini1 on 27/05/18.
//  Copyright © 2018 Dynamic-Mac-01. All rights reserved.
//

import Foundation

class MFFundDetailsDataModel:NSObject{
    
    var fund = ""
    var subCategory = ""
    var fundHouse = ""
    var fundName = ""
    var fundNewName = ""
    var fundOldName = ""
    
    
    
    var totalNetAssets :Int         = 0
    var oneMReturn :Decimal             = 0.00
    var oneWAvgReturn :Decimal            = 0.00
    var yrAvgReturn :Decimal            = 0.00
    var threeMReturn :Decimal           = 0.00
    var sixMReturn :Decimal           = 0.00
    var threeYrAvgReturn :Decimal       = 0.00
    var fiveYrAvgReturn :Decimal        = 0.00
    var rating = ""
    
    
    func getData(Category:String, FundName:String, completion : @escaping ([MFFundDetails])->()){
        
       
        var fName = ""
        
        let allowedCharacterSet = (CharacterSet(charactersIn: "!*'();:@&=+$,/?%#[] ").inverted)
        
        if let escapedString = FundName.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet) {
            //do something with escaped string
             fName = escapedString
        }
        
        let param = "category=\(Category)&fundtype=\(fName)"
        //let param = "category=\(Category)&fund=Small%20cap%20fund"
        
        
        var result = [MFFundDetails]()
        
        let obj = WebService()
        obj.callWebServices(url: Urls.mffunddetails, methodName: "POST", parameters: param, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
            
             print(jsonData)
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                let tempArray = jsonData.value(forKey: "response") as! [NSArray]
                let tempArray1 = tempArray[0] as! [NSDictionary]
                let tempArray2 = tempArray[1] as! [NSDictionary]
                
                if let categoryDesc = tempArray2[0].value(forKey: "CategoryDesc") as? String{
                    MFStaticData.categoryDesc = categoryDesc
                }
                for arr in tempArray1 {
                   
                        if let Fund = arr.value(forKey: "Fund") as? String{
                        self.fund = Fund
                    }
                    if let SubCategory = arr.value(forKey: "SubCategory") as? String{
                        self.subCategory = SubCategory
                    }

//                    if let FundName = arr.value(forKey: "FundName") as? String{
//                        self.fundName = FundName
//                    }

                    if let FundNewName = arr.value(forKey: "FundNewName") as? String{
                        self.fundNewName = FundNewName
                    }

                    if let FundOldName = arr.value(forKey: "FundOldName") as? String{
                        self.fundOldName = FundOldName
                    }

                    if let FundHouse = arr.value(forKey: "FundHouse") as? String{
                        self.fundHouse = FundHouse
                    }

                    if let NetAssets = arr.value(forKey: "NetAssets") as? String{
                        self.totalNetAssets = Int(NetAssets)!
                    }
                    if let OneW_Avg_Return = arr.value(forKey: "1WkRet") as? String{
                        self.oneWAvgReturn = Decimal((OneW_Avg_Return as NSString).doubleValue)
                    }
                    if let OneM_Avg_Return = arr.value(forKey: "1MRet") as? String{
                        self.oneMReturn = Decimal((OneM_Avg_Return as NSString).doubleValue)
                    }
                    if let ThreeM_Avg_Return = arr.value(forKey: "3MRet") as? String{
                        self.threeMReturn = Decimal((ThreeM_Avg_Return as NSString).doubleValue)
                    }
                    if let SixM_Avg_Return = arr.value(forKey: "6MRet") as? String{
                        self.sixMReturn = Decimal((SixM_Avg_Return as NSString).doubleValue)
                    }
                    if let Yr_Avg_Return = arr.value(forKey: "1YrRet") as? String{
                        self.yrAvgReturn = Decimal((Yr_Avg_Return as NSString).doubleValue)
                    }
                    if let ThreeYr_Avg_Return = arr.value(forKey: "3YrRet") as? String{
                        self.threeYrAvgReturn = Decimal((ThreeYr_Avg_Return as NSString).doubleValue)
                    }
                    if let FiveYr_Avg_Return = arr.value(forKey: "5YrRet") as? String{
                        self.fiveYrAvgReturn = Decimal((FiveYr_Avg_Return as NSString).doubleValue)
                    }
                    if let Rating = arr.value(forKey: "Rating") as? String{
                        self.rating = Rating
                    }

                    let obj_mfFundsDetails = MFFundDetails(Fund: self.fund, FundHouse: self.fundHouse, FundName: self.fundName, FundNewName: self.fundNewName, FundOldName: self.fundOldName, TotalNetAssets: self.totalNetAssets, OneWReturn: self.oneWAvgReturn, OneMReturn: self.oneMReturn, ThreeMReturn: self.threeMReturn, SixMReturn: self.sixMReturn,YrAvgReturn: self.yrAvgReturn, ThreeYrAvgReturn: self.threeYrAvgReturn, FiveYrAvgReturn: self.fiveYrAvgReturn, Rating: self.rating, SubCategory: self.subCategory)

                    result.append(obj_mfFundsDetails)
                }
                
            }
            return completion(result)
        }
    }
    
    
}
