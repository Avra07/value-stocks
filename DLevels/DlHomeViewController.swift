
//
//  DlHomeViewController.swift
//  DLevels
//
//  Created by MacMini2 on 11/09/18.
//  Copyright © 2018 Dynamic-Mac-01. All rights reserved.
//

import UIKit
import Charts
import GoogleMobileAds
import FBSDKCoreKit
import FBSDKLoginKit



class collectionVwCell: UICollectionViewCell {
    
    @IBOutlet weak var menuText: UILabel!
    @IBOutlet weak var imgVw: UIImageView!
    
    
    
}

class DlHomeTableViewCell: UITableViewCell {
    @IBOutlet weak var bannerText: UILabel!
    @IBOutlet weak var bannerDesc: UILabel!
    @IBOutlet weak var bannerHighText: UILabel!
    @IBOutlet weak var bannerHighView: UIView!
    
    
}

class DlHomeViewController: UIViewController, ChartViewDelegate, UITableViewDataSource, UITableViewDelegate {
    
    
    
    @IBOutlet weak var clvmenu: UICollectionView!
    @IBOutlet weak var lineChartView: UIView!
    
    @IBOutlet weak var lblDynamicIndexVal: UILabel!
    @IBOutlet weak var lblDynamicIndexDate: UILabel!
    
    @IBOutlet weak var lblNiftyIndexVal: UILabel!
    @IBOutlet weak var lblNiftyIndexDate: UILabel!
    
    @IBOutlet weak var vwDynamicSmallCap: UIView!
    @IBOutlet weak var vwNiftySmallCap: UIView!
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerText: UILabel!
    @IBOutlet weak var contentView: UIView!
    
    //@IBOutlet weak var btnStartUsing: UIButton!
    @IBOutlet weak var btnLearnMore: UIButton!
    @IBOutlet weak var btnSubscribe: UIButton!
   // @IBOutlet weak var bellicon: UIImage!
    @IBOutlet weak var btnVideoTour: UIButton!
    @IBOutlet weak var btnVideoTourAfterSubscription: UIButton!
    @IBOutlet weak var btn_search: UIButton!
    
    
   
    @IBOutlet weak var btn_LearnMore: UIButton!
    
    @IBOutlet weak var labelcount: UILabel!
    
    
    @IBOutlet weak var appBannerList: UITableView!
    @IBOutlet weak var btnNonSubscription: UIButton!
    
    var dataPoints: [String] = []
    var dtDataPoints: [Date] = []
    
    var indPerf: [Float] = []
    var Perf: [Float] = []
    
    var indPerf4Chart   : [Float] = []
    var Perf4Chart      : [Float] = []
    
    var CLOSE_PRICE: [String] = []
    var smallCap: [String] = []
    
    var mcount : [NSDictionary] = []
    
    var navController: UINavigationController?
    
    var bannerArray = [bannerDetails]()
    
    var guruDict = [String:String]()
    var guruDescDict = [String:String]()
    
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    
    
    let nameArray = ["All\nGurus","Stock\nResearch","Stocks\nScreener","Our Research\nStocks","Nifty Trend\n(FII Data)","Sector\nAnalysis", "Mutual Funds\nResearch", "Mutual Funds\nScreener", ""]
    
    
    //let image: [UIImage] = [#imageLiteral(resourceName: "hm_mf_icon"),#imageLiteral(resourceName: "hm_stock_multibaggers_icon"), #imageLiteral(resourceName: "hm_stock_financials_icon") ,#imageLiteral(resourceName: "hm_indices_icon"),#imageLiteral(resourceName: "hm_webinars_icon"),header_logo,#imageLiteral(resourceName: "hm_PMS_icon"),#imageLiteral(resourceName: "hm_fpi_icon"),#imageLiteral(resourceName: "Hamburger")]
    
    
    let image = [ UIImage(named: "guru"), UIImage(named: "stock_research"),UIImage(named: "stock_screener"),  UIImage(named: "our_value_stocks"), UIImage(named: "nifty_open_interest"), UIImage(named: "sector_analysis"),  UIImage(named: "mutual_funds_research"),  UIImage(named: "mutual_funds_screener"), UIImage(named: "zzz")]
    
    
    
    
    
    var red = UIColor(hex: "0xDC5047")
    // var green = UIColor(red: 76.0, green: 217.0, blue: 100.0, alpha: 1.0)
    var green = UIColor(hex: "16B84A")
    var borderClolor = UIColor(red: 192.0/255.0, green: 192.0/255.0, blue: 192.0/255.0, alpha: 1.0)
    
    var lblRegisterColor = UIColor(red: 130.0/255.0, green: 128.0/255.0, blue: 131.0/255.0, alpha: 1.0)
    
    var interstitial: GADInterstitial!
    
    // 5572 4503 2121
    
    var dIndexColor         = UIColor(red: 0.0, green: 79.0, blue: 170.0, alpha: 1.0)
    var smallCapIndexColor  = UIColor(red: 184.0, green: 1.0, blue: 123.0, alpha: 1.0)
    
    
    
    
    @IBOutlet weak var bellicon: UIImageView!
    var instanceOfLeftSlideMenu = LeftSlideMenu()
    var isShowingMenu = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //addSlideMenuButton()
        if User.email == "guest@yopmail.com"{
            self.bellicon.isHidden = true
            self.labelcount.isHidden = true
            self.btnSubscribe.isHidden = false
            self.btnVideoTour.isHidden = false
        }else{
            self.labelcount.isHidden = false
            self.btnSubscribe.isHidden = true
            self.bellicon.isHidden = false
            self.btnVideoTour.isHidden = true
           self.btnVideoTourAfterSubscription.isHidden = false
        }
        
        let notificationCenter = NotificationCenter.default
        NotificationCenter.default.addObserver(self, selector: #selector(handleEvent), name:NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        
        actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0) //CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.gray
        self.view.addSubview(actInd)
        actInd.startAnimating()
        
        btn_LearnMore.setTitleColor(Colors.btn_LearnColor, for: UIControlState.normal)
        
        checkSubscription()
        
        
        NotificationData.category = ""
        NotificationData.clickAction.removeAll()
        NotificationData.data = NSDictionary()
        
        //btnStartUsing.layer.borderColor = UIColor.black.cgColor
        //btnStartUsing.layer.borderWidth = 1
        //btnStartUsing.layer.cornerRadius = 4
        
        
        
        headerView.backgroundColor = Colors.headerViewColor
        headerText.backgroundColor = Colors.headerViewColor
        headerText.textColor = Colors.customBlueColor
        applyShadowOnView()
        
        scrollView.backgroundColor = Colors.viewColor
        contentView.backgroundColor = Colors.viewColor
        
        btnLearnMore.setTitleColor(Colors.customBlueColor, for: .normal)
        btnLearnMore.layer.borderColor = Colors.customBlueColor.cgColor
        btnLearnMore.layer.borderWidth = 1
        btnLearnMore.layer.cornerRadius = 4
        
        btnVideoTour.setTitleColor(Colors.customBlueColor, for: .normal)
        btnVideoTour.layer.borderColor = Colors.customBlueColor.cgColor
        btnVideoTour.layer.backgroundColor = UIColor.white.cgColor
        btnVideoTour.layer.borderWidth = 1
        
        
        btnSubscribe.setTitleColor(UIColor.white, for: .normal)
        btnSubscribe.layer.borderColor = Colors.customBlueColor.cgColor
        btnSubscribe.layer.backgroundColor = Colors.customBlueColor.cgColor
        btnSubscribe.layer.borderWidth = 1
        
        
        
        
        getAppBanner()
        AppManager().setStatusBarBackgroundColor()
        clvmenu.reloadData()
        
        //setChart()
        
        //        vwDynamicSmallCap.layer.borderWidth = 1
        //        vwNiftySmallCap.layer.borderWidth = 1
        // Do any additional setup after loading the view.
        
        getNotificationCount()
        //actInd.stopAnimating()
    }
    
    @objc func handleEvent() {
        getNotificationCount()
        checkSubscription()
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
       
        getNotificationCount()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        getNotificationCount()
    }
    
    
    
    deinit {
        print("Remove NotificationCenter Deinit")
        NotificationCenter.default.removeObserver(self)
    }
    
    func applyShadowOnView() {
        
        headerView.layer.shadowColor = UIColor.lightGray.cgColor
        headerView.layer.shadowOpacity = 0.4
        headerView.layer.shadowRadius = 2
        headerView.layer.shadowOffset = CGSize(width: 0, height: 2)
        headerView.layer.masksToBounds = false
    }
    
    @IBAction func btnLearnMoreClick(_ sender: Any) {
        let valueStrategyController = storyboard?.instantiateViewController(withIdentifier: "ValueStrategy") as? ValueStrategyViewController
        self.navigationController?.pushViewController(valueStrategyController!, animated: true)
    }
    
    @IBAction func btn_contactUs(_ sender: Any) {
        
        let contactController = storyboard?.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
        User.navigation.pushViewController(contactController, animated: true)
    }
    
    @IBAction func btn_Search(_ sender: Any) {
        
        if Subscription_Data.status {
            let searchController = StoryBoard.main.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
            searchController.comeFromHome = true
            User.navigation.pushViewController(searchController, animated: true)
        }else{
            //UIApplication.shared.openURL(NSURL(string: "https://www.dynamiclevels.com/en/vs-premium-benefits/\(User.token)")! as URL) premiumBenefitViewController
            
             let searchController = StoryBoard.main.instantiateViewController(withIdentifier: "premiumBenefitViewController") as! premiumBenefitViewController
            
            User.navigation.pushViewController(searchController, animated: true)
        
            
        }
        
    }
    
    
    
    func getAppBanner()
    {
        bannerArray = []
        appBannerList.delegate = self
        appBannerList.dataSource = self
        let obj = WebService()
        
        let parameter = "guru_id=0"
        
        //actInd.startAnimating()
        
        
        
        obj.callWebServices(url: Urls.get_strategies, methodName: "GET", parameters: parameter, istoken: false, tokenval: User.token) { (returnValue, jsonData) in
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                let bannerArray = jsonData.value(forKey: "response") as! NSArray
                print(bannerArray)
                for bannerArrayDetail in bannerArray{
                    if let bannerDetailDict = bannerArrayDetail as? NSDictionary {
                        var banner = bannerDetails(highlightedText: "", text: "", desc: "", headerText: "", longDesc: "")
                        if let highlightText = bannerDetailDict.value(forKey: "sm_Initial"){
                            banner.highlightedText = highlightText as! String
                        }
                        if let textDetail = bannerDetailDict.value(forKey: "home_display"){
                            banner.text = textDetail as! String
                        }
                        if let descDetail = bannerDetailDict.value(forKey: "sm_Short_Desc"){
                            banner.desc = descDetail as! String
                        }
                        if let header_Text = bannerDetailDict.value(forKey: "sm_Name"){
                            banner.headerText = header_Text as! String
                        }
                        if let longDescDetail = bannerDetailDict.value(forKey: "sm_Desc"){
                            let descString = (longDescDetail as! String).replacingOccurrences(of: "\\n", with: "\n", options: .literal, range: nil)
                            banner.longDesc = descString
                        }
                        self.bannerArray.append(banner)
                        self.guruDict[banner.highlightedText] = banner.longDesc
                        self.guruDescDict[banner.highlightedText] = banner.headerText
                        OperationQueue.main.addOperation({
                            self.appBannerList.reloadData()
                        })
                    }
                }
            }
            else {
            }
            self.actInd.stopAnimating()
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let allGuruController = storyboard?.instantiateViewController(withIdentifier: "AllGuruViewController") as! AllGuruViewController
        allGuruController.index = indexPath.row
        allGuruController.guruInitial = bannerArray[indexPath.row].highlightedText
        allGuruController.guruDataDict = guruDict
        HeaderText.guru_Desc = guruDescDict
        User.navigation.pushViewController(allGuruController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bannerArray.count
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "BannerCell") as! DlHomeTableViewCell
        cell.selectionStyle = .none
        cell.bannerHighText.text = bannerArray[indexPath.row].highlightedText
        cell.bannerText.text = bannerArray[indexPath.row].text
        cell.bannerText.textColor = Colors.customBlueColor
        cell.bannerDesc.text = bannerArray[indexPath.row].desc
        cell.bannerDesc.textColor = Colors.customBlueColor
        cell.bannerHighView.layer.cornerRadius = 3
        return cell
        
    }
    
    
    func getUser (completion : @escaping ([NSDictionary])->()) {
        
        let obj = WebService()
        
        let paremeters = "user=\(User.userId)"//"user_id=211297"
        
        //actInd.startAnimating()
        
        obj.callWebServices(url: Urls.get_user_subscription, methodName: "POST", parameters: paremeters, istoken: false, tokenval: User.token) { (returnValue, jsonData) in
            
            var result : [NSDictionary]
            result=[NSDictionary]()
            print("getuser value is :\(jsonData)")
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                if jsonData.value(forKey: "response") as? [NSDictionary] != nil{
                    result = jsonData.value(forKey: "response") as! [NSDictionary]
                    return completion(result)
                }
                
                return completion(result)
                
            } else{
                
            }
            
        }
    }
    
    func checkSubscription() {
        getUser(completion: { (myuser) in
            
            if (myuser.count > 0) {
                
                let status = myuser[0].value(forKey: "ps_status") as? String
                
                if status?.lowercased() != "active"{
                    //self.btnNonSubscription.isHidden = false
                    Subscription_Data.status = false
                    
                   self.btnVideoTourAfterSubscription.isHidden = true
                    //self.btnVideoTourAfterSubscription.isHidden = false
                    /*
                    */
                //
                }else {
                    //self.btnNonSubscription.isHidden = true
                    Subscription_Data.status = true
                   // self.btnSubscribe.isHidden = false
                   // self.btnVideoTour.isHidden = false
                    self.btnVideoTourAfterSubscription.isHidden = false
                }
            }else{
                
               // self.btnNonSubscription.isHidden = false
                Subscription_Data.status = false
               
                self.btnVideoTourAfterSubscription.isHidden = true
                //self.btnVideoTourAfterSubscription.isHidden = false
                
            }
            
        })
    }
    
    
    @IBAction func btnVideoAfterSubcription(_ sender: Any) {
    }
    
    @IBAction func btn_Learn(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let viewcontroller = storyBoard.instantiateViewController(withIdentifier: "referalloadViewController") as! referalloadViewController
        User.navigation.pushViewController(viewcontroller, animated: false)
    }
    
    func normalRegistrationLogOut() {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let webService = WebService()
        webService.callWebServices(url: Urls.logOut, methodName: "POST", parameters: "", istoken: true, tokenval: User.token, completion: { (success, jsonResult) in
            
            DispatchQueue.main.async {
                
                
                UserDefaults.standard.set(nil, forKey: "phone")
                UserDefaults.standard.set(nil, forKey: "email")
                UserDefaults.standard.set(nil, forKey: "password")
                UserDefaults.standard.set(nil, forKey: "regThrough")
                
                
                
                User.userId         =   ""
                User.socialId         =   ""
                User.firstName        =   ""
                User.lastName         =   ""
                User.email            =   ""
                User.phone            =   ""
                User.socialToken      =   ""
                User.password         =   ""
                User.regThrough       =   ""
                User.token            =   ""
                User.OnboardinString  =   ""
                User.countryCode      =   ""
                User.isLandingDataLaded = false
                User.firebaseToken = ""
                
                User.isCommingFromSpecificPage = false
                User.instanceOfSectorPerformance = SectorPerformanceDetailsViewController()
                //User.tempIndePath:  IndexPath!
                User.navigation = UINavigationController()
                
                User.isAlternetColor = false
                
                
                
                
                /****** Proform Login For Guest User support@dynamiclevels.com ************/
                // self.performDefaultLogin()
                
                /********************/
                let viewcontroller = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                self.navController?.pushViewController(viewcontroller, animated: false)
                
                
            }
        })
    }
    
    
    
    func logoutbeforeSubcription(){
        
        GIDSignIn.sharedInstance().signOut()
        GIDSignIn.sharedInstance().disconnect()
        
        if(User.regThrough == "gmail")
        {
            if GIDSignIn.sharedInstance()?.currentUser != nil {
                DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
                    // Change `2.0` to the desired number of seconds.
                    // Code you want to be delayed
                    self.normalRegistrationLogOut()
                }
            } else {
                self.normalRegistrationLogOut()
            }
        } else if(User.regThrough == "facebook")
        {
            let manager = LoginManager()
            manager.logOut()
            normalRegistrationLogOut()
            
        }else {
            normalRegistrationLogOut()
        }
        
    }
    
    
    
    
    @IBAction func btnSubscriptionClick(_ sender: Any) {
        
        
       // logoutbeforeSubcription()
        
        UIApplication.shared.openURL(NSURL(string: "https://www.dynamiclevels.com/en/vs-premium-benefits/\(User.token)")! as URL)
    }
    
    
    @objc func buttonAction(sender: UIButton!) {
        instanceOfLeftSlideMenu.hideSlideMenu(navigationController: self.navigationController!, viewSize: self.view)
        //vwLeadingConstraint.constant = 0
        //self.view.isUserInteractionEnabled = true
        isShowingMenu = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     
     func setChart() {
     lineChartView.delegate=self
     let obj = WebService()
     let paremeters = "index_type=small&for=chart"
     var result = ""
     
     var linechartDataSet = LineChartDataSet()
     var linechartDataSet2 = LineChartDataSet()
     
     
     //Mark: Call Service for Chart Data
     obj.callWebServices(url: Urls.getindexhistory, methodName: "GET", parameters: paremeters, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
     
     //print("Json Data is :  \(jsonData)")
     if jsonData.value(forKey: "errmsg") as! String == "" {
     
     DispatchQueue.main.async {
     
     let tempArray = jsonData.value(forKey: "response") as! NSArray
     for dict in tempArray {
     let dictValue = dict as! NSDictionary
     
     if let iperf:String = dictValue.value(forKey: "indPerf") as? String{
     if let perf:String = dictValue.value(forKey: "perf") as? String{
     if let iperf4Chart:String = dictValue.value(forKey: "nerwIndPerf") as? String{
     if let perf4Chart:String = dictValue.value(forKey: "newPerf") as? String{
     
     result = dictValue.value(forKey: "DIS_DATE") as! String
     
     self.dataPoints.append(result)
     
     let dateFormatter = DateFormatter()
     dateFormatter.dateFormat = "dd-MMM-yy" //Your date format
     
     let date = dateFormatter.date(from: result) //according to date format your date string
     
     self.dtDataPoints.append(date!)
     
     self.indPerf.append((iperf as NSString).floatValue)
     self.Perf.append((perf as NSString).floatValue)
     
     self.indPerf4Chart.append((iperf4Chart as NSString).floatValue)
     self.Perf4Chart.append((perf4Chart as NSString).floatValue)
     
     var clsprice = ""
     clsprice = dictValue.value(forKey: "CLOSE_PRICE") as! String
     self.CLOSE_PRICE.append(clsprice)
     
     let smallCapPrice = dictValue.value(forKey: "smallCap") as! String
     self.smallCap.append(smallCapPrice)
     
     }
     
     }
     }
     }
     }
     
     var dynaClosePrice = ""
     dynaClosePrice = self.CLOSE_PRICE.last!
     
     var dynaindexperf = ""
     dynaindexperf   = NSString(format: "%.2f", self.indPerf.last!) as String
     
     if self.indPerf.last! > ("0" as NSString).floatValue {
     self.lblDynamicIndexVal.textColor = self.green
     }
     
     if self.indPerf.last! < ("0" as NSString).floatValue {
     self.lblDynamicIndexVal.textColor = self.red
     }
     
     
     var smallCapClosePrice = ""
     smallCapClosePrice = self.smallCap.last!
     
     var smallCapperf = ""
     smallCapperf   = NSString(format: "%.2f", self.Perf.last!) as String
     
     if self.Perf.last! > ("0" as NSString).floatValue {
     self.lblNiftyIndexVal.textColor = self.green
     }
     
     if self.Perf.last! < ("0" as NSString).floatValue {
     self.lblNiftyIndexVal.textColor = self.red
     }
     
     self.lblDynamicIndexVal.text = "\(dynaClosePrice) (\(dynaindexperf)%)"
     self.lblNiftyIndexVal.text   = "\(smallCapClosePrice) (\(smallCapperf)%)"
     
     
     self.lblDynamicIndexDate.text = self.dataPoints.last
     self.lblNiftyIndexDate.text = self.dataPoints.last
     
     
     
     var dataEntries: [ChartDataEntry] = []
     var dataEntries2: [ChartDataEntry] = []
     
     for i in 0..<self.dataPoints.count {
     
     // let dataEntry = ChartDataEntry(x: Double(i), y: values[i])
     let dataEntry = ChartDataEntry(x: Double(i), y: Double(self.indPerf4Chart[i]))
     dataEntries.append(dataEntry)
     }
     
     for i in 0..<self.dataPoints.count {
     
     // let dataEntry = ChartDataEntry(x: Double(i), y: values[i])
     let dataEntry = ChartDataEntry(x: Double(i), y: Double(self.Perf4Chart[i]))
     dataEntries2.append(dataEntry)
     }
     
     linechartDataSet = LineChartDataSet(values: dataEntries, label: "")
     linechartDataSet.axisDependency = .left
     linechartDataSet.drawCirclesEnabled = false
     linechartDataSet.drawValuesEnabled = false
     linechartDataSet.circleColors = [NSUIColor.white]
     linechartDataSet.setCircleColor(UIColor.white)
     // linechartDataSet.circleRadius = 0.0 // the radius of the node circle
     //linechartDataSet.fillAlpha = 65 / 255.0
     //                    linechartDataSet.fillColor = UIColor.white
     //                    linechartDataSet.highlightColor = UIColor.white
     linechartDataSet.drawCircleHoleEnabled = false
     linechartDataSet.colors = [UIColor.init(hex: "004FAA")] // [self.dIndexColor]
     
     
     var dataSets : [LineChartDataSet] = [LineChartDataSet]()
     dataSets.append(linechartDataSet)
     
     linechartDataSet2 = LineChartDataSet(values: dataEntries2, label: "")
     linechartDataSet2.axisDependency = .left
     linechartDataSet2.drawCirclesEnabled = false
     linechartDataSet2.drawValuesEnabled = false
     linechartDataSet2.circleColors = [NSUIColor.white]
     linechartDataSet2.setCircleColor(UIColor.white)
     // linechartDataSet.circleRadius = 0.0 // the radius of the node circle
     //linechartDataSet.fillAlpha = 65 / 255.0
     //                    linechartDataSet2.fillColor = UIColor.white
     //                    linechartDataSet2.highlightColor = UIColor.white
     linechartDataSet2.drawCircleHoleEnabled = false
     linechartDataSet2.colors = [UIColor.init(hex: "B8017B")]
     
     dataSets.append(linechartDataSet2) 
     
     
     self.lineChartView.xAxis.valueFormatter = IndexAxisValueFormatter(values: self.dataPoints)
     
     self.lineChartView.xAxis.granularity = 25 //14.0
     self.lineChartView.xAxis.labelPosition = .bottom
     self.lineChartView.rightAxis.enabled = false
     self.lineChartView.data?.setDrawValues(false)
     self.lineChartView.leftAxis.drawGridLinesEnabled = false
     self.lineChartView.xAxis.drawGridLinesEnabled = false
     self.lineChartView.legend.enabled = false
     self.lineChartView.chartDescription?.text = ""
     
     
     
     
     
     
     let data: LineChartData = LineChartData(dataSets: dataSets)
     
     data.setValueTextColor(UIColor.red)
     self.lineChartView.data = data
     
     }
     
     }
     }
     
     /* END Chart Section */
     }*/
    
    
    
    
    @IBAction func nonSubscriptionClick(_ sender: UIButton) {
         //UIApplication.shared.openURL(NSURL(string: "https://www.dynamiclevels.com/en/vs-premium-benefits/\(User.token)")! as URL)
        
    }
    
    
    @IBAction func onClick_Hamberger(_ sender: UIButton) {
        
        if !isShowingMenu {
            
            
            instanceOfLeftSlideMenu.prepareScreenWithView(navigationController: self.navigationController!, viewSize: self.view)
            
            instanceOfLeftSlideMenu.btnBack.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
            
            //self.view.isUserInteractionEnabled = false
            isShowingMenu = true
        } else {
            
            instanceOfLeftSlideMenu.hideSlideMenu(navigationController: self.navigationController!, viewSize: self.view)
            //vwLeadingConstraint.constant = 0
            
            isShowingMenu = false
        }
        
    }
    
    
    
    func getNotificationCount() {
        getData(completion: { (dataset) in
            self.mcount = dataset
            
            var unread_count = 0
            for i in self.mcount{
                let read = i.value(forKey: "readFlag") as! String
                if read == "N"  {
                    unread_count = unread_count+1
                }
            }
            
            if unread_count == 0 {
                self.labelcount.isHidden = true
                
            }
            else{
                self.labelcount.text! = "\(unread_count)"
            }
            self.labelcount.layer.cornerRadius = self.labelcount.frame.width/2
            self.labelcount.layer.masksToBounds = true
            self.labelcount.backgroundColor = UIColor.red.withAlphaComponent(0.5)
        })
    }
    
    @IBAction func not(_ sender: Any) {
        let s2 = UIStoryboard.init(name: "Main" , bundle : nil)
        let obj2 = s2.instantiateViewController(withIdentifier:"bob") as!
        Notification
        self.navigationController?.pushViewController(obj2, animated: true)
    }
    
  /*  @IBAction func onClick_Search(_ sender: UIButton) {
        
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        
        self.navigationController?.pushViewController(viewController, animated: true)
        
    } */
    
    func getData (completion : @escaping ([NSDictionary])->()) {
        
        let obj = WebService()
        
        let paremeters = "user_id=\(User.userId)"//"user_id=211297"
        
        //actInd.startAnimating()
        
        obj.callWebServices(url: Urls.get_notification, methodName: "GET", parameters: paremeters, istoken: false, tokenval: User.token) { (returnValue, jsonData) in
            
            var result : [NSDictionary]
            result=[NSDictionary]()
            print("Json Data is :\(jsonData)")
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                if jsonData.value(forKey: "response") as? [NSDictionary] != nil{
                    result = jsonData.value(forKey: "response") as! [NSDictionary]
                    return completion(result)
                }
                
                return completion(result)
                
            } else{
                
            }
            
        }
    }
}

//30A400

extension DlHomeViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    
    
    //MARK: Adding AdMob
//    func loadAddMob(success: (_ success:Bool) -> Void) {
//
//        interstitial = GADInterstitial(adUnitID: "ca-mb-app-pub-8834194653550774/3987450882")
//        interstitial.delegate = self
//        let request = GADRequest()
//        //   request.testDevices = [kGADSimulatorID]
//        //        request.testDevices = @[ kGADSimulatorID ];
//        interstitial.load(request)
//        success(true)
//    }
    
    func interstitialWillPresentScreen(_ ad: GADInterstitial) {
        
        print("Testing Will PresentScreen")
    }
    
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        //UIApplication.shared.isStatusBarHidden = false
        print("Add id REceived")
        self.interstitial.present(fromRootViewController: self)
    }
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        
        
    }
    
    
    
    
    
    
    //MARK: Collection View Delegate Methode
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return nameArray.count
        
    }
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        navController = User.navigation
        let currentCell = indexPath.row
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        
        
        //let nameArray = ["Guru Stock Strategies","Individual Stock Research","Stock Screener","Our Value Stocks","Nifty Open Interest","Sector Analysis", "Mutual Funds Research", "Mutual Funds Screener", ""]
        print("Subscription Status \(Subscription_Data.status)")
            
        if Subscription_Data.status {
            if currentCell == 0 {
                let allGuruController = storyboard?.instantiateViewController(withIdentifier: "AllGuruViewController") as! AllGuruViewController
                allGuruController.index = 0
                allGuruController.guruInitial = bannerArray[0].highlightedText
                allGuruController.guruDataDict = guruDict
                HeaderText.guru_Desc = guruDescDict
                User.navigation.pushViewController(allGuruController, animated: true)
            }
            if currentCell == 1 {
                let viewController = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
                viewController.comeFromHome = true
                self.navigationController?.pushViewController(viewController, animated: true)
                
            }else if currentCell == 2 {
                
                if Subscription_Data.status {
                    let viewcontroller = storyBoard.instantiateViewController(withIdentifier: "StockFiltersViewController") as! StockFiltersViewController
                    
                    navController?.pushViewController(viewcontroller, animated: true)
                }else{
                    
                }
                
                
                
                
    //            let viewcontroller = storyBoard.instantiateViewController(withIdentifier: "StockMarketTodayViewController") as! StockMarketTodayViewController
    //
    //            navController?.pushViewController(viewcontroller, animated: true)
                
            } else if currentCell == 5 {
                
                           let viewCont = self.storyboard?.instantiateViewController(withIdentifier: "MultibaggerQuarterlySectorViewController") as! MultibaggerQuarterlySectorViewController
                            self.navigationController?.pushViewController(viewCont, animated: false)
                
             /*   let msg = "Our brand new Mutual Fund App 'MFDirect' is now here, where along with the data available earlier, you can also Invest online in Direct Funds. \n\nYou will be directed to MFDirect now if already downloaded. If not, then we will take you to the App Store for download."
                
                let alertViewController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
                
                let cancelAction = UIAlertAction(title: "Cancel", style: .default) { (alertAction) in
                }
                alertViewController.addAction(cancelAction)
                
                
                let alertAction = UIAlertAction(title: "Ok", style: .default) { (alertAction) in
                    let app = UIApplication.shared
                    let bundleID = "mfdirect://"
                    if app.canOpenURL(URL(string: bundleID)!) {
                        print("App is install and can be opened")
                        let url = URL(string:bundleID)!
                        UIApplication.shared.openURL(url)
                 
                    } else {
                        UIApplication.shared.openURL(NSURL(string: "https://apps.apple.com/us/app/mfdirect/id1451388072?ls=1")! as URL)
                    }
                 
                 
                 
                 
                }
                alertViewController.addAction(alertAction)
                
                
                
                navController?.present(alertViewController, animated: true, completion: nil)*/
                //            let viewController = storyBoard.instantiateViewController(withIdentifier: "MFHomeViewController") as! MFHomeViewController
                //            navController?.pushViewController(viewController, animated: true)
                
            } else if currentCell == 3 {
                let viewcontroller = storyBoard.instantiateViewController(withIdentifier: "MultibaggerViewController") as! MultibaggerViewController
                viewcontroller.isFromLeftMenu = true
                navController?.pushViewController(viewcontroller, animated: true)

            } else if currentCell == 4 {
                let viewcontroller = storyBoard.instantiateViewController(withIdentifier: "NiftyOpenInterestViewController") as! NiftyOpenInterestViewController
                navController?.pushViewController(viewcontroller, animated: true)
                
            } else if currentCell == 6 {
                let storyBoard = UIStoryboard(name: "MFStoryboard", bundle: nil)
                let viewcontroller = storyBoard.instantiateViewController(withIdentifier: "MFViewController") as! MFViewController
                navController?.pushViewController(viewcontroller, animated: true)
                
                
                
            }  else if currentCell == 7 {
                if #available(iOS 11.0, *) {
                    let storyBoard = UIStoryboard(name: "MFStoryboard", bundle: nil)
                    let viewcontroller = storyBoard.instantiateViewController(withIdentifier: "MFStockFiltersViewController") as! MFStockFiltersViewController
                    
                    navController?.pushViewController(viewcontroller, animated: true)
                /*AppManager.showAlertMessage(title: " ", message: "This feature will be available soon.")
                } else {
                    // Fallback on earlier versions
                */
                }
            }
        }
        else {
            if currentCell == 0 {
                let allGuruController = storyboard?.instantiateViewController(withIdentifier: "AllGuruViewController") as! AllGuruViewController
                allGuruController.index = 0
                allGuruController.guruInitial = bannerArray[0].highlightedText
                allGuruController.guruDataDict = guruDict
                HeaderText.guru_Desc = guruDescDict
                User.navigation.pushViewController(allGuruController, animated: true)
            }
            else {
                let informationController = self.storyboard?.instantiateViewController(withIdentifier: "GetInfoViewController") as! GetInfoViewController
                //searchController.comeFromHome = true
                informationController.headerText = nameArray[indexPath.row].replacingOccurrences(of: "\n", with: " ", options: .literal, range: nil)
                User.navigation.pushViewController(informationController, animated: true)
            }
        }
    }
    //MARK: Collection View Delegate Methode
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! collectionVwCell
        
        if nameArray[indexPath.row] != "" {
            cell.menuText.text = nameArray[indexPath.row]
            cell.imgVw.image = image[indexPath.row]
        } else {
            cell.menuText.text = ""
            
        }
        cell.backgroundColor = UIColor.white
        
        
        
        //cell.layer.borderWidth = 0.25
        cell.layer.borderColor = Colors.customBlueColor.cgColor
        //cell.addCornerRadius(value: 4.0)
        cell.layer.borderWidth = 0.5
        
        //        let bottomLine = CALayer()
        //
        //        bottomLine.frame = CGRect(x: 0.0, y: cell.frame.height - 1, width: cell.frame.width - 1, height: 1.0)
        //        bottomLine.backgroundColor = borderClolor.cgColor
        //        cell.layer.addSublayer(bottomLine)
        
        
        
        return cell
    }
    
    //MARK: Collection View Delegate
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        var collectionViewSize = collectionView.frame.size
        collectionViewSize.width = collectionViewSize.width/4 //Display Three elements in a row.
        collectionViewSize.height = collectionViewSize.height/2
        return collectionViewSize
        
        //  return CGSize(width: UIScreen.main.bounds.width / 3 - 20, height: 90)
    }
}

extension UIView {
    
    // Example use: myView.addBorder(toSide: .Left, withColor: UIColor.redColor().CGColor, andThickness: 1.0)
    
    enum ViewSide {
        case left, right, top, bottom
    }
    
    func addBorder(toSide side: ViewSide, withColor color: CGColor, andThickness thickness: CGFloat) {
        
        let border = CALayer()
        border.backgroundColor = color
        
        switch side {
        case .left: border.frame = CGRect(x: frame.minX, y: frame.minY, width: thickness, height: frame.height)
        case .right: border.frame = CGRect(x: frame.maxX, y: frame.minY, width: thickness, height: frame.height)
        case .top: border.frame = CGRect(x: frame.minX, y: frame.minY, width: frame.width, height: thickness)
        case .bottom: border.frame = CGRect(x: frame.minX, y: frame.maxY, width: frame.width, height: thickness)
        }
        
        layer.addSublayer(border)
    }
}
struct bannerDetails {
    var highlightedText: String
    var text: String
    var desc: String
    var headerText: String
    var longDesc : String
}
