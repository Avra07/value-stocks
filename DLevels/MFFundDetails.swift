//
//  FundDetails.swift
//  DLevels
//
//  Created by Dlevels-MacMini1 on 27/05/18.
//  Copyright © 2018 Dynamic-Mac-01. All rights reserved.
//

import Foundation
class MFFundDetails:NSObject {
    
    
    var fund: String?
    var subCategory: String?
    var fundHouse: String?
    var fundName: String?
    var fundNewName: String?
    var fundOldName: String?
    
    
    
    var totalNetAssets: Int?
    var oneWReturn: Decimal?
    var oneMReturn: Decimal?
    var yrAvgReturn: Decimal?
    var threeMReturn: Decimal?
    var sixMReturn: Decimal?
    var threeYrAvgReturn: Decimal?
    var fiveYrAvgReturn: Decimal?
    
    var rating: String?
    
    
    init(Fund:String,FundHouse:String, FundName:String, FundNewName:String, FundOldName:String, TotalNetAssets:Int,OneWReturn: Decimal,OneMReturn: Decimal,ThreeMReturn: Decimal, SixMReturn: Decimal,YrAvgReturn:Decimal, ThreeYrAvgReturn: Decimal, FiveYrAvgReturn: Decimal, Rating:String,SubCategory: String) {
        
       
        self.fund               =   Fund
        self.subCategory               =   SubCategory
        self.fundHouse          =   FundHouse
        self.fundName           =   FundName
        self.fundNewName        =   FundNewName
        self.fundOldName        =   FundOldName
        
        self.totalNetAssets     =   TotalNetAssets
        self.oneWReturn         =   OneWReturn
        self.oneMReturn         =   OneMReturn
        self.yrAvgReturn        =   YrAvgReturn
        self.threeMReturn       =   ThreeMReturn
        self.sixMReturn         =   SixMReturn
        self.threeYrAvgReturn   =   ThreeYrAvgReturn
        self.fiveYrAvgReturn    =   FiveYrAvgReturn
        
        self.rating             =   Rating
        
    }
}
