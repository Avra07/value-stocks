//
//  NiftOpenInterestDataModel.swift
//  DLevels
//
//  Created by Rishi Sachdeva on 14/03/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import Foundation
class NiftOpenInterestDataModel: NSObject {
	
	
	var fiiPro: String?
	var date: String?
	var client: String?
	var NiftyClose: String?
	
	func getData(completion : @escaping ([NiftyOpenInterest])->()){
		let param = ""
		var result = [NiftyOpenInterest]()
		
		let obj = WebService()
		obj.callWebServices(url: Urls.getoireport, methodName: "POST", parameters: param, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
			
			print(jsonData)
			
			if jsonData.value(forKey: "errmsg") as! String == "" {
				
				let tempArray = jsonData.value(forKey: "response") as! [NSDictionary]
				
				for arr in tempArray{
					if let DATE = arr.value(forKey: "DATE") as? String{
						self.date = DATE
					}
					
					if let fPro = arr.value(forKey: "FIIPRO") as? String{
						self.fiiPro = fPro
					}
					
					if let clnt = arr.value(forKey: "CLIENT") as? String{
						self.client = clnt
					}
					
					if let nclose = arr.value(forKey: "NIFTY") as? String{
						self.NiftyClose = nclose
					}
					
					
					let obj_NiftClose = NiftyOpenInterest(Date: self.date!, FIIPRO: self.fiiPro!, Client: self.client!, NClose: self.NiftyClose!)
					
					result.append(obj_NiftClose)
				}
			}
			return completion(result)
		}
	}

}
