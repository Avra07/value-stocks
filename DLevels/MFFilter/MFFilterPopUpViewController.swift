//
//  MFFilterPopUpViewController.swift
//  DLevels
//
//  Created by SAIKAT GHOSH on 20/08/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//


import UIKit



class MFFilterPopUpViewController: UIViewController {
    var items = [String]()
    var query  = ""
    let reuseIdentifier = "cell"
    
    var isComingFromGurus = false
    
    var islossmakingParam = ""
    var islessVolumeParam = ""
    var header = ""
    
    @IBOutlet weak var colvw: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let layout = UICollectionViewCenterLayout()
        layout.estimatedItemSize = CGSize(width: 240, height: 40)
        colvw.collectionViewLayout = layout
        
        
        if let flowLayout = colvw.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.estimatedItemSize = CGSize(width: 1, height: 1)
          
            
        }
        
        
        
    }
    
    override func viewWillLayoutSubviews()
    {
        super.viewWillLayoutSubviews()
        
        //recalculate the collection view layout when the view layout changes
        colvw.collectionViewLayout.invalidateLayout()
    }
    
    //MARK: CROSS BUTTON ACTION
    @IBAction func btnCross_Action(_ sender: UIButton) {
        dismissCurrentPage()
    }
    
    @IBAction func btnokey_Action(_ sender: UIButton){
        
        self.dismiss(animated: true) {
            
            let storyBoard = UIStoryboard(name: "MFStoryboard", bundle: nil)
            
            let vwcntrl = storyBoard.instantiateViewController(withIdentifier: "MFFilteredStocksViewController") as! MFFilteredStocksViewController
            
            User.navigation.pushViewController(vwcntrl, animated: true)
        }
    }
    
    
    //MARK: DISMISS CURRENT PAGE
    private func dismissCurrentPage(){
        self.dismiss(animated: true) {
        }
    }
}





extension MFFilterPopUpViewController: UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mfselectedFilter.mfdispalyQueryArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell",
                                                            for: indexPath) as? RoundedCollectionViewCell else {
                                                                return RoundedCollectionViewCell()
        }
        cell.textLabel.text = mfselectedFilter.mfdispalyQueryArr[indexPath.row]
        cell.textLabel.preferredMaxLayoutWidth = 300
        
        return cell
    }
    
    
    
}
