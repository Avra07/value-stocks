//
//  MFFilteredStocksViewController.swift
//  DLevels
//
//  Created by SAIKAT GHOSH on 20/08/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit
import MaterialComponents

class MFCustomResultCell: UITableViewCell {
    
    @IBOutlet weak var mStockLbl: UILabel!
    @IBOutlet weak var mSectorLbl: UILabel!
    @IBOutlet weak var mNetAssets: UILabel!
    @IBOutlet weak var mChangeLbl: UILabel!
}



class MFFilteredStocksViewController: UIViewController,UITableViewDelegate, UITableViewDataSource, SelectedRecommendationDelegate {
    
    @IBOutlet weak var mfResultsTblVw: UITableView!
    @IBOutlet weak var mStocksCountLbl: UILabel!
    @IBOutlet weak var mStocksAvgLbl: UILabel!
    @IBOutlet weak var mReturnPerionBtn: UIButton!
    @IBOutlet weak var mSortByBtn: UIButton!
    @IBOutlet weak var mSortByImg: UIImageView!
    @IBOutlet weak var mNoDataView: UIView!
    
    @IBOutlet weak var lossMakingSwitch: UISwitch!
    @IBOutlet weak var lowVolumeSwitch: UISwitch!
    
    @IBOutlet weak var lblSortBy: UILabel!
    
    
    weak var switchDelegate: SwitchDataManageDelegate?
    
    var islossMaking = ""
    var islessVolume = ""
    var ourRecommend = ""
    var querystr     = ""
    
    var mfResults = [MFFilteredStocksPerformance]()
    var mOriginalResults = [MFFilteredStocksPerformance]()
    var mAppliedFilters = [String]()
    var mRecommArr = [String]()
    var mRecommFiltered = ""
    
    var avg_count = 0
    var avg_sum = 0.00
    var stocks_avg = 0.00
    
    
    var selectedRecommendation: String = ""
    var paremeters = ""
    
    
    var col = "1Y %"
    var sort_by = "Stocks(A to Z)"
    var isSortByHtoL = true
    fileprivate var dataArray = [MFFilteredStocksPerformance]()
    
    
    var invoiceInfo: [String: AnyObject]!
    var invoiceComposer: InvoiceComposer!
    var HTMLContent: String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mNoDataView.isHidden = true
        
        
        loadtableView()
        
        lossMakingSwitch.addTarget(self, action: #selector(switchChanged), for: UIControlEvents.valueChanged)
        lossMakingSwitch.tag = 1
        
        lowVolumeSwitch.addTarget(self, action: #selector(switchChanged), for: UIControlEvents.valueChanged)
        lowVolumeSwitch.tag = 2
        
        
        
        if islossMaking == "Y" {
            lossMakingSwitch.isOn  = true
        }else {
            lossMakingSwitch.isOn = false
        }
        
        if islessVolume == "Y" {
            lowVolumeSwitch.isOn  = true
        }else {
            lowVolumeSwitch.isOn = false
        }
        
        
        
        /*getData(completion: {result in
         mfResults = result
         self.mfResultsTblVw.reloadData()
         self.mStocksCountLbl.text = "\(mfResults.count)"
         self.stocks_avg = self.avg_sum // / Double(self.avg_count)
         self.mStocksAvgLbl.text = "\(self.stocks_avg)"
         });*/
        // Do any additional setup after loading the view.
    }
    
    func recommendationList(recommedList: String) {
        ourRecommend = recommedList
        loadtableView()
    }
    
    
    func removeDuplicates(array: [String]) -> [String] {
        var encountered = Set<String>()
        var result: [String] = []
        for value in array {
            if encountered.contains(value) {
                // Do not add a duplicate element.
            }
            else {
                // Add value to the set.
                encountered.insert(value)
                // ... Append the value.
                result.append(value)
            }
        }
        return result
    }
    
    @IBAction func btn_Home(_ sender: Any) {
        let homeController = StoryBoard.main.instantiateViewController(withIdentifier: "DlHomeViewController") as! DlHomeViewController
        User.navigation.pushViewController(homeController, animated: false)
    }
    
    func loadtableView() {
        paremeters = "lossMaking=\(islossMaking)&lessVolume=\(islessVolume)&recommendation=\(ourRecommend)&param=\(mfselectedFilter.mfqueryText)"
        print(paremeters)
        let obj_performance = MFFilteredStocksPerformance()
        //print(self.fname)
        obj_performance.getData(param: paremeters, completion: { (dataset) in
            
            print(dataset)
            self.mfResults = dataset
            self.mOriginalResults = dataset
            if self.mfResults.count == 0{
                self.mNoDataView.isHidden = false
            } else{
                
                if self.ourRecommend == "" {
                    self.mRecommArr.removeAll()
                    for arr in self.mfResults {
                        self.mRecommArr.append(arr.CategoryLong)
                    }
                    
                    self.mRecommArr = self.removeDuplicates(array: self.mRecommArr)
                }
                
                
                
                self.mStocksCountLbl.text = "\(self.mfResults.count)"
                self.mNoDataView.isHidden = true
                
                self.isSortByHtoL = true
                self.sortByParticular(particular: "Stock")
                
            }
            
            self.setAvg()
            
            self.mfResultsTblVw.reloadSections(IndexSet(integer: 0), with: .automatic)
        })
    }
    
    func switchChanged(mySwitch: UISwitch) {
        if mySwitch.isOn {
            if mySwitch.tag == 1 {
                self.islossMaking = "Y"
            }else{
                self.islessVolume = "Y"
            }
            
            loadtableView()
            
        }else {
            confirmSwitch(switchBtn: mySwitch)
        }
        
    }
    
    func confirmSwitch(switchBtn: UISwitch) {
        
        var msg = ""
        if switchBtn.tag == 1 {
            msg = "Are you sure you want to include loss making stocks?"
        }else{
            msg = "Are you sure you want to include low volume stocks?"
        }
        
        // Declare Alert message
        let dialogMessage = UIAlertController(title: "", message: msg, preferredStyle: .alert)
        
        // Create OK button with action handler
        let ok = UIAlertAction(title: "YES", style: .default, handler: { (action) -> Void in
            print("Ok button tapped")
            switchBtn.isOn = false
            
            
            if switchBtn.tag == 1 {
                self.islossMaking = "N"
            }else{
                self.islessVolume = "N"
            }
            
            self.loadtableView()
            
        })
        
        // Create Cancel button with action handlder
        let cancel = UIAlertAction(title: "CANCEL", style: .cancel) { (action) -> Void in
            print("Cancel button tapped")
            switchBtn.isOn = true
        }
        
        //Add OK and Cancel button to dialog message
        dialogMessage.addAction(ok)
        dialogMessage.addAction(cancel)
        
        // Present dialog message to user
        self.present(dialogMessage, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "recommendationList" {
            let secondViewController = segue.destination as! MFRecomendationPopUpViewController
            secondViewController.recommendDelegate = self
            
            
            if self.ourRecommend != "" {
                
                var data = [String]()
                data = ourRecommend.components(separatedBy: ",")
                secondViewController.arrSelectedSector = data
            }
            
            
            
        }
    }
    
    
    
    @IBAction func filter(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func share(_ sender: Any) {
        
        
        let pdfFilePath = self.mfResultsTblVw.exportAsPdfFromTable()
        print(pdfFilePath)
        
        createInvoiceAsHTML()
        
        
        
        
        /*
         UIGraphicsBeginImageContext(view.frame.size)
         view.layer.render(in: UIGraphicsGetCurrentContext()!)
         let image = UIGraphicsGetImageFromCurrentImageContext()
         UIGraphicsEndImageContext()
         
         let text = "Be your own Research Analyst.*\n\nDo your own research by applying filters on 30+ fundamental parameters in a unique \"Filter Tool\" to find your Value Stocks.\n\nDownload the Value Stocks App to apply filters and find your stocks now: https://www.dynamiclevels.com/dlevels-app.php"
         
         var imagesToShare = [AnyObject]()
         imagesToShare.append(!)
         imagesToShare.append(text as AnyObject)
         
         let activityViewController = UIActivityViewController(activityItems: imagesToShare , applicationActivities: nil)
         activityViewController.popoverPresentationController?.sourceView = self.view
         present(activityViewController, animated: true, completion: nil)
         
         */
    }
    
    func getDirectoryPath() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
        
    }
    
    func loadPDFAndShare(documentoPath: String){
        
        let fileManager = FileManager.default
        // let documentoPath = (self.getDirectoryPath() as NSString).appendingPathComponent("tablePdf.pdf")
        
        if fileManager.fileExists(atPath: documentoPath){
            
            
            let text = "Be your own Research Analyst.*\n\nDo your own research by applying filters on 30+ fundamental parameters in a unique \"Filter Tool\" to find your Value Stocks.\n\nDownload the Value Stocks App to apply filters and find your stocks now: https://www.dynamiclevels.com/dlevels-app.php"
            
            var fileToShare = [AnyObject]()
            let fileURL = URL(fileURLWithPath: documentoPath)
            fileToShare.append(fileURL as AnyObject)
            fileToShare.append(text as AnyObject)
            
            let activityViewController = UIActivityViewController(activityItems: fileToShare , applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            present(activityViewController, animated: true, completion: nil)
            
            
        }
        else {
            print("document was not found")
        }
    }
    
    @IBAction func refine(_ sender: Any) {
        
        switchfilteroptions.lossMark = self.islossMaking
        switchfilteroptions.lessvol = self.islessVolume
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func back(_ sender: Any) {
        
        switchfilteroptions.lossMark = self.islossMaking
        switchfilteroptions.lessvol = self.islessVolume
        
        
        //switchDelegate?.switchDataInformation(lossMark: self.islossMaking, lessVol: self.islessVolume)
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sortAction(_ sender: Any) {
        let actionSheeController = UIAlertController(title:"Sort By", message: "", preferredStyle: .actionSheet)
        actionSheeController.view.tintColor = UIColor(red: 247/255, green: 196/255, blue: 58/255, alpha: 1)
        
        actionSheeController.addAction(UIAlertAction(title: "Funds(A to Z)", style: .default, handler: { (success) in
            self.lblSortBy.text = "Sort By: Funds "
            self.mSortByImg.image = UIImage(named: "low")
            self.sort_by = "Stocks(A to Z)"
            UIView.animate(withDuration: 0.4) {
                self.sortLogic()
                self.view.layoutIfNeeded()
            }
        }))
        
        actionSheeController.addAction(UIAlertAction(title: "Funds(Z to A)", style: .default, handler: { (success) in
            //self.mSortByBtn.setTitle("Sort By: Stocks", for: .normal)
            self.lblSortBy.text = "Sort By: Funds "
            self.mSortByImg.image = UIImage(named: "high")
            self.sort_by = "Stocks(Z to A)"
            UIView.animate(withDuration: 0.4) {
                self.sortLogic()
                self.view.layoutIfNeeded()
            }
        }))
        
        actionSheeController.addAction(UIAlertAction(title: "Sector(A to Z)", style: .default, handler: { (success) in
            //self.mSortByBtn.setTitle("Sort By: Sector", for: .normal)
            self.lblSortBy.text = "Sort By: Sector "
            self.mSortByImg.image = UIImage(named: "low")
            self.sort_by = "Sector(A to Z)"
            UIView.animate(withDuration: 0.4) {
                self.sortLogic()
                self.view.layoutIfNeeded()
            }
        }))
        
        
        actionSheeController.addAction(UIAlertAction(title: "Sector(Z to A)", style: .default, handler: { (success) in
            //self.mSortByBtn.setTitle("Sort By: Sector", for: .normal)
            self.lblSortBy.text = "Sort By: Sector "
            self.mSortByImg.image = UIImage(named: "high")
            self.sort_by = "Sector(Z to A)"
            UIView.animate(withDuration: 0.4) {
                self.sortLogic()
                self.view.layoutIfNeeded()
            }
        }))
        
        
        
        actionSheeController.addAction(UIAlertAction(title: "Change%(High to Low)", style: .default, handler: { (success) in
            //self.mSortByBtn.setTitle("Sort By: Change%", for: .normal)
            self.lblSortBy.text = "Sort By: Change % "
            self.mSortByImg.image = UIImage(named: "low")
            self.sort_by = "Change%(High to Low)"
            UIView.animate(withDuration: 0.4) {
                self.sortLogic()
                self.view.layoutIfNeeded()
            }
        }))
        
        
        actionSheeController.addAction(UIAlertAction(title: "Change%(Low to High)", style: .default, handler: { (success) in
            //self.mSortByBtn.setTitle("Sort By: Change%", for: .normal)
            self.lblSortBy.text = "Sort By: Change % "
            self.mSortByImg.image = UIImage(named: "high")
            self.sort_by = "Change%(Low to High)"
            UIView.animate(withDuration: 0.4) {
                self.sortLogic()
                self.view.layoutIfNeeded()
            }
        }))
        
        
        actionSheeController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (success) in
        }) )
        
        // mone rakho
        self.present(actionSheeController, animated: false) {
            
        }
    }
    
    @IBAction func changePeriod(_ sender: Any) {
        
        let actionSheet = MDCActionSheetController(title: "", message: "")
        let dataSource: [String] = ["1D %","1W %","1M %","3M %","6M %","1Y %","3Y %","5Y %","10Y %"]
        for data in dataSource {
            
            let newaction = MDCActionSheetAction(title: data, image: nil, handler: { (success) in
                self.mReturnPerionBtn.setTitle(data, for: .normal)
                self.col = data
                self.setAvg()
                UIView.animate(withDuration: 0.4) {
                    self.sortLogic()
                    self.view.layoutIfNeeded()
                }
                
            })
            actionSheet.addAction(newaction)
        }
        User.navigation.present(actionSheet, animated: true) {
            
        }
        
        
        
        /*
        
        let actionSheeController = UIAlertController(title:"Data Fields", message: "", preferredStyle: .actionSheet)
        actionSheeController.view.tintColor = UIColor(red: 247/255, green: 196/255, blue: 58/255, alpha: 1)
        
//        actionSheeController.addAction(UIAlertAction(title: "1D %", style: .default, handler: { (success) in
//            self.mReturnPerionBtn.setTitle("1D %", for: .normal)
//            self.col = "1D %"
//            self.setAvg()
//            UIView.animate(withDuration: 0.4) {
//                self.sortLogic()
//                self.view.layoutIfNeeded()
//            }
//        }))
        
        actionSheeController.addAction(UIAlertAction(title: "1W %", style: .default, handler: { (success) in
            self.mReturnPerionBtn.setTitle("1W %", for: .normal)
            self.col = "1W %"
            self.setAvg()
            UIView.animate(withDuration: 0.4) {
                self.sortLogic()
                self.view.layoutIfNeeded()
            }
        }))
        
        actionSheeController.addAction(UIAlertAction(title: "1M %", style: .default, handler: { (success) in
            self.mReturnPerionBtn.setTitle("1M %", for: .normal)
            self.col = "1M %"
            self.setAvg()
            UIView.animate(withDuration: 0.4) {
                self.sortLogic()
                self.view.layoutIfNeeded()
            }
        }))
        
        
        actionSheeController.addAction(UIAlertAction(title: "3M %", style: .default, handler: { (success) in
            self.mReturnPerionBtn.setTitle("3M %", for: .normal)
            self.col = "3M %"
            self.setAvg()
            UIView.animate(withDuration: 0.4) {
                self.sortLogic()
                self.view.layoutIfNeeded()
            }
        }))
        
        actionSheeController.addAction(UIAlertAction(title: "6M %", style: .default, handler: { (success) in
            self.mReturnPerionBtn.setTitle("6M %", for: .normal)
            self.col = "6M %"
            self.setAvg()
            UIView.animate(withDuration: 0.4) {
                self.sortLogic()
                self.view.layoutIfNeeded()
            }
        }))
        
        actionSheeController.addAction(UIAlertAction(title: "1Y %", style: .default, handler: { (success) in
            self.mReturnPerionBtn.setTitle("1Y %", for: .normal)
            self.col = "1Y %"
            self.setAvg()
            UIView.animate(withDuration: 0.4) {
                self.sortLogic()
                self.view.layoutIfNeeded()
            }
        }))
        
//        actionSheeController.addAction(UIAlertAction(title: "2Y %", style: .default, handler: { (success) in
//            self.mReturnPerionBtn.setTitle("2Y %", for: .normal)
//            self.col = "2Y %"
//            self.setAvg()
//            UIView.animate(withDuration: 0.4) {
//                self.sortLogic()
//                self.view.layoutIfNeeded()
//            }
//        }))
        
        actionSheeController.addAction(UIAlertAction(title: "3Y %", style: .default, handler: { (success) in
            self.mReturnPerionBtn.setTitle("3Y %", for: .normal)
            self.col = "3Y %"
            self.setAvg()
            UIView.animate(withDuration: 0.4) {
                self.sortLogic()
                self.view.layoutIfNeeded()
            }
        }))
        
//        actionSheeController.addAction(UIAlertAction(title: "4Y %", style: .default, handler: { (success) in
//            self.mReturnPerionBtn.setTitle("4Y %", for: .normal)
//            self.col = "4Y %"
//            self.setAvg()
//            UIView.animate(withDuration: 0.4) {
//                self.sortLogic()
//                self.view.layoutIfNeeded()
//            }
//        }))
        
        
        actionSheeController.addAction(UIAlertAction(title: "5Y %", style: .default, handler: { (success) in
            self.mReturnPerionBtn.setTitle("5Y %", for: .normal)
            self.col = "5Y %"
            self.setAvg()
            UIView.animate(withDuration: 0.4) {
                self.sortLogic()
                self.view.layoutIfNeeded()
            }
        }))
        
        actionSheeController.addAction(UIAlertAction(title: "10Y %", style: .default, handler: { (success) in
            self.mReturnPerionBtn.setTitle("10Y %", for: .normal)
            self.col = "10Y %"
            self.setAvg()
            UIView.animate(withDuration: 0.4) {
                self.sortLogic()
                self.view.layoutIfNeeded()
            }
        }))
        
        
        actionSheeController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (success) in
        }) )
        
        // mone rakho
        self.present(actionSheeController, animated: false) {
            
        }*/
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        let fund = mfResults[indexPath.row].value(forKey: "Blb_Symbol") as! String
        let fund_name = mfResults[indexPath.row].value(forKey: "Dispaly_Name") as! String
        let scmCode = mfResults[indexPath.row].value(forKey: "Scm_Code") as! String
        
        let category = mfResults[indexPath.row].value(forKey: "Category") as! String
        
        let mainStoryBoard = UIStoryboard(name: "MFStoryboard", bundle: nil)
        let viewController = mainStoryBoard.instantiateViewController(withIdentifier: "MFSpecificViewController") as! MFSpecificViewController
        
        
        Funddetails.Category    = category
        Funddetails.Fund        = fund
        Funddetails.FundName    = fund_name
        Funddetails.scmCode     = scmCode
        
        viewController.Category = category
        viewController.Fund     = fund
        viewController.FundName = fund_name
        viewController.scmCode  = scmCode
        
        
        
        self.navigationController?.pushViewController(viewController, animated: false)
    }
    
    
    public func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60
        
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return mfResults.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:MFCustomResultCell?
        
        
        cell = tableView.dequeueReusableCell(withIdentifier: "mfCustomResultCell") as? MFCustomResultCell
        cell?.selectionStyle = .none
        
        let stock = mfResults[indexPath.row]
        
        /*cell?.mStockLbl.text = stock.value(forKey: "Dispaly_Name") as? String
         cell?.mSectorLbl.text = stock.value(forKey: "Sector") as? String
         cell?.mRecommendationLbl.text = stock.value(forKey: "CategoryLong") as? String*/
        
        cell?.mStockLbl.text = stock.Dispaly_Name
        cell?.mStockLbl.sizeToFit()
        cell?.mSectorLbl.text = stock.Sector
        cell?.mNetAssets.text = stock.netAssets
        var _chng = ""
        
        /*if self.col == "1D %" {
         _chng = stock.value(forKey: "_1") as? String ?? "--"
         } else if self.col == "1W %" {
         _chng = stock.value(forKey: "1W_Change") as? String ?? "--"
         }  else if self.col == "1M %" {
         _chng = stock.value(forKey: "1M_Change") as? String ?? "--"
         }else if self.col == "3M %" {
         _chng = stock.value(forKey: "3M_Change") as? String ?? "--"
         }else if self.col == "6M %" {
         _chng = stock.value(forKey: "6M_Change") as? String ?? "--"
         }else if self.col == "1Y %" {
         _chng = stock.value(forKey: "1Y_Change") as? String ?? "--"
         }else if self.col == "3Y %" {
         _chng = stock.value(forKey: "3Y_Change") as? String ?? "--"
         }else if self.col == "5Y %" {
         _chng = stock.value(forKey: "3Y_Change") as? String ?? "--"
         }*/
        
        if self.col == "1D %" {
            _chng = "\(stock._1D)"
        } else if self.col == "1W %" {
            _chng = "\(stock._1W)"
        }  else if self.col == "1M %" {
            _chng = "\(stock._1M)"
        }else if self.col == "3M %" {
            _chng = "\(stock._3M)"
        }else if self.col == "6M %" {
            _chng = "\(stock._6M)"
        }else if self.col == "1Y %" {
            _chng = "\(stock._1Y)"
        }else if self.col == "2Y %" {
            _chng = "\(stock._2Y)"
        }else if self.col == "3Y %" {
            _chng = "\(stock._3Y)"
        }else if self.col == "4Y %" {
            _chng = "\(stock._4Y)"
        }else if self.col == "5Y %" {
            _chng = "\(stock._5Y)"
        }else if self.col == "10Y %" {
            _chng = "\(stock._10Y)"
        }
        
        if(_chng=="-999.0"){
            _chng = "--"
        }
        
        if _chng != "--" {
            cell?.mChangeLbl.text = String(format: "%.2f", Double(_chng)!)
        } else {
            cell?.mChangeLbl.text = _chng
        }
        
        
        
        
        if _chng != "--"{
            avg_count = avg_count+1;
            var performance: Double = 0.00
            performance = Double(_chng)!
            avg_sum = avg_sum + performance
            
        }
        
        return cell!
        
    }
    
    
    //MARK: Average FUNCTION
    private func setAvg(){
        avg_count = 0
        avg_sum = 0.00
        stocks_avg = 0.00
        for stock in mfResults{
            var _chng = "--"
            if self.col == "1D %" {
                _chng = "\(stock._1D)"
            } else if self.col == "1W %" {
                _chng = "\(stock._1W)"
            }  else if self.col == "1M %" {
                _chng = "\(stock._1M)"
            }else if self.col == "3M %" {
                _chng = "\(stock._3M)"
            }else if self.col == "6M %" {
                _chng = "\(stock._6M)"
            }else if self.col == "1Y %" {
                _chng = "\(stock._1Y)"
            }else if self.col == "2Y %" {
                _chng = "\(stock._2Y)"
            }else if self.col == "3Y %" {
                _chng = "\(stock._3Y)"
            }else if self.col == "4Y %" {
                _chng = "\(stock._4Y)"
            }else if self.col == "5Y %" {
                _chng = "\(stock._5Y)"
            }else if self.col == "10Y %" {
                _chng = "\(stock._10Y)"
            }
            
            if(_chng=="-999.0"){
                _chng = "--"
            }
            
            if _chng != "--"{
                avg_count = avg_count+1;
                var performance: Double = 0.00
                performance = Double(_chng)!
                avg_sum = avg_sum + performance
            }
        }
        
        stocks_avg = avg_sum / Double(avg_count)
        mStocksAvgLbl.text = String(format: "%.2f", stocks_avg) + "%"
    }
    
    
    
    //MARK: SORT FUNCTION
    private func sortLogic(){
        
        if sort_by == "Stocks(A to Z)"{
            isSortByHtoL = true
            sortByParticular(particular: "Stock")
        } else if sort_by == "Stocks(Z to A)"{
            isSortByHtoL = false
            sortByParticular(particular: "Stock")
        } else if sort_by == "Sector(A to Z)"{
            isSortByHtoL = true
            sortByParticular(particular: "Sector")
        } else if sort_by == "Sector(Z to A)"{
            isSortByHtoL = false
            sortByParticular(particular: "Sector")
        } else if sort_by == "Recommendation(A to Z)"{
            isSortByHtoL = true
            sortByParticular(particular: "Recommendation")
        } else if sort_by == "Recommendation(Z to A)"{
            isSortByHtoL = false
            sortByParticular(particular: "Recommendation")
        } else if sort_by == "Change%(High to Low)"{
            isSortByHtoL = true
            sortByChange()
        } else if sort_by == "Change%(Low to High)"{
            isSortByHtoL = false
            sortByChange()
        }
        
    }
    
    //MARK: SORT By Change FUNCTION
    private func sortByChange(){
        
        //let isSortByHtoL = false
        
        if col == "1D %" {
            mfResults = isSortByHtoL ? mfResults.sorted (by: {$0._1D > $1._1D}) : mfResults.sorted (by: {$0._1D < $1._1D})
            self.mfResultsTblVw.reloadData()
        } else if col == "1W %" {
            mfResults = isSortByHtoL ? mfResults.sorted (by: {$0._1W > $1._1W}) : mfResults.sorted (by: {$0._1W < $1._1W})
            self.mfResultsTblVw.reloadData()
        }else if col == "1M %" {
            mfResults = isSortByHtoL ? mfResults.sorted (by: {$0._1M > $1._1M}) : mfResults.sorted (by: {$0._1M < $1._1M})
            self.mfResultsTblVw.reloadData()
        } else if col == "3M %" {
            mfResults = isSortByHtoL ? mfResults.sorted (by: {$0._3M > $1._3M}) : mfResults.sorted (by: {$0._3M < $1._3M})
            self.mfResultsTblVw.reloadData()
        } else if col == "6M %" {
            mfResults = isSortByHtoL ? mfResults.sorted (by: {$0._6M > $1._6M}) : mfResults.sorted (by: {$0._6M < $1._6M})
            self.mfResultsTblVw.reloadData()
        } else if col == "1Y %" {
            mfResults = isSortByHtoL ? mfResults.sorted (by: {$0._1Y > $1._1Y}) : mfResults.sorted (by: {$0._1Y < $1._1Y})
            self.mfResultsTblVw.reloadData()
        } else if col == "2Y %" {
            mfResults = isSortByHtoL ? mfResults.sorted (by: {$0._2Y > $1._2Y}) : mfResults.sorted (by: {$0._2Y < $1._2Y})
            self.mfResultsTblVw.reloadData()
        }else if col == "3Y %" {
            mfResults = isSortByHtoL ? mfResults.sorted (by: {$0._3Y > $1._3Y}) : mfResults.sorted (by: {$0._3Y < $1._3Y})
            self.mfResultsTblVw.reloadData()
        }else if col == "4Y %" {
            mfResults = isSortByHtoL ? mfResults.sorted (by: {$0._4Y > $1._4Y}) : mfResults.sorted (by: {$0._4Y < $1._4Y})
            self.mfResultsTblVw.reloadData()
        }  else if col == "5Y %" {
            mfResults = isSortByHtoL ? mfResults.sorted (by: {$0._5Y > $1._5Y}) : mfResults.sorted (by: {$0._5Y < $1._5Y})
            self.mfResultsTblVw.reloadData()
        }else if col == "10Y %" {
            mfResults = isSortByHtoL ? mfResults.sorted (by: {$0._10Y > $1._10Y}) : mfResults.sorted (by: {$0._10Y < $1._10Y})
            self.mfResultsTblVw.reloadData()
        }
    }
    
    
    //MARK: SORT By Particular FUNCTION
    private func sortByParticular(particular:String){
        
        
        if particular == "Stock" {
            mfResults = isSortByHtoL ? mfResults.sorted (by: {$0.Dispaly_Name < $1.Dispaly_Name}) : mfResults.sorted (by: {$0.Dispaly_Name > $1.Dispaly_Name})
            self.mfResultsTblVw.reloadData()
        } else if particular == "Sector" {
            mfResults = isSortByHtoL ? mfResults.sorted (by: {$0.Sector < $1.Sector}) : mfResults.sorted (by: {$0.Sector > $1.Sector})
            self.mfResultsTblVw.reloadData()
        }else if particular == "Recommendation" {
            mfResults = isSortByHtoL ? mfResults.sorted (by: {$0.CategoryLong < $1.CategoryLong}) : mfResults.sorted (by: {$0.CategoryLong > $1.CategoryLong})
            self.mfResultsTblVw.reloadData()
        }
    }
    
    func createInvoiceAsHTML() {
        invoiceComposer = InvoiceComposer()
        if let invoiceHTML = invoiceComposer.mfRenderInvoice(filtered_by: mAppliedFilters, items: mfResults, percent_col: self.col) {
            
            HTMLContent = invoiceHTML
            if let filePath: String = invoiceComposer.exportHTMLContentToPDF(HTMLContent: HTMLContent) {
                
                loadPDFAndShare(documentoPath: filePath)
            }
            
            
            
            
        }
    }
    
    
    
    
    //MARK: GET DATA
    func getData (completion : @escaping ([NSDictionary])->()) {
        
        let obj = WebService()
        let recomend = " "
        let paremeters = "lossMaking=Y&lessVolume=Y&recommendation=\(recomend)&param=\(querystr);"
        
        //actInd.startAnimating()
        
        obj.callWebServices(url: Urls.rpt_filter_results, methodName: "POST", parameters: paremeters, istoken: false, tokenval: User.token) { (returnValue, jsonData) in
            
            var result : [NSDictionary]
            result=[NSDictionary]()
            print("Json Data is :\(jsonData)")
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                if jsonData.value(forKey: "response") as! [NSDictionary] != nil{
                    result = jsonData.value(forKey: "response") as! [NSDictionary]
                    return completion(result)
                }
                return completion(result)
            } else{
                
            }
            
        }
    }
    
}

//MARK: COLLECTION VIEW DELEGATE AND DATA SOURCE
extension MFFilteredStocksViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mfselectedFilter.mfdispalyQueryArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let pendingCell = collectionView.dequeueReusableCell(withReuseIdentifier: "filterItem", for: indexPath)
        let contenView = pendingCell.viewWithTag(100) as! UIView
        let filter     = pendingCell.viewWithTag(101) as! UILabel
        
        filter.addCornerRadius(value: 4)
        
        
        
        contenView.addCornerRadius(value: 4)
        
        filter.text = mfselectedFilter.mfdispalyQueryArr[indexPath.row]
        
        
        return pendingCell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        /*let sizeDynamic  = ["str" sizeWithFont:[UIFont fontWithName:@"Helvetica Nue" size:14] constrainedToSize:CGSizeMake(CGFLOAT_MAX,CGFLOAT_MAX) lineBreakMode:NSLineBreakByWordWrapping];*/
        
        let myString: NSString = mfselectedFilter.mfdispalyQueryArr[indexPath.row] as NSString
        let size: CGSize = myString.size(attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14.0)])
        
        return CGSize(width: size.width+20, height: collectionView.bounds.height)
        /*arrPendings.count == 1 ? CGSize(width: collectionView.bounds.width , height: collectionView.bounds.height) : CGSize(width: collectionView.bounds.width - 20, height: collectionView.bounds.height)*/
    }
    
}

extension UITableView {
    
    
    
    // Export pdf from UITableView and save pdf in drectory and return pdf file path
    /*func exportAsPdfFromTable() -> String {
     
     let originalBounds = self.bounds
     self.bounds = CGRect(x:originalBounds.origin.x, y: originalBounds.origin.y, width: self.contentSize.width, height: self.contentSize.height)
     let pdfPageFrame = CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.contentSize.height)
     
     let pdfData = NSMutableData()
     UIGraphicsBeginPDFContextToData(pdfData, pdfPageFrame, nil)
     UIGraphicsBeginPDFPageWithInfo(pdfPageFrame, nil)
     guard let pdfContext = UIGraphicsGetCurrentContext() else { return "" }
     self.layer.render(in: pdfContext)
     UIGraphicsEndPDFContext()
     self.bounds = originalBounds
     // Save pdf data
     return self.saveTablePdf(data: pdfData)
     
     }
     
     // Save pdf file in document directory
     func saveTablePdf(data: NSMutableData) -> String {
     
     let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
     let docDirectoryPath = paths[0]
     let pdfPath = docDirectoryPath.appendingPathComponent("tablePdf.pdf")
     if data.write(to: pdfPath, atomically: true) {
     return pdfPath.path
     } else {
     return ""
     }
     }*/
}


