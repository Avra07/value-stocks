//
//  MFStockFiltersViewController.swift
//  DLevels
//
//  Created by SAIKAT GHOSH on 20/08/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//
//
//  MFStockFiltersViewController.swift
//  DLevels
//
//  Created by Dynamic on 19/08/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit

import UIKit
import MaterialComponents

class MFGroupCell:UITableViewCell{
    @IBOutlet weak var lblGroupName: UILabel!
    @IBOutlet weak var lblCounter: UILabel!
}

class MFStockFiltersViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tblvwGroup: UITableView!
    @IBOutlet weak var tblvwfilters: UITableView!
    @IBOutlet weak var btnresetfilter: UIButton!
    @IBOutlet weak var btnapplyfilter: UIButton!
    
    var allFilterData = [NSDictionary]()
    var filterListDataSource = [groupWiseFilter]()
    var modifiedFilterList = [FilterResult]()
    
    var isComingFromGurus = false
    
    var currentindex = 0
    
    var toolbar:UIToolbar = UIToolbar()
    var txtview = UITextField()
    var dropdownCellHeight = 0
    
    var sectorListDict = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Guru Stock Rules : \(mfselectedFilter.mfdispalyQueryArr)")
        //  hideKeyboardOnTapOutside()
        
        btnresetfilter.layer.borderColor = UIColor.black.cgColor
        btnresetfilter.layer.borderWidth = 1
        btnresetfilter.layer.cornerRadius = 4
        
        
        btnapplyfilter.layer.borderWidth = 1
        btnapplyfilter.layer.cornerRadius = 4
        
        
        var nib = UINib.init(nibName: "mfdropdownTableViewCell", bundle: nil)
        self.tblvwfilters.register(nib, forCellReuseIdentifier: "mfdropdownTableViewCell")
        
        nib = UINib.init(nibName: "mfoperatorFilterTableViewCell", bundle: nil)
        self.tblvwfilters.register(nib, forCellReuseIdentifier: "mfoperatorFilterTableViewCell")
        
        nib = UINib.init(nibName: "mfmultiSelectFilTableViewCell", bundle: nil)
        self.tblvwfilters.register(nib, forCellReuseIdentifier: "mfmultiSelectFilTableViewCell")
        
        nib = UINib.init(nibName: "mftextFilterTableViewCell", bundle: nil)
        self.tblvwfilters.register(nib, forCellReuseIdentifier: "mftextFilterTableViewCell")
        
        
        if mfselectedFilter.mfqueryText != "" {
            mfselectedFilter.mffilterParameter.removeAll()
            AppManager.GetSelectedFilterArr(queryText: mfselectedFilter.mfqueryText, displayText: mfselectedFilter.mfdisplayText, completion: {result in
                
                
            })
        }
        
        
        //  sdsd
        
        
        // Do any additional setup after loading the view.
        getGroupData(completion: { (status) in
            if status == true {
                self.tblvwGroup.reloadData()
                self.getFilterGroupWise(group_id: mfselectedFilter.mffilterGroupDataSource[0].group_id, completion: { (status) in
                   self.tblvwfilters.reloadData()
                    
                    //let indexPath = IndexPath(row: 0, section: 0)
                    
                    //self.tblvwGroup.selectRow(at: indexPath, animated: true, scrollPosition: UITableViewScrollPosition.none)
                    
                })
                
                
            } else {
                
            }
        })
    }
    
    @IBAction func resetbtnclick(_ sender: Any) {
        
        getGroupData(completion: { (status) in
            if status == true {
                self.tblvwGroup.reloadData()
                self.getFilterGroupWise(group_id: mfselectedFilter.mffilterGroupDataSource[0].group_id, completion: { (status) in
                    
                    mfselectedFilter.mffilterParameter.removeAll()
                    mfselectedFilter.mfdispalyQueryArr.removeAll()
                    mfselectedFilter.mfdisplayText = ""
                    mfselectedFilter.mfqueryText = ""
                    
                    
                    
                    self.tblvwfilters.reloadData()
                })
                
            } else {
                
            }
        })
    }
    
    @IBAction func applyfiltersclick(_ sender: Any) {
        
        AppManager.GetMFQueryDisplayText(filterParam: mfselectedFilter.mffilterParameter,completion: { (status) in
            if status == true {
                
            }
        })
        
    }
    
    @IBAction func back(_ sender: Any) {
        
        //switchDelegate?.switchDataInformation(lossMark: self.islossMaking, lessVol: self.islessVolume)
        
        self.navigationController?.popViewController(animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "show" {
            let secondViewController = segue.destination as! MFFilterPopUpViewController
            secondViewController.header = "Mutual Funds Screener"
            secondViewController.items = mfselectedFilter.mfdispalyQueryArr
            secondViewController.query = mfselectedFilter.mfqueryText
            secondViewController.islessVolumeParam = "Y"
            secondViewController.islossmakingParam = "Y"
            secondViewController.isComingFromGurus = isComingFromGurus
            
        }
    }
    
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "show" {
            if mfselectedFilter.mfqueryText == "" {
                AppManager().showAlert(title:"Alert", message: "Minimum one filter is compulsory", navigationController: User.navigation)
                
                return false
            }
        }
        
        return true
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tblvwGroup{
            return mfselectedFilter.mffilterGroupDataSource.count
        }else{
            return filterListDataSource.count
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblvwGroup{
            
            let obj = tblvwGroup.dequeueReusableCell(withIdentifier: "MFGroupCell") as! MFGroupCell
            let str = mfselectedFilter.mffilterGroupDataSource[indexPath.row]
            obj.lblGroupName.text! = str.filter_section
            
            
            if str.filter_count > 0 {
                obj.lblCounter.text = String(str.filter_count)
            }else{
                obj.lblCounter.text = ""
            }
            
            if(obj.isSelected){
                obj.backgroundColor = UIColor.white
            }else{
                obj.backgroundColor = UIColor.clear
            }
            
            
            return obj
        }
        
        
        if tableView == tblvwfilters {
            
            let data = self.filterListDataSource[indexPath.row] as groupWiseFilter
            
            if let filtertype:String = data.filter_type {
                
                if filtertype == "dropdown" {
                    if data.filter_options != "" {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "mfdropdownTableViewCell") as! mfdropdownTableViewCell
                        if let sourceArr: [String] = data.filter_options.split(separator: ";").map({ String($0) }) {
                            cell.mfdataSource = sourceArr
                            cell.mf_title = data.filter_name
                            cell.lblSortName.text = data.filter_short_name
                            cell.lblColName.text = data.filter_column_name
                            cell.mf_group_id = data.group_id
                            cell.awakeFromNib()
                            
                            dropdownCellHeight = sourceArr.count * 70
                            return cell
                        }
                        
                    }
                }
                
                // get-filter-fund-list
                //segment
                
                if filtertype == "multi_select_list" {
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "mfmultiSelectFilTableViewCell") as! mfmultiSelectFilTableViewCell
                    multiple_select_data(seg: data.filter_options, completion: { (status) in
                        if status == true {
                            cell.mf_dataSource = self.sectorListDict
                            cell.lblSortName.text = data.filter_short_name
                            cell.lblColName.text = data.filter_column_name
                            cell.mf_group_id = data.group_id
                            cell.awakeFromNib()
                        }
                    })
                    return cell
                    
                    
                }
                
                if filtertype == "text" {
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "mftextFilterTableViewCell") as! mftextFilterTableViewCell
                    cell.lblTitle.text = data.filter_name
                    cell.lblSortName.text = data.filter_short_name
                    cell.lblColName.text = data.filter_column_name
                    cell.group_id = data.group_id
                    
                    for filterresult_Obj in mfselectedFilter.mffilterParameter {
                        if filterresult_Obj.filter_column_name == data.filter_column_name {
                            cell.txtValue.text = filterresult_Obj.filter_max
                        }
                    }
                    
                    cell.awakeFromNib()
                    return cell
                    
                    
                }
                
                if filtertype == "" {
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "mfoperatorFilterTableViewCell") as! mfoperatorFilterTableViewCell
                    
                    cell.lblTitle.text = data.filter_name
                    cell.btnoperater.setTitle(data.filter_default_operator, for: .normal)
                    cell.lblSortName.text = data.filter_short_name
                    cell.lblColName.text = data.filter_column_name
                    cell.group_id = data.group_id
                    
                    
                    if data.filter_default_operator == "between" {
                        cell.minmaxvw.isHidden      = false
                        cell.singleviewvw.isHidden  = true
                    }else{
                        cell.minmaxvw.isHidden      = true
                        cell.singleviewvw.isHidden  = false
                    }
                    
                    
                    for filterresult_Obj in mfselectedFilter.mffilterParameter {
                        if filterresult_Obj.filter_column_name == data.filter_column_name {
                            
                            cell.btnoperater.setTitle(filterresult_Obj.filter_operator, for: .normal)
                            
                            if filterresult_Obj.filter_operator == "between" {
                                cell.txtmin.text = filterresult_Obj.filter_min
                                cell.txtmax.text = filterresult_Obj.filter_max
                                
                            }else{
                                cell.txtvalue.text = filterresult_Obj.filter_max
                            }
                            
                        }
                    }
                    
                    cell.awakeFromNib()
                    
                    return cell
                }
            }
            
        }
        return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblvwGroup{
            
            
            self.tblvwGroup.reloadData()
            
            let selectedCell:UITableViewCell = tableView.cellForRow(at: indexPath)!
            selectedCell.isSelected = true
            
            selectedCell.backgroundColor = UIColor.white
            
            getFilterGroupWise(group_id: mfselectedFilter.mffilterGroupDataSource[indexPath.row].group_id, completion: { (status) in
                self.tblvwfilters.reloadData()
            })
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tblvwGroup{
            return 50
        }
        else{
            
            let data = self.filterListDataSource[indexPath.row] as groupWiseFilter
            if let filtertype = data.value(forKey: "filter_type") as? String {
                
                if filtertype == "multi_select_list" {
                    return tableView.frame.height
                }else if filtertype == "dropdown" {
                    return CGFloat(dropdownCellHeight)
                }  else {
                    return 90
                }
            }
        }
        
        return 90
    }
    
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if tableView == tblvwGroup{
            let selectedCell:UITableViewCell = tableView.cellForRow(at: indexPath)!
            selectedCell.contentView.backgroundColor = UIColor.clear
        }
    }
    
    
    //colorForCellUnselected is just a var in my class
    
    func toggleMinus(){
        
        // Get text from text field
        if var text = txtview.text {
            if text.isEmpty == false{
                
                // Toggle
                if text.hasPrefix("-") {
                    text = text.replacingOccurrences(of: "-", with: "")
                }
                else
                {
                    text = "-\(text)"
                }
                
                // Set text in text field
                txtview.text = text
            }
        }
    }
    
    //MARK: GET DATA
    func multiple_select_data (seg: String, completion : @escaping (Bool)->()) {
        
        let obj = WebService()
        let param = "segment=\(seg)"
        
        print(seg)
        
        //actInd.startAnimating()
        
        obj.callWebServices(url: Urls.get_filter_fund_list, methodName: "GET", parameters: param, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
            
            // print("Json Data is :  \(jsonData)")
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                let tempArray = jsonData.value(forKey: "response") as! [NSDictionary]
                self.sectorListDict.removeAll()
                for dict in tempArray {
                    self.sectorListDict.append((dict.value(forKey: "Category") as? String)!)
                }
                
                return completion(true)
                
            }
        }
    }
    
    func getFilterGroupWise(group_id: String, completion : @escaping (Bool)->()) {
        
        self.filterListDataSource.removeAll()
        
        // print(allFilterData)
        
        for dict in self.allFilterData {
            let obj_grpwsefil = groupWiseFilter()
            // self.info_value = dict.value(forKey: "filter_info") as? String ?? ""
            
            if group_id == dict.value(forKey: "group_id") as? String{
                obj_grpwsefil.group_id = group_id
                
                if let filter_section = dict.value(forKey: "filter_section") as? String{
                    if filter_section == "" {
                        if let filter_name = dict.value(forKey: "filter_name") as? String{
                            obj_grpwsefil.filter_section = filter_name
                        }else{
                            obj_grpwsefil.filter_section = filter_section
                        }
                    }
                }
                
                if let filter_name = dict.value(forKey: "filter_name") as? String{
                    if filter_name != "" {
                        obj_grpwsefil.filter_name = filter_name
                    }
                }
                
                if let filter_options = dict.value(forKey: "filter_options") as? String{
                    if filter_options != "" {
                        obj_grpwsefil.filter_options = filter_options
                    }
                }
                
                if let filter_type = dict.value(forKey: "filter_type") as? String{
                    if filter_type != "" {
                        obj_grpwsefil.filter_type = filter_type
                        
                    }
                }
                
                if let filter_column_name = dict.value(forKey: "column_name") as? String{
                    if filter_column_name != "" {
                        obj_grpwsefil.filter_column_name = filter_column_name
                        
                    }
                }
                
                if let filter_short_name = dict.value(forKey: "filter_short_name") as? String{
                    if filter_short_name != "" {
                        obj_grpwsefil.filter_short_name = filter_short_name
                        
                    }
                }
                
                if let filter_default_operator = dict.value(forKey: "default_operator") as? String{
                    if filter_default_operator != "" {
                        obj_grpwsefil.filter_default_operator = filter_default_operator
                    }
                }
                self.filterListDataSource.append(obj_grpwsefil)
            }
        }
        
        return completion(true)
        
    }
    
    
    
    func getGroupData (completion : @escaping (Bool)->()) {
        
        let obj = WebService()
        let paremeters = "version=A"
        
        //actInd.startAnimating()
        
        obj.callWebServices(url: Urls.get_filter_list, methodName: "GET", parameters: paremeters, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
            
            // print("Json Data is :  \(jsonData)")
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                let tempArray = jsonData.value(forKey: "response") as! [NSDictionary]
                
                if tempArray.count > 0 {
                    
                    mfselectedFilter.mffilterGroupDataSource.removeAll()
                    
                    
                    self.allFilterData = tempArray
                    for dict in tempArray {
                        var isAppend = true
                        let fGroup = mffilterGroup()
                        // self.info_value = dict.value(forKey: "filter_info") as? String ?? ""
                        
                        if let grpId = dict.value(forKey: "group_id") as? String{
                            
                            fGroup.group_id = grpId
                            if let filter_section = dict.value(forKey: "filter_section") as? String{
                                if filter_section == "" {
                                    if let filter_name = dict.value(forKey: "filter_name") as? String{
                                        fGroup.filter_section = filter_name
                                    }
                                }
                                else {
                                    fGroup.filter_section = filter_section
                                }
                            }
                        }
                        
                        fGroup.filter_count = 0
                        
                        for arr in mfselectedFilter.mffilterGroupDataSource{
                            if arr.group_id == fGroup.group_id {
                                isAppend = false
                                break
                            }
                        }
                        
                        if isAppend {
                            mfselectedFilter.mffilterGroupDataSource.append(fGroup)
                        }
                        
                        
                    }// loop end
                }
                
                //print(self.filterGroupDataSource)
                
                return completion(true)
                
            }
        }
    }
}


extension MFStockFiltersViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        //init toolbar
        toolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 65))
        toolbar.barTintColor = #colorLiteral(red: 0.7812563181, green: 0.8036255836, blue: 0.8297665119, alpha: 1)
        txtview = textField
        
        //create left side empty space so that done button set on right side
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(UITextField.doneButtonAction))
        
        let textbox_type = textField.accessibilityValue
        
        if (textbox_type == "value" || textbox_type == "min" || textbox_type == "max") {
            textField.keyboardType = .decimalPad
            
            let minusButton = UIButton()
            minusButton.frame = CGRect(x:0, y:0, width:120, height:40)
            minusButton.setTitle("-", for: .normal)
            minusButton.setTitle("-", for: .highlighted)
            
            minusButton.titleLabel?.textColor = UIColor(hex: "000000")
            
            minusButton.backgroundColor = UIColor.gray
            minusButton.layer.cornerRadius = 4.0
            minusButton.addTarget(self, action: #selector(toggleMinus), for: .touchUpInside)
            
            let leftBarButton = UIBarButtonItem(customView: minusButton)
            toolbar.setItems([flexSpace,leftBarButton,flexSpace, doneBtn], animated: false)
            
            
        }else{
            toolbar.setItems([flexSpace, doneBtn], animated: false)
        }
        
        toolbar.sizeToFit()
        textField.inputAccessoryView = toolbar
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        print("TextField did end editing method called")
        
        
        // save the text in the map using the stored row in the tag field
        
        let sec = Int(textField.accessibilityHint!)
        let row = Int(textField.tag)
        let textbox_type = textField.accessibilityValue
        
        if textbox_type == "max" {
            //stockFilterDict[sec!][row].setValue(textField.text, forKey: "max_value")
        } else if textbox_type == "min" {
            //stockFilterDict[sec!][row].setValue(textField.text, forKey: "min_value")
        }else {
            // stockFilterDict[sec!][row].setValue(textField.text, forKey: "modified_value")
        }
        //print(stockFilterDict[sec!][row]["modified_value"])
        
    }
}

