//
//  MFRecomendationPopUpViewController.swift
//  DLevels
//
//  Created by SAIKAT GHOSH on 20/08/19.
//  Copyright © 2019 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class MFRecommendationCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgTick: UIImageView!
}




class MFRecomendationPopUpViewController: UIViewController {
    
    var recommArr = ["All","Buy Value Stock","No Recommendation","Hold","Risky", "Exit"]
    var arrSelectedSector = [String]()
    var lst = ""
    
    weak var recommendDelegate: SelectedRecommendationDelegate?
    
    @IBOutlet weak var resultsTblVw: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnCross_Action(_ sender: UIButton) {
        dismissCurrentPage()
    }
    
    @IBAction func btnokey_Action(_ sender: UIButton){
        self.dismiss(animated: true) {
            
            self.lst = ""
            for recomm in self.arrSelectedSector {
                
                var recommSelected = ""
                self.lst.append(recomm)
                self.lst.append(",")
                
            }
            
            self.lst = String(self.lst.dropLast(1))
            //self.lst = self.lst.replacingOccurrences(of: "|", with: "'")
            
            self.recommendDelegate?.recommendationList(recommedList: self.lst)
        }
    }
    
    
    //MARK: DISMISS CURRENT PAGE
    private func dismissCurrentPage(){
        self.dismiss(animated: true) {
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension MFRecomendationPopUpViewController: UITableViewDataSource,UITableViewDelegate {
    
    //MARK: Table View Delegate
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let data = recommArr[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "mfRecommendationCell") as! MFRecommendationCell
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        
        cell.lblTitle.text = data
        
        if arrSelectedSector.count > 0 {
            if arrSelectedSector.contains(data) {
                cell.imgTick.image = UIImage(named: "CheckboxOn")
            } else {
                cell.imgTick.image = UIImage(named: "CheckboxOff")
            }
        }else {
            cell.imgTick.image = UIImage(named: "CheckboxOff")
        }
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.recommArr.count
    }
    
    
    //MARK: Table View Delegate
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //getting the current cell from the index path
        let currentCell = tableView.cellForRow(at: indexPath)! as! MFRecommendationCell
        
        //getting the text of that cell
        let currentItem = currentCell.lblTitle!.text
        
        
        
        if let index = arrSelectedSector.index(of: currentItem!) {
            arrSelectedSector.remove(at: index)
            currentCell.imgTick.image = UIImage(named: "CheckboxOff")
            currentCell.isSelected = true
        } else {
            arrSelectedSector.append(currentItem!)
            currentCell.imgTick.image = UIImage(named: "CheckboxOn")
            currentCell.isSelected = false
        }
        
    }
    
    
}



