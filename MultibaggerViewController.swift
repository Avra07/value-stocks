//
//  DashBoardViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-02 on 25/01/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import Foundation
import UIKit

class MultibaggerViewController: UIViewController, UITableViewDataSource,UITableViewDelegate {
    
    
    @IBOutlet weak var segmentVwSpace: UIView!
        
    @IBOutlet weak var tickerlistview: UITableView!
    @IBOutlet weak var bottomScrollView: UIScrollView!
    
    @IBOutlet weak var navigationView: UIView!
    
    
    @IBOutlet weak var vwTempSelectionView: UIView!
    
    let images  = ["Facebook.png","Google.png","logo 2.png"]
    let names   = ["Current Multibagger","Multibagger Sectors","Our Past Performance"]
    let hint    = ["INDEX","FUTURE","STOCK"]
    
    
    var instanceOfLeftSlideMenu = LeftSlideMenu()
    
    var response: NSArray = []
    var tablevwbindarr = [NSDictionary]()
    
    var color = UIColor(red: 40/255, green: 76/255, blue: 90/255, alpha: 1)
    var highlightcolor = UIColor(red: 247/255, green: 196/255, blue: 58/255, alpha: 1)
    var profitcolor = UIColor(red: 29/255, green: 204/255, blue: 136/255, alpha: 1)
    var losscolor = UIColor(red: 257/255, green: 65/255, blue: 61/255, alpha: 1)
    
    var isShowingMenu = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareScrollView()
        segmentViewLoad()
        self.tickerlist()
        tickerlistview.tableFooterView = UIView()
        
    }
    
    @IBAction func temp_function(_ sender: UIButton) {
        instanceOfLeftSlideMenu.hideSlideMenu(navigationController: self.navigationController!, viewSize: self.view, navigationView: navigationView)
        isShowingMenu = false
    }
    @IBAction func backTo_LoginViewController(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    
    func segmentedControlValueChanged(segment: UISegmentedControl) {
        if segment.selectedSegmentIndex == 0 {
        }
    }
    
    
    
    func segmentViewLoad()
    {
        let items = ["Newly Added", "Retained Position", "Remove From List"]
        let segmentedControl = UISegmentedControl(items: items)
        segmentedControl.frame = CGRect(x:4 , y: bottomScrollView.frame.origin.x + 20, width: UIScreen.main.bounds.width - 24, height: 30)
        
        segmentedControl.addTarget(self, action: #selector(MultibaggerViewController.segmentedControlValueChanged(segment:)), for: .valueChanged)
        
        let font = UIFont.systemFont(ofSize: 10)
        segmentedControl.setTitleTextAttributes([NSFontAttributeName: font],
                                                for: .normal)
        
        
        segmentedControl.selectedSegmentIndex = 0
        segmentVwSpace.addSubview(segmentedControl)
    }
    
    
    
   // func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
//        let headerView = UIView(frame: CGRect(0,0, tableView.frame.size.width, 30))
//        let headerView = UIView(frame: CGRect)
//        headerView.backgroundColor = UIColor.cyan
//        headerView.layer.borderColor = UIColor.white.cgColor
//        headerView.layer.borderWidth = 1.0;
//        
//        let headerLabel = UILabel(frame: CGRect(5,2, tableView.frame.size.width - 5, 30))
//        headerLabel.text = "Header"
//        headerLabel.textAlignment = NSTextAlignment.center
//        headerLabel.font = UIFont(name: "AvenirNext", size: 18)
//        headerLabel.textAlignment = NSTextAlignment.center;
//        headerView.addSubview(headerLabel)
//        
//        return headerView
   // }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
   
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell =
            tableView.dequeueReusableCell(withIdentifier:
                "tickercell", for: indexPath) as! UITableViewCell
        DispatchQueue.main.async {
            
            
            let row = indexPath.row
            
            //cell.attractionLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
            
            var lblval = cell.viewWithTag(1000) as! UILabel
            lblval.text =  self.tablevwbindarr[indexPath.row].value(forKey: "INSTRUMENT_4") as? String
            
            
            
            lblval = cell.viewWithTag(1001) as! UILabel
            lblval.text = ""
            lblval.text =  self.tablevwbindarr[indexPath.row].value(forKey: "ClosePrice") as? String
            
                        let val = self.tablevwbindarr[indexPath.row].value(forKey: "DIFF_PER") as! String
            let myDouble: Double! = Double(val)
            
            if( myDouble > Double(0))
            {
                lblval.textColor = self.profitcolor
                
                
            }
            else
            {
                lblval.textColor = self.losscolor
                            }
            
            
            lblval = cell.viewWithTag(1003) as! UILabel
            lblval.text =  self.tablevwbindarr[indexPath.row].value(forKey: "DIFF") as? String
            
            lblval = cell.viewWithTag(1004) as! UILabel
            lblval.text =  (self.tablevwbindarr[indexPath.row].value(forKey: "DIFF_PER") as? String)! + "%"
            
        }
        
        
         return cell
        //return UITableViewCell()
    }
    
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tablevwbindarr.count
    }
    
    private func prepareScrollView() {
        
      //  bottomScrollView.isPagingEnabled = true
        bottomScrollView.showsHorizontalScrollIndicator = true
        
        
        var fromLeft: CGFloat = 10
        for (index,image) in images.enumerated(){
            
            let button = UIButton()
            
            button.accessibilityHint = hint[index]
            //print(hint[index])
            button.contentMode       = .scaleAspectFill
            
            button.setTitle(names[index], for: .normal)
            button.setTitleColor(color, for: .normal)
            
            button.titleLabel?.font = UIFont(name: "Montserrat-SemiBold", size: 14)
            
            button.titleLabel?.adjustsFontSizeToFitWidth = true
            button.titleLabel?.lineBreakMode = .byClipping
            
            
            //button.sizeToFit()
            
            let btnwidth = UIScreen.main.bounds.width/3-3
            //print(" button width is\(button.frame.width)")
            button.frame = CGRect(x: fromLeft, y: 0, width: 125, height: 35)
            //print(" button width is\(button.frame.width)")
            //button.backgroundColor = UIColor.yellow
            button.contentHorizontalAlignment = .center
            
            //button.layer.borderWidth = 1
            button.layer.borderColor = UIColor.blue.cgColor
            
            fromLeft += button.frame.width - 3
            button.addTarget(self, action: #selector(DashBoardViewController.buttonTappedInScrollView(sender:)), for: .touchUpInside)
            
            button.tag = index
            bottomScrollView.addSubview(button)

            
            
            let highlightvw = UIView(frame: CGRect(x: button.frame.origin.x, y: button.frame.origin.y + button.frame.height, width: 125, height: 2.5))
    
            highlightvw.tag = index
            if(index == 0)
            {
                highlightvw.backgroundColor = highlightcolor
                button.titleLabel?.font = UIFont(name: "Montserrat-SemiBold", size: 10)
            }
            else
            {
                highlightvw.backgroundColor = UIColor.white
                button.titleLabel?.font = UIFont(name: "Montserrat-Light", size: 10)
            }
            
            bottomScrollView.addSubview(highlightvw)
            
            bottomScrollView.contentSize = CGSize(width: 400, height: 1)
            
        }
        
        self.view.layoutIfNeeded()
        
    }
    
    func buttonTappedInScrollView(sender: UIButton){
        
        //print("You have tapped on : \(sender.tag)")
        
        let subViews = self.bottomScrollView.subviews
        
        
        for subview in subViews {
            
            if(subview.isKind(of: UIView.self))
            {
                if(subview.tag == sender.tag)
                {
                    subview.backgroundColor = highlightcolor
                    
                }
                else
                {
                    
                    subview.backgroundColor = UIColor.white
                    
                }
            }
            if(subview.isKind(of: UIButton.self))
            {
            
                if(subview.tag == sender.tag)
                {
                    let btn = subview as! UIButton
                    btn.backgroundColor = UIColor.white
                    btn.titleLabel?.font = UIFont(name: "Montserrat-SemiBold", size: 10)
                    
                }
                else
                {
                    let btn = subview as! UIButton
                    btn.titleLabel?.font = UIFont(name: "Montserrat-Light", size: 10)
                }
            }
            
        }
        
        bindArrLoad(type: sender.accessibilityHint!)
        
        
        
        
    }
    
    func bindArrLoad(type: String)
    {
        self.tablevwbindarr.removeAll()
        
        //print(response)
        
        for element in response {
            
            //print("Response data is: \(element)")
            let data = element as! NSDictionary
            if(data.value(forKey: "Type") as! String == type)
            {
                
                let testData = data
                //print("  test data is: \(testData)")
             //   self.tablevwbindarr.adding(data)
                self.tablevwbindarr.append(data)
                //print("Array cont is : \(tablevwbindarr.count)")
                
            }
            
        }
        
        DispatchQueue.main.async { 
            self.tickerlistview.reloadData()
        }
        
        
        
        
        
    }
    
    func tickerlist()
    {
        let obj = WebService()
        let token = ""
        let type_param = "all"
        
        obj.callWebServices(url: Urls.dashboardTicker, methodName: "GET", parameters: "type=\(type_param)", istoken: true, tokenval: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL3dzLmRsZXZlbHMuY29tL2xvZ2luIiwiaWF0IjoxNDg1OTI5NTQ2LCJuYmYiOjE0ODU5Mjk1NDYsImp0aSI6IjBmcnBDeWxkZjRJbG4zM2oiLCJzdWIiOjE5NjI3MSwieHBhc3MiOiIkUCRCWjE4LzdDZlBDbnZVSmZMbG50cU9iSlVnRWx6WngxIn0.UmuklA_4ZRrMWh7Ey4o-UutEJtjmxe9uH5-B9pc-tNQ") { (returnValue, jsonData) in
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
//                ClosePrice = "227.15";
//                DIFF = "26.95";
//                "DIFF_PER" = "11.86";
//                "INSTRUMENT_4" = "FUTURE VEDL";
//                LastTradedPrice = "254.10";
//                Type = "STOCK FUTURE";
//                UpdtTime = "31-01-2017 03:30 PM";
                
        
               self.response = jsonData.value(forKey: "response") as! NSArray
                self.bindArrLoad(type: "INDEX")

               // print(response)
            }
            else
            {
                let alertobj = AppManager()
                
                alertobj.showAlert(title: "Error!", message: jsonData.value(forKey: "errmsg") as! String , navigationController: self.navigationController!)
                
            }
            
        }
        
    }

    
    @IBAction func onclick3Months_inView(_ sender: UIButton) {
        
        
    }
    
    
    @IBAction func onclick6Months_inView(_ sender: UIButton) {
        
        
    }
    
    
    @IBAction func onclick9Months_inView(_ sender: UIButton) {
        
        
    }
    
    @IBAction func onclick12Months_inView(_ sender: UIButton) {
        
        
    }
    
    
    
    @IBAction func onClick_Hamberger(_ sender: UIButton) {
        
        if !isShowingMenu {
//            instanceOfLeftSlideMenu.prepareScreen(navigationController: self.navigationController!, viewSize: self.view)
            
     //       instanceOfLeftSlideMenu.prepareScreenWithView(navigationController: self.navigationController!, viewSize: self.view, navigationView: navigationView)
            isShowingMenu = true
        } else {
          //  instanceOfLeftSlideMenu.hideSlideMenu(navigationController: self.navigationController!, viewSize: self.view)
            instanceOfLeftSlideMenu.hideSlideMenu(navigationController: self.navigationController!, viewSize: self.view, navigationView: navigationView)
            isShowingMenu = false
        }
    }   
    
}
    
    

